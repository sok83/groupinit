# encoding: utf-8
import mimetypes
import re
from django.core.urlresolvers import reverse
from PIL import Image, ImageOps


def order_name(name):
    """order_name -- Limit a text to 20 chars length, if necessary strips the
    middle of the text and substitute it for an ellipsis.

    name -- text to be limited.

    """
    name = re.sub(r'^.*/', '', name)
    if len(name) <= 20:
        return name
    return name[:10] + "..." + name[-7:]


def serialize(instance, file_attr='file'):
    """serialize -- Serialize a Picture instance into a dict.

    instance -- Picture instance
    file_attr -- attribute name that contains the FileField or ImageField

    """
    obj = getattr(instance, file_attr)
    try:
        thumb = instance.thumbnails.url
    except:
        thumb = ''
    data = {
        'url': obj.url,
        'name': obj.name,
        'id':instance.pk,
        'type': mimetypes.guess_type(obj.path)[0],
        'thumbnailUrl': thumb,

        #'deleteUrl': reverse('upload-delete', args=[instance.pk]),
        'deleteUrl': '/delete_upload/'+str(instance.pk),
        'deleteType': 'DELETE',
    }
    return data

from django import forms

class DocumentForm(forms.Form):
    file_directory = forms.FileField(
        label='Select a file',
        help_text='max. 5 megabytes'
    )
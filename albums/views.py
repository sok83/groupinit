# Create your views here.
import json
import os
import subprocess
from django.http import HttpResponse
from django.views.generic import CreateView, DeleteView, ListView
from .models import Picture, ProfilePicture, UploadTempFile
from albums.utlis import resize_image, save_image_to_db, create_thumbnail, save_file_to_db, ContentType,generate_token
from .response import JSONResponse, response_mimetype
from .serialize import serialize
from PIL import Image, ImageOps
from subprocess import check_output

from albums.forms import DocumentForm

from groupbox.models import FriendsBox

from posts.models import Post, TravelPost, LifeTimePost
from mongo_models.models import *
from django.views.generic.base import View
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic.edit import CreateView
from groupinit import settings

class PictureCreateView(CreateView):
    model = UploadTempFile
    fields = "__all__"
    def form_valid(self, form):
        token =self.request.GET.get('token',"")
        try:
            self.object = form.save()
            temp = [serialize(self.object)]
            temp_file = self.object.file
            album_obj = Picture.objects.create(file_owner=self.request.user,
                                               slug=self.object.file,
                                               file_name=self.object.file)
            if temp[0]['type'] in ContentType.Image:
                image = resize_image(self.object.file, 470, 470)
                save_image_to_db(image, album_obj.file)
            elif temp[0]['type'] in ContentType.Video:
                save_file_to_db(temp_file.name,album_obj.file)
            vid_thumb=create_thumbnail(album_obj.file, album_obj.thumbnails)
            if vid_thumb:
                save_file_to_db(vid_thumb, album_obj.thumbnails)
                os.remove((settings.MEDIA_ROOT+'/' + vid_thumb))
            album_obj.save()
            temp_file.close()
            temp_file.delete()
            files = [serialize(album_obj)]
            files[0]['id'] = album_obj.id
            filecount, token = self.get_file_count(token)
            data = {'files': files, 'id': album_obj.id,'file_count':filecount,'token':token,'status': "Ok"}
            print "xx",data
        except Exception as e:
            print e
            data = {'status': "Error"}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        res =form.errors
        res['status']="Error"
        res['message'] = res.pop('file')
        data = json.dumps(res)
        return HttpResponse(content=data, status=400, content_type='application/json')

    def get_file_count(self, token):
        if token not in self.request.session:
            token = generate_token()
            print "token gen",token
            self.request.session[token]=0
        self.request.session[token]+=1
        filecount = self.request.session[token]
        print "count", filecount
        return filecount, token

class BasicVersionCreateView(PictureCreateView):
    template_name_suffix = '_basic_form'


class BasicPlusVersionCreateView(PictureCreateView):
    template_name_suffix = '_basicplus_form'


class AngularVersionCreateView(PictureCreateView):
    template_name_suffix = '_angular_form'


class jQueryVersionCreateView(PictureCreateView):
    template_name_suffix = '_jquery_form'


class PictureDeleteView(DeleteView):
    #model = Picture

    def get(self, request, *args, **kwargs):
        try:
            obj = Picture.objects.get(pk=int(kwargs['pk']))
            obj.thumbnails.delete()
            obj.delete()
            response = json.dumps({'status': '0k'})
            return HttpResponse(response, status=200)
        except:
            response = json.dumps({'status': 'Error'})
            return HttpResponse(response, status=200)


class PictureListView(ListView):
    model = Picture

    def render_to_response(self, request, context, **response_kwargs):
        files = [serialize(p) for p in self.get_queryset()]
        data = {'files': files}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class PictureCropView(CreateView):
    def post(self, request, *args, **kwargs):
        """image croping"""
        pic_id = request.POST['id']
        pic = Picture.objects.get(id=pic_id)
        file_name = str(pic.file)
        fname = pic.file.url[:pic.file.url.index(".")]
        if pic:
            test_image = pic.file
            original = Image.open(test_image)
            width, height = original.size
            new_height = request.POST['height']
            new_width = request.POST['width']
            x = request.POST['x']
            y = request.POST['y']
            left = x
            top = y
            a = int(x) + int(new_width)
            b = int(y) + int(new_height)
            cropped_example = original.crop((int(left), int(top), int(a), int(b)))
            cropped_example.save(settings.MEDIA_ROOT +"/"+ pic.file.name[:pic.file.name.index(".")] + "_crop.jpg", 'JPEG')
            img = Image.open(settings.MEDIA_ROOT +"/"+ pic.file.name[:pic.file.name.index(".")] + "_crop.jpg")
            pic1 = Picture.objects.create(file_owner=request.user, file=pic.file.name[:pic.file.name.index(".")] + "_crop.jpg",
                                          file_name=pic.file_name)

            files = [serialize(pic1)]
            files[0]['id'] = pic1.id
            data = {'files': files, 'id': pic1.id, 'result': "ok"}

        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class LoadVideo(View):
    def get(self, request, *args, **kwargs):
        videoid = request.GET.get('id')
        file_obj = Picture.objects.get(id=videoid)
        files = [serialize(file_obj)]
        files[0]['id'] = file_obj.id
        data = {'files': files, 'id': file_obj.id, 'result': "ok"}

        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response


class ProfilePictureCreateView(CreateView):
    model = ProfilePicture
    fields = "__all__"

    def form_valid(self, form):
        form.instance.file_owner = self.request.user

        self.object = form.save()
        files = [serialize(self.object)]
        files[0]['id'] = self.object.id
        data = {'files': files, 'id': self.object.id}
        response = JSONResponse(data, mimetype=response_mimetype(self.request))
        response['Content-Disposition'] = 'inline; filename=files.json'
        return response

    def form_invalid(self, form):
        data = json.dumps(form.errors)
        return HttpResponse(content=data, status=400, content_type='application/json')


class AngularVersionprofileCreateView(ProfilePictureCreateView):
    template_name_suffix = '_angular_form'

from urllib2 import urlopen, Request, quote
import re
def google_image_search(keyword):
    url='https://www.google.com/search?q='+keyword+'&tbm=isch&noj=1'
    try:
        header={'User-Agent':"Mozilla/5.0 (X11; Linux x86_64; rv:40.0) Gecko/20100101 Firefox/40.0"}
        page=urlopen(Request(url,headers=header))
        html = page.read().decode('UTF-8')
        images = re.findall(',"ou":"(.*?)"', html)
        return images
    except Exception as e:
        return str(e)


class LoadGoogleImage(View):
    def get(self, request, *args, **kwargs):
        keyword = request.GET.get('keyword')
        keyword= quote(keyword, safe='')
        result=google_image_search(keyword)
        images=[]
        i=0
        for img in result:
            if img.endswith(".jpg") and len(img) < 100:
                images.append(img)
                i=i+1
            if i==15:
                break
        response = json.dumps({'keyword': keyword, 'images': images, 'result': 'ok'})
        return HttpResponse(response, status=200, content_type='application/json')

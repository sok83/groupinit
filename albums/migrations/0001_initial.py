# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import albums.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'albums/%Y/%m/%d', validators=[albums.models.validate_file])),
                ('slug', models.SlugField(max_length=150, blank=True)),
                ('file_name', models.CharField(max_length=150, null=True, verbose_name=b'File name', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Created date')),
                ('thumbnails', models.FileField(null=True, upload_to=b'albums/%Y/%m/%d/thumbnails', blank=True)),
                ('status', models.CharField(default=b'Pending', max_length=50, null=True, blank=True)),
                ('file_owner', models.ForeignKey(related_name='file creator', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfilePicture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'albums/profile_pics')),
                ('slug', models.SlugField(max_length=150, blank=True)),
                ('file_name', models.CharField(max_length=150, null=True, verbose_name=b'File name', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Created date')),
                ('thumbnails', models.FileField(null=True, upload_to=b'albums/profile_pics', blank=True)),
                ('file_owner', models.ForeignKey(related_name='file owner', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UploadTempFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'temp', validators=[albums.models.validate_file])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

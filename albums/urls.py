from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import  (BasicVersionCreateView, BasicPlusVersionCreateView,
        jQueryVersionCreateView, AngularVersionCreateView,
        PictureCreateView, ProfilePictureCreateView,AngularVersionprofileCreateView,PictureDeleteView,\
                     PictureListView,PictureCropView,LoadVideo,LoadGoogleImage)

urlpatterns = patterns('',

    # url(r"^create_album/$", login_required(CreateAlbum.as_view(), login_url='/login/'), name="create_album"),
    url(r'^basic/$', BasicVersionCreateView.as_view(), name='upload-basic'),
    url(r'^basic/plus/$', BasicPlusVersionCreateView.as_view(), name='upload-basic-plus'),
    url(r'^new/$', PictureCreateView.as_view(), name='upload-new'),
    url(r'^crop_image/$', PictureCropView.as_view(), name='crop_image'),
    url(r'^uploadimage/$', AngularVersionCreateView.as_view(), name='upload-angular'),
    url(r'^uploadimage/(?P<token>\w{0,50})$', AngularVersionCreateView.as_view(), name='upload-angular'),
    url(r'^jquery-ui/$', jQueryVersionCreateView.as_view(), name='upload-jquery'),
    url(r'^delete_upload/(?P<pk>\d+)$', PictureDeleteView.as_view(), name='upload-delete'),
    url(r'^view/$', PictureListView.as_view(), name='upload-view'),
    url(r'^Loadvideo/$', LoadVideo.as_view(), name='Loadvideo'),
    url(r'^upload_profile_image/$', AngularVersionprofileCreateView.as_view(), name='upload-profile_image'),
    url(r'^Load/google/image/$', LoadGoogleImage.as_view(), name='LoadGoogleimage'),
    )

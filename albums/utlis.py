from PIL import Image, ImageOps, ImageDraw
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile
import os
import binascii
from groupinit import settings
from subprocess import call

class ContentType(object):
    """Basic ContentType."""
    Image = ['image/jpeg', 'image/png']
    Audio = []
    Video = ['video/mp4','video/x-msvideo']
    All = Image+Video+Audio

class Thumbnail(object):
    """Thumbnail diamension"""
    width = 64
    height = 64
    diamension = str(width)+"x"+str(height)

# resize image
def resize_image(source_field, width, height):
    if not source_field:
        return
    # Set our max image size in a tuple (max width, max height)
    image_size = (width, height)
    file_type = source_field.url.split(".")[-1].lower()
    PIL_TYPE = file_type
    if file_type in ['jpeg', 'jpg']:
        PIL_TYPE = 'jpeg'
    # Open original photo which we want to resize using PIL's Image
    image = Image.open(StringIO(source_field.read()))
    image.thumbnail(image_size, Image.ANTIALIAS)
    # Save the resized image
    temp_handle = StringIO()
    image.save(temp_handle, PIL_TYPE)
    temp_handle.seek(0)

    # Save image to a SimpleUploadedFile which can be saved into FileField
    suf = SimpleUploadedFile(os.path.split(source_field.name)[-1], temp_handle.read(), content_type=file_type)
    return suf

def save_image_to_db(file, dest_field):
    file_type = file.name.split(".")[-1].lower()
    dest_field.save('%s.%s'%(os.path.splitext(file.name)[0], file_type), file, save=False)

# create thumbnail from both image and video formate
def create_thumbnail(source_field, dest_field):
    import mimetypes
    mimetypes = mimetypes.guess_type(source_field.path)[0]
    if mimetypes in ContentType.Image:
        image= resize_image(source_field, Thumbnail.width, Thumbnail.height)
        save_image_to_db(image, dest_field)
        return None
    elif mimetypes in ContentType.Video:
        return create_thumbnail_from_video(source_field.name)

# # create thumbnail from video formate
def create_thumbnail_from_video(file):

    filename = file.split(".")[0]
    k = filename.rfind("/")
    thumb_file= filename[:k] + '/thumbnails' + filename[k:]+".jpg"
    if not os.path.exists(settings.MEDIA_ROOT+'/' +filename[:k] + '/thumbnails'):
        os.makedirs(settings.MEDIA_ROOT+'/' +filename[:k] + '/thumbnails')
    try:
        print "eeeeeezzzzzzeevvvveeexxxxxxxxxxeeeeee",settings.MEDIA_ROOT+'/' + file
        #call('ffmpeg -i '+ settings.MEDIA_ROOT+'/' + file + ' -c:v libvpx -b:v 1M -c:a libvorbis '+ settings.MEDIA_ROOT+'/output.webm')
        #call('ffmpeg -i '+ settings.MEDIA_ROOT+'/' + file + ' -s 1280x720 -vpre libvpx-720p -b 3900k -pass 2 -acodec libvorbis -ab 100k -f webm -y '+ settings.MEDIA_ROOT+'/output.webm')
        #print "zzzzzzzzzzzzzzzzzzzzzz"
    except Exception as e:
        print e
    call('ffmpeg  -itsoffset -1  -i '+ settings.MEDIA_ROOT+'/' + file + ' -vcodec mjpeg -vframes 1 -an -f rawvideo -s '+Thumbnail.diamension+' '+ settings.MEDIA_ROOT + '/'+thumb_file, shell=True)
    return thumb_file


def save_file_to_db(file, db_field):
    from django.core.files.base import ContentFile
    # Open  file which we want to save db
    with open(settings.MEDIA_ROOT+'/' + file) as f:
        data = f.read()
    # album_obj.file is the FileField
    db_field.save(file, ContentFile(data))


def generate_token():
    return binascii.hexlify(os.urandom(20)).decode()

def map_marker_gen(profile_pic,color):
    try:
        profile_pic=profile_pic.replace("site_media", "media")
        bg_select={'red':"/authentication/static/images/icons/map-marker-red.png",'grey':"/authentication/static/images/icons/map-marker-grey.png"}
        print settings.BASE_DIR+bg_select[color]
        background= Image.open(settings.BASE_DIR+bg_select[color])
        foreground= Image.open(settings.BASE_DIR+profile_pic)
        size = (26, 26)
        mask = Image.new('L', size, 0)
        draw = ImageDraw.Draw(mask)
        draw.ellipse((0, 0) + size, fill=255,outline=20)
        output = ImageOps.fit(foreground, size, centering=(0, 0))
        output.putalpha(mask)
        bg_w, bg_h = background.size
        img_w, img_h = output.size
        offset = ((bg_w - img_w) / 2, 9)
        background.paste(output, offset,output)
        background.save(settings.BASE_DIR+"/authentication/static/map_marker.png")
        return settings.BASE_DIR+"/authentication/static/map_marker.png"
    except Exception as res:
        print "map marker ex:",res
        return False

# import urllib
# from django.core.files import File
# from os import path
# from albums.models import Picture
# def add_pics(url,user):
#     image = urllib.urlretrieve(url)
#     pic = Picture.objects.create(file_owner=user)
#     fname = path.basename(url)
#     with open(image[0]) as fp:
#         pic.file.save(fname, File(fp))
#         pic.save()
#     return pic
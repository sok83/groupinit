from django.db import models
import os

from django.contrib.auth.models import User
from django.conf import settings
# from authentication.utils import get_json_user
from image_cropping import ImageRatioField

from django.utils.translation import ugettext_lazy as _
# from audiofield.intermediate_model_base_class import Model
# from audiofield.fields import AudioField
from django.core.exceptions import ValidationError
from albums.utlis import create_thumbnail, ContentType


ALBUM_VISIBILITY = (
    ('all', 'all'),
    ('friends', 'friends'),
    ('selected friends', 'selected friends'),
)

def validate_file(value):

    content_type = value.file.content_type
    valid_content_type = ContentType.All
    if not content_type in valid_content_type:
        raise ValidationError(u'File not supported!')
    if value.file._size > 20971520:
        raise ValidationError(u'Please keep file size under 20 MB')

class UploadTempFile(models.Model):
    file = models.FileField(upload_to="temp", validators=[validate_file])

    # i need to delete the temp uploaded file from the file system when i delete this model
    # from the database
    def delete(self, using=None):
        name = self.file.name
        # i ensure that the database record is deleted first before deleting the uploaded
        # file from the filesystem.
        super(UploadTempFile, self).delete(using)
        self.file.storage.delete(name)


class Picture(models.Model):

    file = models.FileField(upload_to="albums/%Y/%m/%d", validators=[validate_file])
    slug = models.SlugField(max_length=150, blank=True)

    file_name = models.CharField('File name', max_length=150, null=True, blank=True)
    file_owner = models.ForeignKey(User, related_name="file creator", null=True, blank=True)
    created_date = models.DateTimeField('Created date', auto_now_add=True)
    thumbnails = models.FileField(upload_to="albums/%Y/%m/%d/thumbnails", null=True, blank=True)
    status = models.CharField(null=True, blank=True, max_length=50, default="Pending")     # Pending or Success

    def __unicode__(self):
        # return self.file.name
        return self.file.name

    @models.permalink
    def get_absolute_url(self):
        return ('upload-new',)

    # def save(self, *args, **kwargs):
    #
    #     if self.file:
    #         self.slug = self.file.name
    #         self.file_name = self.file.name
    #
    #     return super(Picture, self).save(*args, **kwargs)

    def save_croped_image(self, *args, **kwargs):

        self.slug = self.file.name
        self.file_name = self.file.name

        self.id = self.id + 1
        super(Picture, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        """delete -- Remove to leave file."""
        self.file.delete(False)
        super(Picture, self).delete(*args, **kwargs)

    class Meta:
        app_label = 'albums'


class ProfilePicture(models.Model):
    file = models.FileField(upload_to="albums/profile_pics")
    slug = models.SlugField(max_length=150, blank=True)

    file_name = models.CharField('File name', max_length=150, null=True, blank=True)
    file_owner = models.ForeignKey(User, related_name="file owner", null=True, blank=True)
    created_date = models.DateTimeField('Created date', auto_now_add=True)
    thumbnails = models.FileField(upload_to="albums/profile_pics", null=True, blank=True)

    def __unicode__(self):
        # return self.file.name
        return self.file.url

    class Meta:
        app_label = 'albums'

# Create your models here.

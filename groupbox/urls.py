from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import Test,AddBoxSettings, BoxView,CreateEditBox, DeleteFriendsBox, ReorderBox, GroupView,CreateEditGroup,CreateFriendRequest,\
    AddRequestToBox, AssignGroup, RejectFriendRequest, SearchPeople, UserFriendRequest,BoxFriendRequest, GroupFriendRequest,\
    UserFriends, BoxFriends, GroupFriends,FriendsTree,MoveGroup,ExitGroup, UserSuggestion, NewNotificationCount




urlpatterns = patterns('',
    url(r"^group_test/$", login_required(Test.as_view(), login_url='/login/'), name="test_api_end_point"),

    url(r"^api/box/create/$", login_required(CreateEditBox.as_view(), login_url='/login/'), name="create_box"),
    url(r"^api/box/view/$", login_required(BoxView.as_view(), login_url='/login/'), name="view_box"),
    url(r"^api/box/view/(?P<box_id>\d+)/$", login_required(BoxView.as_view(), login_url='/login/'), name="view_box"),
    url(r"^api/box/edit/$", login_required(CreateEditBox.as_view(), login_url='/login/'), name="edit_box"),
    url(r"^api/box/delete/$", login_required(DeleteFriendsBox.as_view(), login_url='/login/'), name="delete_box"),
    url(r"^api/box/reorder/$", login_required(ReorderBox.as_view(), login_url='/login/'), name="reorder_box"),

    url(r"^api/group/create/$", login_required(CreateEditGroup.as_view(), login_url='/login/'), name="create_group"),
    url(r"^api/group/view/$", login_required(GroupView.as_view(), login_url='/login/'), name="view_group"),
    url(r"^api/group/edit/$", login_required(CreateEditGroup.as_view(), login_url='/login/'), name="edit_group"),
    url(r"^api/group/exit/$", login_required(ExitGroup.as_view(), login_url='/login/'), name="exit_box"),
    url(r"^api/group/move/$", login_required(MoveGroup.as_view(), login_url='/login/'), name="move_group"),

    url(r"^api/search_people/$", login_required(SearchPeople.as_view(), login_url='/login/'), name="search_people"),

    url(r"^api/request/send/$", login_required(CreateFriendRequest.as_view(), login_url='/login/'), name="send_request"),
    url(r"^api/request/add/box/$", login_required(AddRequestToBox.as_view(), login_url='/login/'), name="request_add_box"),
    url(r"^api/request/add/group/$", login_required(AssignGroup.as_view(), login_url='/login/'), name="request_add_group"),
    url(r"^api/request/reject/$", login_required(RejectFriendRequest.as_view(), login_url='/login/'), name="reject_request"),

    url(r"^api/request/view/user/$", login_required(UserFriendRequest.as_view(), login_url='/login/'), name="New_request_list"),
    url(r"^api/request/view/box/$", login_required(BoxFriendRequest.as_view(), login_url='/login/'), name="Hub_request_list"),
    url(r"^api/request/view/group/$", login_required(GroupFriendRequest.as_view(), login_url='/login/'), name="group_request_list"),

    url(r"^api/notification/count/$", login_required(NewNotificationCount.as_view(), login_url='/login/'), name="New_request_list"),

    url(r"^api/friends/view/user/$", login_required(UserFriends.as_view(), login_url='/login/'), name="friends_list"),
    url(r"^api/friends/view/box/$", login_required(BoxFriends.as_view(), login_url='/login/'), name="Hub_friends_list"),
    url(r"^api/friends/view/group/$", login_required(GroupFriends.as_view(), login_url='/login/'), name="group_friends_list"),
    url(r"^api/friends/suggestion/$", login_required(UserSuggestion.as_view(), login_url='/login/'), name="suggestion_list"),

    url(r"^api/audience/tree/$", login_required(FriendsTree.as_view(), login_url='/login/'), name="get_box"),

    url(r"^api/boxsettings/add/$", login_required(AddBoxSettings.as_view(), login_url='/login/'), name="get_box"),

)

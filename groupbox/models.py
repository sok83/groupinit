from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.contrib.contenttypes import generic
from notification.models import Notification
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from albums.models import Picture
from authentication.utils import get_db

GROUP_TYPE = (
    ('Private', 'Private'),
    ('Shared', 'Shared')
)

BOX_TYPES = (
    ('box', 'box'),
    ('discussion', 'discussion'),
    ('public group', 'public group'),
)

STATE_CHOICES = [
    ("applied", "applied"),
    ("invited", "invited"),
    ("declined", "declined"),
    ("rejected", "rejected"),
    ("accepted", "accepted"),
    ("auto joined", "auto joined")
]


class Group(models.Model):
    name = models.CharField('Group Name', max_length=200)
    group_admins = models.ManyToManyField(User, related_name="group_admin", null=True, blank=True)
    group_bar_color = models.CharField('Group color Hexcode', max_length="7", null=True, blank=True)
    group_created_on = models.DateTimeField(default=timezone.now)
    default_sharing = models.BooleanField('Default Sharing', default=False)
    hide_group_activity = models.BooleanField('Hide Group Activity', default=False)
    members = models.ManyToManyField(User, related_name="Friends list", null=True, blank=True)
    group_type = models.CharField('Group Type', max_length=15, choices=GROUP_TYPE, default='Private')
    notification = generic.GenericRelation(Notification)

    def __unicode__(self):
        return self.name

    def get_json(self, user_id=None):
        return {
            'id': self.id,
            'name': self.name,
            'members': [
                {
                    'id': x.id,
                    'name': x.first_name,
                    'profile_pic': x.profile.profile_picture(box=Friends(user_id=user_id).friends_box(x)),
                    'first_name': x.first_name,
                    'last_name': x.profile.last_name,
                    'member_profile': x.profile.get_json(box=Friends(user_id=user_id).friends_box(x)),
                    'status': 'accepted',

                } for x in self.members.all() if not x.id == user_id
                ],
            'number_of_members': self.members.all().count(),
            'group_type': self.group_type,
            'profile_pic': "/static/images/pictures/pic-14.jpg",
            'default_sharing': self.default_sharing,
            'hide_group_activity': self.hide_group_activity,
            'group_bar_color': self.group_bar_color,
            'group_created_on': self.group_created_on.strftime('%d/%m/%Y')
        }

    def is_friend(self, user):
        return True if user in self.members.all() else False


class FriendsBox(models.Model):
    name = models.CharField('Box Name', max_length=200)
    owner = models.ForeignKey(User)
    color = models.CharField('Box color Hexcode', max_length="7")
    order = models.IntegerField('Box display order', max_length=1)
    box_type = models.CharField('Box Type', default='box', max_length=15, choices=BOX_TYPES)
    profile_pic = models.ImageField(upload_to='uploads/', null=True, blank=True)
    groups = models.ManyToManyField(Group, null=True, blank=True)
    hub_pic = models.ForeignKey(Picture, related_name="hub pic", null=True, blank=True)
    comic_pic = models.CharField(max_length=100, null=True, blank=True)
    status = models.CharField(max_length=200, null=True, blank=True)
    status_pic = models.CharField(max_length=100, null=True, blank=True)
    red_map_marker = models.ForeignKey(Picture, related_name="red map marker", null=True, blank=True)
    grey_map_marker = models.ForeignKey(Picture, related_name="grey map marker", null=True, blank=True)

    def __unicode__(self):
        return self.name + "-" + self.owner.first_name

    def get_json(self, user_id=None):
        all_group = [group.get_json(user_id=user_id) for group in self.groups.all()]
        return {
            'name': self.name,
            'profile_pic': self.hub_pic.file.url if self.hub_pic else "/static/images/pictures/avatar.jpg",
            'comic_pic': self.comic_pic,
            'status': self.status,
            'status_pic': self.status_pic,
            'owner': self.owner.id,
            'color': self.color,
            'order': self.order,
            'id': self.id,
            'groups': all_group,
            'box_type': self.box_type,
        }


class FriendRequest(models.Model):
    invitor = models.ForeignKey(User, related_name="from user", null=True)
    invitee = models.ForeignKey(User, related_name="to user", null=True)
    state = models.CharField(max_length=20, choices=STATE_CHOICES)
    created_date = models.DateTimeField(default=timezone.now)
    invited_group = models.ForeignKey(Group, related_name="Group", null=True)
    invitor_box = models.ForeignKey(FriendsBox, related_name="from box", null=True)
    invitee_box = models.ForeignKey(FriendsBox, related_name="to box", null=True)
    notification = generic.GenericRelation(Notification)

    def get_json(self, user_id=None):
        return {
            'request_id': self.id,
            'state': self.state,
            'invitee': self.invitee.id,
            'invitor_profile': (self.invitor).profile.get_json(),
            'invitee_profile': (self.invitee).profile.get_json(),
            'invitor': self.invitor.id,
            'invitor_name': self.invitor.first_name,
            'invited_group': self.invited_group.get_json(user_id=user_id) if self.invited_group else None,
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'invitor_box': self.invitor_box.get_json() if self.invitor_box else None,
            'invitee_box': self.invitee_box.get_json() if self.invitee_box else None,
            'is_grouprequest': 'True' if self.invited_group else 'False',
        }


class Box:
    def __init__(self, owner=None):
        self.owner = owner

    @staticmethod
    def get(id):
        try:
            box = FriendsBox.objects.get(id=id)
            return box
        except Exception as e:
            return False

    def get_all(self):
        try:
            box = FriendsBox.objects.filter(owner=self.owner).order_by("order")
            return box
        except Exception as e:
            return False

    def next_box(self, box_id):
        try:
            boxes = FriendsBox.objects.filter(owner=self.owner).order_by("order")
            for i in range(0, boxes.count()):
                if boxes[i].id == box_id:
                    if boxes[i].id != boxes[self.count() - 1].id:
                        next_box = {'id': boxes[i + 1].id, 'name': boxes[i + 1].name, 'color': boxes[i + 1].color}
                    else:
                        next_box = {'id': boxes[0].id, 'name': boxes[0].name, 'color': boxes[0].color}
            return next_box
        except Exception as e:
            return None

    def count(self):
        try:
            box = self.get_all()
            return len(list(box))
        except Exception as e:
            return False

    def type(self, id):
        try:
            box = FriendsBox.objects.get(owner=self.owner, id=id)
            return box.box_type
        except Exception as e:
            return False

    def is_exist(self, name):
        try:
            FriendsBox.objects.get(owner=self.owner, name=name)
            return True
        except ObjectDoesNotExist as e:
            return False

    def create(self, name, color, type):
        try:
            order = self.count()
            box = FriendsBox.objects.create(owner=self.owner,
                                            name=name,
                                            color=color,
                                            order=order + 1,
                                            box_type=type)
            return box
        except Exception as e:
            return False

    def edit(self, id, name, color):
        try:
            box = FriendsBox.objects.get(id=id, owner=self.owner)
            print box
            box.name = name
            box.color = color
            box.save()
            return box
        except Exception as e:
            return False

    def delete(self, id):
        try:
            box = FriendsBox.objects.get(id=id, owner=self.owner)
            if box.box_type == "box":
                if not box.groups.all():
                    box.delete()
                    return "ok"
                else:
                    return "group_exist"
            else:
                return "box_error"
        except Exception as e:
            print e
            return "error"

    def reorder_box(self, list):
        i = 0
        try:
            for lis in list:
                box = FriendsBox.objects.get(id=lis, owner=self.owner)
                i = i + 1
                print lis, "--", i
                box.order = i
                box.save()
            return self.get_all()
        except ObjectDoesNotExist as e:
            return False

    @staticmethod
    def add_group(box, group):
        if not group in box.groups.all():
            box.groups.add(group)
            box.save()
            return box
        else:
            return None

    def remove_group(self, box_id, group_id):
        group = Grp(group_id).get()
        box = Box.get(box_id)
        if group in box.groups.all():
            box.groups.remove(group)
            return True
        else:
            return False

    def group_exist(self, group_id):
        group = Grp(group_id).get()
        for box in self.get_all():
            if group in box.groups.all():
                return True
        return False


class Grp:
    def __init__(self, id=None):
        self.id = id
        self.grp = Group.objects.get(id=id)

    def get(self):
        try:
            return self.grp
        except Exception as e:
            return None

    def type(self):
        try:
            return self.grp.group_type
        except Exception as e:
            return None

    def is_exist(self, name):
        try:
            return True if self.grp.name == name else False
        except ObjectDoesNotExist as e:
            return False

    def edit(self, name, color):
        try:
            self.grp.name = name
            self.grp.group_bar_color = color
            self.grp.save()
            return self.grp
        except Exception as e:
            return None

    def delete(self):
        try:
            self.grp.delete()
            return True
        except Exception as e:
            return False

    def reorder(self, list):
        i = 0
        try:
            for lis in list:
                gpr = Group.objects.get(id=lis)
            return self.get_all()
        except ObjectDoesNotExist as e:
            return False

    @staticmethod
    def create(name, color, type):
        try:
            grp = Group.objects.create(name=name, group_bar_color=color, group_type=type)
            return grp
        except Exception as e:
            return None

    @staticmethod
    def search_all(key):
        try:
            return Group.objects.filter(name__istartswith=key)
        except Exception as e:
            return None

    @staticmethod
    def get_by_box(box_id):
        try:
            box = Box().get(box_id)
            grp = box.groups.all()
            return grp
        except Exception as e:
            return None

    def add_members(self, user):
        if user not in self.grp.members.all():
            self.grp.members.add(user)

    def remove_members(self, user):
        if user in self.grp.members.all():
            self.grp.members.remove(user)
            return True
        else:
            return False

    def members(self, user):
        gr = self.grp.get_json(user_id=user.id)
        meme = gr['members']
        gr['notification'] = 0
        meme[:] = [d for d in meme if d.get('id') != user.id]
        gr['members'] = meme
        for i in range(0, len(gr['members'])):
            gr['members'][i]['member_profile']['menu']['muteBtn'] = self.get_mute(user.id, gr['members'][i]["id"])
            gpp = gr['members'][i]
            gr['members'][i]["msg"] = 'Friends'
            if 'menu' in gpp:
                gr['members'][i]['menu']['muteBtn'] = self.get_mute_user(user.id, gr['members'][i]["id"])
        return gr

    def get_mute(self, user_id, next_user):
        con = get_db()
        userposts = con.UserPosts
        userpost = userposts.find_one({"user_id": user_id})
        mute = userpost.get("mute", [])
        return "Unmute" if int(next_user) in mute else 'Mute'


class Request:
    def __init__(self, request_id=None, request=None):
        if request_id:
            self.id = request_id
            self.request = FriendRequest.objects.get(id=self.id)
        else:
            self.request = request

    def get_json(self, user_id=None):
        try:
            return self.request.get_json(user_id=user_id)
        except Exception as e:
            return False

    def get(self):
        try:
            return self.request
        except Exception as e:
            return False

    def is_pending(self):
        return True if self.request.state == "applied" else False

    def add_group(self, group):
        try:
            # if Request.can_send(self.request.invitor,self.request.invitee,group.id):
            if self.request.invitee not in group.members.all() or self.request.invitor not in group.members.all():
                self.request.invited_group = group
                self.request.save()
                return self.request
            else:
                return False
                # else:
                #     return False
        except Exception as e:
            print e
            return False

    def add_invitor_box(self, box):
        try:
            self.request.invitor_box = box
            self.request.save()
            return self.request
        except Exception as e:
            return None

    def add_invitee_box(self, box):
        try:
            self.request.invitee_box = box
            self.request.save()
            return self.request
        except Exception as e:
            return None

    def applied(self):
        try:
            self.request.state = "applied"
            self.request.save()
            return self.request
        except Exception as e:
            return False

    def rejected(self):
        try:
            self.request.state = "rejected"
            self.request.save()
            return self.request
        except Exception as e:
            return False

    def accepted(self):
        try:
            self.request.state = "accepted"
            self.request.save()
            Request.auto_invite(from_user=self.request.invitor,
                                grp=self.request.invited_group,
                                hub=self.request.invitor_box)
            return self.request
        except Exception as e:
            return False

    '''This fuction will decide the given box is belongs to invitee's box or invitor's box '''

    def add_box(self, user, box):
        if not self.request.invitor_box or not self.request.invitee_box:
            decide_box = {str(self.request.invitor): self.add_invitor_box,
                          str(self.request.invitee): self.add_invitee_box}
            return decide_box.get(str(user))(box)
        else:
            return None

    def can_accept(self):
        if self.request.invitee_box and self.request.invitor_box and \
                self.request.invited_group and self.request.state in ['applied', 'auto joined']:
            return True
        else:
            return False

    def accept(self):
        try:
            """Add group to box"""
            Box.add_group(self.request.invitor_box, group=self.request.invited_group)
            Box.add_group(self.request.invitee_box, group=self.request.invited_group)
            grp = Grp(self.request.invited_group.id)

            """Add members to invited group"""
            grp.add_members(self.request.invitee)
            grp.add_members(self.request.invitor)
            self.accepted()
            return True
        except Exception as e:
            print e
            return False

    def remove(self, user):
        if self.request.state == "applied" and self.request.invitor == user:
            self.request.delete()
            return True
        else:
            return False

    def reject(self):
        if self.request.state == "applied":
            self.request.rejected()
            return True
        else:
            return False

    @staticmethod
    def is_connected(from_user, to_user, group_id=None):
        try:
            search_prams = [(Q(invitor=from_user, invitee=to_user) | Q(invitor=to_user, invitee=from_user))
                            & (Q(invitee_box__isnull=False) & Q(invitor_box__isnull=False))]
            if group_id:
                grp = Grp(group_id).get()
                search_prams.append(Q(invited_group=grp))
            return True if FriendRequest.objects.filter(*search_prams) else False
        except Exception as e:
            print "cant send: Exception", e
            return False

    @staticmethod
    def can_send(from_user, to_user, group_id=None):
        try:
            search_prams = [(Q(invitor=from_user, invitee=to_user) | Q(invitor=to_user, invitee=from_user))]
            if group_id:
                grp = Grp(group_id).get()
                if to_user in grp.members.all() and from_user in grp.members.all():
                    return False
                # grp = Grp(group_id).get()
                search_prams.append(Q(invited_group=grp))
            # else:
            #     search_prams.append(Q(invited_group__isnull=True))
            #     print "invited_group__isnull"
            return False if FriendRequest.objects.filter(*search_prams) else True
        except Exception as e:
            print "cant send: Exception", e
            return False

    @staticmethod
    def create(from_user, to_user):
        try:
            rqst = FriendRequest.objects.create(invitor=from_user, invitee=to_user, state="applied")
            return Request(request=rqst)
        except Exception as e:
            return False

    @staticmethod
    def auto_invite(from_user, grp, hub):
        try:
            mem_list = []
            for groups in hub.groups.all():
                for mem in groups.members.all():
                    freq = FriendRequest.objects.filter(invitee=mem, invited_group=grp, state="auto joined")
                    if (not mem == from_user) and (not mem in mem_list) and (not mem in grp.members.all()) and len(freq) == 0:
                        mem_list.append(mem)
                        FriendRequest.objects.create(invitor=from_user, invitee=mem,
                                                     invited_group=grp, invitor_box=hub, state="auto joined")
                        #print "xxxxxxxxxxxxxxxxxxxxxauto suggestion>>>>>>>>>>>>>>>>>",mem
            return True
        except Exception as e:
            print e
            return False


class Friends:
    def __init__(self, user=None, user_id=None):
        self.user = user
        if not user_id == None:
            self.user = User.objects.get(id=int(user_id))

    def group(self, group):
        friends_list = []
        for friends in group.members.all():
            if not friends.id == self.user.id:
                frnds = friends.profile.get_json()
                frnds['msg'] = 'Friends'
                friends_list.append(frnds)
        return friends_list

    def box(self, box):
        friends_list = []
        for group in box.groups.all():
            friends_list += self.group(group)
        friends_unique = {v['user']: v for v in friends_list}.values()
        return friends_unique

    def get(self):
        boxes = Box(owner=self.user).get_all()
        friends_list = []
        for box in boxes:
            friends_list += self.box(box)
        friends_unique = {v['user']: v for v in friends_list}.values()
        return friends_unique

    def get_connected_friends(self, box):
        friends = self.box(box)
        friendreq = NewRequest(self.user).box(box)
        for grp in box.groups.all():
            friendreq = friendreq + (NewRequest(self.user).group(grp))
        friends = friends + friendreq
        friends = {v['user']: v for v in friends}.values()
        return friends

    def get_group(self):
        boxes = Box(owner=self.user).get_all()
        grp_list = []
        for box in boxes:
            for grp in box.groups.all():
                grp_list.append(grp.get_json())
        grp_unique = {v['id']: v for v in grp_list}.values()
        # print grp_unique
        return grp_unique

    """Never use this function again..this is not the right way to remove a key value  """

    def _remove_key(self, data, key_list):
        if not isinstance(data, (dict, list)):
            return data
        if isinstance(data, list):
            return [self._remove_key(v, key_list) for v in data]
        return {k: self._remove_key(v, key_list) for k, v in data.items()
                if k not in key_list}

    def get_tree(self):
        boxes = FriendsBox.objects.filter(owner=self.user, box_type='box').order_by('order')
        result = [box.get_json(user_id=self.user.id) for box in boxes]
        rm_keys = ['member_profile', 'default_sharing', 'hide_group_activity', 'group_created_on"']
        result = self._remove_key(result, rm_keys)
        res = {'box': result, 'status': 'ok'}
        return res

    def common_group(self, invitee):
        invitor = Friends(self.user).get_group()
        invitee = Friends(invitee).get_group()
        grp_list = []
        for invte in invitee:
            for invtor in invitor:
                if invtor['id'] == invte['id'] and invte['id'] not in grp_list:
                    grp_list.append(invte['id'])
        return grp_list

    def common_friends(self, invitee):
        invitor = Friends(self.user).get()
        invitee = Friends(invitee).get()
        frn_list = []
        for invte in invitee:
            for invtor in invitor:
                if invtor['id'] == invte['id'] and invte['id'] not in frn_list:
                    frn_list.append(invte['id'])
        return frn_list

    def relation_msg(self, invitee):
        boxes = Box(owner=self.user).get_all()
        boxes_inv = Box(owner=invitee).get_all()
        grp_count = 0
        frnd_count = 0
        frnd_inv_list = []
        for box in boxes:
            for grp in box.groups.all():
                for box_inv in boxes_inv:
                    for grp_inv in box_inv.groups.all():
                        if grp == grp_inv:
                            grp_count = grp_count + 1
                        for frnd in grp.members.all():
                            for frnd_inv in grp_inv.members.all():
                                if not frnd in frnd_inv_list and frnd == frnd_inv:
                                    frnd_inv_list.append(frnd_inv)
                                    frnd_count = frnd_count + 1
        msg = str(frnd_count) + " friends and " + str(grp_count) + " groups in common"
        return msg

    def friends_box(self, user):
        try:
            boxes = Box(owner=user).get_all()
            for box in boxes:
                for grp in box.groups.all():
                    for mem in grp.members.all():
                        if mem == self.user:
                            return box
        except Exception as e:
            return None
        return None

    def suggestion(self, limit=3):
        try:
            user_list = []
            user_json = []
            group_list = []
            group_json = []
            g_count = 0
            u_count = 0
            boxes = Box(owner=self.user).get_all()
            all_mem = []
            for box in boxes:
                for grp in box.groups.all():
                    for mem in grp.members.all():
                        if not mem in all_mem:
                            all_mem.append(mem)

            for mem in all_mem:
                f_boxes = Box(owner=mem).get_all()
                for f_box in f_boxes:
                    for fg in f_box.groups.all():
                        # if not g_count == limit and not fg.id in group_list and not fg in box.groups.all():
                        #     g_count += 1
                        #     frnds = ''
                        #     more = ''
                        #     mm_c = 0
                        #     for fr in fg.members.all():
                        #         mm_c += 1
                        #         if mm_c <= 4:
                        #             frnds = frnds + fr.first_name + ","
                        #         else:
                        #             more = "&" + str(mm_c - 4) + "more"
                        #     g = {'name': fg.name, 'id': fg.id, 'pic': "/static/images/pictures/pic-14.jpg",
                        #          'status': "Group suggestion", 'msg': frnds + more}
                        #     group_list.append(fg.id)
                        #     group_json.append(g)
                        for f_mem in fg.members.all():
                            if not u_count == limit and not f_mem.first_name in user_list and not f_mem in all_mem:
                                u_count += 1
                                u = {'name': f_mem.first_name, 'id': f_mem.id, 'pic': f_mem.profile.profile_picture(),
                                     'status': "Friends suggestion", 'msg': Friends(self.user).relation_msg(f_mem)}
                                user_list.append(f_mem.first_name)
                                user_json.append(u)
                        if g_count == limit and u_count == limit:
                            return group_json, user_json
            return group_json, user_json
        except Exception as e:
            print e
            return [], []


class NewRequest:
    def __init__(self, user):
        self.user = user

    def group(self, group):
        search_prams = [(Q(invitor=self.user, state="applied", invited_group=group))]
        send = self._filter(search_prams, "Pending")

        search_prams = [(Q(invitee=self.user, state="applied", invited_group=group))]
        recieved = self._filter(search_prams, "Pending")
        return send + recieved

    def box(self, box):
        search_prams = [(Q(invitor=self.user, state="applied", invitor_box=box, invited_group__isnull=True))]
        send = self._filter(search_prams, "Added to hub")

        search_prams = [(Q(invitee=self.user, state="applied", invitee_box=box, invited_group__isnull=True))]
        recieved = self._filter(search_prams, "Added to hub")
        return send + recieved

    def get(self):
        search_prams = [
            (Q(invitor=self.user, state="applied", invitor_box__isnull=True, invitee_box__isnull=True, invited_group__isnull=True))]
        send = self._filter(search_prams, "Already sent")
        search_prams = [(Q(invitor=self.user, state="applied", invitor_box__isnull=True, invitee_box__isnull=False,
                           invited_group__isnull=True))]
        send += self._filter(search_prams, "Already sent invitee")
        search_prams = [(Q(invitor=self.user, state="applied", invitor_box__isnull=True, invitee_box__isnull=False,
                           invited_group__isnull=False))]
        send += self._filter(search_prams, "Added to group")

        search_prams = [
            (Q(invitee=self.user, state="applied", invitee_box__isnull=True, invitor_box__isnull=True, invited_group__isnull=True))]
        recieved = self._filter(search_prams, "Recieved")
        search_prams = [(Q(invitee=self.user, state="applied", invitee_box__isnull=True, invitor_box__isnull=False,
                           invited_group__isnull=True))]
        recieved += self._filter(search_prams, "Recieved invitor")
        search_prams = [(Q(invitee=self.user, state="applied", invitee_box__isnull=True, invitor_box__isnull=False,
                           invited_group__isnull=False))]
        recieved += self._filter(search_prams, "Added to group")
        return send + recieved

    def get_suggestion(self):
        search_prams = [(Q(invitee=self.user, state="auto joined"))]
        recieved = self._filter(search_prams, "Suggested to group")
        return recieved

    def _filter(self, search_prams, status):
        friends_list = []
        friend_requests = FriendRequest.objects.filter(*search_prams)
        for f_req in friend_requests:
            user_dict = {f_req.invitee: f_req.invitor.profile.get_json(),
                         f_req.invitor: f_req.invitee.profile.get_json()}
            frnds = user_dict.get(self.user)
            status_list = {'Already sent': 'Request sent, Add in this hub',
                           'Recieved': Friends(f_req.invitor).relation_msg(f_req.invitee),  #'Added to hub'
                           'Added to hub': 'Connected, Assign group',
                           'Already sent invitee': 'Connected, Add in this hub',
                           'Recieved invitor': 'Connected, Add to this hub',
                           'Added to group': frnds['name'] + ' invited you to join this group',
                           'Suggested to group': frnds['name'] + ' Suggested you to join this group'}
            frnds["msg"] = status_list.get(status, 'Request pending')
            frnds["status"] = status
            frnds["req_date"] = str(f_req.created_date) if f_req.created_date else ""
            frnds["relation_msg"] = ""  # Friends(f_req.invitor).relation_msg(f_req.invitee)
            if status in ['Added to group', 'Suggested to group']:
                frnds['name'] = f_req.invited_group.name
                frnds['profile_pic'] = '/static/images/pictures/pic-14.jpg'
                frnds['avatar'] = '/static/images/pictures/pic-14.jpg'
                frnds["status"] = 'Already sent'
                frnds["relation_msg"]='Group request'
            if status in ['Already sent invitee', 'Recieved invitor']:
                frnds["status"] = 'Already sent'

            frnds["request_id"] = f_req.id
            friends_list.append(frnds)
        return friends_list

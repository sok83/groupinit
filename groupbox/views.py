from django.http import HttpResponse
from django.views.generic import View
import json as simplejson
from django.core.validators import validate_email
from slugify import slugify
from django.conf import settings
from django.core.mail import send_mail

from authentication.models import UserProfile
from .models import Box, Grp, Request, Friends, NewRequest, FriendsBox
from django.contrib.auth.models import User
from notification.models import Notification


class BoxView(View):
    def get(self, request, *args, **kwargs):
        box_id = kwargs.get('box_id', request.session['box_id'])
        box = Box(owner=request.user)
        if box_id:
            try:
                bx = Box.get(box_id)
                res = {'result': 'ok', 'box': bx.get_json()}
            except Exception as e:
                res = {'result': 'error', 'box': None, 'message': 'No hub with this id'}
        else:
            boxes = box.get_all()
            box_list = [bx.get_json() for bx in boxes]
            res = {'result': 'ok', 'boxes': box_list}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class CreateEditBox(View):
    def post(self, request, *args, **kwargs):
        name = request.POST['name']
        color = request.POST.get('color', "#F65B42")
        boxid = request.POST.get('id', "")
        try:
            box = Box(owner=request.user)
            if boxid:
                """edit box"""
                print "edit"
                friend_box = box.edit(boxid, name, color)
                res = {'result': "ok", 'box': friend_box.get_json()}
            elif box.is_exist(name):
                res = {'result': "error", 'message': "Hub With This Name Already Exists"}
            elif box.count() < 7:
                """create new box"""
                friend_box = box.create(name, color, "box")

                # """create new group"""
                # grp_name_list=['Default-1','Default-2']
                # for gr_name in grp_name_list:
                #     grp = Grp.create(gr_name, color, "Shared")
                #     grp.members.add(request.user)
                #     Box.add_group(box=friend_box, group=grp)
                #     Request.auto_invite(from_user=request.user,grp=grp,hub=friend_box)

                res = {'result': "ok", 'box': friend_box.get_json()}
            else:
                res = {'result': "error", 'message': "You Already Have 7 Hubs"}
        except Exception as e:
            res = {'result': "error", 'message': "Error occurred"}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class DeleteFriendsBox(View):
    def post(self, request, *args, **kwargs):
        box_id = request.POST.get('box_id', '')
        box = Box(owner=request.user)
        delete = box.delete(box_id)
        if delete == 'ok':
            res = {'msg': 'Removed hub successfully', 'result': 'ok'}
        elif delete == 'group_exist':
            res = {'msg': 'Please remove all groups first', 'result': 'error'}
        elif delete == 'box_error':
            res = {'msg': 'Error in removing hub', 'result': 'error'}
        elif delete == 'error':
            res = {'msg': 'Error in removing hub', 'result': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class ReorderBox(View):
    def post(self, request, *args, **kwargs):
        try:
            current_box = request.session['box_id']
            new_order = request.POST['order']
            li = new_order.split('box_')
            lis1 = [str(i) for i in li if i]
            lis = [int(s.strip(',')) for s in lis1]

            bx = Box(request.user)
            print lis
            boxs = bx.reorder_box(lis)
            boxes = [{"id": box.id, "order": box.order, "name": box.name, "color": box.color} for box in boxs]
            next_box = Box(request.user).next_box(current_box)
            if box:

                res = {'result': 'ok', "box": boxes, 'next_box': next_box}
            else:
                res = {'result': 'error', 'next_box': next_box}
        except Exception as e:
            res = {"result": "error", 'msg': str(e)}
        print res
        response = simplejson.dumps(res)
        print response
        return HttpResponse(response, status=200, content_type="application/json")


class GroupView(View):
    def get(self, request, *args, **kwargs):
        group_id = request.GET.get('group_id', '')
        box_id = request.GET.get('box_id', request.session['box_id'])

        if group_id:
            try:
                grp = Grp(group_id)
                gp = grp.get()
                res = {'result': 'ok', 'group': gp.get_json()}
            except Exception as e:
                res = {'result': 'error', 'group': None, 'error': 'No group with this id'}
        else:
            grp = Grp.get_by_box(box_id=box_id)
            grp_list = [gp.get_json() for gp in grp]
            res = {'result': 'ok', 'groups': grp_list}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class CreateEditGroup(View):
    def post(self, request, *args, **kwargs):
        name = request.POST.get('name', "")
        grp_id = request.POST.get('group_id', "")
        color = request.POST.get('color', "#F65B42")
        color_default = {'#EFEFEF': '#F65B42', '#FFFFFF': '#F65B42'}
        color = color_default.get(color, color)
        boxid = request.POST.get('box_id', request.session['box_id'])

        try:
            if name == '':
                res = {'result': "error", 'message': "Please Enter the Name of Group"}
            elif grp_id:
                """ edit group """
                grp = Grp(grp_id).edit(name, color)
                gr = Grp(grp.id).members(request.user)
                res = {'result': "ok", 'group': gr}
            else:
                """create new group"""
                grp = Grp.create(name, color, "Shared")
                grp.members.add(request.user)
                box = Box.get(boxid)
                Box.add_group(box=box, group=grp)
                gr = Grp(grp.id).members(request.user)
                req = Request.auto_invite(from_user=request.user, grp=grp, hub=box)
                res = {'result': "ok", 'group': gr}
        except Exception as e:
            print e
            res = {'result': "error", 'message': "Something went wrong"}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class MoveGroup(View):
    def post(self, request, *args, **kwargs):
        try:
            to_box = request.POST['box_id']
            group_id = request.POST['group_id']
            grp = Grp(group_id).get()
            box = Box.get(to_box)
            group_moved = Box.add_group(box, grp)
            if group_moved:
                res = {'result': 'ok'}
            else:
                res = {'result': 'error'}
        except Exception as e:
            res = {"result": "error", 'msg': str(e)}
        response = simplejson.dumps(res)
        print response
        return HttpResponse(response, status=200, content_type="application/json")


class ExitGroup(View):
    def post(self, request, *args, **kwargs):
        try:
            user = request.user
            group_id = request.POST['group_id']
            box_id = request.session['box_id']

            bx = Box(request.user).remove_group(box_id, group_id)
            if not Box(request.user).group_exist(group_id):
                Grp(group_id).remove_members(user)
            if bx:
                res = {'result': 'ok'}
            else:
                res = {'result': 'error'}
        except Exception as e:
            res = {"result": "error", 'msg': str(e)}
        response = simplejson.dumps(res)
        print response
        return HttpResponse(response, status=200, content_type="application/json")


class CreateFriendRequest(View):
    def post(self, request, *args, **kwargs):
        box_id = request.POST.get('box_id', request.session['box_id'])
        group_id = request.POST.get('group_id', "")
        user_id = request.POST.get('user_id', "")
        from_user = request.user
        to_user = User.objects.get(id=user_id)

        try:
            if Request.can_send(from_user=from_user, to_user=to_user, group_id=group_id):
                f_request = Request.create(from_user=from_user, to_user=to_user)
                f_request.applied()
                if group_id and box_id:
                    box = Box.get(box_id)
                    grp = Grp(group_id).get()
                    f_request.add_invitor_box(box)
                    f_request.add_group(grp)
                res = {'result': "ok", 'request': [], 'user': to_user.profile.get_json(), 'message': 'Request Sent'}
            else:
                res = {'result': "ok", 'request': [], 'message': 'connected'}
        except Exception as e:
            res = {'result': "error", 'request': [], 'message': 'wrong'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class AddRequestToBox(View):
    def post(self, request, *args, **kwargs):
        request_id = request.POST.get('request_id', '')
        login_user = request.user
        box_id = request.POST.get('box_id', request.session['box_id'])
        box = Box.get(id=box_id)
        Frnd_req = Request(request_id=request_id)
        box_added = Frnd_req.add_box(login_user, box)
        if box_added:
            if Frnd_req.can_accept():
                Frnd_req.accept()
                frndrq = Frnd_req.get_json(user_id=login_user.id)
                frndr = frndrq["invitee_profile"]
                frndrq["status"] = "Accepted"
                res = {'result': 'ok', 'request': frndrq, 'status': 'Accepted', 'message': 'Friend Request Accepted'}
            else:
                frndrq = Frnd_req.get_json()
                frndr = frndrq["invitee_profile"]
                frndrq["status"] = "Added to hub"
                res = {'result': 'ok', 'request': frndrq, 'status': 'Added to hub', 'message': 'Connected, Assign group'}
        else:
            res = {'result': 'error', 'request': [], 'message': 'Error occurred'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class AssignGroup(View):
    def post(self, request, *args, **kwargs):
        login_user = request.user
        request_id = request.POST.get('request_id', '')
        grpid = request.POST.get('group_id', "")
        group = Grp(id=grpid).get()
        Frnd_req = Request(request_id=request_id)

        box_id = request.POST.get('box_id', request.session['box_id'])
        box = Box.get(id=box_id)
        box_added = Frnd_req.add_box(login_user, box)

        group_added = Frnd_req.add_group(group)
        if group_added:
            req = Frnd_req.get()
            print Frnd_req.get_json()
            print req.invited_group
            request = {'invited_group': self.members(req.invited_group, request.user)}
            if Frnd_req.can_accept():
                Frnd_req.accept()
                '''request.invited_group'''

                res = {'result': 'ok', 'request': request, 'message': 'Friend Request Accepted'}
            else:
                res = {'result': 'ok', 'request': request, 'message': 'Friend Request Moved to group'}
        else:
            res = {'result': 'error', 'request': [], 'message': 'Friend Request already sent'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")

    def members(self, grp, user):
        gr = grp.get_json(user_id=user.id)
        meme = gr['members']
        meme[:] = [d for d in meme if d.get('id') != user.id]
        gr['members'] = meme
        for i in range(0, len(gr['members'])):
            # gr['members'][i]['member_profile']['menu']['muteBtn'] = FriendsPost(user.id).get_mute_user(gr['members'][i]['id'])
            gr['members'][i]["msg"] = 'Friends'
            # gr['members'][i]["notification"] = len(Notify(user.id).friend_unread_post(grp.id, gr['members'][i]['id']))
            # gr['members'][i]["chat_notification"] = FriendsChat.notify_count(user.id,int(gr['members'][i]['id']))
        # gr['notification'] = len(Notify(user.id).group_unread_post(grp.id))
        gr['request'] = NewRequest(user).group(grp)
        return gr


class RejectFriendRequest(View):
    def post(self, request, *args, **kwargs):
        request_id = request.POST.get('request_id', "")

        rejected = Request(request_id=request_id).reject()
        if rejected:
            res = {'result': 'ok', 'message': 'Friend Request Rejected'}
        else:
            res = {'result': 'error', 'request': [], 'message': 'Error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


# class DeleteFriendRequest(View):
#     def post(self, request, *args, **kwargs):
#         request_id = request.POST.get('request_id',"")
#         user = request.user
#
#         removed = Request(request_id=request_id).remove(user)
#         if removed:
#             res = {'result': 'ok', 'message': 'Friend Request Deleted'}
#         else:
#             res={'result': 'error', 'request': [], 'message': 'Error'}
#         response = simplejson.dumps(res)
#         return HttpResponse(response, status=200)



class UserFriendRequest(View):
    def get(self, request, *args, **kwargs):
        box_id = request.GET.get("box_id", request.session['box_id'])
        box = Box.get(box_id)
        new_request = NewRequest(request.user)
        newreqbox = new_request.box(box)
        newreq = new_request.get()
        new_sugg = new_request.get_suggestion()
        json = newreqbox + newreq + new_sugg

        json = sorted(json, key=lambda x: x['req_date'], reverse=True)
        res = {'result': 'ok', 'name': 'New Requests', 'requests': json}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


from mongo_models.chat import FriendsChat


class NewNotificationCount(View):
    def get(self, request, *args, **kwargs):
        box_id = request.GET.get("box_id", request.session['box_id'])
        box = Box.get(box_id)
        new_request = NewRequest(request.user)
        newreqbox = new_request.box(box) + new_request.get()
        request_filter = [v for v in newreqbox if not v["msg"] == 'Connected, Assign group']
        # new_sugg = len(new_request.get_suggestion())
        total = len(request_filter)

        chat_count = 0

        friends = Friends(request.user).get_connected_friends(box)

        grps = Grp.get_by_box(box.id)

        notify = FriendsChat.notification(request.user.id)
        for v in notify:
            chat_count = chat_count + int(v['unread'])
        res = {'result': 'ok', 'nrg_count': total,
               'chat_count': chat_count, 'frnds_count': len(friends), 'grp_count': len(grps)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class BoxFriendRequest(View):
    def get(self, request, *args, **kwargs):
        box_id = request.GET.get("box_id", request.session['box_id'])
        box = Box.get(box_id)
        newreq = NewRequest(request.user).box(box)
        # sorted_requests = sorted(newreq, key=lambda x: x['created_date'], reverse=True)
        res = {'result': 'ok', 'name': 'New hub Requests', 'requests': newreq}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class GroupFriendRequest(View):
    def get(self, request, *args, **kwargs):
        group_id = request.GET.get("group_id", "")
        box_id = request.session['box_id']
        box = Box.get(box_id)
        grp = Grp(group_id).get()
        nr = NewRequest(request.user)
        newreq = nr.group(grp) + nr.get() + nr.box(box)
        request_filter = [v for v in newreq if not v["relation_msg"] == 'Group request']
        # sorted_requests = sorted(newreq, key=lambda x: x['created_date'], reverse=True)
        res = {'result': 'ok', 'name': 'New group Requests', 'requests': request_filter}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class UserFriends(View):
    def get(self, request, *args, **kwargs):
        newreq = Friends(request.user).get()
        # sorted_friends = sorted(newreq, key=lambda x: x['created_date'], reverse=True)
        res = {'result': 'ok', 'friends': newreq}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


""" to list and search friends under a specific hub"""


class BoxFriends(View):
    def get(self, request, *args, **kwargs):
        key = request.GET.get('search_key', "").lower()
        box_id = request.GET.get("box_id", request.session['box_id'])
        box = Box.get(box_id)
        # friends = Friends(request.user).box(box)
        #
        # friendreq = NewRequest(request.user).box(box)
        # for grp in box.groups.all():
        #     friendreq = friendreq + (NewRequest(request.user).group(grp))
        friends = Friends(request.user).get_connected_friends(box)

        """ if search key not given it ll return all the unique friends under this hub"""
        # friends = [v for v in friends if not v['id']==request.user.id]
        # friends_filter = [v for v in friends if v["name"].startswith(key)] if key else friends
        # friends_unique = {v['user']:v for v in friends_filter}.values()
        result = []
        for frnd in friends:
            if not frnd['id'] == request.user.id:
                if key == "":
                    result.append(frnd)
                elif frnd["name"].lower().startswith(key.lower()):
                    result.append(frnd)
        result = {v['user']: v for v in result}.values()
        result = sorted(result, key=lambda x: x['name'], reverse=False)
        # print sorted_friends
        res = {'result': 'ok', 'friends': result}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class GroupFriends(View):
    def get(self, request, *args, **kwargs):
        group_id = request.GET.get("group_id", "")
        grp = Grp(group_id).get()
        newreq = NewRequest(request.user).group(grp)
        frnds = Friends(request.user).group(grp)
        data = frnds + newreq
        # sorted_friends = sorted(newreq, key=lambda x: x['created_date'], reverse=True)
        res = data
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class FriendsTree(View):
    def get(self, request, *args, **kwargs):
        friends = Friends(request.user).get_tree()
        res = {'result': 'ok', 'audience': friends}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class UserSuggestion(View):
    def get(self, request, *args, **kwargs):
        try:
            limit = request.GET.get('limit', 5)
            groups, users = Friends(request.user).suggestion(limit=int(limit))
            res = {'result': 'ok', 'users': users, 'groups': groups}
        except Exception:
            res = {'result': 'error', 'users': [], 'groups': []}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class SearchPeople(View):
    def get(self, request, *args, **kwargs):
        key = request.GET.get('search_key').lower()
        group_id = request.GET.get('group_id', None)
        group_details = []
        people = []
        stat = "notfound"
        if key:
            try:
                validate_email(key)
                users = User.objects.filter(email=key)
                print users
                if not users:
                    stat = "send_mail"
            except Exception:
                users = User.objects.filter(first_name__istartswith=key)
            users = users.exclude(username=request.user.username)
            for user in users:
                friend_box = Friends(request.user).friends_box(user)
                user_dict = {
                    'name': user.first_name + " " + user.last_name,
                    'username': user.username,
                    'email': user.email,
                    'user_id': user.id,
                    'slug': slugify(user.first_name + "_" + user.last_name),
                    'profile_details': user.profile.get_json(box=friend_box),
                    'relation_msg': 'Open',
                }
                user_profile = UserProfile.objects.get(user=request.user)
                user_dict['status'] = 'Open'
                user_dict['msg'] = 'Send invitation'
                for blk_user in user_profile.blocked_by.all():
                    if user_dict['user_id'] == blk_user.id:
                        user_dict['status'] = 'blocked'
                        user_dict['msg'] = 'Blocked user'
                if Request.can_send(from_user=request.user, to_user=user, group_id=group_id):
                    user_dict['status'] = 'open'
                    user_dict['msg'] = Friends(request.user).relation_msg(user)
                else:
                    user_dict['status'] = 'Sent Request'
                    user_dict['msg'] = "Already a connection" if Request.is_connected(from_user=request.user, to_user=user,
                                                                                      group_id=group_id) else 'Request has been sent'

                people.append(user_dict)

            groups = Grp.search_all(key)
            for group in groups:
                group_details.append(group.get_json())

            res = {
                'result': 'ok' if people or group_details else stat,
                'search_key': key,
                'people': people,
                # 'friends': friend_list,
                'groups': group_details,
            }
        else:
            res = {'result': 'empty'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


from albums.models import Picture, ProfilePicture
from albums.utlis import map_marker_gen


class AddBoxSettings(View):
    def post(self, request, *args, **kwargs):
        try:
            flg = True
            box_settings = simplejson.loads(request.POST.get('box_settings', '[]'))
            for box in box_settings:
                box
                box_obj = FriendsBox.objects.get(id=int(box['id']))
                box_obj.status = box.get('status', box_obj.status)
                box_obj.status_pic = box.get('status_pic', box_obj.status_pic)
                box_obj.comic_pic = box.get('comic_pic', box_obj.comic_pic)
                try:
                    pic = Picture.objects.get(id=int(box['profile_pic']['id']))
                    if not box_obj.hub_pic == pic:
                        box_obj.hub_pic = pic

                        """ create and save red and grey map marker with profile pic"""
                        grey_path = map_marker_gen(pic.file.url, 'grey')
                        obj_grey = add_pics(grey_path, request.user)
                        box_obj.grey_map_marker = obj_grey

                        red_pic = map_marker_gen(pic.file.url, 'red')
                        obj_red = add_pics(red_pic, request.user)
                        box_obj.red_map_marker = obj_red
                        # if flg:
                        #     profile_pic=pic.file.url.replace("site_media", "media")
                        #     pro_pic=add_pics(settings.BASE_DIR+profile_pic,request.user,db=ProfilePicture)
                        #     UserProfile.profile_pic=pro_pic
                        #     flg=False
                        '''google map marker'''
                except Exception as e:
                    print "exin", e
                box_obj.save()
            res = {"result": 'ok'}
        except Exception as e:
            print e
            res = {"result": 'error', 'msg': str(e)}

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


import urllib
from django.core.files import File
from os import path


def add_pics(url, user):
    image = urllib.urlretrieve(url)
    pic = Picture.objects.create(file_owner=user)
    fname = path.basename(url)
    with open(image[0]) as fp:
        pic.file.save(fname, File(fp))
        pic.save()
    return pic


class Test(View):
    def get(self, request, *args, **kwargs):
        res = []
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")

    def post(self, request, *args, **kwargs):
        pass

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('groupbox', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='friendsbox',
            name='comic_pic',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='hub_pic',
            field=models.ForeignKey(related_name='hub pic', blank=True, to='albums.Picture', null=True),
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='status',
            field=models.CharField(max_length=200, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='status_pic',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]

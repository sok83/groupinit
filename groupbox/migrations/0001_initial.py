# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FriendRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('state', models.CharField(max_length=20, choices=[(b'applied', b'applied'), (b'invited', b'invited'), (b'declined', b'declined'), (b'rejected', b'rejected'), (b'accepted', b'accepted'), (b'auto joined', b'auto joined')])),
                ('created_date', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FriendsBox',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Box Name')),
                ('color', models.CharField(max_length=b'7', verbose_name=b'Box color Hexcode')),
                ('order', models.IntegerField(max_length=1, verbose_name=b'Box display order')),
                ('box_type', models.CharField(default=b'box', max_length=15, verbose_name=b'Box Type', choices=[(b'box', b'box'), (b'discussion', b'discussion'), (b'public group', b'public group')])),
                ('profile_pic', models.ImageField(null=True, upload_to=b'uploads/', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, verbose_name=b'Group Name')),
                ('group_bar_color', models.CharField(max_length=b'7', null=True, verbose_name=b'Group color Hexcode', blank=True)),
                ('group_created_on', models.DateTimeField(default=django.utils.timezone.now)),
                ('default_sharing', models.BooleanField(default=False, verbose_name=b'Default Sharing')),
                ('hide_group_activity', models.BooleanField(default=False, verbose_name=b'Hide Group Activity')),
                ('group_type', models.CharField(default=b'Private', max_length=15, verbose_name=b'Group Type', choices=[(b'Private', b'Private'), (b'Shared', b'Shared')])),
                ('group_admins', models.ManyToManyField(related_name='group_admin', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
                ('members', models.ManyToManyField(related_name='Friends list', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='groups',
            field=models.ManyToManyField(to='groupbox.Group', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='owner',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendrequest',
            name='invited_group',
            field=models.ForeignKey(related_name='Group', to='groupbox.Group', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendrequest',
            name='invitee',
            field=models.ForeignKey(related_name='to user', to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendrequest',
            name='invitee_box',
            field=models.ForeignKey(related_name='to box', to='groupbox.FriendsBox', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendrequest',
            name='invitor',
            field=models.ForeignKey(related_name='from user', to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='friendrequest',
            name='invitor_box',
            field=models.ForeignKey(related_name='from box', to='groupbox.FriendsBox', null=True),
            preserve_default=True,
        ),
    ]

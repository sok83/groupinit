# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('groupbox', '0002_auto_20161001_1149'),
    ]

    operations = [
        migrations.AddField(
            model_name='friendsbox',
            name='grey_map_marker',
            field=models.ForeignKey(related_name='grey map marker', blank=True, to='albums.Picture', null=True),
        ),
        migrations.AddField(
            model_name='friendsbox',
            name='red_map_marker',
            field=models.ForeignKey(related_name='red map marker', blank=True, to='albums.Picture', null=True),
        ),
    ]

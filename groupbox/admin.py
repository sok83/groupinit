from django.contrib import admin

# import reversion

from .models import FriendsBox, Group, FriendRequest


admin.site.register(FriendsBox)
admin.site.register(Group)
admin.site.register(FriendRequest)
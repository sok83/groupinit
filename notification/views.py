import ast
import json as simplejson
import datetime

# from datetime import datetime
from django.views.generic import View
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.conf import settings
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point
from django.db.models import Q
from notification.models import Notification
from authentication.utils import *
from announce import AnnounceClient
announce_client = AnnounceClient()

class ReadNotification(View):
    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            notification_id = request.POST['id']
            notification = Notification.objects.get(id = notification_id)
            notification.notification_read_users.add(request.user)
            notification.save()
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, mimetype='application/json')   
        else:
            return render(request, 'homepage.html', {})

class UserMessage(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, mimetype='application/json')  
    def post(self,request,*args,**kwargs):
        if request.is_ajax():
            
            reciever = request.POST['userid']
           
            msg = request.POST['msg']
            
            last_msg = request.POST['last_message']
           
            
            # con = get_db()
            # coll= con.Messages
            # coll1 = con.Comment
            uid = User.objects.get(first_name=reciever)
            if last_msg != 0:
                message = Comment.objects.create(author=int(request.user.id),text=msg,created_on=datetime.datetime.now())
                # chat = coll.insert( {"Participents" :[int(uid.id),int(request.user.id)}, { "$addToSet": { "message":[message]}})
                chat = Messages.objects.get(id= last_msg )
                chat.reply.append(message)
                chat.save()                
                announce_client.emit(
                    int(uid.id),
                    'notifications',
                    data={ 'msg' : msg ,
                        'key':'chat'}
                ) 
                
                res = {'result':'ok'}
            else:
                
                # message = coll1.insert({"author":int(request.user.id),"text":msg,"created_on":datetime.datetime.now()})
                message = Comment.objects.create(author=int(request.user.id),text=msg,created_on=datetime.datetime.now())
                
                # chat = coll.insert( {"Participents" :[int(uid.id),int(request.user.id)]}, { "$addToSet": { "message":[message]}})
                chat = Messages.objects.create( Participents =[int(uid.id),int(request.user.id)])
                chat.message.append(message)
               
                # chat = coll.update( {"reciever" :int(uid.id)}, { "$addToSet": { "message":[message]}}, upsert= True )
              
                announce_client.emit(
                    int(uid.id),
                    'notifications',
                    data={ 'msg' : msg ,
                        'key':'chat'}
                ) 
                
                res = {'result':'ok'}
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, mimetype='application/json') 

class Groupchat(View):
    def post(self,request,*args,**kwargs):
        if request.is_ajax():
            grp = request.POST['groupid']
            msg = request.POST['msg']
            con = get_db()
            # coll= con.GroupMessage
            # coll1 = con.Comment
            # message = coll1.insert({"author":int(request.user.id),"text":msg,"created_on":datetime.datetime.now()})
            message = Comment.objects.create(author=int(request.user.id),text=msg,created_on=datetime.datetime.now())
            # chat = coll.update( {"group" :int(grp)}, { "$addToSet": { "message":[message]}}, upsert= True )
            chat = GroupMessage.objects.get(group=int(grp))
            chat.message.append(message)
            # group_chat = coll.find_one({"group" :int(grp)})
            group_chat = GroupMessage.objects.get(group=int(grp))
            group = Group.objects.get(id=grp)
            for mem in group.members.all():
                announce_client.emit(
                    int(mem.id),
                    'notifications',
                    data={ 'msg' : msg ,
                        'key':'chat'}
                ) 
            res = {
                'result':'ok',
                'chat':group_chat,
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, mimetype='application/json')   
        else:
            return render(request, 'homepage.html', {})            

        

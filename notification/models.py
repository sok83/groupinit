
from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic 


class Notification(models.Model):
    creator = models.ForeignKey(User, related_name="notification_creator",null=True, blank=True)
    notified_users = models.ManyToManyField(User,related_name="notified_users",null=True, blank=True)
    notification_read_users = models.ManyToManyField(User,related_name="notification_read_users",null=True, blank=True)
    message = models.CharField('notification message', max_length="200", null=True, blank=True)
    created_date = models.DateTimeField('Created date', auto_now_add=True,null=True, blank=True)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type','object_id')

    def __unicode__(self):
        return self.creator.first_name
    def get_json(self):
        users = []
        read_users=[]
        for x in self.notified_users.all():
            not_user = x.profile.get_json(),
            users.append(not_user)
        for x in self.notification_read_users.all():
            not_read_user = x.profile.get_json(),
            read_users.append(not_read_user)    
        return {
        
            'id': self.id,
            'creator': self.creator.profile.get_json(),
            'notified_users':users,
            'notification_read_users':read_users,
            'message':self.message,
            'created_date':self.created_date.strftime('%Y/%m/%d %H:%M'),
            
        }
        
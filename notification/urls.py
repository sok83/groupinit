from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from .views import  (ReadNotification,Groupchat,UserMessage)


urlpatterns = patterns('',

    url(r"^read_notifications/$", login_required(ReadNotification.as_view(), login_url='/login/'), name="read_notifications"),
    url(r"^user_message/$", login_required(UserMessage.as_view(), login_url='/login/'), name="user_message"),
    url(r"^group_chat/$", login_required(Groupchat.as_view(), login_url='/login/'), name="group_chat"),
    
)

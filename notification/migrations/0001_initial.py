# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.CharField(max_length=b'200', null=True, verbose_name=b'notification message', blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Created date', null=True)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(to='contenttypes.ContentType')),
                ('creator', models.ForeignKey(related_name='notification_creator', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('notification_read_users', models.ManyToManyField(related_name='notification_read_users', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
                ('notified_users', models.ManyToManyField(related_name='notified_users', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

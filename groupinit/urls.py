from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
					   url(r'^admin/', include(admin.site.urls)),
					   url('', include('social.apps.django_app.urls', namespace='social')),
					   url(r'', include('authentication.urls')),
					   url(r'', include('posts.urls')),
					   url(r'', include('albums.urls')),
					   url(r'', include('notification.urls')),
					   url(r'', include('groupbox.urls')),
					   url(r'', include('mongo_models.urls')),

					   url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
						   {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
					   )

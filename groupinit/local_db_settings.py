DATABASE_ROUTERS = ['mongo_models.routers.DatabaseAppsRouter']
DATABASE_APPS_MAPPING = {'mongo_models': 'mongodb'}

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis', #'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'groupinit_17',                      
        'USER': 'groupinit',
        'PASSWORD': 'groupinit',
        #'NAME': 'test17',
        #'USER': 'postgres',
        #'PASSWORD':'psql',
        'HOST': 'localhost',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    },
    'mongodb': {
        'ENGINE': 'django_mongodb_engine', #'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'groupinit_17mongo',                      # Or path to database file if using sqlite3.
        'HOST': 'localhost',
    }
}


import mongoengine
#SESSION_ENGINE = 'mongoengine.django.sessions'
_MONGODB_NAME = 'groupinit_17mongo'
mongoengine.connect(_MONGODB_NAME, alias='default')
# AUTHENTICATION_BACKENDS = (
#        'mongoengine.django.auth.MongoEngineBackend',
#         )

# Groupinit
***

Short Description

## Prerequisite

* `$ sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib`

* `$ sudo apt-get install libpq-dev python-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev libffi-dev`

- Installing Geospatial libraries

* `$ sudo apt-get install binutils libproj-dev gdal-bin`

- Installing PostGIS

* `$ sudo apt-get install postgis postgresql-9.3-postgis-2.1`

- Accessing the postgres

* `$ sudo su - postgres`

* `$ psql -h localhost -d groupinit_17 -U groupinit`

* `groupinit_17=# CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;`

## Application Installation

* `python manage.py makemigrations`

* `python manage.py migrate`

* `python manage.py runserver`
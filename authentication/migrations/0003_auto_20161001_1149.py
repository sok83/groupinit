# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('authentication', '0002_auto_20160729_1255'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='fav_pic',
            field=models.ForeignKey(related_name='favorite pic', blank=True, to='albums.Picture', null=True),
        ),
        migrations.AlterField(
            model_name='profilesettings',
            name='profile_visibility',
            field=models.CharField(default=b'Public', max_length=20, verbose_name=b'profile_visibility', choices=[(b'Public', b'Public'), (b'Friends only', b'Friends Only'), (b'None', b'None')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='InvitationToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('recipient_email', models.CharField(max_length=100)),
                ('invitation_token', models.CharField(max_length=40, null=True, blank=True)),
                ('invitation_token_created', models.DateTimeField(null=True, blank=True)),
                ('invitation_token_verified', models.BooleanField(default=False, verbose_name=b'Invitation Token Verified')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfileSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('profile_visibility', models.CharField(default=b'Public', max_length=20, verbose_name=b'profile_visibility', choices=[(b'Public', b'Public'), (b'None', b'None')])),
                ('personal_info_visibility', models.CharField(default=b'Public', max_length=20, verbose_name=b'personal_info_visibility', choices=[(b'Public', b'Public'), (b'Friends only', b'Friends Only'), (b'No One', b'No One')])),
                ('shared_group_post_visibility', models.CharField(default=b'Only Group Friends', max_length=20, verbose_name=b'shared_group_post_visibility', choices=[(b'Only Group Friends', b'Only Group Friends'), (b'All Friends', b'All Friends'), (b'Public', b'Public')])),
                ('other_group_post_visibility', models.CharField(default=b'Public', max_length=20, verbose_name=b'other_group_post_visibility', choices=[(b'Public', b'Public'), (b'All Friends', b'All Friends'), (b'Selected Friends', b'Selected Friends')])),
                ('permission_for_share_posted_data', models.CharField(default=b'No', max_length=20, verbose_name=b'permission_for_share_posted_data', choices=[(b'No', b'No'), (b'Yes', b'Yes')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RandomKey',
            fields=[
                ('key', models.CharField(max_length=40, serialize=False, primary_key=True)),
                ('key_created', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Token',
            fields=[
                ('token', models.CharField(max_length=40, serialize=False, primary_key=True)),
                ('token_created', models.DateTimeField(auto_now_add=True)),
                ('token_verified', models.BooleanField(default=False, verbose_name=b'Token Verified')),
                ('password_token', models.CharField(max_length=40, null=True, blank=True)),
                ('pass_token_created', models.DateTimeField(null=True, blank=True)),
                ('pass_token_verified', models.BooleanField(default=False, verbose_name=b'Password Token Verified')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('birth_date', models.DateField(default=b'1990-01-01', null=True, verbose_name=b'Birthdate', blank=True)),
                ('place', models.CharField(max_length=200, null=True, verbose_name=b'Place', blank=True)),
                ('about', models.TextField(null=True, verbose_name=b'About', blank=True)),
                ('last_name', models.CharField(max_length=50, null=True, verbose_name=b'Last name', blank=True)),
                ('email', models.CharField(max_length=100, null=True, verbose_name=b'Email', blank=True)),
                ('joined_date', models.DateTimeField(auto_now_add=True, null=True)),
                ('gender', models.CharField(default=b'Male', max_length=10, choices=[(b'Male', b'Male'), (b'Female', b'Female')])),
                ('contact_no', models.CharField(max_length=20, null=True, verbose_name=b'Contact No', blank=True)),
                ('last_activity', models.DateTimeField(null=True, verbose_name=b'last_activity', blank=True)),
                ('blocked_by', models.ManyToManyField(related_name='Blocked By', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
                ('blocked_users', models.ManyToManyField(related_name='Blocked users', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

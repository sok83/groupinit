# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('authentication', '0001_initial'),
        ('posts', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('groupbox', '__first__'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='location',
            field=models.OneToOneField(related_name='user location', null=True, blank=True, to='posts.LocationPoints'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='profile_pic',
            field=models.ForeignKey(related_name='profile pic', blank=True, to='albums.ProfilePicture', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='shared_groups',
            field=models.ManyToManyField(to='groupbox.Group', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='shared_posts',
            field=models.ManyToManyField(to='posts.Post', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='token',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='randomkey',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='profilesettings',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='invitationtoken',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
    ]

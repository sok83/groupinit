import binascii
import os
from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from django.conf import settings
from posts.models import Post
from albums.models import Picture, ProfilePicture
from groupbox.models import Group, FriendsBox
from posts.models import LocationPoints

PROFILE_VISIBILITY = (
    ('Public', 'Public'),
    ('Friends only', 'Friends Only'),
    ('None', 'None'),
)
PERSONAL_INFO_VISIBILITY = (
    ('Public', 'Public'),
    ('Friends only', 'Friends Only'),
    ('No One', 'No One'),
)
SHARED_GROUP_POST_VISIBILITY = (
    ('Only Group Friends', 'Only Group Friends'),
    ('All Friends', 'All Friends'),
    ('Public', 'Public'),
)

OTHER_GROUP_POST_VISIBILITY = (
    ('Public', 'Public'),
    ('All Friends', 'All Friends'),
    ('Selected Friends', 'Selected Friends'),
)

PERMISSION_FOR_SHARE_POSTED_DATA = (
    ('No', 'No'),
    ('Yes', 'Yes'),
)

GENDER = (
    ('Male', 'Male'),
    ('Female', 'Female'),

)


class Token(models.Model):
    user = models.ForeignKey(User)
    token = models.CharField(max_length=40, primary_key=True)
    token_created = models.DateTimeField(auto_now_add=True)
    token_verified = models.BooleanField('Token Verified', default=False)
    password_token = models.CharField(max_length=40, null=True, blank=True)
    pass_token_created = models.DateTimeField(null=True, blank=True)
    pass_token_verified = models.BooleanField('Password Token Verified', default=False)

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = self.generate_token()
        return super(Token, self).save(*args, **kwargs)

    def generate_token(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return self.token + "-" + self.user.username


class InvitationToken(models.Model):
    user = models.ForeignKey(User)
    recipient_email = models.CharField(max_length=100)
    invitation_token = models.CharField(max_length=40, null=True, blank=True)
    invitation_token_created = models.DateTimeField(null=True, blank=True)
    invitation_token_verified = models.BooleanField('Invitation Token Verified', default=False)

    def save(self, *args, **kwargs):
        if not self.invitation_token:
            self.invitation_token = self.generate_token()
            self.invitation_token_created = datetime.now()
        return super(InvitationToken, self).save(*args, **kwargs)

    def generate_token(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return self.invitation_token + "-" + self.user.username


class RandomKey(models.Model):
    user = models.ForeignKey(User)
    key = models.CharField(max_length=40, primary_key=True)
    key_created = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_token()
        return super(RandomKey, self).save(*args, **kwargs)

    def generate_token(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __unicode__(self):
        return self.key + "-" + self.user.username


class UserProfile(models.Model):
    # user = models.ForeignKey(User)
    user = models.OneToOneField(User, related_name='profile')
    birth_date = models.DateField('Birthdate', null=True, blank=True, default='1990-01-01')
    place = models.CharField('Place', null=True, blank=True, max_length=200)
    location = models.OneToOneField(LocationPoints, related_name="user location", null=True, blank=True)
    about = models.TextField('About', null=True, blank=True)
    shared_posts = models.ManyToManyField(Post, null=True, blank=True)
    shared_groups = models.ManyToManyField(Group, null=True, blank=True)
    profile_pic = models.ForeignKey(ProfilePicture, related_name="profile pic", null=True, blank=True)
    fav_pic = models.ForeignKey(Picture, related_name="favorite pic", null=True, blank=True)
    last_name = models.CharField('Last name', null=True, blank=True, max_length=50)
    email = models.CharField('Email', null=True, blank=True, max_length=100)
    joined_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    gender = models.CharField(choices=GENDER, max_length=10, default="Male")
    contact_no = models.CharField('Contact No', null=True, blank=True, max_length=20)
    blocked_by = models.ManyToManyField(User, null=True, blank=True, related_name="Blocked By")
    blocked_users = models.ManyToManyField(User, null=True, blank=True, related_name="Blocked users")
    last_activity = models.DateTimeField('last_activity', null=True, blank=True)

    def __unicode__(self):
        return self.user.username

    def profile_picture(self, box=None):
        boxes = FriendsBox.objects.filter(owner=self.user)
        pic = "/static/images/pictures/avatar.jpg"
        try:
            if box == None:
                flg = True
                for box in boxes:
                    if box.hub_pic and flg:
                        pic = box.hub_pic.file.url
                        flg = False
            else:
                if box.hub_pic:
                    pic = box.hub_pic.file.url
                else:
                    flg = True
                    for box in boxes:
                        if box.hub_pic and flg:
                            pic = box.hub_pic.file.url
                            flg = False
        except Exception as e:
            print 'profile pic exception:', e
        return pic

    def get_picture(self, box=None):
        boxes = FriendsBox.objects.filter(owner=self.user)
        profile_pic = "/static/images/pictures/avatar.jpg"
        comic_pic = "/static/images/comic_1.png"
        try:
            if box == None:
                flg_profile = True
                flg_comic = True
                for box in boxes:
                    if box.hub_pic and flg_profile:
                        profile_pic = box.hub_pic.file.url
                        flg_profile = False
                    if box.comic_pic and flg_comic:
                        comic_pic = box.comic_pic
                        flg_comic = False
            else:
                if box.hub_pic:
                    profile_pic = box.hub_pic.file.url
                else:
                    flg = True
                    for box in boxes:
                        if box.hub_pic and flg:
                            profile_pic = box.hub_pic.file.url
                            flg = False
                comic_pic = box.comic_pic if box.comic_pic else comic_pic
        except Exception as e:
            print 'profile pic exception:', e
        return profile_pic, comic_pic

    def comic_picture(self, box=None):
        pic = "/static/images/comic_1.png"
        try:
            if box == None:
                boxes = FriendsBox.objects.filter(owner=self.user)
                flg = True
                for box in boxes:
                    if box.comic_pic and flg:
                        pic = box.comic_pic
                        flg = False
            else:
                pic = box.comic_pic if box.comic_pic else pic
        except Exception as e:
            print 'profile pic exception:', e
        return pic

    def get_status(self, box=None):
        pic = ""
        msg = ""
        try:
            if box == None:
                boxes = FriendsBox.objects.filter(owner=self.user)
                flg_msg = True
                flg_pic = True
                for box in boxes:
                    if box.status_pic and flg_pic:
                        pic = box.status_pic
                        flg_pic = False
                    if box.status and flg_msg:
                        msg = box.status
                        flg_msg = False
            else:
                pic = box.status_pic if box.status_pic else pic
                msg = box.status if box.status else ""
        except Exception as e:
            print 'status pic exception:', e
        return pic, msg

    def map_marker(self, box=None):
        boxes = FriendsBox.objects.filter(owner=self.user)
        red = "/static/images/icons/map-marker-red.png"
        grey = "/static/images/icons/map-marker-grey.png"
        try:
            if box == None:
                flg = True
                for box in boxes:
                    if box.red_map_marker and flg:
                        red = box.red_map_marker.file.url
                        grey = box.grey_map_marker.file.url
                        flg = False
            else:
                red = box.red_map_marker.file.url if box.red_map_marker else red
                grey = box.grey_map_marker.file.url if box.grey_map_marker else grey
        except Exception as e:
            print 'map marker exeption:', e
        return red, grey

    def get_json(self, box=None):
        profile_pic, comic_pic = self.get_picture(box=box)
        # comic_pic=self.comic_picture(box=box)
        status_pic, status_msg = self.get_status(box=box)
        red, grey = self.map_marker(box=box)
        return {
            'user': self.user.id,
            'id': self.user.id,
            'first_name': self.user.first_name,
            # 'birth_date': self.birth_date.strftime('%d/%m/%Y'),
            'place': self.place,
            'about': self.about,
            'location': self.location.get_json() if self.location else "",
            'profile_pic': profile_pic,
            'comic_pic': comic_pic,
            'status_pic': status_pic,
            'status_msg': status_msg,
            'fav_pic': self.fav_pic.file.url if self.fav_pic else "",
            'map_marker_red': red,
            'map_marker_grey': grey,
            'last_name': self.last_name if self.last_name else "",
            'email': self.email,
            # 'joined_date':self.joined_date.strftime('%d/%m/%Y') if self.joined_date != None else None,
            'gender': self.gender,
            'member_profile': {'menu': {'muteBtn': 'Mute1', 'mayMove': 'Move'}, 'first_name': self.user.first_name,
                               'last_name': self.last_name if self.last_name else "", 'dotsBtn': 'false'},
            'contact_no': self.contact_no,
            'avatar': profile_pic,
            'avatarDescription': 'Picture',
            'avatarLink': '#',
            'avatarActive': 'active',
            'name': self.user.first_name,
            'nameLink': '#',
            'descriptions': self.about,
            'notificationsLink': '#individual-msg-block',
            'notificationsActive': 'active',
            'addButtonActive': 'false',
            'addButtonLink': '#add-button-link',
            'addButtonText': 'Add',
            'dotsBtn': 'true',
            'menu': {'muteBtn': 'Mute', 'mayMove': 'Move'}

        }


class ProfileSettings(models.Model):
    user = models.ForeignKey(User)
    profile_visibility = models.CharField('profile_visibility', max_length=20, choices=PROFILE_VISIBILITY, default='Public')
    personal_info_visibility = models.CharField('personal_info_visibility', max_length=20, choices=PERSONAL_INFO_VISIBILITY,
                                                default='Public')
    shared_group_post_visibility = models.CharField('shared_group_post_visibility', max_length=20,
                                                    choices=SHARED_GROUP_POST_VISIBILITY, default='Only Group Friends')
    other_group_post_visibility = models.CharField('other_group_post_visibility', max_length=20,
                                                   choices=OTHER_GROUP_POST_VISIBILITY, default='Public')
    permission_for_share_posted_data = models.CharField('permission_for_share_posted_data', max_length=20,
                                                        choices=PERMISSION_FOR_SHARE_POSTED_DATA, default='No')

    def __unicode__(self):
        return self.user.username

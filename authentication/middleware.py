
from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from social import exceptions as social_exceptions 
from social.exceptions import AuthAlreadyAssociated 
from django.utils import timezone
import traceback 
from models import UserProfile
import json

class MySocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        if hasattr(social_exceptions, exception.__class__.__name__):
            if type(exception) == AuthAlreadyAssociated:
                return HttpResponseRedirect(reverse('dashboard'))

            return HttpResponse("catched: %s" % exception)
        else:
            tb = traceback.format_exc()
            return HttpResponse(tb)





class UpdateLastActivityMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        assert hasattr(request, 'user'), 'The UpdateLastActivityMiddleware requires authentication middleware to be installed.'
        if request.user.is_authenticated():
            UserProfile.objects.filter(user__id=request.user.id) \
                           .update(last_activity=timezone.now())


import os
import datetime

class ApiVersionMiddleware(object):
    def process_request(self, request):
        path = request.path
        data=json.dumps(request.GET) if request.GET else json.dumps(request.POST)
        #path = path[0:path.find('/',1)]
        # Log req
        with open(os.path.realpath(__file__+'/../static/reqlog.html'), "a") as logfile:
            if logfile.tell() == 0:
                logfile.write(
                    '<style>'
                    '  table{font:12px "Bitstream Vera Sans Mono",monospace;'
                    '        border-collapse: collapse;}'
                    '  tr:target{background-color: yellow;}'
                    '</style><table border="1px" cellpadding=4>')
            time = str(datetime.datetime.now()).split()[1]
            logfile.write(
                "<tr id='{}'><td>{} {}</td><td>{}</td><td>{}</td></tr>\n".format(
                time, request.META['REMOTE_ADDR'], time, path, data
                ))
        return None
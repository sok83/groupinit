
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required


from views import (LoadNextFavPic,AddUserSettings, AddFavoritePic, LoadPofileSettings, Login, Logout, Signup, ForgotPassword,CheckEmailExists,ResendEmail,ResendResetPassword,\
     ResetPassword, ActivateAccount, Dashboard, AppSettings, ImportGoogleContacts, InviteContacts, import_contacts, ListGroups,\
 import_outlook_contacts, SendInvitation, Invite, BlockThisUser, ListBlockedUser,UnBlockThisUser,ProfileDetails,LoadGroups,LoadFriends,GetGroupData,GetAllProfiles,EditProfile)


LOGIN_URL = '/login/'

urlpatterns = patterns('',
    url(r'^$', Login.as_view(), name="login"),
    url(r'^login/$', Login.as_view(), name="login"),
    url(r'^login/(?P<username>[-\w\d]+)/(?P<paswd>[-\w\d]+)/$', Login.as_view(), name="login"),
    url(r'^logout/$', Logout.as_view(), name="logout"),
    url(r'^signup/$', Signup.as_view(), name="signup"),
    url(r'^check_email/$', CheckEmailExists.as_view(), name="check_email"),
    url(r'^profile/$', ProfileDetails.as_view(), name="profile"),
    url(r'^load_groups/$',LoadGroups.as_view(), name="load_groups"),
    url(r'^list_groups/$',ListGroups.as_view(), name="list_groups"),
    url(r'^get_all_profiles/$',GetAllProfiles.as_view(), name="get_all_profiles"),
    url(r'^get_group_details/$',GetGroupData.as_view(), name="get_group_details"),
    url(r'^load_friends/$',LoadFriends.as_view(), name="load_friends"),
    url(r'^edit_profile/$',EditProfile.as_view(), name="edit_profile"),
    url(r'^resend/$', ResendEmail.as_view(), name="resend"),
    url(r'^forgot_password/$', ForgotPassword.as_view(), name="forgot_password"),
    url(r'^resend_reset_password/$', ResendResetPassword.as_view(), name="resend_reset_password"),
    url(r'^reset_password/$', ResetPassword.as_view(), name="reset_password"),
    url(r'^activate/$', ActivateAccount.as_view(), name="activate_account"),
    url(r'^dashboard/$', login_required(Dashboard.as_view(), login_url="/login/"), name="dashboard"),
    url(r'^appsettings/$', AppSettings.as_view(), name="appsettings"),
    url(r'^invite_contacts/$', InviteContacts.as_view(), name="invite_contacts"),
    url(r'^import_google_contacts/$', login_required(ImportGoogleContacts.as_view(), login_url="/login/"), name="import_google_contacts"),
    url(r'^contact-import/$', import_contacts, name='import_contacts'),
    url(r'^import_outlook_contacts/$', import_outlook_contacts, name='import_outlook_contacts'),
    url(r'^invite/$', Invite.as_view(), name='invite'),
    url(r'^block_this_user/$', BlockThisUser.as_view(), name="block_this_user"),
    url(r'^list_block_user/$', ListBlockedUser.as_view(), name="block_this_user"),
    url(r'^unblock_this_user/$', UnBlockThisUser.as_view(), name="block_this_user"),
    url(r"^send_invitation/$", login_required(SendInvitation.as_view(), login_url='/login/'), name="send_invitation"),

    url(r"^api/settings/load/$", login_required(LoadPofileSettings.as_view(), login_url='/login/'), name="load_profile_settings"),
    url(r"^api/favpic/add/$", login_required(AddFavoritePic.as_view(), login_url='/login/'), name="add fav pic"),
    url(r"^api/favpic/next/$", login_required(LoadNextFavPic.as_view(), login_url='/login/'), name="next fav pic"),
    url(r"^api/usersettings/add/$", login_required(AddUserSettings.as_view(), login_url='/login/'), name="add user settings"),
)

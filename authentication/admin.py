
from django.contrib import admin

from models import Token, InvitationToken, ProfileSettings,UserProfile


admin.site.register(Token)
admin.site.register(InvitationToken)
admin.site.register(ProfileSettings)
admin.site.register(UserProfile)

from djcelery import celery



@celery.task
def process_feed(feed, gd_client, request):
    
    contacts = []
    i = 1
    while feed:
        entries = feed.entry
        for entry in entries:
            for email in entry.email:
                cont = {}
                cont['name'] = entry.title.text
                if email.primary:
                    cont['email'] = email.address
                    contacts.append(cont)
        i = i + 1

        if feed.get_next_link():
            feed = gd_client.GetContacts(uri=feed.get_next_link().href)
            request.session['google_feed'] = feed
            request.session['gd_client'] = gd_client
        else:
            feed = None
        if i == 100:
            break;
    return contacts


import pymongo

def get_name_user(user):

    name = ''
    if user.first_name:
        name = user.first_name
        if user.last_name:
            name = name + user.last_name
    else:
        name = user.username
    return name


def get_json_user(user):

    return {
        'id': user.id,
        'name': get_name_user(user)
    }



def get_db():
    con = pymongo.MongoClient()
    coll = con.groupinit_17mongo
    # coll=""
    return coll


from django.views.generic.list import ListView


$(document).ready(function () {
	
	var id = $('span[id^="user_"]').attr('id');
	var user_id = id.split('_')[1];
	/*$.ajax({		
		  type: "GET",
		  //url: "./profile.php/?userid="+user_id,
		  url: "/profile/?userid="+user_id,
		  dataType: 'json',
		  success: function(response) {
			 
			if(response.result === 'ok')
			{
				//  -------- loading profile details ---------
				$('span[id^="user_"]').html(response.user);
				$('span[id^="frnd-req-count"]').html(response.friend_request_count);
				
				if(response.profile.profile_pic != null){
					
					$('img#profile_pic').attr('src','/static/images/pictures/'+response.profile.profile_pic);
				}
				
				//  -------- loading box details ---------
				htmldesk = '';   htmlmob = ''; htmlBottom = '';
				 
				$(response.boxes).each(function(){ 
					bx = this; 

					htmldesk += '<li class="'+bx.name+'" data-id="'+bx.name+'" id="box_'+bx.id+'" ><a href="/load_groups/?grp_id='+bx.id+'" style="color:'+bx.color+'" class="grp_link" id="'+bx.id+'">'+bx.name+'<span> </span></a></li>';
					htmlmob += '<div class="'+bx.name+'" data-id="'+bx.name+'" id="box_'+bx.id+'"><a href="/load_groups/?grp_id='+bx.id+'" style="color:'+bx.color+'">'+bx.name+'<span> 0 </span></a></div>';
					// htmlBottom += '<li class=" '+(bx.order == 2?'active':'')+'" style="background:'+bx.color+'"><a href="/load_groups/?grp_id='+bx.id+'" style="background:'+bx.color+';border-top:3px solid '+bx.color+'">'+bx.name+'</a></li>';
					htmlBottom += '<li class=" '+(bx.order == 2?'active':'')+'" style="background:'+bx.color+'"><a href="#" id="'+bx.id+'" style="background:'+bx.color+';border-top:3px solid '+bx.color+'" class="next-box">'+bx.name+'</a></li>';
					if(bx.order == 1){
						
						
						$(bx.groups).each(function(){ 

							var grp_htm = $('#grp_accord').html();
							grp = this;
							
							grp_htm = grp_htm.replace(/{grp_id}/g, grp.id);
							grp_htm = grp_htm.replace(/{group_name}/g, grp.name);
							grp_htm = grp_htm.replace(/{memb_count}/g, grp.number_of_members);
							grp_htm = grp_htm.replace(/{bg_color}/g, grp.group_bar_color);
							$('#group_accordion').append(grp_htm);
						});
					}
					jQuery('#group_accordion').slideAccordion({
						opener: '.accordion-btn',
						slider: '.accordion-content',
						animSpeed: 300
					});
				});
				
				
				//  -------- loading posts ---------
				post_htm = '';
				$(response.posts).each(function(){ 
					pst = this;
					var htm = $('#post_skelton').html();
					htm = htm.replace(/{user_name}/g, pst.creator.name);
					htm = htm.replace(/{posted_date}/g, pst.created_date);
					htm = htm.replace(/{description}/g, pst.description);
					post_htm += htm;
				});
				
				
				$('#groups_list_desk').html(htmldesk);
				$('#groups_list_mob').html(htmlmob);
				$('.next-bar').html(htmlBottom);
				$('#main-content').html(post_htm);
				//$('.grp_link').ajaxCategoryLoad();
				
				
			}else{
				//## write the codes if the result is error
			}
		  }
	});*/
	
	$('#groups_list_desk').on('click','.grp_link',function(){
		$('#groups_list_desk li').removeClass('active');
		$(this).parent().addClass('active');
		grp_id = $(this).attr('id');
		$.ajax({		
		  type: "GET",
		  url: "/load_groups/?box_id="+grp_id,
		  dataType: 'json',
		  //url: "./groups.php/?grp_id="+grp_id,
		  success: function(response) {
		  	
			  $('#group_accordion').html('');
			  $(response.groups).each(function(){ 
			  	
				var grp_htm = $('#grp_accord').html();
				grp = this;

				grp_htm = grp_htm.replace(/{grp_id}/g, grp.id);
				grp_htm = grp_htm.replace(/{group_name}/g, grp.name);
				grp_htm = grp_htm.replace(/{memb_count}/g, grp.number_of_members);
				grp_htm = grp_htm.replace(/{bg_color}/g, grp.group_bar_color);
				$('#group_accordion').append(grp_htm);

				jQuery('#group_accordion').slideAccordion({
					opener: '.accordion-btn',
					slider: '.accordion-content',
					animSpeed: 300
				});
			});

			post_htm = '';
			$(response.posts).each(function(){ 
			  	pst = this;
				var htm = $('#post_skelton').html();
				htm = htm.replace(/{user_name}/g, pst.creator.name);
				htm = htm.replace(/{posted_date}/g, pst.created_date);
				htm = htm.replace(/{description}/g, pst.description);
				post_htm += htm;
			});
			$('#main-content').html(post_htm);
			$('#bottom-indicator li').removeClass('active');
			$('#bottom-indicator li a[id='+response.next_box+']').parent().addClass('active');
		  }
		  
		});
		
		return false;
	});

	$('#group_accordion').on('click','li[id^="accor_"]',function(){
		grp_id = $(this).attr('id').split('_')[1];
		$.ajax({		
		  type: "GET",
		  url: "/load_friends/?group_id="+grp_id,
		  dataType: 'json',
		 // url: "./load_freinds/?grp_id="+grp_id,
		  success: function(response) {
		  	
		  	
			   if(response.result == 'ok'){
				 $('.friend_list_'+grp_id).html('');

				  $(response.friends.members).each(function(){ 

				  	frnd = this;
				  	
					html = $('#frnd_list').html();
					if(frnd.member_profile.profile_pic != null){
						html = html.replace(/{mem_profile_pic}/g, '<img src="/static/images/pictures/'+frnd.member_profile.profile_pic+'" alt="Picture">');
					}else{
						html = html.replace(/{mem_profile_pic}/g, '<img src="/static/images/pictures/avatar.jpg" alt="Picture">');
					}
					
					html = html.replace(/{first_name}/g, frnd.member_profile.first_name);
					$('.friend_list_'+grp_id).append(html);
				});
			  }
		  }
		  
		});
		
		return false;
	});

	$('#add_general_post').magnificPopup({
	  	type: 'inline',
        preloader: false,
        focus: '#post_type',
        closeOnBgClick:false,

        // When elemened is focused, some mobile browsers in some cases zoom in
        // It looks not nice, so we disable it:
        callbacks: {
            beforeOpen: function() {
              if($(window).width() < 700) {
                this.st.focus = false;
              } else {
                this.st.focus = '#post_type';
              }
            }
          }
	});

	$('#post_general').click(function(){
		$('#post_success').show();
		$('#post_success').html('Saving....');
		$('#form_general_post').hide();
				
		$.post('/posts/',
		{post_type:$('#post_type').val(),title:$('#title').val(),description:$('#description').val(),csrfmiddlewaretoken: $('#csrf_token').val()},
		
			function(response){
				$('#post_success').html(response.msg);
				
				var htm = $('#post_skelton').html();
				htm = htm.replace(/{user_name}/g, response.post.creator.name);
				htm = htm.replace(/{posted_date}/g, response.post.created_date);
				htm = htm.replace(/{description}/g, response.post.description);
				
				$('#main-content').prepend(htm);
				
		},'json');
	});

	$('#general-popup').on('click','.mfp-close',function(){
		$('#form_general_post').show();
		$('#post_success').hide();
		$('#post_success').html('');
		$('#post_type').val('general');
		$('#title').val('');
		$('#description').val('');
		
	});

	$('form.search-form input[type="search"]').keyup(function(){
		//$('ul#seach-results-list').html('');
		var key = $(this).val();
		$.ajax({		
		  type: "GET",
		  url: "/api/search_people/?search_key="+key,
		  dataType: 'json',
		  //url: "./profile.php/",
		  success: function(response) {
			   
			   if(response.result == 'ok'){
			   		$('#seach-results-container').show();
				   var result_ppl = '';
				   $(response.people).each(function(){ 
						ppl = this;
						var html = $('#search_result').html();
						html = html.replace(/{name}/g, ppl.name);
						html = html.replace(/{btn_add}/g, '<a href="#" class="btn-add" id="useradd_'+ppl.user_id+'">Add</a>');
						if(ppl.profile_details.profile_pic != null){
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/'+ppl.profile_details.profile_pic+'">');
						}else{
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/avatar.jpg">');
						}
						result_ppl += html;
						
					});
					
					result_frnd = '';
					$(response.friends).each(function(){ 
						frnd = this;
						var html = $('#search_result').html();
						html = html.replace(/{name}/g, frnd.name);
						html = html.replace(/{btn_add}/g, '');
						if(frnd.profile_details.profile_pic != null){
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/'+frnd.profile_details.profile_pic+'">');
						}else{
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/avatar.jpg">');
						}
						result_frnd += html;
						
					});
				
				if(result_ppl != ''){
				   $('#seach-results-block-people ul#seach-results-list').html(result_ppl);
				   $('#seach-results-block-people').show();
				}
				if(result_frnd != ''){
				   $('#seach-results-block-friends ul#seach-results-list').html(result_frnd);
				   $('#seach-results-block-friends').show();
				}
			   }else{
			   	$('ul.seach-results-list').html('');
			   	$("#seach-results-container").fadeOut(); 
			   	$('#seach-results-block-friends').hide();
				$('#seach-results-block-people').hide();
			   }
			   
		  }
		  
		});
	});
	
	$("ul#seach-results-list").on("click",'.btn-add',function(e){ 
		 
		 var user_id = $('a.add-friends-btn').attr('id').split('_')[1];
		 invitor_id = $(this).attr('id').split('_')[1];

		 $.ajax({		
		  type: "GET",
		  url: "/get_group_details/?userid="+user_id,
		  dataType: 'json',
		  //data: {csrfmiddlewaretoken: $('#csrf_token').val(),userid:user_id},
		  
		  success: function(response) {
			  
			 if(response.result == "ok"){
					$('#grp_details').val(JSON.stringify(response));
					var opt  = '<option value=""> --  select --</option>';
					$(response.box_details).each(function(){ 
					    bxs = this;
						opt  =opt+'<option value="'+bxs.id +'">'+bxs.name+'</option>';
						
					});
					var formhtm = $('#form_accept_friend_req').html();

					formhtm = formhtm.replace(/{options}/g, opt);
					formhtm = formhtm.replace(/{invitor_id}/g, invitor_id);
					formhtm = formhtm.replace(/{button_add}/g, '<button type="button" id="send_request">Add as friend</button>');
					
					
					//alert(formhtm);
					$.magnificPopup.open({
					  items: {
						src: formhtm, // can be a HTML string, jQuery object, or CSS selector
						type: 'inline'
					  }
					  ,closeOnBgClick:false
					  ,preloader: false
					});

			  }
			 
			  			
			  $(this).parent().hide();

		  	  
			  

		  }
		 });

		 /*$.ajax({		
		  type: "POST",
		  url: "/friend_request/",
		  data: {csrfmiddlewaretoken: $('#csrf_token').val(),userid:user_id},
		  //url: "./groups.php/?grp_id="+grp_id,
		  success: function(response) {
			
			  //$(this).parent().hide();
		  }
		 });*/
		  
		 return false;
	});
	
	$(document).on("click", function(e) { 
	
		  var $clicked = $(e.target);
		  if (! $clicked.hasClass("seach-results-list")){
		  	$("#seach-results-container").fadeOut(); 
			$("#friend-request-container").fadeOut(); 
			$('#seach-results-block-friends').hide();
			$('#seach-results-block-people').hide();
			
		  }
	});

	$('form.search-form input[type="search"]').click(function(){
      	$('#seach-results-container').fadeIn();
	});
	
	
	$('a.add-friends-btn').click(function(){
		$('ul#friend-request-list').html('');
		user_id = $(this).attr('id').split('_')[1];
		$.ajax({		
		  type: "GET",
		  url: "/friend_request/?userid="+user_id,
		  dataType: 'json',
		  //url: "./profile.php/",
		  success: function(response) {
			   
			   $('#friend-request-container').show();
			   if(response.result == 'ok'){
				  $(response.friends).each(function(){ 
						req = this;
						var html = $('#search_result').html();
						html = html.replace(/{name}/g, req.invitor_name);
						html = html.replace(/{user_id}/g, req.invitor);
						html = html.replace(/{btn_add}/g, '<a href="#" class="btn-add" id="useradd_'+req.invitor+'">Add</a>');
						
						if(req.invitor_pofile.profile_pic != null){
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/'+req.invitor_pofile.profile_pic+'">');
						}else{
							html = html.replace(/{avatar}/g, '<img src="/static/images/pictures/avatar.jpg">');
						}
						$('ul#friend-request-list').append(html);
					});
			   }
			   
		  }
		  
		});
	});

	 

	$("ul#friend-request-list").on("click",'.btn-add',function(e){ 
		 //var id = $('a.add-friends-btn').attr('id');
		 var user_id = $('a.add-friends-btn').attr('id').split('_')[1];
		 invitor_id = $(this).attr('id').split('_')[1];
		 $.ajax({		
		  type: "GET",
		  url: "/get_group_details/?userid="+user_id,
		  dataType: 'json',
		  //data: {csrfmiddlewaretoken: $('#csrf_token').val(),userid:user_id},
		  
		  success: function(response) {
			  
			 if(response.result == "ok"){
					$('#grp_details').val(JSON.stringify(response));
					var opt  = '<option value=""> --  select --</option>';
					$(response.box_details).each(function(){ 
					    bxs = this;
						opt  =opt+'<option value="'+bxs.id +'">'+bxs.name+'</option>';
						
					});
					var formhtm = $('#form_accept_friend_req').html();

					formhtm = formhtm.replace(/{options}/g, opt);
					formhtm = formhtm.replace(/{invitor_id}/g, invitor_id);
					formhtm = formhtm.replace(/{button_add}/g, '<button type="button" id="accept_request">Accept </button>');
					
					//alert(formhtm);
					$.magnificPopup.open({
					  items: {
						src: formhtm, // can be a HTML string, jQuery object, or CSS selector
						type: 'inline'
					  }
					  ,closeOnBgClick:false
					  ,preloader: false
					});

			  }
			 
			  			
			  $(this).parent().hide();

		  	  
			  

		  }
		 });
		  
		 return false;
	});

$(document.body).on('change','#box_name',function(){ 
	var box = $(this).val()
	var grp_det  = $('#grp_details').val();
	//alert(grp_det);
	response = $.parseJSON(grp_det);

	var opt = '';
	if(response.result == 'ok')
	{
		$(response.box_details).each(function(){ 

	    bxs = this;

	    if(box == bxs.id){
			$(bxs.groups).each(function(){ 
				grp = this
				
				opt  =opt+'<option value="'+grp.id +'">'+grp.name+'</option>';
			});
	    	
	    }
	    
		
	});
	}
	
	
	$('#group_name').html(opt);

});

$(document.body).on('click','#accept_request',function(){ 
	invitor = $('#invitor').val();
	var box = $('#box_name').val();
	var grp = $('#group_name').val();
	if($('#skip').is(':checked'))
	{
		box = null; grp = null;
	}
	$.ajax({		
		  type: "GET",
		  url: "/accept_request/",
		  dataType: 'json',
		  data: {csrfmiddlewaretoken: $('#csrf_token').val(),invitor:invitor,box:box,group:grp},
		  success: function(response) {
			  if(response.result == 'ok'){
			  	$('#form_accept_friend').hide();
				$('#frnd_req_success').html(response.message).show();

				var count = parseInt($('#frnd-req-count').html());
				$('#frnd-req-count').html(count-1);
				var grp_count = parseInt($('#membcount_'+grp).html());
				$('#membcount_'+grp).html(grp_count+1);

			  } 	
		  }
		 });
});

$(document.body).on('click','#send_request',function(){ 
	user_id = $('#invitor').val();
	var box = $('#box_name').val();
	var grp = $('#group_name').val();
	if($('#skip').is(':checked'))
	{
		box = null; grp = null;
	}
	
		
	$.ajax({		
		  type: "POST",
		  url: "/friend_request/",
		  dataType: 'json',
		  data: {csrfmiddlewaretoken: $('#csrf_token').val(),userid:user_id,box:box,group:grp},
		  success: function(response) {
			  if(response.result == 'ok'){
			  	$('#form_accept_friend').hide();
				$('#frnd_req_success').html(response.message).show();

			  } 	
		  }
		 });
});

$('#bottom-indicator').on('click','.next-box',function(){ 
	
	box_id = $(this).attr('id');
	$.ajax({		
	  type: "GET",
	  url: "/load_groups/?box_id="+box_id,
	  dataType: 'json',
	  //url: "./groups.php/?grp_id="+grp_id,
	  success: function(response) {
	  	
		  $('#group_accordion').html('');
		  $(response.groups).each(function(){ 
		  	
			var grp_htm = $('#grp_accord').html();
			grp = this;

			grp_htm = grp_htm.replace(/{grp_id}/g, grp.id);
			grp_htm = grp_htm.replace(/{group_name}/g, grp.name);
			grp_htm = grp_htm.replace(/{memb_count}/g, grp.number_of_members);
			grp_htm = grp_htm.replace(/{bg_color}/g, grp.group_bar_color);
			$('#group_accordion').append(grp_htm);

			jQuery('#group_accordion').slideAccordion({
				opener: '.accordion-btn',
				slider: '.accordion-content',
				animSpeed: 300
			});
		});

	    post_htm = '';
		$(response.posts).each(function(){ 
		  	pst = this;
			var htm = $('#post_skelton').html();
			htm = htm.replace(/{user_name}/g, pst.creator.name);
			htm = htm.replace(/{posted_date}/g, pst.created_date);
			htm = htm.replace(/{description}/g, pst.description);
			post_htm += htm;
		});
		$('#main-content').html(post_htm);
		$('#bottom-indicator li').removeClass('active');
		$('#bottom-indicator li a[id='+response.next_box+']').parent().addClass('active');
		$('#groups_list_desk li').removeClass('active');
		$('#groups_list_desk li[id="box_'+box_id+'"]').addClass('active');
	  }
	  
	});
	
	return false;
});


var btn = document.getElementById('file_upload');
  
var uploader = new ss.SimpleUpload({
	button: btn,
	url: '/create_album/',
	name: 'uploadfile',
	hoverClass: 'hover',
	focusClass: 'focus',
	data:{csrfmiddlewaretoken: $('#csrf_token').val()},
	startXHR: function() {
		
	},
	onSubmit: function() {
		//btn.innerHTML = 'Uploading...'; // change button text to "Uploading..."
		//$('#uploadBtn1').html('Uploading...');
	  },
	onComplete: function( filename, response ) {
		
		//btn.innerHTML = 'Change Doc 1';
		//$('#uploadBtn1').html('Change Doc 1');

		if ( !response ) {
			alert('Unable to upload file');
			return;
		}
		
		if ( response != '' ) {
			$('#f_doc_1').val(filename); 

		} else {
			if ( response.msg )  {
				alert(response.msg);

			} else {
				alert('An error occurred and the upload failed.');
			}
		}
	  },
	onError: function() {
		alert('Unable to upload file');
	  }
});

$('#new_event').magnificPopup({
	type: 'inline',
	preloader: false,
	focus: '#name',
	closeOnBgClick:false,

	// When elemened is focused, some mobile browsers in some cases zoom in
	// It looks not nice, so we disable it:
	callbacks: {
		beforeOpen: function() {
		  if($(window).width() < 700) {
			this.st.focus = false;
		  } else {
			this.st.focus = '#name';
		  }
		  $.ajax({		
				  type: "GET",
				  url: "/get_all_profiles/",
				  dataType: 'json',
				  data: {csrfmiddlewaretoken: $('#csrf_token').val()},
				  success: function(response) {
					  if(response.result == 'ok'){
						
							if(response.result == 'ok'){
								htm = '';
								$(response.profile).each(function(){ 
									prof = this;
									htm += '<option value="'+prof.user+'">'+prof.first_name+'</option>';
								});
								$('.add-memb').html(htm);
								$(".chzn-select").chosen({placeholder_text_multiple:'Invite Members'});
							 } 
					  } 	
				  }
			});
			
		}
	  }
});

$('#when').datepicker();

$(document.body).on('click','#post_event',function(){ 
	var name = $('#name').val();
	var description = $('#description').val();
	var where = $('#where').val();
	var when = $('#when').val();
	var time = $('#time').val();
	
	$.ajax({		
		  type: "POST",
		  url: "/events/",
		  dataType: 'json',
		  data: {csrfmiddlewaretoken: $('#csrf_token').val(),name:name,description:description,where:where,when:when,time:time},
		  success: function(response) {
			  if(response.msg != null){
				$('#form_events').hide();
				$('#event_success').html(response.msg).show();
				
				var htm = $('#event-skelton').html();
				htm = htm.replace(/{name}/g, name);
				htm = htm.replace(/{description}/g, description);
				//htm = htm.replace(/{description}/g, where);
				
				$('.events-list-area').prepend(htm);
				
			  } 	
		  }
		 });
});


$('#event-popup').on('click','.mfp-close',function(){
		$('#form_events').show();
		$('#event_success').hide();
		$('#event_success').html('');
		$('#name').val('');
		$('#where').val('');
		$('#description').val('');
		$('#when').val('');
		$('#time').val('');
		
	});


});
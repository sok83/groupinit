function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validateUserName(username) {
    
    /*var re = /^[a-zA-Z0-9 ]+(?=.*?[a-zA-Z])[a-zA-Z0-9.,-]{2,30}$/;*/
    /* var re = /^[a-zA-Z0-9]+(?=.*?[a-zA-Z])+(\s)[a-z0-9.,-]{2,30}$/;*/
    /* var re = /^[a-zA-Z0-9]+(?=.*?[a-zA-Z ])+[a-z 0-9.,-]{2,30}$/;*/
    var re = /^[a-zA-Z0-9]+(?=.*?[a-zA-Z ])+[a-zA-Z 0-9.,-]{2,30}$/;
       
    return re.test(username.name);
}

function paginate(list, $scope, page_interval) {
    if(!page_interval)
        $scope.page_interval = 20;
    else
        $scope.page_interval = page_interval;
    $scope.current_page = 1;
    $scope.pages = list.length / $scope.page_interval;
    if($scope.pages > parseInt($scope.pages))
        $scope.pages = parseInt($scope.pages) + 1;
    $scope.visible_list = list.slice(0, $scope.page_interval);
}

function select_page(page, list, $scope, page_interval) {
    if(!page_interval)
        $scope.page_interval = 20;
    var last_page = page - 1;
    var start = (last_page * $scope.page_interval);
    var end = $scope.page_interval * page;
    $scope.visible_list = list.slice(start, end);
    $scope.current_page = page;
}

function show_loader($scope){
    // $('#overlay').css('display', 'block');
    $scope.loading_flag = true

    $('.spinner').css('display', 'block');
}
function hide_loader($scope){
    // $('#overlay').css('display', 'none');
    $scope.loading_flag = false
    $('.spinner').css('display', 'none');
}
function ForgotPasswordController($scope, $element, $http, $timeout, $location, authentication_flags)

{



    $scope.init = function(csrf_token){

        $scope.csrf_token = csrf_token;

        $scope.email = '';

        $scope.mail_flag = false;

        $scope.success_flag = false;

        $scope.authentication_flags = authentication_flags;

    }

    $scope.clear_mail_flag = function(){

        $scope.msg = "";

        $scope.mail_flag = true;

        $scope.success_flag = false;

    }

    $scope.send_email = function(){



        if($scope.email == '') {

            $scope.msg = "Please enter Email";

        } else if(!validateEmail($scope.email)) {

            $scope.msg = "Please Enter a valid Email";

        } else {

            show_loader($scope);

            params = {

                'email': $scope.email,

                "csrfmiddlewaretoken" : $scope.csrf_token,

            }

            $http({

                method : 'post',

                url : "/forgot_password/",

                data : $.param(params),

                headers : {

                    'Content-Type' : 'application/x-www-form-urlencoded'

                }

            }).success(function(data, status) {

                hide_loader($scope);

                if(data.result == 'Ok'){

                    $scope.msg = "";

                    $scope.success_flag = true;

                    $scope.random_key = data.random_key;

                    

                } else {

                    $scope.msg = data.message;

                }

            }).error(function(data, status){

                hide_loader($scope);

                $scope.message = data.message;

            });

        }

    }

    $scope.return_to_login = function(){

        authentication_flags.authentication = true;

        authentication_flags.forgot_password = false;

    }

    $scope.resend_mail = function(random_key){

        show_loader($scope);

        

        $scope.random_key= random_key;

        if($scope.random_key != ''){

            $http.get('/resend_reset_password/?key='+$scope.random_key).success(function(data){

                hide_loader($scope);

                if(data.result == 'ok'){

                    $scope.msg = "";

                    $scope.error_msg_flag = false;

                    $scope.success_msg = data.message;

                    $scope.random_key = data.random_key;

                    $scope.success_flag = true;

                    

                }

            }).error(function(data){





            })

        } else {



        }

    }

}


function LoginRegistrationController($scope, $element, $http, $timeout, $location, authentication_flags)

{

    $scope.init = function(csrf_token, invitation_token){

        $scope.csrf_token = csrf_token;

        $scope.invitation_token = invitation_token;

        $scope.edit_flag = false;

        $scope.error_msg_flag = false;

        $scope.success_flag = false;

        $scope.hide_error_div = true;

        $scope.check_box_error_flag = true;

        $scope.username = '';

        $scope.password = '';

        $scope.new_user = {

            'email': '',

            'password': '',

            'confirm_password': '',

            'name': '',

            'terms': false,

            'invitation_token': $scope.invitation_token,

        }

        $scope.authentication_flags = authentication_flags;

    }

    $scope.reset_user = function(){

        $scope.msg = '';

        $scope.new_user = {

            'email': '',

            'password': '',

            'confirm_password': '',

            'name': '',

            'terms': false,

        }

    }

    $scope.validate_login = function(){

        $scope.login_error_msg_flag = false;
        $scope.act_link_flag = false;

        $scope.login_msg = '';
        $scope.act_link = "";

        if($scope.username == '') {

            $scope.login_error_msg_flag = true;

            $scope.login_msg = "Please enter Email ";

            return false;

        } else if(!validateEmail($scope.username)) {

            $scope.login_error_msg_flag = true;

            $scope.login_msg = "Please Enter a valid Email";
            return false;

        } else if($scope.password == '') {

            $scope.login_error_msg_flag = true;

            $scope.login_msg = "Please enter Password";
            return false;

        }

        return true;

    }

    $scope.validate_name = function(){

        $scope.name_msg = '';

        $scope.name_error_msg_flag = false;

        $scope.email_error_msg_flag = false;

        $scope.pass_error_msg_flag = false;

        $scope.conf_pass_error_msg_flag = false;



        if($scope.new_user.name == '' || $scope.new_user.name.length == 0){

            $scope.name_msg = "Please enter Your Name";

            $scope.name_error_msg_flag = true;

            $scope.email_msg = '';

            $scope.email_error_msg_flag = false;

            $scope.pass_error_msg_flag = false;

            $scope.pass_msg = '';

            $scope.conf_pass_error_msg_flag = false;

            return false;

        } else if($scope.new_user.name.length < 3 ) {

            $scope.name_msg = "Please enter a valid name";

            $scope.name_error_msg_flag = true;

            $scope.email_msg = '';

            $scope.email_error_msg_flag = false;

            $scope.pass_error_msg_flag = false;

            $scope.pass_msg = '';

            $scope.conf_pass_error_msg_flag = false;

            return false;

        }else if($scope.new_user.name.length > 30 ) {

            $scope.name_msg = "Please enter a valid name";

            $scope.name_error_msg_flag = true;

            $scope.email_msg = '';

            $scope.email_error_msg_flag = false;

            $scope.pass_error_msg_flag = false;

            $scope.pass_msg = '';

            $scope.conf_pass_error_msg_flag = false;

            return false;

        }else if(validateUserName($scope.new_user) != true) {



            $scope.name_msg = "Please enter a valid name";

            $scope.name_error_msg_flag = true;

            $scope.email_msg = '';

            $scope.email_error_msg_flag = false;

            $scope.pass_error_msg_flag = false;

            $scope.pass_msg = '';

            $scope.conf_pass_error_msg_flag = false;

            return false;

        }

        return true;

    }

    $scope.validate_email = function(){

        // $scope.name_msg = "";

        // $scope.name_error_msg_flag = false;

        $scope.email_msg = '';

        $scope.email_error_msg_flag = false;

            if($scope.new_user.email == '') {

                $scope.email_msg = "Please enter Email";

                $scope.email_error_msg_flag = true;

                $scope.name_msg = "";

                $scope.name_error_msg_flag = false;

                $scope.pass_error_msg_flag = false;

                $scope.conf_pass_error_msg_flag = false;

                $scope.pass_msg = '';

                return false;

            } else if(!validateEmail($scope.new_user.email)) {

                $scope.email_msg = "Please Enter a valid Email";

                $scope.email_error_msg_flag = true;

                $scope.name_msg = "";

                $scope.name_error_msg_flag = false;

                $scope.pass_error_msg_flag = false;

                $scope.conf_pass_error_msg_flag = false;

                $scope.pass_msg = '';

                return false;

            }else{



                $http.get('/check_email/?email_id='+$scope.new_user.email+'&ajax=true').success(function(data){



                if (data.result == 'ok') {

                    $scope.email_error_msg_flag = false;

                    return true;

                } else{

                    $scope.email_error_msg_flag = true;

                    $scope.email_msg = "User Already Exists";

                    $scope.name_msg = "";

                    $scope.name_error_msg_flag = false;

                    if ($scope.pass_error_msg_flag == true){

                        $scope.pass_error_msg_flag = false;

                        $scope.hide_error_div = true;

                        $scope.clear_password_error_flag()

                        $scope.pass_msg = '';





                    }
                    return false;

                }

                })

            // }

        return true;

    }

    }



    $scope.clear_error_flag = function(){

        $scope.login_error_msg_flag = false;
        $scope.act_link_flag=false;

        $scope.login_msg = "";
        $scope.act_link = "";

    }

    $scope.clear_name_error_flag = function(){

        $scope.name_msg = "";

        $scope.name_error_msg_flag = false;
         $scope.new_user.terms = false;
         $scope.hide_error_div = true;


    }



    $scope.clear_email_error_flag = function(){

        $scope.email_msg = "";

        $scope.email_error_msg_flag = false;

         $scope.new_user.terms = false;

        $scope.hide_error_div = true;

      /*  $scope.clear_password_error_flag()*/

    }

    $scope.clear_password_error_flag = function(){

        $scope.pass_msg = '';

        $scope.pass_error_msg_flag = false;

        $scope.hide_error_div = true;
        $scope.new_user.terms = false;

        if ($scope.new_user.confirm_password != ''){
          $scope.validate_conf_password();

      }

    }

    $scope.clear_conf_password_error_flag = function(){
        $scope.pass_msg = '';
        $scope.conf_pass_error_msg_flag = false;
        $scope.hide_error_div = true;
        $scope.new_user.terms = false;
        $scope.validate_pass();


    }
$scope.validate_conf_pass_intern = function(){
  $scope.pass_msg = '';

  $scope.pass_error_msg_flag = false;

  $scope.pass_msg = '';

  $scope.conf_pass_error_msg_flag = false;

  if($scope.new_user.password != $scope.new_user.confirm_password) {

      $scope.pass_msg = "Password mismatch";

      $scope.conf_pass_error_msg_flag = true;

      $scope.name_msg = "";

      $scope.name_error_msg_flag = false;

      $scope.email_error_msg_flag = false;

      $scope.email_msg = "";

      return false;

  }

  $scope.check_box_error_flag = false;

  return true;


}

$scope.validate_conf_password = function(){

if($scope.new_user.password != ''){
  if ( $scope.validate_pass() )  {
    $scope.validate_conf_pass_intern();
    }

else {
  return false;
}
}
else{
  $scope.pass_msg = "Please Enter the Password Field First";
  $scope.conf_pass_error_msg_flag = true;
  $scope.name_msg = "";
  $scope.name_error_msg_flag = false;
  $scope.email_error_msg_flag = false;
  $scope.email_msg = "";
  return false;

}
}
    $scope.validate_pass = function(){

        // $scope.email_error_msg_flag = false;

        // $scope.email_msg = "";

        $scope.pass_msg = '';

        $scope.pass_error_msg_flag = false;

        if($scope.new_user.password == '') {

            $scope.pass_msg = "Please Enter the Password Field First";

            $scope.pass_error_msg_flag = true;

            $scope.name_msg = "";

            $scope.name_error_msg_flag = false;

            $scope.email_error_msg_flag = false;

            $scope.email_msg = "";

            if($scope.email_error_msg_flag == true && $scope.pass_error_msg_flag ==true){

                $scope.pass_msg = "";

                $scope.pass_error_msg_flag = false;

                $scope.hide_error_div = true;

                // $scope.email_error_msg_flag = false;

                // $scope.email_msg="";



            }

            return false;



        } else if($scope.new_user.password.length < 6){

            $scope.pass_msg = "Password should contain atleast 6 characters ";

            $scope.pass_error_msg_flag = true;

            $scope.name_msg = "";

            $scope.name_error_msg_flag = false;

            $scope.email_error_msg_flag = false;

            $scope.email_msg = "";

            if($scope.email_error_msg_flag == true && $scope.pass_error_msg_flag ==true){

                $scope.pass_msg = "";

                $scope.pass_error_msg_flag = false;

                $scope.hide_error_div = true;

                // $scope.email_error_msg_flag = false;

                // $scope.email_msg="";



            }

            return false;

        }else if($scope.new_user.password.length > 30){

            $scope.pass_msg = "password length cannot exceed over 30 characters ";

            $scope.pass_error_msg_flag = true;

            $scope.name_msg = "";

            $scope.name_error_msg_flag = false;

            $scope.email_error_msg_flag = false;

            $scope.email_msg = "";

            if($scope.email_error_msg_flag == true && $scope.pass_error_msg_flag == true){

                $scope.pass_msg = "";

                $scope.pass_error_msg_flag = false;

                $scope.hide_error_div = true;

                // $scope.email_error_msg_flag = false;

                // $scope.email_msg="";



            }

            return false;

        }



        return true;

    }

    $scope.validate_form = function(){

        if ($scope.validate_name()){

            if($scope.validate_email()){

                if($scope.validate_password()){

                    if($scope.validate_conf_password()){

                        $scope.check_box_error_flag = false;

                    }

                    else

                        $scope.new_user.terms = false;



                }

                else

                    $scope.new_user.terms = false;

            }

            else

                $scope.new_user.terms = false;

        }

        else

            $scope.new_user.terms = false;



    }

    $scope.validate_password = function(){

        $scope.pass_msg = '';

        $scope.pass_error_msg_flag = false;

        

        if($scope.email_error_msg_flag == false && $scope.name_error_msg_flag == false){


            if($scope.new_user.password == '') {

                $scope.pass_msg = "Please enter Password";

                $scope.pass_error_msg_flag = true;

                return false;

            } else if($scope.new_user.password != $scope.new_user.confirm_password) {

                $scope.pass_msg = "Password mismatch";

                $scope.pass_error_msg_flag = true;

                return false;

            } else if($scope.new_user.password.length < 6){

                $scope.pass_msg = "Password should contain atleast 6 characters ";

                $scope.pass_error_msg_flag = true;

                return false;

            }else if($scope.new_user.password.length > 30){

                $scope.pass_msg = "password length cannot exceed over 30 characters ";

                $scope.pass_error_msg_flag = true;

                return false;

            }

        }

        return true;

    }

    $scope.validate_user = function(){

        $scope.terms_msg = '';

        $scope.terms_error_msg_flag = false;

        if($scope.validate_name() && $scope.validate_email() && $scope.validate_password()){

            if(!$scope.new_user.terms) {

                $scope.terms_msg = "Please Agree Terms and Conditions";

                $scope.terms_error_msg_flag = true;

                return false;

            } else {

                return true;

            }

        } else {

            return false;

        }

    }
$scope.user_login = function(){
        
        
        if ($scope.remember_me == undefined){
            $scope.remember_me = false;
        }
        
        if($scope.validate_login()) {

            show_loader($scope);

            params = {

                'username': $scope.username,

                'password': $scope.password,
                'remember_me':$scope.remember_me,
                "csrfmiddlewaretoken" : $scope.csrf_token,

            }

            $http({

                method : 'post',

                url : "/login/",

                data : $.param(params),

                headers : {

                    'Content-Type' : 'application/x-www-form-urlencoded'

                }

            }).success(function(data, status) {
                hide_loader($scope);

                if(data.result == 'Ok'){

                    $scope.msg = "";

                    document.location.href = data.next_url;

                } else{
                    if (data.message) {
                        $scope.login_error_msg_flag = true;
                        $scope.login_msg = data.message;
                    }
                    $scope.act_link_flag=true;
                    $scope.act_link =data.activation_link;




angular.module('bindHtmlExample', ['ngSanitize'])
.controller('ExampleController', ['$scope', function($scope) {
  $scope.myHTML =
     'I am an <code>HTML</code>string with ' +
     '<a href="#">links!</a> and other <em>stuff</em>';
}]);




                }

            }).error(function(data, status){
                
                hide_loader($scope);

                $scope.message = data.message;

                

                $scope.login_msg=$scope.message

            });

        }

        

    }



    $scope.save_new_user = function(){



       

        $scope.msg = '';

        if($scope.validate_user()){

            show_loader($scope);

            $scope.new_user.terms = String($scope.new_user.terms);

            params = {

                'user_details': angular.toJson($scope.new_user),

                "csrfmiddlewaretoken" : $scope.csrf_token,

            }

            $http({

                method : 'post',

                url : "/signup/",

                data : $.param(params),

                headers : {

                    'Content-Type' : 'application/x-www-form-urlencoded'

                }

            }).success(function(data, status) {

                hide_loader($scope);

                if(data.result == 'ok'){

                    $scope.msg = "";

                    $scope.reset_user();

                    $scope.edit_flag = false;

                    $scope.error_msg_flag = false;

                    $scope.success_msg = data.message;

                    $scope.random_key = data.random_key;

                    $scope.success_flag = true;

                    
                } else {

                    $scope.email_msg = "Email already registered!";

                    $scope.email_error_msg_flag = true;

                }

            }).error(function(data, status){

                hide_loader($scope);

                $scope.message = data.message;

                

            });

        }

    }
$scope.resend_mail = function(random_key){

        $scope.success_flag = false;

        show_loader($scope);

        

        $scope.random_key= random_key;

        if($scope.random_key != ''){

            $http.get('/resend/?key='+$scope.random_key).success(function(data){

                hide_loader($scope);

                if(data.result == 'ok'){

                    $scope.msg = "";

                    $scope.error_msg_flag = false;

                    $scope.success_msg = data.message;

                    $scope.random_key = data.random_key;

                    $scope.success_flag = true;

                    

                }

            }).error(function(data){

               



            })

        } else {



        }

    }

    $scope.forgot_password = function(){

        authentication_flags.authentication = false;

        authentication_flags.forgot_password = true;

    }

}
function ResetPasswordController($scope, $element, $http, $timeout, $location)
{
    $scope.init = function(csrf_token, token_id){
        $scope.csrf_token = csrf_token;
        if(token_id != ''){
            $scope.token_id = token_id;
        } else {
            $scope.token_id = ''
        }
        $scope.new_password = '';
        $scope.confirm_password = '';
    }
    $scope.clear_pass_err_flag = function(){
        $scope.pass_msg = '';
        $scope.pass_error_msg_flag = false;
    }
    $scope.validate_pass = function(){
        $scope.pass_msg = '';
        $scope.pass_error_msg_flag = false;

        if($scope.new_password == '') {
            $scope.pass_msg = "Please enter Password";
            $scope.pass_error_msg_flag = true;
            return false;
        } else if($scope.new_password.length < 6){
            $scope.pass_msg = "Password should contain atleast 6 characters ";
            $scope.pass_error_msg_flag = true;
            return false;
        }else if($scope.new_password.length > 30){
            $scope.pass_msg = "password length cannot exceed over 30 characters ";
            $scope.pass_error_msg_flag = true;
            return false;
        }
        return true;
    }
    $scope.validate_password = function(){
        $scope.pass_msg = '';
        $scope.pass_error_msg_flag = false;
        
        if($scope.new_password == '') {
            $scope.pass_msg = "Please enter Password";

            $scope.pass_error_msg_flag = true;
            return false;
        } else if($scope.new_password != $scope.confirm_password) {
            $scope.pass_msg = "Password mismatch";
            $scope.pass_error_msg_flag = true;
            return false;
        } else if($scope.pass_strength < 5){
            $scope.pass_msg = "Password should contain atleast 5 characters ";
            $scope.pass_error_msg_flag = true;
            return false;
        }
        return true;
    }
    $scope.save_new_password = function(){

        $scope.pass_error_msg_flag=false
        $scope.msg = '';
        if($scope.validate_password()){
            show_loader($scope);
            params = {
                'token_id': $scope.token_id,
                'new_password': $scope.new_password,
                'confirm_password': $scope.confirm_password,
                "csrfmiddlewaretoken": $scope.csrf_token,
            }
            $http({
                    method: 'post',
                    url: '/reset_password/',
                    data: $.param(params),
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }).success(function(data){
                    hide_loader($scope);
                    $scope.error_message = data.message;
                    $scope.new_password = '';
                    $scope.confirm_password = '';
                    $scope.success_flag = true;
                    $scope.success_msg = "Your password has been reset succeccfully!"
                    // document.location.href = '/login/';
                }).error(function(data){
                    hide_loader($scope);

                })
        }
    }
}

function SearchPeopleController($scope, $element, $http, $timeout, $location, search_service)
{
    $scope.init = function(csrf_token, profile){
        $scope.csrf_token = csrf_token;
        $scope.people_search_key = profile;
        $scope.choose_box = false;
        $scope.search_service = search_service;

    }
    $scope.search_people = function(){
        if($scope.people_search_key != ''){
            $http.get('/search_people/?key='+$scope.people_search_key).success(function(data){
                $scope.people = data.people;
                $scope.friends=data.friends;
                if(data.people.length == 0){
                    $scope.people_search_error = "No Result";
                }
                if(data.friends.length == 0){
                    $scope.friend_search_error = "No Friend with this name";
                }
            }).error(function(data){

            })
        } else {
            $scope.people_search_error = "Please enter Key";
        }
    }
    $scope.select_friend = function(user) {
        document.location.href = "/profile/"+user.slug+"/"+user.user_id+"/";
    }
    $scope.send_friend_request = function(suggestion) {
        $scope.quick_view = true;
        search_service.quick_view = true;
        $http.get("/profile/"+suggestion.slug+"/"+suggestion.user_id+"/").success(function(data){
            $scope.profile = data.profile;
            search_service.quick_profile = $scope.profile;
        }).error(function(data){

        })
    }
}

function ProfileController($scope, $element, $http, $timeout, $location)
{
    $scope.init = function(csrf_token){
        $scope.csrf_token = csrf_token;

    }
    $scope.send_friend_request = function(){
        if($scope.people_search_key != ''){
            $http.get('/search_people/?key='+$scope.people_search_key).success(function(data){
                $scope.people = data.people;
                if(data.people.length == 0){
                    $scope.people_serach_error = "No Result";
                }
            }).error(function(data){

            })
        } else {
            $scope.people_serach_error = "Please enter Key";
        }
    }
    $scope.select_friend = function(user) {
        document.location.href = "/profile/"+user.slug+"/"+user.user_id+"/";
    }
}

function DashboardController($scope, $element, $http, $timeout, $location, search_service)
{
    $scope.box_flag = false;
    $scope.init = function(csrf_token, current_user_id){
        $scope.csrf_token = csrf_token;
        $scope.current_user_id = current_user_id;
        $scope.get_friend_suggestions();
        $scope.get_boxes();
        $scope.search_service = search_service;
        $scope.choose_box = false;
    }
    $scope.get_friend_suggestions = function(){
        $http.get('/friend_suggestions/').success(function(data){
            $scope.suggestions = data.suggestions;
           
        }).error(function(data){

        })
    }
    $scope.send_friend_request = function() {
        params = {
            'box': angular.toJson($scope.selected_box),
            'group': angular.toJson($scope.selected_group),
            'profile': angular.toJson($scope.selected_profile),
            "csrfmiddlewaretoken" : $scope.csrf_token,
        }
        $http({
            method: 'post',
            url: '/friend_request/'+$scope.selected_profile.user_id+"/",
            data: $.param(params),
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }).success(function(data, status) {
            $scope.box_flag = false;
            //document.location.href ="/dashboard/";
        })
    }
    $scope.get_boxes = function(){
        $http.get('/friends_box/').success(function(data){
            $scope.boxes = data.boxes;
        }).error(function(data){

        })
    }
    $scope.new_box = function(){
        $scope.box_flag = true;
    }
    $scope.save_box = function(){
        params = {
            'box_details': angular.toJson($scope.box_details),
            "csrfmiddlewaretoken" : $scope.csrf_token,
        }
        $http({
            method: 'post',
            url: "/friends_box/",
            data: $.param(params),
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }).success(function(data, status) {
            $scope.box_flag = false;
            document.location.href ="/dashboard/";
        })
    }
    $scope.notification_popup = function(){
        $scope.notification_popup_flag= true;
        $http.get('/friends_box/').success(function(data){
            $scope.boxes = data.boxes;
        }).error(function(data){

        })
    }
    $scope.choose_friendsbox = function(selected_profile){
        $scope.choose_box = true;
        $scope.selected_profile = selected_profile;
    }
    $scope.select_box_and_group = function(box, group) {
        $scope.selected_box = box;
        $scope.selected_group = group;
        $scope.send_friend_request();

    }
}


function InviteContacts($scope, $element, $http, $timeout, $location)
{
    $scope.init = function(csrf_token){
        $scope.csrf_token = csrf_token;
        $scope.flag = true;
        $scope.google_contacts = []
    }
    $scope.import_google_contacts = function(){
        $http.get('/import_google_contacts/').success(function(data){
            for(i=0; i<data.contacts.length; i++){
                $scope.google_contacts.append(data.contacts[i]);
            }
            if(data.contacts.length > 0 || $scope.flag){
                //$scope.import_google_contacts();
            } else {
                $scope.flag = false;
            }
        }).error(function(data){

        })
    }
    $scope.select_friend = function(user) {
        document.location.href = "/profile/"+user.slug+"/"+user.user_id+"/";
    }
    $scope.invite = function(email){
        $http.get('/invite/?email='+email).success(function(data){
            if(data.result=="success") {
            }
        }).error(function(data){

        })
    }
}

function GroupsController($scope, $element, $http, $timeout, $location)
{
    $scope.init = function(csrf_token, box_id){
        $scope.new_group_flag = false;
        $scope.add_group_memebers_flag = false;
        $scope.csrf_token = csrf_token;
        $scope.box_id = box_id;
        $scope.get_groups();
    }
    $scope.get_groups = function(){
        $http.get('/groups/'+$scope.box_id+'/').success(function(data){
            $scope.groups = data.groups;
        }).error(function(data){

        })
    }

}
function ManageGroupsController($scope, $element, $http, $timeout, $location){
    $scope.init = function(csrf_token, box_id){
        $scope.new_group_flag = false;
        $scope.add_group_memebers_flag = false;
        $scope.csrf_token = csrf_token;
        $scope.box_id = box_id;
        // $scope.get_groups();
    }
    // $scope.get_groups = function(){
    //     console.log("hii")
    //     $http.get('/groups/'+$scope.box_id+'/').success(function(data){
    //         $scope.groups = data.groups;
    //     }).error(function(data){

    //     })
    // }
    $scope.new_group = function(){
        $scope.new_group_flag = true;
    }
    $scope.save_group = function(){
        params = {
            'group_details': angular.toJson($scope.group_details),
            "csrfmiddlewaretoken" : $scope.csrf_token,
        }
        $http({
            method: 'post',
            url: '/groups/'+$scope.box_id+'/',
            data: $.param(params),
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }).success(function(data, status) {
            $scope.new_group_flag = false;
            document.location.href ="/dashboard/";
        })
    }
    $scope.add_new_group_memebers = function(){
        $scope.add_group_memebers_flag = true;
    }
    $scope.save_group_members = function(){
        params = {
            'group_members': angular.toJson($scope.group_members),
            "csrfmiddlewaretoken" : $scope.csrf_token,
        }
        $http({
            method: 'post',
            url: '/add_group_members/',
            data: $.param(params),
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        }).success(function(data, status) {
            $scope.add_group_memebers_flag = false;
            document.location.href ="/dashboard/";
        })
    }
}
function AppsettingsController($scope, $element, $http, $timeout, $location){



    $scope.init = function(csrf_token){

       
        $scope.csrf_token = csrf_token;

        $scope.profile_settings = {

            'see_profile':'Public',

            'see_personal_info':'Public',

            'see_post_in_shared_groups':'Only Group Friends',

            'see_post_in_other_groups':'Public',

            'allow_post_shared_with_personal':'No',

        }  

    }

    

    $scope.submit_settings = function (){

        

        show_loader($scope);

        

        params = { 

            'profile_settings': angular.toJson($scope.profile_settings),

            "csrfmiddlewaretoken" : $scope.csrf_token,

        }

        $http({

                method : 'post',

                url : "/appsettings/",

                data : $.param(params),

                headers : {

                    'Content-Type' : 'application/x-www-form-urlencoded'

                }

            }).success(function(data, status) { 

                hide_loader($scope);

               

                if(data.result == 'ok'){

                    $scope.msg = "";

                    document.location.href = data.next_url;

                } 

            }).error(function(data, status){

                

            });

        }    

}

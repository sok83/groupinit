'use strict';

/* Services */

// Demonstrate how to register services
// In this case it is a simple value service.
var stock_serv = angular.module('groupinit.services', ['ngResource']);

stock_serv.value('version', '0.1');

stock_serv.factory('share', function()
{
    return {
        messages : {
            show : false,
            type : '',
            message : ''
        },
        loader : {
            show : false
        }
    };
});

stock_serv.factory('authentication_flags', function() {
    
    var flags = {
        'authentication': true,
        'forgot_password': false
    };
       
    return flags;
});

stock_serv.factory('search_service', function() {
    
    var flags = {
        'quick_view': false,
        'quick_profile': {}
    };
       
    return flags;
});
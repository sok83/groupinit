groupinit.controller('LoginForm', ['$scope', function($scope) {
	$scope.csrf_token =	'jwi89uyc89373yt78c4yt89j';
	$scope.action = '/login/';
	
	$('form').submit(function(){ return false; });
	
	$scope.submitTheForm = function(){
		console.log("ooooo")
		$.ajax({
			url: $scope.action,
			cache: false,
			dataType: 'json',
			data: { 
				'username': $scope.username,
				'password': $scope.password,
				"csrfmiddlewaretoken" : $scope.csrf_token,
			},
		}).done(function(data) {
			if(typeof data.result != "undefined" && data.result.toLowerCase() == 'ok'){
				showMessage(data.message);
				window.location = data.next_url
			} else {
				showMessage(data.message, true);
			}
		}).fail(function(jqXHR, textStatus) {
			showMessage(textStatus, true);
		});
	
		return false;
	};
}]);

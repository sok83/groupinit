// All master binding logic & top level operations goes here
function ajaxGet(url,callback){
    $.ajax({
      type: "GET",
      url: url,
      dataType: 'json',
      data: {csrfmiddlewaretoken: $('#csrf_token').val()},
      success: callback
    });
  }

 function ajaxPost(url,data,callback){
     var uri = url;
     if( typeof url == 'object'){
         uri = url.url;
         data = url.data;
         callback = url.callback;
     }
    $.ajax({
      type: "POST",
      url: uri,
      dataType: 'json',
      data:data,
      success: callback
    });
  }

//Shared object for fileupload
window.shared = {
    percent: 0,
    videos: [],
    images: [],
    currtype: '',
    finished: false
};
// Shared Location
window.locationpoints = [];
//Lifetime coverphoto
window.coverphoto = {
    photo:'',
    which: 0
};
//Poster Media post
window.media = {
  video: '',
  audio:''
};
window.searchimages = {
    images: [],
    type: 'video',
    show:false
};

window.post = {
    type: 'box'
};

window.upload = {
  file: {}
};

window.postextend ={
    show: false,
    post: {},
    groups:[],
    typecomment:'',
    commentreadonly: true,
    likesarray:[],
    showlikes:false,
    hiddenPagination: true,
    shareapost:false,
    audiencepopup:false,
    audience: [],
    showdelete: false
};

  // First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
function refreshIScroll() {
    if (scrollArea.data('IScroll') && scrollArea.length) {
        scrollArea.data('IScroll').refresh();
    }
}
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function ExtendedPost() {

    jQuery('.slideshow-menu-holder .slideshow-menu').openClose({
        activeClass: 'active-drop',
        hideOnClickOutside: true,
        opener: '.slideshow-menu-opener',
        slider: '.slideshow-menu-slider',
        animSpeed: 400,
        effect: 'none'
    });

    // jQuery('.slideshow-images-post .slideshow-menu-holder').openClose({
    //     activeClass: 'active-drop',
    //     opener: '.opener',
    //     slider: '.pagination-section',
    //     animSpeed: 400,
    //     effect: 'none'
    // });

    // jQuery('.gallery-popup-heading-bar .gallery-popup-menu').openClose({
    //     activeClass: 'active-drop',
    //     hideOnClickOutside: true,
    //     opener: '.gallery-popup-menu-opener',
    //     slider: '.gallery-popup-menu-slider',
    //     animSpeed: 400,
    //     effect: 'none'
    // });


    jQuery('.slideshow-images-post').fadeGallery({
        slides: '.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination-images li',
        event: 'click',
        useSwipe: true,
        autoRotation: false,
        autoHeight: true,
        switchTime: 3000,
        animSpeed: 500
    });

}
function contentAccordion(x){
    var widget = {};
    if(x==null)
        widget = $('#invite-friends');
    else
        widget = $('#'+x);

    $('.checkbox-gallery', widget).off();

    $('.checkbox-gallery a', widget).off();
    $('.checkbox-gallery .row-group ', widget).off();
    $('.checkbox-gallery .row-group > div ', widget).off();
    console.log('executed');

    $('.checkbox-gallery .row-group', widget).contentTabs({
        addToParent: true,
        tabLinks: 'a'
    });

    initIScroll();
    initAccordion();

    $('.checkbox-gallery', widget).scrollGallery({
        mask: '.mask',
        slider: '.row-group',
        slides: '.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        stretchSlideToMask: false,
        maskAutoSize: false,
        autoRotation: false,
        switchTime: 3000,
        animSpeed: 500,
        step: 1,
        activeClass: 'sactive',
        generatePagination: true
    });

}

function capitalize(str) {
    var splittedEnter = str.split(" ");
    var capitalized;
    var capitalizedResult;
    for (var i = 0 ; i < splittedEnter.length ; i++){
        capitalized = splittedEnter[i].charAt(0).toUpperCase();
        splittedEnter[i] = capitalized + splittedEnter[i].substr(1).toLowerCase();
    }
    return splittedEnter.join(" ");
}

$(function() {
    //Remove default event bindings top search
    $('.search-form').children().off();

//File upload (Index will be changed if user deletes the file before all files are uploaded)
    GeneralUploader();
    //Fix Add friends popup position
    $('body').on('click','.add-people-btn',function(e) {
        var top =$(this).offset().top-165;
        if(($(window).height()-$(this).offset().top) <= 340){
            console.log('Small');
            var clip = 350-($(window).height()-$(this).offset().top);
            top-=clip;
        }
        $('.add-new-friends-popup').css('top',top);
        // console.log(top);
    });


});

function GeneralUploader(){
    $('.upload-form-menu input[type=file]').off();
    $('.upload-form-menu input[type=file]').change(function(event){
    var index = 0;
    var type = event.target.files[0].type.indexOf('video') > -1 ? 'video' : 'image';
    if(type == 'video'){
        index = shared.videos.length == 0 ? 0 : shared.videos.length;
    }else{
        index = shared.images.length == 0 ? 0 : shared.images.length;
    }
    console.log(index);
    $(this).simpleUpload("/uploadimage/", {
            init: function(num) {
                window.shared.finished = false;
            },
			start: function(file){
			    this.index = index;
			    this.type = file.type.indexOf('video');

                if(this.type > -1){
                    window.shared.videos.push({percent : 0,status : 'uploading'});
                }else{
                    window.shared.images.push({percent : 0,status : 'uploading'});
                }
                index +=1;
			},
			progress: function(progress){
                console.log(this.index.toString()+' : ' + progress);
                if(this.type > -1) {
                    window.shared.videos[this.index].percent = progress;
                }else{
                    window.shared.images[this.index].percent = progress;
                }
			},
			success: function(data){
                    console.log(JSON.stringify(data));
                      if(this.type > -1){
                         window.shared.videos.splice(this.index,1,data);
                      }else{
                         window.shared.images.splice(this.index,1,data);
                      }
			},
			error: function(error){
			    console.log(error);
                //Video
                console.log(this.index);
                if(this.type > -1){
                    window.shared.videos.splice(this.index,1,error);
                }else{
                    window.shared.images.splice(this.index,1,error);
                }
			},
            finish: function(){
                window.shared.finished = true;
             }

		});


});
}
function OneTimeQueries() {
    	jQuery('.tab-bar').openClose({
		activeClass: 'tab-edit-active',
		opener: '.edit-btn',
		slider: '.test-block',
		animSpeed: 400,
		effect: 'none'
	});
}

function ElargeTabSet(){
    	// jQuery('.general-post-gallery-popup .contacts-tabset').contentTabs({
	// 	addToParent: true,
	// 	tabLinks: 'a'
	// });

    // $('.general-post-gallery-popup .view-all').on('click',function(event){
    //     $('.general-post-gallery-popup  .contacts-tab-content').addClass('show-all');
    // });

    // initIScroll();
    // $(window).trigger('resize');
}

function  BoxReorder() {
        //Sortable Bar (Top Navigation)
    sortable('#toptab',{
        forcePlaceholderSize: true,
        placeholderClass: 'box-sort'
    });

    //NB: Next Box is part of Root element
    var TopBarEl = [];
    $('#toptab').off();
    $('#toptab').on('sortupdate',function (e) {
        TopBarEl = [];
        $('#toptab').children().each(function(index) {
            TopBarEl.push($(this).attr('data-id'));
        });
        ajaxPost('/api/box/reorder/',{
            order: TopBarEl.toString()
        },function(response) {
            console.log(response);
            if(response.result == 'ok'){
                //Changing Boxinfo Directly from script (App is root object)
                App.$data.boxinfo.next_box = response.next_box;
            }
        });
    });
}

//Tabclick (Underline)
function TabClick() {
    $('.tabs-area ul li a').off();
    $('.tabs-area ul li a').on('click', function (event) {
        event.preventDefault();
        $('.tabs-area ul li').removeClass('active');
        $(this).parent().addClass('active');
        console.log($(this).parent());
    });
}

function initiateLifetimeUploads() {
    $('#create-post0303 input[type=file]:first').off();
    $('#create-post0303 input[type=file]:first').change(function (event) {
        $(this).simpleUpload("/uploadimage/", {
           start: function (file) {
                $('.logo').hide();
               $('.uploading-heading').show();
                this.progressBar = $('<div class="progress-bar-center"></div>');
                $('.header-center-box').append(this.progressBar);
           },
            progress: function(progress) {
                  this.progressBar.width(progress + "%");
            },
           success: function (data) {
               console.log(data);
               this.progressBar.remove();
               $('.logo').show();
               $('.uploading-heading').hide();

               if(data.status == 'Ok'){
                    window.coverphoto.which = 1;
                    window.coverphoto.photo = data.files[0];
               }
               // $('#drag .meter').remove();
               // $('#drag .frame').show();
           }
        });
    });
}
function initiateProfilePicUpload(el,box) {
    $(el).change(function (event) {
        $(this).simpleUpload("/uploadimage/", {
           start: function (file) {
                $('.logo').hide();
               $('.uploading-heading').show();
                this.progressBar = $('<div class="progress-bar-center"></div>');
                $('.header-center-box').append(this.progressBar);
           },
            progress: function(progress) {
                  this.progressBar.width(progress + "%");
            },
           success: function (data) {
               console.log(data);
               this.progressBar.remove();
               $('.logo').show();
               $('.uploading-heading').hide();

               if(data.status == 'Ok'){
                   var file = data.files[0];
                   box.profile_pic.pic = file.url;
                   box.profile_pic.id = file.id;
                    // window.coverphoto.which = 1;
                    // window.coverphoto.photo = data.files[0];
               }
               // $('#drag .meter').remove();
               // $('#drag .frame').show();
           }
        });
    });
}

function initiateFavpicUpload() {
    $('#favpicupload').off();
    $('#favpicupload').change(function (event) {
        $(this).simpleUpload("/uploadimage/", {
           start: function (file) {
                $('.logo').hide();
               $('.uploading-heading').show();
                this.progressBar = $('<div class="progress-bar-center"></div>');
                $('.header-center-box').append(this.progressBar);
           },
            progress: function(progress) {
                  this.progressBar.width(progress + "%");
            },
           success: function (data) {
               console.log(data);
               this.progressBar.remove();
               $('.logo').show();
               $('.uploading-heading').hide();

               if(data.status == 'Ok'){
                   window.upload.file  = data.files[0];
               }
           }
        });
    });
}

function shadeColor2(color, percent) {
    var f=parseInt(color.slice(1),16),t=percent<0?0:255,p=percent<0?percent*-1:percent,R=f>>16,G=f>>8&0x00FF,B=f&0x0000FF;
    return "#"+(0x1000000+(Math.round((t-R)*p)+R)*0x10000+(Math.round((t-G)*p)+G)*0x100+(Math.round((t-B)*p)+B)).toString(16).slice(1);
}

function blendColors(c0, c1, p) {
    var f=parseInt(c0.slice(1),16),t=parseInt(c1.slice(1),16),R1=f>>16,G1=f>>8&0x00FF,B1=f&0x0000FF,R2=t>>16,G2=t>>8&0x00FF,B2=t&0x0000FF;
    return "#"+(0x1000000+(Math.round((R2-R1)*p)+R1)*0x10000+(Math.round((G2-G1)*p)+G1)*0x100+(Math.round((B2-B1)*p)+B1)).toString(16).slice(1);
}


function initiateTravelUploads(){
    $('.popup-resultat-uploaded-images input[type=file]').off();
    $('.popup-resultat-uploaded-images input[type=file]').change(function(event){
    var uploadsection = $(this).parents('.uploaded-section'),
        uploadindex= uploadsection.attr('locationindex');

        $(this).simpleUpload("/uploadimage/", {
			start: function(file){
			     uploadsection.addClass('upload-active');
                 var element = uploadsection.find('.uploaded-images-holder');
                 this.progressBar = $('<div class="meter"><span style="width:0%">' +
                     '</span></div>');
                this.block = $('<div class="uploaded-image"></div>');
                this.block.append(this.progressBar);
                element.append(this.block);
			},
			progress: function(progress){
                $('span',this.progressBar).width(progress + "%");
			},
			success: function(data){
			    console.log(data);
                var self = this;
                this.block.empty();
                if(data.status == 'Ok'){
                    this.block.data('file',data.files[0]);
                    this.block.append('<img src="'+data.files[0].thumbnailUrl+
                        '" height="88" width="69" alt="">');
                    this.block.append('<a href="#" class="close">close</a>');
                    //Removing
                    $('a',this.block).on('click',function(e){
                        e.preventDefault();
                        self.block.remove();
                    });
                }else{
                    this.block.remove();
                }

			},
			error: function(error){
			    console.log(error);
			},
			finish: function() {

            }

		});

});
}

function Notify() {
    const ERROR = false, SUCCESS = true;
    var list = [] ,
        notify = $('#notification-bar'),
        timer = null;

    function showLogo() {
        notify.fadeOut('slow', function () {
            notify.siblings('.logo').animate({
                opacity:1,
            },1000);
        });
    }

    function hideLogo() {
        notify.siblings('.logo').animate({
            opacity: 0.3
        },1000, function () {
            // notify.fadeIn('slow');
        });
    }



    function showMessage(msg, typ) {
        // var prefix = typ ? 'Success! {0}' : 'Error! {0}';
        var prefix = typ ? '{0}' : '{0}';
        typ ? notify.addClass('success').removeClass('error')
            : notify.addClass('error').removeClass('success');
        notify.fadeOut('slow', function () {
            notify.find('span').text(prefix.format(msg));
            notify.fadeIn();
        });
    }

    function messageLoop() {
        if (list.length == 0) {
            showLogo();
            clearInterval(timer);
            timer = null;
        } else {
            showMessage(list[0].msg, list[0].type);
            list.splice(0, 1);
        }
    }

    function render() {
        if (timer == null) {
            messageLoop();
            hideLogo();
            timer = setInterval(messageLoop,6000);
        }
    }

    this.success = function (msg) {
        list.push({type: SUCCESS,msg: msg});
        render();
    };
    this.error = function (msg) {
        list.push({type: ERROR, msg: msg});
        render();
    };
}

var notification = new Notify();
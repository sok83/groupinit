// Custom components and popups with bound data goes here

var ll = console.log.bind(console);


function FriendsPopup(type) {
    var widget = $('#invite-friends');
    if(type)
        var boxname ='';

    function apply_selection(){
        $( "#friends_li",widget).off();
        var data = {
            all : 'partial',
            box : []
        };
        $( "#friends_li",widget).on( "click", function( event ) {
            event.preventDefault();
            if($(".row-group div:first-child input",widget).prop("checked") == true){
                //all
                data.all = true;
            }else{
                var i = 0;
                $('.tab-content input',widget).each(function() {
                    if($(this).prop('checked')==true){
                        i+=1;
                    }
                });
                //no Input
                if(!i){
                    data.all = false;
                }
                //Partial input
                else{
                    var elements = $('.row-group div:not(:first-child)',widget);
                    elements.each(function(){
                        var box = {};
                        var boxname = $(this).find('span').text();
                        var boxid = $(this).find('span').attr('data-id');
                        if($(this).find('input').prop('checked')==true){
                            box = {   name : boxname,id:boxid, groups: "true"};
                        }
                        else{
                            var href= $(this).find('a').attr('href');
                            href = href.substring(href.indexOf("#"),href.length);
                            var panel = $(href,widget).find('.accordion-area').children();
                            var groups = [];
                            panel.each(function(){
                                var groupname = $(this).find('.accordion-title .accordion-btn').text();
                                var grpid = $(this).find('.accordion-title .accordion-btn').attr('data-id');
                                if($(this).find('.accordion-title input').prop('checked')){
                                    groups.push({name: groupname,id: grpid,users: "true"});
                                }else{
                                    var users = $(this).find('li');
                                    var userlist = [];
                                    users.each(function(){
                                        if($(this).find('input').prop('checked') == true){
                                            var user = $(this).find('input').attr('name').split('-');
                                            userlist.push({name: user[0],
                                                id: user[1]
                                            });
                                        }
                                    });
                                    if(userlist.length>0)
                                        groups.push({name: groupname,id:grpid,users: userlist});
                                }
                            });
                            box = { name: boxname, id: boxid, groups: groups};
                        }
                        data.box.push(box);
                    });

                }}
            $("#whocansee").val(encodeURI(JSON.stringify(data)));
            ll(JSON.stringify(data));
            widget.removeClass('lightbox-active');
        });
    }

    function render_friends(friend,grpid) {
        var html = "";
        for (var i = 0; friend && i < friend.length; i++)
            html += FriendsPopup.tpl.friend.format(
                friend[i].profile_pic, friend[i].name, friend[i].id , grpid
            );
        return html;
    }

    function render_groups(group) {
        var html = "";
        for (var i = 0; group && i < group.length; i++)
            html += FriendsPopup.tpl.group.format(
                group[i].id, group[i].name, render_friends(group[i].members,group[i].id)
            );
        return html;
    }

    function checkAll(){
        $('.row-group div.slide', widget).addClass('active');
        widget.find('input').prop('checked', true);

    }

    function counter(){
        function writeCount(){
            function countContent(){
                var count = 0;
                $('.accordion-content li input',widget).each(function(index){
                    if($(this).prop('checked') == true){
                        count +=1;
                    }
                });
                return count;
                }
                var selectedUsers = countContent();
                $('#selectioncount',widget).text(selectedUsers + " people selected");
         }
        return writeCount();
    }

    function selection_logic(){

        if(type == 1){
            $('.checkboxGroup',widget).off();
            $('.checkboxGroup',widget).on('click',function(){
                $('.accordion-panel .accordion-title input').prop('checked',false);
                $(this).find('input').prop('checked',true);
            });
            return;
        }


        // SLIDES
    $(".slide",widget).off();
    $('.slide',widget).each(function() {
        var slide = jQuery(this);
        slide.on('click', function(){
            if( $(this).is(':first-child') == false){
               $(".row-group div:first-child",widget).removeClass('active');
               $(".row-group div:first-child input",widget).prop("checked",false);
               if($(this).hasClass('active')){
                $(this).removeClass('active');
                $(this).find('input').prop('checked', false);
                   var link = $(this).find('a').attr('href');
                   link = link.substr(link.indexOf("#"),link.length);
                $(link,widget).find('input').prop("checked",false);
            }else{
                $(this).addClass('active');
                $(this).find('input').prop('checked', true);
                   var link = $(this).find('a').attr('href');
                   link = link.substr(link.indexOf("#"),link.length);
                $(link,widget).find('input').prop("checked",true);
                if($('.row-group div.active',widget).children().length == $('.row-group div',widget).children().length - 1){
                    checkAll();
                }
            }
        }else{
          if($(this).hasClass('active')){
            $(".row-group div",widget).removeClass('active');
            widget.find('input').prop("checked",false);
            }else{
                checkAll();
            }
        }
        counter();
        });
    });

    //GROUPS
    $('.checkboxGroup',widget).off();
    $('.checkboxGroup',widget).on('click',function(){
        if($(this).find('input').prop('checked') == true){
            $(this).parent().parent().find('input').prop('checked',true);
            $('.checkboxGroup',widget).parent().children('input').prop("checked",true);
            var length = 0;
                //checking selected groups
                $(this).closest('.accordion-area').find('input').each(function(){
                    if($(this).prop('checked') == true){
                        length +=1;
                    }
                });
                if($(this).closest('.accordion-area').find('input').length == length){
                    var href =  $(this).parents('div[id^="checkbox-gallery"]').attr('id');
                    href = $('a[href="#'+href+'"]');
                    href.find("input").prop('checked',true);
                    href.parent().addClass('active');
                    if($('.row-group div.active',widget).children().length == $('.row-group div',widget).children().length - 1){
                        checkAll();
                    }
                }
            }else{
                $(this).parent().parent().find('input').prop('checked',false);
                $('.checkboxGroup',widget).parent().children('input').prop("checked",false);
                var href =  $(this).parents('div[id^="checkbox-gallery"]').attr('id');
                href = $('a[href="#'+href+'"]');
                href.find("input").prop('checked',false);
                href.parent().removeClass('active');
                $(".row-group div:first-child",widget).removeClass('active');
                $(".row-group div:first-child input",widget).prop("checked",false);
            }
            counter();
        });

    //USERS
    $('.checkboxUsers',widget).off();
    $(".checkboxUsers",widget).on('click',function(){
            if($(this).find('input').prop('checked') == true){
                $(this).find('input').prop('checked',false);
                $(this).parents('.accordion-panel').find('input:first').prop('checked',false);
                //Box
                var href =  $(this).parents('div[id^="checkbox-gallery"]').attr('id');
                href = $('a[href="#'+href+'"]');
                href.find("input").prop('checked',false);
                href.parent().removeClass('active');
                $(".row-group div:first-child",widget).removeClass('active');
                $(".row-group div:first-child input",widget).prop("checked",false);
            }else{
                $(this).find('input').prop('checked',true);
                var length = 0;
                $(this).parent().parent().find('input').each(function(){
                    if($(this).prop('checked') == true){
                        length +=1;
                    }
                });
                if($(this).parent().parent().find('input').length == length){
                    $(this).parents('.accordion-panel').find('input:first').prop('checked',true);
                    //BOX
                    length = 0;
                    $(this).closest('.accordion-area').find('input').each(function(){
                        if($(this).prop('checked') == true){
                            length +=1;
                        }
                    });
                    if($(this).closest('.accordion-area').find('input').length == length){
                        var href =  $(this).parents('div[id^="checkbox-gallery"]').attr('id');
                        href = $('a[href="#'+href+'"]');
                        href.find("input").prop('checked',true);
                        href.parent().addClass('active');
                        if($('.row-group div.active',widget).children().length == $('.row-group div',widget).children().length - 1){
                            checkAll();
                        }

                    }

                }
            }
            counter();
        });
    }

    this.binder = function(apply,cancel){
        if(typeof apply == 'function'){
            $("#friends_li",widget).off();
            $("#friends_li",widget).on('click',apply);
        }
        if(typeof cancel == 'function'){
            $('#deskclose',widget).off();
            $('#deskclose',widget).on('click',cancel);
        }
        return this;
    };
    this.update = function (data) {
        if(type)
            boxname = data.box[0].name;
        for (var i = 0; i < data.box.length; i++) {
            var box = data.box[i];
            $('.row-group', widget).append(
                FriendsPopup.tpl.tab.format(
                    i,
                    box.name.toLowerCase(),
                    box.name,box.id
                )
            );
            $('.tab-content', widget).append(
                FriendsPopup.tpl.box.format(
                    i,
                    render_groups(box.groups)
                )
            );
        }
        return this;
    };
    this.show = function () {

         $('.checkbox-gallery .row-group ', widget).off();
         $('.checkbox-gallery .row-group ', widget).children().off();
         $('.checkbox-gallery', widget).off();
         $('.checkbox-gallery a', widget).off();

        $('.checkbox-gallery .row-group', widget).contentTabs({
            addToParent: true,
            tabLinks: 'a'
        });

        selection_logic();
        apply_selection();

        if(type !=1) {

            if(type ==2){
                $('.tab-content #checkbox-gallery-tab01',widget).hide();
                $('.row-group div:first', widget).hide();
            }else{
                $('.tab-content #checkbox-gallery-tab01',widget).show();
                $('.row-group div:first', widget).show();
                 checkAll();
            }


            initIScroll();
            initAccordion();
            $('.checkbox-gallery', widget).scrollGallery({
            mask: '.mask',
            slider: '.row-group',
            slides: '.slide',
            btnPrev: 'a.btn-prev',
            btnNext: 'a.btn-next',
            pagerLinks: '.pagination li',
            stretchSlideToMask: false,
            maskAutoSize: false,
            autoRotation: false,
            switchTime: 3000,
            animSpeed: 500,
            step: 1,
            activeClass: 'sactive',
            generatePagination: true
        });
            $('.title-pop-up', widget).text('Invite Friends');
            $('#selectioncount',widget).show();
            $('.checkbox-gallery',widget).show();
            $('.accordion-content',widget).show();
        }
        else{
            $('.checkbox-gallery',widget).hide();
            $('.tab-content #checkbox-gallery-tab01',widget).hide();
            $('#selectioncount',widget).hide();
            $('.accordion-content',widget).hide();

            $('.row-group div input', widget).attr('readonly','true');

            $('.title-pop-up',widget).text(boxname);
            $('a[href="#checkbox-gallery-tab0"]',widget).trigger('click');
            $('a[href="#checkbox-gallery-tab0"]',widget).off();
        }
        counter();
        widget.addClass('lightbox-active').show();
        return this;
    };
    this.clear = function(){
        $('.row-group div.slide:not(:first)',widget).remove();
        $('.tab-content div:not(:first)',widget).remove();
        widget.removeClass('lightbox-active');
        return true;
    };
    return this;
}

FriendsPopup.tpl = {
    'tab': '<div class="slide"><a href="#checkbox-gallery-tab{0}">\
             <label for="checkboxPopup{0}" class="checkboxPopup">\
             <input type="checkbox" id="checkboxPopup{0}" name="{1}"><span data-id="{3}">{2}</span>\
             </label></a></div>',
    'box': '<div id="checkbox-gallery-tab{0}" > <div class="scroll-area checkbox-gallery-wrapper">\
            <div class="accordion-area">{1}</div> </div> </div>',
    'group': '<div class="accordion-panel bg-{0}"><div class="accordion-title">\
            <span class="accordion-btn" data-id="{0}">{1}</span>\
            <label for="{1}-{0}" class="checkboxPopup checkboxGroup">\
            <input type="checkbox" id="{1}-{0}" name="{1}-{0}">\
            <span>&nbsp;</span> </label><a href="#" class="opener-accordion-panel">opener</a>\
            </div><div class="accordion-content scroll-area">\
            <ul class="scrolling-area">{2}</ul></div></div>',
    'friend': '<li class="small-row"><a href="#"><img src="{0}" alt="Picture">\
            <span>{1}</span></a>  <label for="{1}-{2}-{3}" class="checkboxPopup checkboxUsers">\
			<input type="checkbox" id="{1}-{2}-{3}" name="{1}-{2}"><span>&nbsp;</span>\
		    </label></li>'
};

// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}
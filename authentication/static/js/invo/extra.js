/**
 * Created by bijoy on 26/7/16.
 */

//Post comment //Not completed
$('#main-content').on('click','.send-comment',function(){
    var id = $(this).attr('id').split('-')[1],
    comment = $('#commentbox-'+id).val();
    var data = {
        postid:id,
        comment:comment,
        csrfmiddlewaretoken: $('#csrf_token').val()
    };
    var callback = function(respondse) {
    };
    ajaxPost('/post_comments/',data,callback);
});

//Like
function UpdateLike(key,data,section){
    console.log(data);
    key = key.replace('_','-');
    var selector = $('a[data-type='+key+']',section),
        dropbox = selector.next().find('ul'),
        userTemplate ='<li><a id="like_people" data-type="like" data-post="17">\
        <strong><img src="{0}" alt="Picture" height="45" width="45"></strong></a></li>',
        blankProf='/static/images/pictures/avatar.jpg';
    //updating count
    selector.find('.number').text(data.length);
    //Dropbox
    var userlist = '';
    dropbox.empty();
    $.each(data,function (index,user) {
        userlist +=userTemplate.format(user.profile_pic == null?
            blankProf: user.profile_pic);
        if(index > 3 ){
            // new LikeViewer(data).update();
            userlist+='<li><a class="lightbox" href="#peopleliked">\
                       <span class="number-persons">'+data.length+
                       '</span></a></li>';
            return false;
        }
    });
    dropbox.append(userlist);
}

$('#main-content').on('click','#like-box',function(event){
  event.preventDefault();
  var target = $(this),postid = $(this).attr('data-post'),
      liketype = $(this).attr('data-type'),
      section = $(this).closest('.like-section');
  if(!target.parent().hasClass('active')){
    var dataobj = {
        url : "/post_like/",
        data : {postid:postid,
                type:liketype,
                csrfmiddlewaretoken: $('#csrf_token').val()
                },
        callback :  function(response){
            $.each(response,function(key,value){
                if($.isArray(value)){
                    UpdateLike(key,value,section);
                }
            });
            section.children('.like-box').removeClass('active');
            target.parent().addClass('active');
        }
    };
    ajaxPost(dataobj);
  }
  return true;
});

//Group id injection to add-friends popup
$('.left-sidebar .new-friends-popup-opener').on('click',function() {
    //Second Handler
    var GrpID = $(this).attr('id');
    GrpID =  GrpID.substr(GrpID.indexOf('&')+1,GrpID.length);
    $('#add-new-friends-popup').attr('data-gid',GrpID);
});

//New Request Group Accordion
var NewRequest = {
    el : $('#accor_newrequest'),
    li: '<li id={0}><div class="left-cell"><a href="#" class="active wrap-img"><img alt="Picture" src={1}></a></div><div class="text-box"><h3><a href=#>{2}</a></h3><p>{3}</div>{4}</li>',
    type : '<div class="right-cell"><a href="#" class="{0}">{1}</a></div>',
    request_count: 0,
    getCount : function() {
        return this.request_count;
    },
    setCount : function(count) {
        this.request_count = count;
        $('#membcount_id', this.el).text(this.request_count);
        return this;
    },
    invitelist: function() {
        var callback = function (response) {
            NewRequest.request_count = response.requests.length;
            var html = '';
            if (response.requests.length > 0)
                $.each(response.requests, function (key, user) {
                    if (typeof user != 'undefined') {
                        var dp = user.profile_pic == null ? '/static/images/pictures/avatar.jpg'
                            : user.profile_pic,
                            about = user.about == null ? 'I am new to Groupinit' : user.about;
                        var rightcell = '';
                        switch (user.status) {
                            case 'Already sent' :
                                rightcell = NewRequest.type.format('btn-add btn-add-hub', 'Add');
                                break;
                            case 'Recieved':
                                rightcell = NewRequest.type.format('btn-add btn-add-hub', 'Accept');
                                break;
                            case 'Added to hub':
                                rightcell = NewRequest.type.format('btn-add btn-assign-group btn-assign', 'Assign Group');
                                break;
                            default:
                        }
                        html += NewRequest.li.format(user.request_id, dp, user.first_name, about, rightcell);
                    }
                });
            $('.accordion-content ul', NewRequest.el).empty();
            $('.accordion-content ul', NewRequest.el).append(html);
            NewRequest.setCount(response.requests.length);
        };
        ajaxGet('/api/request/view/user/', callback);
    },
    addtobox: function(el) {
        var callback = function(response) {
            if(response.status == 'Accepted'){
                el.closest('li').fadeOut('slow',function(){
                    $(this).remove();
                    NewRequest.setCount(NewRequest.getCount() - 1);
                });
                return;
            }
            el.removeClass('btn-add-hub').addClass('btn-assign btn-assign-group')
                .text('Assign Group');
        };
        var data ={
            request_id: el.closest('li').attr('id')
        };
        ajaxPost('/api/request/add/box/',data,callback);
    },
    assigngroup:function (el){
        ajaxGet('/api/box/view/',function(response) {
            var data= {
                box :[]
            };
            data.box.push(response.box);
            delete data.box.members;
            //Handler of apply (Group Selection popup)
            var post ={request_id : el.closest('li').attr('id')};
            var applyhandler =function(event) {
                event.preventDefault();
                post.group_id = $(this).closest('form')
                    .serializeArray()[0].name.split('-')[1];
                ajaxPost('/api/request/add/group/',post,function() {
                    el.closest('li').fadeOut('slow',function(){
                        $(this).remove();
                        NewRequest.setCount(NewRequest.getCount() - 1);
                    });
                    $("#deskclose").trigger('click');
                });
            };
            new FriendsPopup(1).update(data).show().
            binder(applyhandler);
        });
    },
    bindEvents: function(){
        $('h3',this.el).on('click',function(){
            NewRequest.invitelist();
        });
        $('.accordion-content',this.el).on('click','.btn-add-hub',function() {
            var scope = $(this);
            NewRequest.addtobox(scope);
        });
        $('.accordion-content',this.el).on('click','.btn-assign-group',function() {
            var scope = $(this);
            NewRequest.assigngroup(scope);
        });
    }
};



var FriendsList = {
    el:$('.left-sidebar #group_accordion > li:not(:first-child)'),
    li: '<li id={0}><div class="left-cell"><a href="#" class="active wrap-img"><img alt="Picture" src={1}></a></div><div class="text-box"><h3><a href=#>{2}</a></h3><p>{3}</div><div class=right-cell><a class="active initedMessagesOpeners notifications"href=#individual-msg-block></a></div></li>',
    show: function() {
        console.log(FriendsList.el);
    },
    getFriends: function(el) {
        var grpid = el.attr('data-group'),
            url = '/api/friends/view/group/?group_id='+grpid ;
        console.log(grpid,url);
        ajaxGet(url ,function(response) {
            var html = '';
            if (response.friends.length > 0)
            $.each(response.friends,function(index,friend) {
                if(typeof friend != 'undefined'){
                    var dp = friend.profile_pic == null ? '/static/images/pictures/avatar.jpg'
                        : friend.profile_pic,
                        about = friend.about == null ? 'I am new to Groupinit' : friend.about;
                    html += FriendsList.li.format(friend.user, dp, friend.first_name, about);
                    //Appending to accordion
                    $('ul',el.next()).empty();
                    $('ul',el.next()).append(html);
                }
            });

        });
    },
    bindEvents: function() {
        $('h3',FriendsList.el).on('click',function() {
            var scope = $(this);
            FriendsList.getFriends(scope);
        });
    }
};


var GroupSettings = {
    action: {
        edit: $('.left-sidebar .accordion-title-tow-edit'),
        delete: $('.left-sidebar .accordion-title-tow-delete')
    },
    bindEvents : function() {

    }
};


    //Personal Accordion Setup
    NewRequest.bindEvents();
    FriendsList.bindEvents();



function SearchBox(element) {

    var widget = element==null ? $('#global-search') : $(element);
    console.log(widget);
    function iterateOver(data,key) {
        var html='';
        $.each(data,function(index,user){
            var profpic = user.profile_details.profile_pic == null?
                '/static/images/pictures/avatar.jpg':
                user.profile_details.profile_pic;
            // if(key == 'people' || key == 'friends')
                if(user != null )
                html+= SearchBox.Display.People.format(profpic,user.name,user.user_id);
        });
        switch(key){
            case 'poeple':
                html = SearchBox.Display.Friends.format(html);
                break;
            case 'groups':
                html = SearchBox.Display.Groups.format(html);
                break;
            default:
                html = SearchBox.Display.Friends.format(html);
            }
        return html;
    }

    this.update = function(data) {
        var html='';
            $.each(data,function(key,value){
                if($.isArray(value) && value.length > 0){
                    html+=iterateOver(value,key);
                }
            });
        console.log(html);
        $('#seach-results-container',widget).empty();
        $('#seach-results-container',widget).append(html);
        return this;
    };
    this.show = function(){
        $('#seach-results-container',widget).show();
    };
}

SearchBox.Display = {
    People : '<li> <div class="image-box"> <div class="img-holder">' +
             '<img src="{0}"></div></div>' +
             '<div class="text-holder"><div class="frame"><div class="holder">' +
             '<h3>{1}</h3><p>Invite this user to your friends list</p></div>' +
             '</div></div><div class="overlay"></div>' +
             '<a href="#" class="btn-add" id="useradd_{2}">Invite</a></li>',
    Friends : '<article class="seach-results-block" id="seach-results-block-friends" >' +
             '<div class="heading"><strong class="title">Friends</strong> <a class="link" href="#">Friends Only</a> ' +
             '</div><ul class="seach-results-list">{0}</ul></article>',
    Groups  : '<article class="seach-results-block" id="seach-results-block-groups" > ' +
             '<div class="heading"> <strong class="title">Groups</strong> <a class="link" href="#">Groups Only</a> ' +
             '</div><ul class="seach-results-list">{0}</ul></article>'
};


$(function(){

  $('#globsearch').keyup(function() {
      var SearchString = $(this).val(),
          url = "/search_people/?search_key="+SearchString;
      var callback = function(response){
          console.log(response);
          var x = new SearchBox();
          x.update(response);
          x.show();


      };
      ajaxGet(url,callback);
      }
  );

});



/**
 * Created by bijoy on 26/7/16.
 */
var XHRData =    [
      {
         "profile_pic":'/static/images/pictures/man-1.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"The ONE",
         "contact_no":null,
         "user":1,
         "first_name":"bijoy",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-2.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"Python flows in my blood stream",
         "contact_no":null,
         "user":2,
         "first_name":"Prasoon",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },

      {
         "profile_pic":'/static/images/pictures/man-3.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"You never know my full potential",
         "contact_no":null,
         "user":3,
         "first_name":"Renjith",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-4.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"World revolves arround me",
         "contact_no":null,
         "user":4,
         "first_name":"Divya",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-5.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"She is more than just meets the eye",
         "contact_no":null,
         "user":5,
         "first_name":"Sariga",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-6.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"I am king of the universe",
         "contact_no":null,
         "user":6,
         "first_name":"Nahar",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-7.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"Blunder is my life",
         "contact_no":null,
         "user":7,
         "first_name":"Shakthiman",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      },
      {
         "profile_pic":'/static/images/pictures/man-8.jpg',
         "last_name":null,
         "joined_date":"14/07/2016",
         "about":"World is as is it ",
         "contact_no":null,
         "user":8,
         "first_name":"Suhad",
         "gender":"Male",
         "place":null,
         "birth_date":"01/01/1990",
         "email":null
      }]

;
// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
                ;
        });
    };
}

function LikeViewer(data){
    var widget = $('#peopleliked');
    function generateMarkup(){
        var list = '<li><div class=image-box>' +
        '<div class=img-holder>' +
        '<img src="{0}">' +
        '</div></div>' +
        '<div class=text-holder><div class=frame>' +
        '<div class=holder><h3><a href=#>{1}</a>' +
        '</h3><p>{2}</p></div></div></div></li>';
    var html = "";
    $.each(data,function(index,v){
         html += list.format(v.profile_pic,v.first_name,v.about);
    });
        return html;
    }


    this.update = function(){
        widget.find('.list-people-base').append(generateMarkup());
    };

}

new LikeViewer(XHRData).update();



Vue.config.delimiters = ['{!', '!}'];
// alertify.logPosition("bottom right");
//Directives to apply jquery filters
Vue.directive('slideshow', {
  bind:function() {
    $(this.el).fadeGallery({
                slides: '.slide',
                btnPrev: 'button.slide-prev',
                btnNext: 'button.slide-next',
                pagerLinks: '.pagination-photos li',
                event: 'click',
                useSwipe: true,
                autoRotation: false,
                autoHeight: true,
                switchTime: 3000,
                animSpeed: 500
            });
  }
});

Vue.directive('comments', {
  update:function() {
      initCarousel();
  }
});

Vue.directive('openclose', {
  bind:function() {
	$(this.el).openClose({
		hideOnClickOutside: true,
		activeClass: 'active-post-menu',
		opener: '.opener-post-menu',
		slider: '.drop-post-menu',
		animSpeed: 400,
		effect: 'none'
	});
  }
});
Vue.directive('textmore', {
  bind:function() {
	$(this.el).openClose({
		activeClass: 'active-text-container',
		opener: '.more-link',
		slider: '.slide-text-holder',
		animSpeed: 400,
		effect: 'none'
	});
  }
});


Vue.directive('tagopener', {
  bind:function() {
	jQuery(this.el).openClose({
		hideOnClickOutside: true,
		activeClass: 'active-add-menu',
		opener: '.opener-add-menu',
		slider: '.slide-add-menu',
		animSpeed: 400,
		effect: 'none'
	});
  }
});

Vue.directive('threedot', {
  update:function(color) {
    jQuery(this.el).openClose({
        hideOnClickOutside: true,
        activeClass: 'active-tow-menu-drop',
        opener: '.accordion-title-tow-menu',
        slider: '.accordion-title-tow-menu-drop',
        animSpeed: 400,
        effect: 'none'
    });


    jQuery(this.el).find('.eye-btn').hover(function() {
        $(this).css('background',shadeColor2(color,.3));
    },function() {
        $(this).removeAttr('style');
    }) ;
  }
});

Vue.directive('upload', {
  update:function(box) {
    initiateProfilePicUpload($(this.el),box);
  }
});

Vue.directive('readmore', {
  bind:function() {
      	jQuery('.text-container',$(this.el)).openClose({
		activeClass: 'active-text-container',
		opener: '.more-link',
		slider: '.slide-text-holder',
		animSpeed: 400,
		effect: 'none'
	});
  }
});
Vue.directive('comment', {
  bind:function() {
    //   var scope  = $(this.el);
    // var allComments = jQuery('.js-comments__btn_all',scope),
    //         tabSet = jQuery('.custom-comments-tabs',scope),
    //         groupSelect = jQuery('.custom-comments-tabs-drop',scope),
    //         groupComments = jQuery('.js-comments__btn_group',scope),
    //         groupBlock = jQuery('.contacts-tab-content >.contacts__tab_content',scope),
    //         groupHeader = groupBlock.find('.contacts-tab-heading > .contacts-count-friends',scope);
    //
    //     allComments.click(function(event) {
    //         tabSet.removeClass('active');
    //         groupSelect.addClass('js-slide-hidden').hide();
    //         groupBlock.addClass('js-tab-show');
    //         $(this).addClass('is-red-bg');
    //         groupComments.removeClass('is-red-bg');
    //         // groupHeader.hide();
    //         event.preventDefault();
    //     });
    //     groupComments.click(function(event) {
    //         event.preventDefault();
    //         groupBlock.removeClass('js-tab-show');
    //         $(this).toggleClass('is-red-bg');
    //         allComments.removeClass('is-red-bg');
    //         groupHeader.show();
    //     });

  }
});
Vue.directive('customcomments', {
  bind:function() {
    // jQuery(this.el).openClose({
    //     activeClass: 'active',
    //     opener: '.opener',
    //     slider: '.custom-comments-tabs-drop',
    //     animSpeed: 400,
    //     effect: 'none'
    // });
  }
});

var TravelImage = new Vue({
    el : '#popup-resultat-uploaded-images',
    data : {
        location: locationpoints,
        show: false
    },
    methods: {
        Apply: function(){
            this.show = false;
        },
        HidePopup: function () {
            this.show = false;
            // $(this.$el).find('.uploaded-images-holder').empty();
        }
    },
    watch: {
        'location': {
           handler: function() {
            var self = this;
            initiateTravelUploads();
            console.log('Test');
        },
            deep: true
        }
    }

});

var TravelVideos = new Vue({
    el : '#popup-resultat-uploaded-videos',
    data : {
        location: window.locationpoints,
        show: false
    },
    methods:{
        Apply: function(){
            this.show = false;
        },
        HidePopup: function(){
            // $(this.$el).find('.uploaded-images-holder').empty();
            this.show = false;
        }
    },
    watch: {
        'location': {
           handler: function() {
            var self = this;
            initiateTravelUploads();
            console.log('Test');
        },
            deep: true
        }
    }

});

var PostEnlarge = new Vue({
   el: '#post-enlargement' ,
   data: window.postextend,
    ready:function(){
        $(window).trigger('resize');
    },
    events: {
        'shareaudienceup' : function(x) {
            var self = this;
            self.audience = x;
            self.audiencepopup = false;
        },
        'shareaudiencecancel': function() {
            var self = this;
            self.audiencepopup = false;
        }
    },
   methods: {
       ShowConfirmation:function(){
         this.showdelete = true;
       },
       HideDeletePopup: function() {
           var self = this;
           self.showdelete = false;
       },
       DeleteConfirm: function(postid){
           var self = this;
           ajaxGet('/delete_posts/?postid='+postid,function (response) {
               if(response.result == 'ok'){
                   self.HideDeletePopup();
                   App.$data.boxinfo.posts.splice(self.postindex,1);
                   initScrollToFixed();
                   self.show = false;
                   notification.success(response.msg);
                   $(window).trigger('resize');
               }else{
                   notification.error(response.msg);
               }
           });
       },
       ShareThePost : function() {
           var self = this;
           if(typeof self.og_post != 'undefined')
               self.post = self.og_post;
           ajaxPost('/post_resharing/',{
               post_id:self.post.id,countbox:0,countgroup:0,countmemb:0
           },function(response) {
               console.log(response);
               if(response.result == 'ok'){
                   self.shareapost = false;
                   this.audience = [];
                   self.$dispatch('generalpostcreated',response.post_reshare);
               }
           });

       },
       OpenPagination: function () {
           this.hiddenPagination = false;
       },
       HidePagination: function(){
           this.hiddenPagination = true;
       },
       HideExtendedPost: function() {
           this.show = false;
       },
       PublicComments: function(){
           var self = this;
           self.commentreadonly = true;
           $.each(self.post.groups,function (index,value) {
               if(value.id== 0){
                   self.groups = value;
                   return false;
               }
           });
       },
       SelectGroup: function(grp){
            this.groups = grp;
            this.commentreadonly = false;
       },
        SendComment : function(event) {
            var self = this;
            event.preventDefault();
            console.log(self.typecomment);
            if(self.typecomment !=''){
                console.log('not null');
                ajaxPost('/post_comments/',{
                    postid : self.post.id,
                    group_id : self.groups.id,
                    comment : self.typecomment
                },function(response){
                    console.log(response);
                    self.typecomment = '';
                    response.comment.reply = [];
                    //Pushing to the comment stack
                    self.groups.comments.push(response.comment);
                    self.$nextTick(function(){
                       $(window).trigger('resize');
                       self.scrollDown();
                    });
                    // console.log(JSON.stringify(self.post.groups[self.groupindex].comments));
                });
            }
        },
       OpenLikeBox: function(likes) {
            var self = this;
            self.likesarray = likes;
            self.showlikes= true;
        },
       likePost : function(type) {
            var self = this;
            ajaxPost('/post_like/',{
                postid:self.post.id,
                type:type,
                csrfmiddlewaretoken: $('#csrf_token').val()
            },function(response){
                if(response.result == "ok"){
                    self.post.like_plus = response.like_plus;
                    self.post.likes = response.like;
                    self.post.like_minus = response.like_minus;
                    if(type == 'like-plus')
                        self.post.like_status = 'like_plus';
                    else if(type == 'like')
                        self.post.like_status = 'like';
                    else if(type == 'like-minus')
                        self.post.like_status = 'like_minus';

                }
            })
        },
       LikePlus: function(event) {
            var self = this;
            event.preventDefault();
            self.likePost('like-plus');
        },
        LikeMinus: function(event) {
          var self = this;
            event.preventDefault();
            self.likePost('like-minus');
        },
        LikeNormal: function(event) {
            var self = this;
            event.preventDefault();
            self.likePost('like');
        },
       scrollDown : function() {
           var el = $('#postenlarge-comment').data('IScroll');
           el.refresh();
           el.scrollTo(0, el.maxScrollY, 0);
       },
       HideSharePost : function() {
           var self = this;
           self.shareapost = false;
       },
       showSharePost: function() {
           var self = this;
           self.shareapost = true;
       },
       AudiencePopup: function(event) {
           var self =this;
           event.preventDefault();
           self.audiencepopup = true;
           console.log('clicked');
       }
   },
    computed: {
        totalcomments: function() {
            var self = this;
            var total = 0;
            $.each(self.post.groups,function(index,value) {
                total+= value.comments.length;
            });
            return total;
        }
    },
    watch:{
        'show': function(){
            if(this.show){
                // this.groups = this.post.groups;
                this.PublicComments();
                this.$nextTick(function(){
                    // ElargeTabSet();
                    ExtendedPost();
                    initCarousel();
                    initCustomForms();
                    customScroll('#postenlarge-comment');
                });

                $('html, body').css({
                    overflow: 'hidden',
                    height: '100%'
                });
            }else{
                $('html, body').removeAttr('style');
                this.$dispatch('updatebox');
            }
        },
        'hiddenPagination': function() {
            if(!this.hiddenPagination){
                this.$nextTick(function() {
                    $(window).trigger('resize');
                });
            }
        },
        'audiencepopup': function() {
            var self = this;
            if(self.audiencepopup == true){
                this.$broadcast('shareaud', self.audience);
            }
        }
    }
});



var App = new Vue({
    el: '#main-bar',
    data: {
        show: true,
        boxinfo: {},
        showprofile: false,
        mapdata: [],
        searchimages: window.searchimages,
        boxid: null,
        likes:[],
        DeletedBox: [],
        edit: false,
        showboxedit: false,
        boxcolor: '',
        scrollload: false,
        showdelete: false,
        deleteboxindex: 0,
        discussion: false  //Discussion or Normal area in this case
    },
    events: {
        'homelocation': function(home){
            this.$broadcast('homelocation',home);
        },
        'showprofile': function (show) {
            var self = this;
            self.showprofile = show;
        },
        'clearAudience':function() {
           this.$broadcast('clearAudience');
        },
        'editpost': function (post,index) {
            var self = this;
            $(window).off('scroll');
            window.scrollTo(0, 0);
            setTimeout(function () {
                self.scrollDown();
            },500);
            self.$broadcast('editpost',post,index);
        },
        'updatebox': function () {
            this.updateApp();
        },
        'showcreatearea': function (state) {
            var self = this;
            self.$broadcast('showcreatearea', state);
        },
        'changebox': function(id) {
            var self = this;
            if(typeof id == 'undefined')
                return this.ChangeBox(self.boxinfo.current_box.id);
            else
                return this.ChangeBox(id);
        },
        'loadmediaposts': function() {
            var self = this;
            self.loadmediaposts();
        },
        'loadlifeposts': function() {
            var self = this;
            self.loadlifetimeposts();
        },
        'loadmapposts': function() {
            var self = this;
            self.loadmapposts();
        },
        'loadgroupposts': function (grpid,index) {
            var self = this;
            self.loadgroupposts(grpid,index);
        },
        'loaduserposts': function(uid,gid) {
            var self = this;
            self.loaduserposts(uid,gid);
        },
        'boxadded': function(box) {
            var self = this;
            self.boxinfo.boxes.push(box);
            setTimeout(function() {
                sortable('#toptab');
            },300);

        },
        'boxedit': function (box,index) {
            var self = this;
            self.boxinfo.boxes.splice(index,1,box);
            self.updateApp();
        }
    },
    methods:{
        loadmediaposts: function() {
            var self = this;
            window.post = {type: 'media'};
            $('.loader-container').addClass('lightbox-private');
              ajaxGet('/api/box/load/?post_type=media',function(response) {
                 self.boxinfo = response;
            });
            console.log(window.post);
        },
        loadlifetimeposts: function() {
            var self = this;
            window.post = {type: 'lifetime'};
            $('.loader-container').addClass('lightbox-private');
              ajaxGet('/api/box/load/?post_type=lifetime',function(response) {
                 self.boxinfo = response;
            });
            console.log(window.post);
        },
        loadmapposts: function() {
            var self = this;
            window.post = { type: 'travel'};
            $('.loader-container').addClass('lightbox-private');
              ajaxGet('/api/box/load/?post_type=travel',function(response) {
                 self.boxinfo = response;
            });
            console.log(window.post);
        },
        loadgroupposts: function(grpid,index) {
            var self = this;
            window.post = {
                type: 'group',
                grpid: grpid
            };
            $('.loader-container').addClass('lightbox-private');
              ajaxGet('/api/box/load/?group_id='+grpid,function(response) {
                 response.groups[index]['clicked'] = true;
                 self.boxinfo = response;
            });
            console.log(window.post);
        },
        loaduserposts: function (uid,gid) {
            var self = this;
            window.post = {
                type: 'user',
                grpid: gid,
                usrid: uid
            };
            $('.loader-container').addClass('lightbox-private');
              ajaxGet('/api/box/load/?group_id='+gid+'&friend_id='+uid,function(response) {
                 self.boxinfo = response;
            });
            console.log(window.post);
        },
        ButtonOpener:function() {
          notification.success('GroupInit');
        },
        ShowProfile: function() {
            var self = this;
            self.showprofile = !self.showprofile;
            // self.$dispatch('showprofile',self.showprofile);
        },
        ShowBoxEdit: function() {
            var self = this;
            self.showboxedit = true;
        },
        SelectImage: function(img) {
            var self = this;
            if(self.searchimages.type == 'video'){
                window.media.video=img;
            }else {
                window.media.audio = img;
            }
            console.log(window.media);
             self.searchimages.images = [];
             self.searchimages.show = false;
        },
        CancelMediaSelection: function() {
            var self= this;
            self.searchimages.show = false;
            self.searchimages.images = [];

        },
        RemoveImage: function(index) {
            var self = this;
            self.searchimages.images.splice(index,1);
        },
        TriggerEdit: function() {
          var self = this;
            self.edit = true;
            BoxReorder();
        },
        TriggerDone: function() {
          var self = this;
            self.edit = false;
            sortable('#toptab', 'destroy');
        },
        ChangeBox: function (id,box,index) {
            var self= this;
            //Setting post type
            window.post = {type: 'box'};
            if(self.edit && typeof index != 'undefined'){
                console.log(box);
                box.index = index;
                self.$broadcast('editbox',box);
                return;
            }
                if(self.scrollload){
                    // $('#content').addClass('ajax-load');
                    $('.loader-container').addClass('lightbox-private');
                }else{
                    $('.loader-container').addClass('lightbox-private');
                }


            // $('.tabs-area ul li').removeClass('active');
            // $('#box_'+id).addClass('active');
            self.$broadcast('showcreatearea',false);
            self.boxid = id;
            // sortable('#toptab');
            console.log('clicked');
              ajaxGet('/api/box/load/?box_id='+id,function(response) {
                 self.discussion = response.current_box.type == "discussion";
                 self.boxinfo = response;
                  // $('#content').removeClass('ajax-load');
                  window.scrollTo(0, 0);
            });
        },
        CloseDeletePopup: function() {
          var self = this;
            self.showdelete = false;
        },
        ConfirmBoxDelete: function() {
            var self = this;
            self.DeletedBox.push(self.boxinfo.boxes[self.deleteboxindex]);
            ajaxPost('/api/box/delete/',{
                box_id: self.boxinfo.boxes[self.deleteboxindex].id
            },function(response) {
                if(response.result == 'ok'){
                    if(self.boxinfo.boxes[self.deleteboxindex].id == self.boxinfo.current_box.id){
                        self.ChangeBox(self.boxinfo.next_box.id);
                    }else{
                        self.boxinfo.boxes.splice(self.deleteboxindex,1);
                    }
                    self.boxinfo.boxes.splice(self.deleteboxindex,1);
                    notification.success(response.msg);
                }else{
                    notification.error(response.msg);
                }
            });
             self.showdelete = false;
        },
        RemoveBox: function(index) {
            var self = this;
            self.deleteboxindex = index;
            self.showdelete = true;
        },
      updateApp: function() {
          var self = this;
          ajaxGet('/api/box/load/', function (response) {
              self.discussion = response.current_box.type == "discussion";
              self.boxinfo = response;
        });
      },
        SettingsMenuOpener : function() {
            jQuery('#content .accordion-panel').openClose({
                hideOnClickOutside: true,
                activeClass: 'active-tow-menu-drop',
                opener: '.accordion-title-tow-menu',
                slider: '.accordion-title-tow-menu-drop',
                animSpeed: 400,
                effect: 'none'
            });
        },
        scrollDown: function() {
            var self = this;
            $(window).scroll(function() {
               if($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                    window.scrollTo(0, 0);
                   self.scrollload = true;
                   self.ChangeBox(self.boxinfo.next_box.id);
               }else if($(window).scrollTop() == 0){
                   var boxid = 0;
                   $.each(self.boxinfo.boxes,function (index,box) {
                        if(self.boxinfo.current_box.id == box.id){
                            if (index == 0) {
                                boxid = self.boxinfo.boxes[self.boxinfo.boxes.length - 1].id;
                            } else {
                                boxid = self.boxinfo.boxes[index - 1].id;
                            }
                            return ;
                        }
                   });
                   self.scrollload = true;
                   self.ChangeBox(boxid);
               }
            });
        }

    },
    created: function(){
        var self = this;
        self.updateApp();
    },
    watch: {
        'discussion': function() {
            this.$nextTick(function() {
                GeneralUploader();
            });
            if(this.discussion){
                this.$nextTick(function () {
                    $(window).trigger('resize');
                });
            }
        },
        'boxinfo': function() {
            var self = this;
            console.log('changed data');
            this.boxes = this.boxinfo.boxes;
                    initTabs();
                    initItemsZindex();
                    initSameHeight();
                    initPersonalGroup();
                    initStretchHeight();
                    initScrollToFixed();
                    setTimeout(function() {
                         // BoxReorder();
                        initSameHeight();
                        initStretchHeight();
                        initScrollToFixed();
                    },500);
            //Underline Trick (for fzirst loading)
            if(self.boxid == null) {
                self.boxid = self.boxinfo.boxes[0].id;
            }

            $('#toptab').css('display','block');
            $('#bottom-indicator').css('display','block');
            //Removing loader
            $('.loader-container').removeClass('lightbox-active');
            $('.loader-container').removeClass('lightbox-private');
            if($('body').hasClass('group-setting-editing-active')){
                $('.group-setting-editing-drop .btn').trigger('click');
            }
        },
        'searchimages':{
            handler:function () {
                var self = this;
                self.$nextTick(function() {
                    $(window).trigger('resize');
                });
            },
            deep:true
        },

    },
    ready: function() {
        OneTimeQueries();
        window.scrollTo(0, 0);
        // this.scrollDown();

    }
});

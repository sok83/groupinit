var Lat = -34.347;
var Lng = 150.644;
var Zoom = 11;
var people = [
	{	"name": 'Simon McCoy',
		"link": "#",
		"icon" : "./public/images/icons/map-1.png",
		"style" : "right gray",
		"location" : {
			"lat" : -34.327,
			"lng" : 150.618
	}},
	{	"name": 'Jazz Fry',
		"link": "#",
		"icon" : "./public/images/icons/map-2.png",
		"style" : "left orange",
		"location":{
			"lat" : -34.347,
			"lng" : 150.634
	}},
	{	"name": 'Sam Gann',
		"link": "#",
		"icon": "./public/images/icons/map-3.png",
		"style" : "right gray",
		"location":{
			"lat" : -34.367,
			"lng" : 150.664
	}},
	{	"name": 'George Alanberger',
		"link": "#",
		"icon" : "./public/images/icons/map-4.png",
		"style" : "right gray",
		"location" : {
			"lat" : -34.387,
			"lng" : 150.614
	}},
];

var C = '';

groupinit.controller('MapCtrl', function ($scope, $timeout) {
	$scope.title = null;
	$scope.lat = Lat;
	$scope.lon = Lng;
	$scope.country_selection = false;
	$scope.zoomChangeRemarkerTimeout = null;
	$scope.positionChangeRemarkerTimeout = null;

	$scope.map = new google.maps.Map(document.getElementById('map'), {
		zoom: Zoom,
		center: new google.maps.LatLng($scope.lat, $scope.lon),
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		disableDefaultUI: true,
		zoomControl: false,
		zoomControlOptions: {
			style: google.maps.ZoomControlStyle.LARGE,
			position: google.maps.ControlPosition.RIGHT_CENTER
		}
	});
	$scope.center_marker = null;
	$scope.geocoder = new google.maps.Geocoder();

	$scope.infoBox = new InfoBox({
		disableAutoPan: true,
		pixelOffset: new google.maps.Size(-16, -45),
		zIndex: null,
		boxStyle: {
			width: "32px",
			height: "52px",
			'z-index' : "-1"
		},
		infoBoxClearance: new google.maps.Size(1, 1),
		isHidden: false,
		pane: "floatPane",
		enableEventPropagation: false
	});

	$scope.createMapCenterMarker = function(){
		var center = $scope.map.getCenter();
		$scope.createMarker({
			lat: center.lat(),
			lon: center.lng()
		});
	};

	$scope.createMarker = function(data) {
		var marker = $scope.marker = new google.maps.Marker({
			map: $scope.map,
			icon: data.icon,
			position: new google.maps.LatLng(data.location.lat, data.location.lng),
		});
		
		google.maps.event.addListener(marker, "mouseover", function(e) {
			jQuery('#map').append('<span class="hidden">'+data.name+'</span>');
			var width = jQuery('#map span.hidden').width();
			jQuery('#map span.hidden').remove();
			$scope.showInfoBox('<div style="width: '+width+'px" class="info-box '+data.style+'"><a href="'+data.link+'">'+data.name+'</a></div>', marker);
		});
	};
	
	$scope.showInfoBox = function(info, marker){
		$scope.infoBox.setContent(info);
		$scope.infoBox.open($scope.map, marker);
		$scope.infoBox.show();
	};
	
	$scope.hideInfoBox = function(info, marker){
		$scope.infoBox.hide();
	};
	
	// Initial point
	for(var i = 0; i < people.length; i++){
		$scope.createMarker(people[i]);
	}
	
	
	var slider = $( "#zoom-slider" ).slider({
		orientation: "vertical",
		max: 12,
		min: 1,
		step: 1,
		value: 11,
		create: function( event, ui ) {
			$('#zoom-slider').append('<span class="active-line">&nbsp;</span>')
			var  lineHeight = $('#zoom-slider .ui-slider-handle').css('bottom');
			$('#zoom-slider .active-line').css('height', lineHeight);
		},
		slide : function( event, ui ) {
			var  lineHeight = $('#zoom-slider .ui-slider-handle').css('bottom');
			$('#zoom-slider .active-line').css('height', lineHeight);
		},
		change: function( event, ui ) {
			$scope.map.setZoom(ui.value);
		}
	});
	
	$('.zoom .zoom-plus').on('click', function(){
		$scope.map.setZoom( $scope.map.getZoom()+1);
		slider.slider( "option", "value", $scope.map.getZoom());
	});
	
	$('.zoom .zoom-minus').on('click', function(){
		$scope.map.setZoom( $scope.map.getZoom()-1);
		slider.slider( "option", "value", $scope.map.getZoom());
	});
	
	$(".map .full-map").on('click', function(){
		if($('.map').hasClass('full')){
			var map = $("#content .map.full").removeClass('full').detach();
			$("#content .right-sidebar").prepend(map);
			$("#map").height("auto");
		} else {
			var map = $("#content .map").addClass('full').detach();
			$("#content .left-sidebar").after(map);
			var width = $(".map.full").width();
			var height = (width*3)/4;
			$("#map").height(height);
		}
		
		google.maps.event.trigger($scope.map, 'resize');
		$scope.map.panTo(new google.maps.LatLng($scope.lat, $scope.lon));
		
		return false;
	});
});


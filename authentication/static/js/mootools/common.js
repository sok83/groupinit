
var TabView = new Class({
	Implements: [Options],
	options: {
		tab_header: '.tab_header',
		tabs: '.tab',
		current_header: 'current'
	},
	initialize: function(options, element) {

        this.element = element;
		this.setOptions(options);
		this.tab_headers = this.element.getElements(this.options.tab_header);
		this.tabs = this.element.getElements(this.options.tabs);
		if(this.tab_headers.length > 0) {
			this.tabs.each(function(tab, index){	
				if(index!=0)			
					tab.setStyle('display', 'none');
			}.bind(this));
			this.current_index = 0;
			this.tabs[this.current_index].setStyle('display', 'block');
			this.tab_headers.each(function(header, ind){
				if(header.hasClass('current')){
					this.tabs[this.current_index].setStyle('display', 'none');			
					this.current_index = ind;
					this.tabs[this.current_index].setStyle('display', 'block');			
				}
				header.addEvent('click', function(ev){
					ev.stop();	
					this.change_tab(ind);
				}.bind(this));
			}.bind(this));		
		}		
	},
	change_tab: function(ind){
		var tab = this.tabs[ind];
		this.tabs[this.current_index].setStyle('display', 'none');
		this.tab_headers[this.current_index].removeClass(this.options.current_header);
		this.tab_headers[ind].addClass(this.options.current_header);
		tab.setStyle('display', 'block');
		this.current_index = ind;
	}
});

window.addEvent('domready',function() {
    $$('.tab_container').each(function(container, index){
        var tab_view = new TabView({}, container);
    });
});


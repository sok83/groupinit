jQuery(document).ready(function ($) {
	if($( "#tabs" ).length){
		$( "#tabs" ).tabs({
			active: false,
			collapsible: true,
			hide: "slideUp",
			show: "slideDown"
		});
	}
	if($(".password-strength").length){
		
		$(".password-strength").each(function(){
			var characters = 0;
			var capitalletters = 0;
			var loweletters = 0;
			var number = 0;
			var special = 0;
			var upperCase= new RegExp('[A-Z]');
			var lowerCase= new RegExp('[a-z]');
			var numbers = new RegExp('[0-9]');
			var specialchars = new RegExp('([!,%,&,@,#,$,^,*,?,_,~])');
			$(this).after('<div class="wrap-password-indicator"><div class="password-indicator">&nbsp;</div></div>');
			var thismeter = $('.password-indicator');
			function GetPercentage(a, b) {
					return ((b / a) * 100);
			}

			/*function check_strength(thisval){
				console.log("hh")
				console.log(thisval.length)
				if (thisval.length > 5) { characters = characters+1; console.log(characters);} else { characters = -1; };
				// if (thisval.match(upperCase)) { capitalletters = 1} else { capitalletters = 0; };
				// if (thisval.match(lowerCase)) { loweletters = 1}  else { loweletters = 0; };
				// if (thisval.match(numbers)) { number = 1}  else { number = 0; };
				// if (thisval.match(specialchars)) { special = 1}  else { special = 0; };
				// var total = characters + capitalletters + loweletters + number + special;
				var total = characters;
				var totalpercent = GetPercentage(7, total).toFixed(0);
				if (!thisval.length) {total = -1;}
				console.log(total)
				get_total(total);
			}*/
 function check_strength(thisval){

				console.log("hh");

				console.log(thisval.length);

				/*if (thisval.length >= 1 ) { characters = characters+1; console.log(characters);} else { characters = 0; };*/
				var characters = thisval.length;

				// if (thisval.match(upperCase)) { capitalletters = 1} else { capitalletters = 0; };

				// if (thisval.match(lowerCase)) { loweletters = 1}  else { loweletters = 0; };

				// if (thisval.match(numbers)) { number = 1}  else { number = 0; };

				// if (thisval.match(specialchars)) { special = 1}  else { special = 0; };

				// var total = characters + capitalletters + loweletters + number + special;

				var total = characters;

				// var totalpercent = GetPercentage(7, total).toFixed(0);

				if (!thisval.length) {total = 0;}

				console.log("total");

				console.log(total);

				get_total(total);

			}

              function check_strength_back(thisval){

				console.log("hh")

				console.log(thisval.length)

				if (thisval.length >= 1) { characters = characters-1; console.log(characters);} else { characters = 0; };

				// if (thisval.match(upperCase)) { capitalletters = 1} else { capitalletters = 0; };

				// if (thisval.match(lowerCase)) { loweletters = 1}  else { loweletters = 0; };

				// if (thisval.match(numbers)) { number = 1}  else { number = 0; };

				// if (thisval.match(specialchars)) { special = 1}  else { special = 0; };

				// var total = characters + capitalletters + loweletters + number + special;

				var total = characters;

				var totalpercent = GetPercentage(7, total).toFixed(0);

				if (!thisval.length) {total = 0;}

				console.log(total)

				get_total(total);

			}
      function get_total(total){

				console.log("total function");

				console.log(total);

				if (total <= 1) {

				   thismeter.removeClass().addClass('veryweak');

				} else if (total >= 2  && total <= 5){

				   thismeter.removeClass().addClass('weak');

				} else if(total >= 6  && total <= 9){

				   thismeter.removeClass().addClass('medium');

				} else if(total >= 10  && total <= 12){

				   thismeter.removeClass().addClass('strong');

				} else {

				   thismeter.removeClass().addClass('verystrong');

				}

				if (total == -1) { thismeter.removeClass(); }
				if (total == 0) {thismeter.removeClass();}

			}
			/*function get_total(total){
				if (total <= 1) {
				   thismeter.removeClass().addClass('veryweak');
				} else if (total >= 3  && total <= 6){
				   thismeter.removeClass().addClass('weak');
				} else if(total >= 7  && total <= 9){
				   thismeter.removeClass().addClass('medium');
				} else if(total >= 10  && total <= 12){
				   thismeter.removeClass().addClass('strong');
				} else {
				   thismeter.removeClass().addClass('verystrong');
				}
				if (total == -1) { thismeter.removeClass(); }
			}*/

	/*		$(this).bind('keyup keydown', function(event) {
				thisval = $(this).val();
				console.log(thisval)
				check_strength(thisval);
			});*/
                        /*$(this).bind('keypress', function(event) {

				var keycode = (event.keyCode ? event.keyCode : event.which);



				thisval = $(this).val();

				console.log("passss")

				console.log(thisval)



				if (keycode > 36 && keycode < 41) { }



				else

					check_strength(thisval);



			});*/
			/*
 $(this).bind('keypress', function(event) {

				var keycode = (event.keyCode ? event.keyCode : event.which);



				thisval = $(this).val();


			if (keycode > 36 && keycode < 41) { }



				else if(keycode == 8){

					check_strength_back(thisval);

				}



				else

					check_strength(thisval);



			});*/
			$(this).keyup(function() {
				thisval = $(this).val();
				check_strength(thisval);
			});
		});
	}

	// slider
  	var $sliderNav = $('.slider-nav');
	var $sliderFor = $('.slider-for');
	function showSliderScreen($widthScreen) {
		if ($widthScreen <= "990") {
			if (!$sliderNav.hasClass('slick-initialized') && !$sliderFor.hasClass('slick-initialized')) {
				$sliderNav.slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: true,
					asNavFor: '.slider-for',
					slide: 'li',
					focusOnSelect: true
				});
				$sliderFor.slick({
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					asNavFor: '.slider-nav',
					slide: 'div'
				});
			}
		} else {
			if ( $sliderNav.hasClass('slick-initialized') && $sliderFor.hasClass('slick-initialized')) {
				$sliderNav.slick('unslick');
				$sliderFor.slick('unslick');
			}
		}
	}
	var widthScreen = $(window).width();
	$(window).ready(showSliderScreen(widthScreen)).resize(
		function () {
			var widthScreen = $(window).width();
			showSliderScreen(widthScreen);
		}
	);

	// scrollbar
	$(".scroll-area").mCustomScrollbar({
		autoHideScrollbar: true
	});


	// accordion
	/*$('.accordion-btn').click(function() {
		if($(this).parents('.accordion-panel').children('.accordion-content').is(':hidden') != true) {
			$(this).parents('.accordion-panel').removeClass('active');
			$(this).parents('.accordion-panel').children('.accordion-content').slideUp("normal");
		} else {
			$(this).parents('.accordion-area').children('.accordion-panel').removeClass('active');
			$(this).parents('.accordion-area').children().children('.accordion-content').slideUp('normal');
			if($(this).parents('.accordion-panel').children('.accordion-content').is(':hidden') == true) {
				$(this).parents('.accordion-panel').addClass('active');
				$(this).parents('.accordion-panel').children('.accordion-content').slideDown('normal');
			}
		}
	});
	$('.accordion-content').hide();
 	$('.all-btn').click(function(event){
		event.preventDefault();
		event.stopPropagation();
		$(this).toggleClass('open');
		if ($('.expand-collapse').children('.accordion-panel').hasClass('active') && $('.expand-collapse').children().children('.accordion-content').is(':hidden') != true){
			$('.expand-collapse').children().children('.accordion-content').slideUp('normal');
			$('.expand-collapse').children('.accordion-panel').removeClass('active');
		} else {
			$('.expand-collapse').children('.accordion-panel').addClass('active');
			$('.expand-collapse').children().children('.accordion-content').slideDown('normal');
		}
	});*/

	// pop-up
	$('.pop-up-link').magnificPopup({});

	// datepicker
	$( '#start-date, #end-date' ).datepicker({
	  dateFormat: "d M yy"
	});
});

// drop-down
$(function () {
	var wrapper =$('.wrapper'),
	dropdown =$('.drop-area'),
	dropdownBox = dropdown.find('.drop-box'),
	buttonDrop = dropdown.find('.drop-btn'),
	clickAllowed = true;

	buttonDrop.on('click', function(event) {
		if(clickAllowed) {
			if($(this).parents('.drop-area').children(".drop-box").is(':visible')){
				$(this).parent().removeClass('open');
				$(this).parents('.drop-area').children(".drop-box").slideUp("slow");
			} else {
				dropdown.removeClass('open');
				dropdownBox.slideUp("slow");
				$(this).parent().addClass('open');
				$(this).parents('.drop-area').children(".drop-box").slideDown("slow");
			}
		}
	});
	wrapper.on('click', function(event) {
		if (!$(event.target).is($(buttonDrop)) && !$(event.target).is($(buttonDrop).children()) && !$(event.target).closest('.drop-box').length){
			dropdown.removeClass('open');
			dropdownBox.slideUp("slow");
		}
	});

	onResize = function() {
        if($(window).width() < 990){
            clickAllowed = true;
        }
        else{
			clickAllowed = false;
			dropdown.removeClass('open');
			dropdownBox.removeAttr('style');
        }
    }
    $(document).ready(onResize);
    $(window).bind('resize', onResize);
});


// Show messages(errors) in top|right fixed
var showMessage = function(message, error){
	if(!$('.message-area span').length){
		$('body').prepend('<div class="message-area"><a class="close-message">&times;</a><span></span></div>');
	}

	if(error == true)
		$('.message-area').addClass('error');
	else
		$('.message-area').removeClass('error');

	$('.message-area').show();
	$('.message-area span').text(message);

	$('.message-area a.close-message').on('click', function(){
		$('.message-area').hide();
	});

	setTimeout(function() {
      $('.message-area').fadeOut();
	}, 5000);

	return false;
}



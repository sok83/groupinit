// All master binding logic & top level operations goes here
function ajaxGet(url,callback){
    $.ajax({
      type: "GET",
      url: url,
      dataType: 'json',
      data: {csrfmiddlewaretoken: $('#csrf_token').val()},
      success: callback
    });
  }

 function ajaxPost(url,data,callback){
     var uri = url;
     if( typeof url == 'object'){
         uri = url.url;
         data = url.data;
         callback = url.callback;
     }
    $.ajax({
      type: "POST",
      url: uri,
      dataType: 'json',
      data:data,
      success: callback
    });
  }

function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}
function contentAccordion(x){
    var widget = {};
    if(x==null)
        widget = $('#invite-friends');
    else
        widget = $('#'+x);
     $('.checkbox-gallery', widget).off();

     $('.checkbox-gallery a', widget).off();
     $('.checkbox-gallery .row-group ', widget).off();
     $('.checkbox-gallery .row-group > div ', widget).off();
    console.log('executed');

    $('.checkbox-gallery .row-group', widget).contentTabs({
        addToParent: true,
        tabLinks: 'a'
    });



    $('.checkbox-gallery', widget).scrollGallery({
        mask: '.mask',
        slider: '.row-group',
        slides: '.slide',
        btnPrev: 'a.btn-prev',
        btnNext: 'a.btn-next',
        pagerLinks: '.pagination li',
        stretchSlideToMask: false,
        maskAutoSize: false,
        autoRotation: false,
        switchTime: 3000,
        animSpeed: 500,
        step: 1,
        activeClass: 'sactive',
        generatePagination: true
    });
     initIScroll();
     initAccordion();
}

$(function() {

    //Remove Event bindings top search
    $('.search-form').children().off();

    $('#itemsContainer path').on('click',function(event) {
        event.preventDefault();
        console.log($(this).css('fill'));
        $('#changecolor').css('background',$(this).css('fill'));
        $('.color-box-drop').addClass('js-slide-hidden');
    });

    $('#new-group .btn-base').click(function() {
        var groupname =$('#new-group form input').val();
        if(groupname == ''){
            alert('Cannot be empty');
            return false;
        }
        ajaxPost('/api/group/create/',{
            name:  $('#new-group form input').val(),
            color: rgb2hex($('#changecolor').css('backgroundColor')),
            csrfmiddlewaretoken: $('#csrf_token').val()
        },function(response) {
            console.log(response);
            $('#new-group form input').val('');
            $('#new-group').removeClass('lightbox-active');
            // initPersonsAccordion();
        });
    });
});




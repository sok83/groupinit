import json as simplejson
import ast
from datetime import datetime, timedelta
import binascii
import os
import re
import itertools
from mongoengine import *
import pymongo
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter

from django.shortcuts import render
from django.views.generic.base import View
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.gis.geoip import GeoIP
from django.conf import settings
from django.core.mail import send_mail

from albums.models import Picture
from models import Token, InvitationToken, RandomKey, ProfileSettings, UserProfile
from posts.models import Post, TravelPost, LifeTimePost, MediaPost, ViewPosts, LocationPoints
from groupbox.models import FriendRequest, FriendsBox, Group, Request, Box
from mongo_models.models import *
from notification.models import Notification
from authentication.utils import *

from groupbox.models import Grp

from django.contrib.gis.geos import GEOSGeometry


def generate_token():
    return binascii.hexlify(os.urandom(20)).decode()


def validate_signup_form(user_details):
    """validation of signup form"""
    if (user_details['name'] == ''):

        return False;

    elif ((bool(re.compile('^[a-zA-Z0-9]+(?=.*?[a-zA-Z ])+[a-zA-Z 0-9.,-]{2,30}$').match(user_details['name']))) == False):

        return False;

    elif (len(user_details['name']) < 3):

        return False;

    elif (len(user_details['name']) > 30):

        return False;

    elif (user_details['email'] == ''):

        return False;

    elif ((bool(re.compile('[^@]+@[^@]+\.[^@]').match(user_details['email']))) == False):

        return False;

    elif (user_details['password'] == ''):

        return False;

    elif (user_details['confirm_password'] == ''):

        return False;

    elif (user_details['terms'] == ''):

        return False;



    else:

        return True;


class GetAllProfiles(View):
    def get(self, request, *args, **kwargs):
        profile_data = []
        user = request.user
        box_id = request.session['box_id']
        box = FriendsBox.objects.get(id=box_id)
        group = Grp.get_by_box(box_id)
        # group = Group.objects.filter(box=box)

        for i in range(0, len(group)):
            for x in group[i].members.all():
                profile_data.append(x.profile.get_json())
        res = {
            'result': 'ok',
            'profile': profile_data,

        }

        response = simplejson.dumps(res)

        return HttpResponse(response, status=200)


from posts.models import Notify


class Dashboard(View):
    """ to call the home-page html.
        fetch the users basic informations
        posts friend requests etc	"""

    def get(self, request, *args, **kwargs):
        user = request.user
        request.session['user'] = user.id
        boxes = FriendsBox.objects.filter(owner=user).order_by('order')
        count = -1
        for box in boxes:
            unread_count = len(Notify(request.user.id).box_unread_post(box.id))
            if count < unread_count:
                count = unread_count
                request.session['box_id'] = box.id
        return render(request, 'home-page.html', {'user': request.user, 'profile': user.profile})


class AppSettings(View):
    """ after the first login of the new user,there is a settings page displayed
    this view loads the appsettings html and saves the settings data into database"""

    def get(self, request, *args, **kwargs):

        token = request.user.token_set.all()
        if token.count() > 0:
            token = token[0]
            if token.token_verified:
                return HttpResponseRedirect(reverse('dashboard'))
        # else:
        #     token = Token.objects.create(user=request.user)
        #token.token_verified = True
        user = token.user
        user.is_active = True
        user.save()
        #token.save()

        return render(request, 'appsettings.html', {})

    def post(self, request, *args, **kwargs):

        validation = True

        if request.session.get('next_url', ''):

            next_url = request.session.get('next_url', '')

        else:

            next_url = '/dashboard/'

        if request.is_ajax():

            profile_settings = ast.literal_eval(request.POST['profile_settings'])
            try:

                token = request.user.token_set.all()
                if token.count() > 0:
                    token = token[0]
                    if token.token_verified:
                        return HttpResponseRedirect(reverse('dashboard'))
                else:
                    token = Token.objects.create(user=request.user)
                token.token_verified = True
                token.save()

                settings = ProfileSettings.objects.create(user=request.user)

                settings.profile_visibility = profile_settings['see_profile']

                settings.personal_info_visibility = profile_settings['see_personal_info']

                settings.shared_group_post_visibility = profile_settings['see_post_in_shared_groups']

                settings.see_post_in_other_groups = profile_settings['see_post_in_other_groups']

                settings.permission_for_share_posted_data = profile_settings['allow_post_shared_with_personal']

                settings.save();

                boxes = Box(owner=request.user).get_all()
                colors = ['#D1AB4A', '#B741A7']
                grp_name_dict = {'Friends': ['My Best friends', 'Old Friends'], 'Family': ['My Family', 'Relatives'],
                                 'Work': ['My Team', 'Colleagues']}
                for box in boxes:
                    """create new group"""
                    if box.box_type == 'box':
                        grp_name_list = grp_name_dict.get(box.name, [])
                        i = 0
                        for gr_name in grp_name_list:
                            grp = Grp.create(gr_name, colors[i], "Shared")
                            grp.members.add(request.user)
                            Box.add_group(box=box, group=grp)
                            Request.auto_invite(from_user=request.user, grp=grp, hub=box)
                            i += 1
                res = {

                    'result': 'ok',

                    'next_url': next_url,

                    'message': 'Profile settings saved successfully'

                }

            except Exception as Ex:

                res = {

                    'result': 'error',

                    'message': str(Ex),

                }

        response = simplejson.dumps(res)

        return HttpResponse(response, status=200)

class Login(View):
    """ login """

    def get(self, request, *args, **kwargs):
        invitation = request.GET.get('inivitation', '')
        username = request.GET.get('username', '')
        paswd = request.GET.get('paswd', '')
        response = {}
        if invitation:
            try:
                inv_token = Token.objects.get(token=invitation)

                username = inv_token.user.email
                paswd = inv_token.password_token
            except:
                return HttpResponse('Invalid token')

            response = {
                'invitation': invitation
            }
        print username
        if username and paswd:
            try:
                user = User.objects.get(email__iexact=username)
                user = authenticate(username=user.username, password=paswd)
                if user:
                    token = user.token_set.all()[0]

                    settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True

                    try:
                        user_profile = UserProfile.objects.get(user=user)
                        if request.session.get('next_url', ''):
                            next_url = request.session.get('next_url', '')
                        else:
                            if token.token_verified:
                                next_url = '/dashboard/'
                            else:
                                next_url = '/appsettings/'

                        login(request, user)
                        result = {
                            'message': 'Successfully logged in',
                            'next_url': next_url,
                            'result': 'Ok'
                        }
                        response = simplejson.dumps(result)
                        return redirect(next_url)
                    except:
                        user_profile = UserProfile.objects.create(user=user)

                        if request.session.get('next_url', ''):
                            next_url = request.session.get('next_url', '')
                        else:
                            if token.token_verified:
                                next_url = '/dashboard/'
                            else:
                                next_url = '/appsettings/'

                        login(request, user)
                        result = {
                            'message': 'Successfully logged in',
                            'next_url': next_url,
                            'result': 'Ok'
                        }


                else:
                    result = {

                        'message': 'Username or password is incorrect'
                    }
            except User.DoesNotExist:
                result = {
                    'message': 'Username or password is incorrect'
                }

            response = simplejson.dumps(result)
            return render(request, 'base.html', result)
        # return HttpResponse(response, status=200)

        return render(request, 'base.html', response)

    def post(self, request, *args, **kwargs):

        try:

            user = User.objects.get(email__iexact=request.POST['username'])

            user = authenticate(username=user.username, password=request.POST['password'])
            if user and user.is_active:
                token = user.token_set.all()[0]

                if request.POST['remember_me'] == True:
                    request.session.set_expiry(0)
                try:
                    remember = request.POST['remember_me']
                    if remember == True:
                        settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
                except MultiValueDictKeyError:
                    # is_private = False
                    settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True

                try:
                    user_profile = UserProfile.objects.get(user=user)
                    if request.session.get('next_url', ''):
                        next_url = request.session.get('next_url', '')
                    else:
                        if token.token_verified:
                            next_url = '/dashboard/'
                        else:
                            next_url = '/appsettings/'

                    login(request, user)
                    result = {
                        'message': 'Successfully logged in',
                        'next_url': next_url,
                        'result': 'Ok'
                    }
                except:
                    user_profile = UserProfile.objects.create(user=user)
                    # box1 = FriendsBox.objects.create(owner=user, name='Friends', color='#F65B42', order='1', box_type='box')
                    # group1 = Group.objects.create(box=box1, name='Public Groups', group_bar_color='#F65B42')
                    # group1.group_admins.add(user)
                    #
                    # box2 = FriendsBox.objects.create(owner=user, name='Family', color='#72D668', order='2', box_type='box')
                    # group2 = Group.objects.create(box=box2, name='Public Groups', group_bar_color='#F65B42')
                    # group2.group_admins.add(user)
                    #
                    # box3 = FriendsBox.objects.create(owner=user, name='Work', color='#4182B6', order='3', box_type='box')
                    # group3 = Group.objects.create(box=box3, name='Public Groups', group_bar_color='#F65B42')
                    # group3.group_admins.add(user)
                    #
                    # box4 = FriendsBox.objects.create(owner=user, name='Public Groups', color='#D1AB4A', order='4', box_type='box')
                    # group4 = Group.objects.create(box=box4, name='Public Groups', group_bar_color='#F65B42')
                    # group4.group_admins.add(user)
                    #
                    # box5 = FriendsBox.objects.create(owner=user, name='Discussions', color='#B741A7', order='5',
                    #                                  box_type='discusson')
                    # group5 = Group.objects.create(box=box5, name='Public Groups', group_bar_color='#F65B42')
                    # group5.group_admins.add(user)

                    invitation_token = request.POST.get('invitation_token', '')
                    if invitation_token:
                        invitation = InvitationToken.objects.get(invitation_token=invitation_token)

                        invitation.invitation_token_verified = True
                        invitation.save()
                        # friends_list, created = Friends.objects.get_or_create(user=user)
                        # friends_list.friends.add(invitation.user)
                        # friends_list.save()
                        #
                        # friends_list, created = Friends.objects.get_or_create(user=invitation.user)
                        # friends_list.friends.add(user)
                        # friends_list.save()
                    if request.session.get('next_url', ''):
                        next_url = request.session.get('next_url', '')
                    else:
                        if token.token_verified:
                            next_url = '/dashboard/'
                        else:
                            next_url = '/appsettings/'

                    login(request, user)
                    result = {
                        'message': 'Successfully logged in',
                        'next_url': next_url,
                        'result': 'Ok'
                    }
            elif user and user.is_active == False:
                token = Token.objects.get(user=user)
                link= "/resend/?key=" + token.token
                result = {'activation_link':link}

            else:
                result = {

                    'message': 'Username or password is incorrect'
                }
        except Exception as e:
            print e
            result = {
                'message': 'Username or password is incorrect'
            }
        if request.is_ajax():
            response = simplejson.dumps(result)
            return HttpResponse(response, status=200)
        return render(request, 'authentication.html', result)


class ActivateAccount(View):
    """ for activating user accounts.."""

    def get(self, request, *args, **kwargs):
        try:
            key_token = request.GET.get('token', '')
            token = Token.objects.get(token=key_token, token_verified=False)
            if token.user.is_active == False and token.token_created.date() != datetime.now().date():
                return render(request, 'base.html', {'token': key_token, 'result': 'expired',
                                                     'message': 'This link is expired Click Here to resend the link',
                                                     'link': '/resend/?key=' + key_token})
            user = token.user
            user.is_active = True
            user.save()

        except Exception as ex:
            pass
            # return render(request,'base.html',{'token': key_token,'result': 'error','message': 'This link is expired Click Here to resend the link',
            #                                    'link':'/resend/?key='+key_token})
            # return HttpResponse('This link is expired, please see the latest email with link')
        return HttpResponseRedirect(reverse('login'))


class CheckEmailExists(View):
    """ for check the existance of the user during the registration """

    def get(self, request, *args, **kwargs):

        if request.is_ajax():

            email_id = request.GET.get('email_id', '')
            user = User.objects.filter(username=email_id)

            if user:
                res = {
                    'result': 'error',
                    'message': 'User Already Exists'
                }
                response = simplejson.dumps(res)

            else:
                res = {
                    'result': 'ok',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class GetGroupData(View):
    """ To get the details of boxes of a particular user """

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user_id = request.user.id
            user = User.objects.get(id=user_id)
            box_details = []
            box = FriendsBox.objects.filter(owner=user)
            for i in range(0, box.count()):
                box_data = {}
                box_data['id'] = box[i].id
                box_data['name'] = box[i].name
                box_data['color'] = box[i].color
                box_data['box_type'] = box[i].box_type
                box_details.append(box_data)
            new_grp_details = []
            for b in range(0, len(box)):

                grp_order = GroupOrder.objects.filter(user=request.user.id, box_id=int(box[b].id))
                grp_details = []
                grps_data = {}
                for x in range(0, grp_order.count()):
                    # shared_grpids.append(grp_order[x]['group_id'])
                    # shared_grpids.append(grp_order[x].group_id)
                    new_grps = Group.objects.get(id=grp_order[x].group_id)

                    grp_details.append(new_grps.get_json())

                id = 'box_' + str(box[b].id)
                grps_data[box[b].id] = grp_details
                new_grp_details.append(grps_data)
            res = {
                'result': 'ok',
                'box_details': box_details,
                'group_data': new_grp_details

            }
            response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class LoadGroups(View):
    """to load the details of groups, group members, and posts in a particular box """

    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            print "ss", request.GET.get('box_id', '')
            request.session['box_id'] = request.GET.get('box_id', '')
            friend_request_deatils = []
            box_id = request.GET.get('box_id', '')
            box = FriendsBox.objects.get(id=box_id)
            user = User.objects.get(username=request.user)

            boxes = FriendsBox.objects.filter(owner=user).order_by('order')
            last_index = boxes.count() - 1
            map_data = []
            media_data = []
            lifetime_data = []
            post = {}
            post['post_status'] = "null"
            for i in range(0, boxes.count()):
                if boxes[i].id == box.id:
                    if boxes[i].id != boxes[last_index].id:
                        next_box = boxes[i + 1].id
                    else:
                        next_box = boxes[0].id

            data = []
            friends_posts = []
            if box:
                all_boxes = FriendsBox.objects.filter(owner=user).order_by('id')
                if box == all_boxes[0]:

                    post_details = []

                    friends_posts = Post.objects.filter(post_creator=user, post_type="general").order_by('created_date')
                    friends_life_time_post = LifeTimePost.objects.filter(post_creator=user).order_by('created_date')
                    friends_travel_post = TravelPost.objects.filter(post_creator=user).order_by('created_date')
                    friends_media_post = MediaPost.objects.filter(post_creator=user).order_by('created_date')
                    friends_shared_posts = Post.objects.filter(post_creator=user, post_type="shared").order_by('created_date')
                    if friends_posts.count() != 0:
                        for i in range(0, friends_posts.count()):
                            post = friends_posts[i].get_json()
                            for i in range(0, len(post['likes'])):
                                l_id = post['likes'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'likes'
                                else:
                                    post['post_status'] = 'null'
                            for i in range(0, len(post['like_plus'])):
                                l_id = post['like_plus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_plus'
                                else:
                                    post['post_status'] = 'null'

                            for i in range(0, len(post['like_minus'])):
                                l_id = post['like_minus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_minus'
                                else:
                                    post['post_status'] = 'null'
                            post_details.append(post)
                    if friends_life_time_post.count() != 0:
                        for i in range(0, friends_life_time_post.count()):
                            post = friends_life_time_post[i].get_json()
                            for i in range(0, len(post['likes'])):
                                l_id = post['likes'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'likes'
                                else:
                                    post['post_status'] = 'null'
                            for i in range(0, len(post['like_plus'])):
                                l_id = post['like_plus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_plus'
                                else:
                                    post['post_status'] = 'null'

                            for i in range(0, len(post['like_minus'])):
                                l_id = post['like_minus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_minus'
                                else:
                                    post['post_status'] = 'null'
                            post_details.append(post)
                    if friends_travel_post.count() != 0:
                        for i in range(0, friends_travel_post.count()):
                            post = friends_travel_post[i].get_json()
                            for i in range(0, len(post['likes'])):
                                l_id = post['likes'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'likes'
                                else:
                                    post['post_status'] = 'null'
                            for i in range(0, len(post['like_plus'])):
                                l_id = post['like_plus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_plus'
                                else:
                                    post['post_status'] = 'null'

                            for i in range(0, len(post['like_minus'])):
                                l_id = post['like_minus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_minus'
                                else:
                                    post['post_status'] = 'null'
                            post_details.append(post)

                    if friends_media_post.count() != 0:
                        for i in range(0, friends_media_post.count()):
                            post = friends_media_post[i].get_json()
                            for i in range(0, len(post['likes'])):
                                l_id = post['likes'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'likes'
                                else:
                                    post['post_status'] = 'null'
                            for i in range(0, len(post['like_plus'])):
                                l_id = post['like_plus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_plus'
                                else:
                                    post['post_status'] = 'null'

                            for i in range(0, len(post['like_minus'])):
                                l_id = post['like_minus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_minus'
                                else:
                                    post['post_status'] = 'null'
                            post_details.append(post)
                    if friends_shared_posts.count() != 0:
                        for i in range(0, friends_shared_posts.count()):
                            post = friends_shared_posts[i].get_json()
                            for i in range(0, len(post['likes'])):
                                l_id = post['likes'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'likes'
                                else:
                                    post['post_status'] = 'null'
                            for i in range(0, len(post['like_plus'])):
                                l_id = post['like_plus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_plus'
                                else:
                                    post['post_status'] = 'null'

                            for i in range(0, len(post['like_minus'])):
                                l_id = post['like_minus'][i]['user']
                                if request.user.id == l_id:
                                    post['post_status'] = 'like_minus'
                                else:
                                    post['post_status'] = 'null'
                            post_details.append(post)
                else:
                    post_details = []
                    # frnds = Friends.objects.filter(user=user, box=box)
                    # friends = frnds
                    # if frnds:
                    #     friends_first_box = Friends.objects.filter(user=user, box=all_boxes[0])
                    #     if friends_first_box:
                    #         friends = [x for x in frnds if x not in friends_first_box]
                    #
                    #     else:
                    #         friends = frnds
                    #
                    # if friends:
                    #
                    #     userposts = UserPosts.objects.filter(user_id=user.id)
                    #     print len(userposts)
                    #     if len(userposts) != 0:
                    #         index = len(userposts) - 1
                    #         posts = userposts[index].posts
                    #         for post in posts:
                    #             friends_posts = Post.objects.get(id=post)
                    #             if friends_posts.post_creator in friends[0].friends.all():
                    #                 if friends_posts.post_type == 'general':
                    #                     post = friends_posts.get_json()
                    #                     for i in range(0, len(post['likes'])):
                    #                         l_id = post['likes'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'likes'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     for i in range(0, len(post['like_plus'])):
                    #                         l_id = post['like_plus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_plus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #
                    #                     for i in range(0, len(post['like_minus'])):
                    #                         l_id = post['like_minus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_minus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     post_details.append(post)
                    #                 if friends_posts.post_type == 'travel':
                    #                     friends_travel_post = TravelPost.objects.get(id=friends_posts.id)
                    #                     # friends_travel_post =  friends_posts[i].TravelPost
                    #                     post = friends_travel_post.get_json()
                    #                     for i in range(0, len(post['likes'])):
                    #                         l_id = post['likes'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'likes'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     for i in range(0, len(post['like_plus'])):
                    #                         l_id = post['like_plus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_plus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #
                    #                     for i in range(0, len(post['like_minus'])):
                    #                         l_id = post['like_minus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_minus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     post_details.append(post)
                    #                     map_data.append(friends_travel_post.get_json())
                    #                 if friends_posts.post_type == 'lifetime':
                    #                     friends_life_time_post = LifeTimePost.objects.get(id=friends_posts.id)
                    #                     post = friends_life_time_post.get_json()
                    #                     for i in range(0, len(post['likes'])):
                    #                         l_id = post['likes'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'likes'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     for i in range(0, len(post['like_plus'])):
                    #                         l_id = post['like_plus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_plus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #
                    #                     for i in range(0, len(post['like_minus'])):
                    #                         l_id = post['like_minus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_minus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     post_details.append(post)
                    #                     lifetime_data.append(friends_life_time_post.get_json())
                    #                 # lifetime_post = LifeTimePost.objects.get(id = post)
                    #                 # post_details.append(lifetime_post.get_json())
                    #                 if friends_posts.post_type == 'media':
                    #                     friends_media_post = MediaPost.objects.get(id=friends_posts.id)
                    #
                    #                     post = friends_media_post.get_json()
                    #                     for i in range(0, len(post['likes'])):
                    #                         l_id = post['likes'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'likes'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     for i in range(0, len(post['like_plus'])):
                    #                         l_id = post['like_plus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_plus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #
                    #                     for i in range(0, len(post['like_minus'])):
                    #                         l_id = post['like_minus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_minus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     post_details.append(post)
                    #                     media_data.append(friends_media_post.get_json())
                    #                 if friends_posts.post_type == 'shared':
                    #                     friends_shared_post = Post.objects.get(id=friends_posts.id)
                    #
                    #                     post = friends_shared_post.get_json()
                    #                     for i in range(0, len(post['likes'])):
                    #                         l_id = post['likes'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'likes'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     for i in range(0, len(post['like_plus'])):
                    #                         l_id = post['like_plus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_plus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #
                    #                     for i in range(0, len(post['like_minus'])):
                    #                         l_id = post['like_minus'][i]['user']
                    #                         if request.user.id == l_id:
                    #                             post['post_status'] = 'like_minus'
                    #                         else:
                    #                             post['post_status'] = 'null'
                    #                     post_details.append(post)

                new_posts = sorted(post_details, key=lambda x: x['created_date'], reverse=True)
                if request.GET.get('page'):
                    last_post = 5 * int(request.GET.get('page'))
                    next_posts = new_posts[last_post:last_post + 5]
                    res = {
                        'result': 'ok',
                        'posts': next_posts,
                    }
                    response = simplejson.dumps(res)
                    return HttpResponse(response, status=200)

                new_map = sorted(map_data, key=lambda x: x['created_date'], reverse=True)
                new_media = sorted(media_data, key=lambda x: x['created_date'], reverse=True)
                new_lifetime = sorted(lifetime_data, key=lambda x: x['created_date'], reverse=True)


                # group_details=[]
                grp = Group.objects.all()

                # for i in range(0, groups.count()):
                #	 group_data = groups[i].get_json()
                #	 group_details.append(groups[i].get_json())

                # friends = Friends.objects.filter(user=request.user, box=box)
                groups = []

                # if friends:
                #	 group1=grp.filter(group_admins=request.user,box=box)
                #	 if group1:
                #		 for i in range(0,len(group1)):
                #			 groups.append(group1[i].get_json())
                #	 group2=grp.filter(members=request.user,group_type='Shared')

                #	 for i in range(0,len(friends)):
                #		 frnd = friends[i].friends.all()
                #		 if len(group2) !=0:
                #			 for x in range(0,len(group2)):
                #				 for friend in frnd:
                #					 if friend in group2[x].group_admins.all():
                #						 groups.append(group2[x].get_json())

                # else:

                #	 group = grp.filter(group_admins=request.user,box=box)
                #	 for i in range(0, group.count()):

                #		 groups.append(group[i].get_json())
                con = get_db()
                coll = con.GroupOrder

                grps = []
                # grp_order = coll.find({"$and":[{"user": { "$in": [request.user.id]}},{"box_id": { "$in": [int(box.id)]}}]})
                #  for x in range(0,grp_order.count()):
                #
                # 	 new_grp = grp_order[x]
                #
                # 	 if new_grp != None:
                #
                # 		 grps.append(new_grp)

                new_grps = sorted(grps, key=lambda x: x['order'])
                grps = []
                grp_order = coll.find({"$and": [{"user": {"$in": [request.user.id]}}, {"box_id": {"$in": [int(box.id)]}}]})

                # grp_order = GroupOrder.objects.filter(user=request.user.id, box_id=int(box.id))

                for x in range(0, grp_order.count()):

                    new_grp = grp_order[x]

                    if new_grp != None:
                        grps.append(new_grp.get_json())

                new_grps = sorted(grps, key=lambda x: x['order'])

                for x in range(0, len(new_grps)):
                    group1 = grp.filter(id=new_grps[x]['group_id'])
                    if group1:
                        for i in range(0, len(group1)):
                            groups.append(group1[i].get_json())
                # print "xxxxx",box.id
                # grp = Group.objects.filter(box=box)
                grp = Grp.get_by_box(box.id)
                grps = []
                for gr in grp:
                    grps.append(gr.get_json())

                new_posts_unique = [g.next() for k, g in itertools.groupby(new_posts, lambda x: x['id'])]
                friend_requests = FriendRequest.objects.filter(invitee=user, state="applied").order_by('-created_date')

                res = {
                    'result': 'ok',
                    # 'groups' : group_details,
                    'groups': grps,
                    'posts': new_posts_unique,
                    'next_box': next_box,
                    'map_data': new_map,
                    'media_data': new_media,
                    'lifetime_data': new_lifetime,
                    'friend_request_count': friend_requests.count(),

                }
                print res
                response = simplejson.dumps(res)

            else:
                res = {
                    'result': 'error',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadFriends(View):
    """ to load the deatils of  friends in a particular group"""

    def get(self, request, *args, **kwargs):

        if request.is_ajax():

            grp_id = request.GET.get('group_id', '')
            group = Group.objects.get(id=grp_id)
            requests = FriendRequest.objects.filter(invited_group=group, invitor=request.user, state='applied')
            grp_data = group.get_json()
            # remove logged in user from the members list
            if len(grp_data['members']) != 0:
                for x in grp_data['members']:

                    if x['id'] == request.user.id:
                        grp_data['members'].remove(x)

            requests_list = []
            for i in range(0, len(requests)):

                if requests[i].invitee not in group.members.all():
                    requests_list.append(requests[i].get_json())
            if group:
                res = {
                    'result': 'ok',
                    'friends': grp_data,
                    'requests': requests_list,
                }
                response = simplejson.dumps(res)

            else:
                res = {
                    'result': 'error',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class ListGroups(View):
    def get(self, request, *args, **kwargs):
        if not request.is_ajax():
            box_id = request.GET.get('id', '')
            grp = []
            # groups = Group.objects.filter(box__id=box_id)
            groups = Grp.get_by_box(box_id)
            if len(groups) != 0:
                for i in range(0, len(groups)):
                    grp.append(groups[i].get_json())
                res = {
                    'result': 'ok',
                    'groups': grp,

                }
            else:
                res = {
                    'result': 'error',
                    'msg': 'no groups in this box',

                }

            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)


class EditProfile(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user_id = request.GET.get('userid', '')
            user = User.objects.get(id=user_id)
            user = request.user
            user = User.objects.get(id=user.id)
            res = {
                'result': 'ok',
                'user': user.first_name,
                'profile': user.profile.get_json(),

            }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)
        else:
            user = request.user
            user = User.objects.get(id=user.id)
            res = {
                'result': 'ok',
                'user': user.first_name,
                'profile': user.profile.get_json(),

            }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():

            try:

                user_id = request.POST['id']
                user = User.objects.get(id=user_id)
                user.first_name = request.POST['first_name']
                user.save()
                user_profile = UserProfile.objects.get(user=user)
                user_profile.place = request.POST['place']
                user_profile.about = request.POST['about']
                user_profile.last_name = request.POST['last_name']
                user_profile.email = request.POST['email']
                user_profile.gender = request.POST['gender']
                user_profile.contact_no = request.POST['contact_no']
                if request.POST['joined_date'] != '':
                    user_profile.joined_date = request.POST['joined_date']
                if request.POST['birth_date'] != '':
                    user_profile.birth_date = request.POST['birth_date']

                user_profile.save()

                res = {
                    'result': 'ok',
                    'profile': user_profile.get_json(),
                }
                response = simplejson.dumps(res)
            except:
                res = {
                    'result': 'error',
                    'msg': "Error!! Something went wrong"
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class ProfileDetails(View):
    """  fetch profile details, posts and friends of a particular user """

    def get(self, request, *args, **kwargs):

        if request.is_ajax():
            friend_request_deatils = []
            box_details = []
            post_details = []

            user_id = request.GET.get('userid', '')
            user = User.objects.get(id=user_id)
            """ fetch details of Friend Requests """

            friend_requests = FriendRequest.objects.filter(invitee=user, state="applied").order_by('created_date')
            if friend_requests:
                for i in range(0, friend_requests.count()):
                    friend_request_deatils.append(friend_requests[i].get_json())

            """ fetch details of Boxes """

            box = FriendsBox.objects.filter(owner=user).order_by('id')

            request.session['box_id'] = box[0].id
            if box:
                for i in range(0, box.count()):
                    box_details.append(box[i].get_json())

            """ fetch post details"""

            # friends = Friends.objects.filter(user=user)
            friends = []

            if friends.count() != 0:

                for i in range(0, friends.count()):
                    for x in friends[0].friends.all():
                        friends_id = x.id

                        friend = User.objects.get(id=friends_id)
                        friend_req1 = FriendRequest.objects.filter(invitee=user, invitor=friend, state="accepted")

                        friend_req2 = FriendRequest.objects.filter(invitee=friend, invitor=user, state="accepted")

                        if friend_req1 != [] or friend_req2 != []:
                            friends_posts = Post.objects.filter(
                                Q(Q(post_creator=user) & Q(box=box[0])) | Q(post_creator=friend)).order_by('created_date')

                            if friends_posts.count() != 0:
                                for i in range(0, friends_posts.count()):
                                    post_details.append(friends_posts[i].get_json())
            else:
                friends_posts = Post.objects.filter(Q(post_creator=user) & Q(box=boxes[0])).order_by('-created_date')

            res = {
                'result': 'ok',
                'user': user.first_name,
                'profile': user.profile.get_json(),
                'boxes': box_details,
                'friend_request_count': friend_requests.count(),
                'posts': friends_posts,
                'next_box': box[1].id,

            }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)


class BlockThisUser(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            """ Added the blocked users to blocked_by and blocked_user
            fields in the user profile model"""

            blk_user_id = request.GET.get('id', '')
            user = User.objects.get(id=blk_user_id)
            user_profile = UserProfile.objects.get(user=request.user)
            user_profile.blocked_users.add(user)
            user_profile.save()
            user_profile1 = UserProfile.objects.get(user__id=blk_user_id)
            user_profile1.blocked_by.add(request.user)
            user_profile1.save()
            posts = Post.objects.filter(post_creator=request.user)
            blk_user = [request.GET.get('id', '')]
            """Remove the users from the audience lists of posts which
            created by blocked user"""

            # for post in posts:

            #	 con = get_db()
            #	 coll=con.PostAudienceComments

            #	 post_audience = coll.update( {"post_id" :post.id}, { "$pull": { "audience":blk_user}}, upsert= True , multi= True )
            #	 audience_data = coll.find_one({"post_id" :post.id})


            blk_user_posts = Post.objects.filter(post_creator=user)
            blk_user1 = [request.user.id]

            # for post in blk_user_posts:
            #	 con = get_db()
            #	 coll=con.PostAudienceComments

            #	 post_audience = coll.update( {"post_id" :post.id}, { "$pull": { "audience":blk_user1}}, upsert= True , multi= True )
            #	 audience_data = coll.find_one({"post_id" :post.id})



            """Remove the users from friends lists and from list of group members"""
            # friends = Friends.objects.filter(user=request.user)
            # for friend in friends:
            #     if user in friend.friends.all():
            #         friend.friends.remove(user)
            #
            # friends1 = Friends.objects.filter(user=user)
            # for friend in friends1:
            #     if request.user in friend.friends.all():
            #         friend.friends.remove(request.user)
            groups = Group.objects.filter(group_admins=request.user, members=user)
            for grp in groups:
                grp.members.remove(user)
            groups = Group.objects.filter(group_admins=user, members=request.user)
            for grp in groups:
                grp.members.remove(request.user)
            res = {
                'result': 'ok',
                'profile': user_profile.get_json(),

            }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)


class ListBlockedUser(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            blocked_users = []
            user_profile = UserProfile.objects.get(user=request.user)
            blocked_users_list = user_profile.blocked_users.all()
            if len(blocked_users_list) != 0:
                for user in blocked_users_list:
                    blocked_users.append(user.profile.get_json())

                res = {
                    'result': 'ok',
                    'blocked_users': blocked_users,
                }
            else:
                res = {
                    'result': 'error',
                    'msg': 'No users are in blocked list',
                }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)


class UnBlockThisUser(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            blk_user_id = request.GET.get('id', '')
            user = User.objects.get(id=blk_user_id)
            user_profile = UserProfile.objects.get(user=request.user)
            user_profile.blocked_users.remove(user)
            user_profile.save()
            user_profile1 = UserProfile.objects.get(user__id=blk_user_id)
            user_profile1.blocked_by.remove(request.user)
            res = {
                'result': 'ok',

            }
            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)


class Signup(View):
    """ signup """

    def post(self, request, *args, **kwargs):
        if request.session.get('next_url', ''):
            next_url = request.session.get('next_url', '')
        else:
            next_url = '/'
        if request.is_ajax():
            user_details = ast.literal_eval(request.POST['user_details'])
            try:
                user = User.objects.get(username=user_details['email'])
                res = {
                    'result': 'ok',
                    'next_url': next_url,
                    'message': 'User Already Exists'
                }
                response = simplejson.dumps(res)
                return HttpResponse(response, status=200)
            except:
                pass
            user, created = User.objects.get_or_create(email=user_details['email'])
            if created:

                user.first_name = user_details['name']
                user.username = user_details['email']
                if user_details['password']:
                    user.set_password(user_details['password'])

                try:
                    user.is_active = False
                    user.save()
                    user_profile = UserProfile.objects.create(user=user)

                    # create hub1 with name friends and 2 default groups public groups and new requests
                    for x in range(0, 5):
                        names = ['Friends', 'Family', 'Work', 'Public Groups', 'Discussions']
                        colors = ['#F65B42', '#72D668', '#4182B6', '#D1AB4A', '#B741A7']
                        if x < 3:
                            box = FriendsBox.objects.create(owner=user, name=names[x], color=colors[x], order=x + 1, box_type='box')
                        elif (x == 3):
                            box = FriendsBox.objects.create(owner=user, name=names[x], color=colors[x], order=x + 1,
                                                            box_type='public group')
                        else:
                            box = FriendsBox.objects.create(owner=user, name=names[x], color=colors[x], order=x + 1,
                                                            box_type='discussion')

                    random_key = RandomKey.objects.create(user=user)
                    random_key.key_created = datetime.now()
                    random_key.save()

                    tokens = Token.objects.create(user=user)
                    token = tokens.token
                    email_to = user.email

                    expire_time = tokens.token_created + timedelta(days=1)

                    subject = " Groupinit Account Activation "
                    message = " Please activate your account by clicking the following link " + settings.SITE_ROOT + "activate/?token=" + token + " This activation email will expire in " + expire_time.strftime(
                        '%d/%m/%Y %H:%M:%S')

                    from_email = settings.DEFAULT_FROM_EMAIL

                    invitation_token = request.POST.get('invitation_token', '')
                    if invitation_token:
                        invitation = InvitationToken.objects.get(invitation_token=invitation_token)

                        invitation.invitation_token_verified = True
                        invitation.save()

                    send_mail(subject, message, from_email, [email_to])

                    res = {
                        'result': 'ok',
                        'next_url': next_url,
                        'message': 'Soon you will get an email',
                        'random_key': random_key.key,
                    }

                except Exception as Ex:

                    res = {
                        'result': 'error',
                        'message': str(Ex),
                    }
            else:
                res = {
                    'result': 'User',
                    'next_url': next_url,
                    'message': 'User Already Exists'
                }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        return render(request, 'base.html', {})


class ResendEmail(View):
    """if the user is failed to get the activation email after successfull login groupinit resend the email
    this view is to resend the activation email"""

    def get(self, request, *args, **kwargs):
        try:
            resend_key = request.GET.get('key', '')
            random_key = RandomKey.objects.get(key=resend_key)
            user = random_key.user
            email_id = random_key.user.username
            if (random_key.user.is_active == False):
                token_obj = Token.objects.get(user=random_key.user)
                token_obj.delete();
                new_token_obj = Token.objects.create(user=random_key.user)
                expire_time = new_token_obj.token_created + timedelta(days=1)
                random_key.delete()
                new_random_key = RandomKey.objects.create(user=user)
                email_to = email_id
                subject = " Groupinit Account Activation "
                message = " Please activate your account by clicking the following link " + settings.SITE_ROOT + "activate/" + "?token=" + new_token_obj.token + ". This activation email will expire in " + expire_time.strftime(
                    '%d/%m/%Y %H:%M:%S')

                from_email = settings.DEFAULT_FROM_EMAIL

                send_mail(subject, message, from_email, [email_to])
                res = {
                    'result': 'ok',
                    'message': 'Account Activation link is sent to your email',
                    'random_key': new_random_key.key,
                }
            else:
                res = {
                    'result': 'error',
                    'message': 'Your account is already activated',
                }
        except Exception as Ex:

            res = {
                'result': 'error',
                'message': str(Ex),
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class ResendResetPassword(View):
    """to resend the reset password email when user forgots the password"""

    def get(self, request, *args, **kwargs):

        resend_key = request.GET.get('key', '')

        random_key = RandomKey.objects.get(key=resend_key)

        user = random_key.user

        email_id = random_key.user.username

        if (random_key.user):

            try:

                token_obj = Token.objects.get(user=random_key.user)

                token_obj.delete();

                new_token_obj = Token.objects.create(user=random_key.user)

                pass_token = new_token_obj.generate_token()

                new_token_obj.password_token = pass_token

                new_token_obj.pass_token_verified = False

                new_token_obj.pass_token_created = datetime.now()

                new_token_obj.save()
                expire_time = new_token_obj.pass_token_created + timedelta(days=1)
                random_key.delete()

                new_random_key = RandomKey.objects.create(user=user)

                email_to = email_id

                subject = " Password Reset Email - Groupinit "

                message = " You can reset your password by clicking the following link " + settings.SITE_ROOT + "reset_password/?token=" + pass_token + ". This Password Reset Email  will expire in " + expire_time.strftime(
                    '%d/%m/%Y %H:%M:%S')

                from_email = settings.DEFAULT_FROM_EMAIL

                send_mail(subject, message, from_email, [email_to])

                res = {

                    'result': 'ok',

                    'message': 'Account Activation link is sent to your email',

                    'random_key': new_random_key.key,

                }



            except Exception as Ex:

                res = {

                    'result': 'error',

                    'message': str(Ex),

                }

        else:

            res = {

                'result': 'error',

                'message': 'Your account is already activated',

            }

        response = simplejson.dumps(res)

        return HttpResponse(response, status=200)


class Logout(View):
    """ logout"""

    def get(self, request, *args, **kwargs):
        try:
            token = Token.objects.get(user=request.user).password_token
        except:
            token = ""
        if token == "groupinit123":  # special case for email invitation
            print 'error:signout'
            return HttpResponse(simplejson.dumps(
                {
                    'result': 'error',
                    'message': 'Please reset your login password'
                }), status=200)
        else:
            logout(request)
            return HttpResponse(simplejson.dumps(
                {
                    'result': 'success',
                    'message': 'Success'
                }), status=200)


class ForgotPassword(View):
    """forgot password"""

    def get(self, request, *args, **kwargs):

        return render(request, 'forgot-your-password.html', {})

    def post(self, request, *args, **kwargs):

        if request.is_ajax():

            pass_dict = request.POST

            try:

                user = User.objects.get(email__iexact=pass_dict['email'])

                user.save()

                token, created = Token.objects.get_or_create(user=user)

                pass_token = token.generate_token()

                token.password_token = pass_token

                token.pass_token_verified = False

                token.pass_token_created = datetime.now()

                token.save()
                expire_time = token.pass_token_created + timedelta(days=1)
                random_key = RandomKey.objects.get(user=user)

                random_key.delete()

                new_random_key = RandomKey.objects.create(user=user)

                email_to = user.email

                subject = " Password Reset Email - Groupinit "

                message = " You can reset your password by clicking the following link " + settings.SITE_ROOT + "reset_password/?token=" + pass_token + " . This Password Reset Email  will expire in " + expire_time.strftime(
                    '%d/%m/%Y %H:%M:%S')

                from_email = settings.DEFAULT_FROM_EMAIL

                send_mail(subject, message, from_email, [email_to])

                res = {

                    'result': 'Ok',

                    'message': 'Email with instructions to change the password has been sent to this email, Resend the Email, if you have not received ',

                    'random_key': new_random_key.key,

                }

            except User.DoesNotExist:

                res = {

                    'result': 'This email is not registered',

                    'message': 'This email is not registered',

                }

            except Exception as e:

                res = {

                    'message': 'Some thing went wrong',

                    'result': str(e),

                }

            response = simplejson.dumps(res)

            return HttpResponse(response, status=200)

        return render(request, 'reset_password.html', {})


class ResetPassword(View):
    """Resetting the user password..
    loads the html and save the password"""

    def get(self, request, *args, **kwargs):

        if request.user.is_anonymous():
            token = request.GET.get('token', '')

            try:
                token = Token.objects.get(password_token=token, pass_token_verified=False)
                token.pass_token_verified = True
                token.save()

                if token.pass_token_created.date() != datetime.now().date():
                    return HttpResponse('Invalid Token')
                user = token.user
                return render(request, 'reset_password.html', {
                    'token_id': user.id
                })
            except Exception as e:

                return render(request, 'reset_password.html', {

                })
        else:
            return render(request, 'reset_password.html', {
            })

    def post(self, request, *args, **kwargs):

        if request.is_ajax():
            pass_dict = request.POST
            try:
                token_id = pass_dict.get('token_id', '')
                if token_id:
                    user = User.objects.get(id=int(token_id))
                else:
                    user = request.user

                user.set_password(str(pass_dict['new_password']))
                user.save()
                logout(request)
                res = {
                    'result': 'ok',
                    'message': 'Password Changed Successfully',
                }
            except:
                res = {
                    'result': 'ok',
                    'message': 'User Doest Not Exists',
                }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        return render(request, 'reset_password.html', {})


class DeactivateAccount(View):
    def get(self, request, *args, **kwargs):
        res = {}
        return HttpResponse(response, status=200)


class DeleteAccount(View):
    def get(self, request, *args, **kwargs):
        user = request.user
        user_profile = UserProfile.objects.get(user=user)

        user_profile.delete()
        con = get_db()
        coll = con.PostAudienceComments

        res = {}
        return HttpResponse(response, status=200)


import gdata.data
import gdata.contacts.client
# import var_dump
import gdata.contacts.data

USER_AGENT = ""
from django.shortcuts import redirect

from tasks import process_feed


class InviteContacts(View):
    """ invite contacts from the google contacts """

    def get(self, request, *args, **kwargs):
        auth_token = request.session.get('google_auth_token')
        code = request.GET.get('code')
        if not code:
            auth_token = gdata.gauth.OAuth2Token(
                client_id=settings.GOOGLE_CLIENT_ID,
                client_secret=settings.GOOGLE_CLIENT_SECRET,
                scope=settings.GOOGLE_SCOPE,
                user_agent=USER_AGENT)

            request.session['google_auth_token'] = auth_token
            authorize_url = auth_token.generate_authorize_url(redirect_uri=settings.GOOGLE_APPLICATION_REDIRECT_URI)
            return redirect(authorize_url)
        return render(request, 'invite_google_contacts.html', {})


class ImportGoogleContacts(View):
    """ import google contacts.."""

    def get(self, request, *args, **kwargs):

        if request.is_ajax():
            feed = request.session.get('feed')
            gd_client = request.session.get('gd_client')
            gd_client = gdata.contacts.client.ContactsClient()

            contacts = process_feed(feed, gd_client, request)
            res = {
                'result': 'ok',
                'contacts': contacts,
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            auth_token = request.session.get('google_auth_token')
            code = request.GET.get('code')
            if code and auth_token:
                # Set the redirect url on the token
                auth_token.redirect_uri = settings.GOOGLE_APPLICATION_REDIRECT_URI

                # Generate the access token
                auth_token.get_access_token(code)

                request.session['google_auth_token'] = auth_token

                # Populate a session variable indicating successful authentication
                request.session[settings.GOOGLE_COOKIE_CONSENT] = code

                # Redirect to your base page
                # return redirect(request.session.get(settings.GOOGLE_REDIRECT_SESSION_VAR))
            if request.session.get(settings.GOOGLE_COOKIE_CONSENT):
                # Create a data client, in this case for the Contacts API
                gd_client = gdata.contacts.client.ContactsClient()

                # Authorize it with your authentication token
                auth_token.authorize(gd_client)

                # Get the data feed
                feed = gd_client.GetContacts()
                contacts_groupinit = []
                contacts = []
                while feed:
                    entries = feed.entry
                    for entry in entries:
                        for email in entry.email:
                            if entry.title.text:
                                cont = {}
                                cont['name'] = entry.title.text
                                if email.primary:
                                    cont['email'] = email.address
                                    friend_user = None
                                    try:
                                        friend_user = User.objects.get(email=email.address)
                                        friends = friend_user.friends_set.all()[0].friends
                                        if friend_user not in friends:
                                            contacts.append(cont)
                                    except:
                                        if friend_user:
                                            if friend_user.email != request.user.email:
                                                cont['user_id'] = friend_user.id
                                                contacts_groupinit.append(cont)
                                        else:
                                            contacts.append(cont)
                    if feed.get_next_link():
                        feed = gd_client.GetContacts(uri=feed.get_next_link().href)

                    else:
                        feed = None

            return render(request, 'invite_google_contacts.html', {
                'contacts': contacts,
                'contacts_groupinit': contacts_groupinit,
            })

    def PrintPaginatedFeed(self, feed, gd_client):
        """ Print all pages of a paginated feed.

        This will iterate through a paginated feed, requesting each page and
        printing the entries contained therein.

        Args:
          feed: A gdata.contacts.ContactsFeed instance.
          print_method: The method which will be used to print each page of the
              feed. Must accept these two named arguments:
                  feed: A gdata.contacts.ContactsFeed instance.
                  ctr: [int] The number of entries in this feed previously
                      printed. This allows continuous entry numbers when paging
                      through a feed.
        """
        contacts = []
        while feed:

            entries = feed.entry
            for entry in entries:
                for email in entry.email:
                    cont = {}
                    cont['name'] = entry.title.text
                    if email.primary:
                        cont['email'] = email.address
                        contacts.append(cont)
            # Prepare for next feed iteration
            next = feed.GetNextLink()
            if next:
                feed = gd_client.GetContacts(uri=next.href)
            else:
                feed = None
        return contacts


from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required

from contact_importer.decorators import get_contacts


@csrf_exempt  # Windows Live returns POST request
@login_required
@get_contacts
def import_contacts(request, contact_provider):
    contacts = contact_provider.get_contact_list()
    return render_to_response('contact_list.html', {
        'contacts': contacts,
    }, context_instance=RequestContext(request))


def import_outlook_contacts(request):
    """import outlook contacts"""

    import win32com.client
    import pywintypes

    o = win32com.client.Dispatch("Outlook.Application")
    ns = o.GetNamespace("MAPI")
    profile = ns.Folders.Item("Profile Name")
    out_contacts = profile.Folders.Item("Contacts")


from django.core.mail import EmailMultiAlternatives


class Invite(View):
    """send invitation"""

    def get(self, request, *args, **kwargs):

        email = request.GET['email']
        try:
            user = request.user
            subject = " Inivation - Groupinit "
            from_email = settings.DEFAULT_FROM_EMAIL
            invitation_token = InvitationToken(user=user, recipient_email=email)
            invitation_token.save()
            text_content = user.first_name + " " + user.last_name + "has sent you an invite from  <a href='" + settings.SITE_ROOT + "?inivitation=" + invitation_token.invitation_token + "'>Groupinit</a>"

            html_content = '<p>' + user.first_name + " " + user.last_name + "has sent you an invite from  <a href='" + settings.SITE_ROOT + "?inivitation=" + invitation_token.invitation_token + "'>Groupinit</a>" + '.</p>'
            msg = EmailMultiAlternatives(subject, text_content, from_email, [email])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            res = {
                'result': 'Success',
                'message': 'Email sent',
            }
        except Exception as e:
            res = {
                'message': 'Some thing went wrong',
                'result': str(e),
            }
        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        return render(request, 'reset_password.html', {})


class SendInvitation(View):
    def post(self, request, *args, **kwargs):
        mailid = request.POST['mail_id']
        group_id = request.POST.get('group_id', None)
        box_id = request.session.get('box_id', None)
        profile = request.user
        email_to = mailid
        try:
            user = User.objects.get(username=email_to)
            res = {
                'result': 'ok',
                'message': 'User Already Exists',
                'mail_id': mailid
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        except:
            pass
        user, created = User.objects.get_or_create(email=email_to)
        if created:
            user.first_name = email_to.split('@')[0]
            user.username = email_to
            password = 'groupinit123'
            user.set_password(password)

            try:
                group_id = int(group_id) if not group_id == None else None
                box_id = int(box_id) if not box_id == None else None
                user.is_active = False
                user.save()
                user_profile = UserProfile.objects.create(user=user)

                names = ['Friends', 'Family', 'Work', 'Public Groups', 'Discussions']
                colors = ['#F65B42', '#72D668', '#4182B6', '#D1AB4A', '#B741A7']
                type = ['box', 'box', 'box', 'public group', 'discussion']
                for x in range(0, 5):
                    box = FriendsBox.objects.create(owner=user, name=names[x], color=colors[x], order=x + 1, box_type=type[x])

                random_key = RandomKey.objects.create(user=user)
                random_key.key_created = datetime.now()
                random_key.save()

                tokens = Token.objects.create(user=user, password_token=password)
                token = tokens.token
                expire_time = tokens.token_created + timedelta(days=10)
                email_to = mailid
                subject = "  Groupinit Membership "
                from_email = settings.DEFAULT_FROM_EMAIL
                message1 = " Please activate your account by clicking the following link " + settings.SITE_ROOT + "login/?inivitation=" + tokens.token
                message = "\n\nHi " + mailid + "\n\n\n please use the following credentials to login your account\
                 \n\n " + settings.SITE_ROOT + " \n\nUser name :" + email_to + " \n\npaasword:groupinit123 \n\n\n" + message1 + "\n\n Thanks and Regards \n " + from_email
                send_mail(subject, message, from_email, [email_to])
                if Request.can_send(from_user=profile, to_user=user, group_id=group_id):
                    f_request = Request.create(from_user=profile, to_user=user)
                    f_request.applied()
                    if group_id and box_id:
                        box = Box.get(box_id)
                        grp = Grp(group_id).get()
                        f_request.add_invitor_box(box)
                        f_request.add_group(grp)
                res = {
                    'result': "ok",
                    'message': 'Mail Sent successfully',
                    'mail_id': mailid,
                    'user': user.profile.get_json()
                    # 'request': friend_request.get_json(),
                }

            except Exception as Ex:
                res = {
                    'result': 'error',
                    'message': str(Ex),
                    'mail_id': mailid
                }

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class LoadPofileSettings(View):
    def get(self, request, *args, **kwargs):
        try:
            user = UserProfile.objects.get(user=request.user)
            user_json = user.get_json()
            usersettings = ProfileSettings.objects.get(user=request.user)
            user_info = {}
            user_info['id'] = user.id
            user_info['name'] = user.user.first_name
            user_info['location'] = user.location.location_address if user.location else ""
            user_info['visibility'] = usersettings.profile_visibility
            user_info['profile_pic'] = user_json['profile_pic']
            user_info['fav_pic'] = user_json['fav_pic'] if not user_json['fav_pic'] == "" else "/static/images/pictures/pic-85.jpg"
            boxes_info = []
            boxes = Box(request.user).get_all()

            mem = []
            friends = []
            for box in boxes:
                box_info = {}
                box_info['id'] = box.id
                box_info['name'] = box.name
                box_info['color'] = box.color
                box_info['profile_pic'] = {'id': box.hub_pic.id, 'pic': box.hub_pic.file.url} if box.hub_pic else {'id': '',
                                                                                                                   'pic': '/static/images/pictures/avatar.jpg'}
                box_info['comic_pic'] = box.comic_pic if not box.comic_pic in [None, ''] else '/static/images/pictures/pic-104.png'
                box_info['status'] = box.status
                box_info['status_pic'] = box.status_pic
                boxes_info.append(box_info)

                for grp in box.groups.all():
                    for members in grp.members.all():
                        if not members.id in mem:
                            mem.append(members.id)
                            user = members.profile.get_json()
                            if not user['id'] == request.user.id:
                                data = {}
                                data['id'] = user['id']
                                data['profile_pic'] = user['profile_pic']
                                data['name'] = user['name']
                                data['location'] = user['location'].get('location_address', "") if user['location'] else ""
                                data['fav_pic'] = user['fav_pic'] if not user[
                                                                             'fav_pic'] == "" else "/static/images/pictures/pic-85.jpg"
                                friends.append(data)
            request.session['lastfavid'] = friends[1]['id'] if not len(friends) < 2 else None
            res = {
                'result': 'ok',
                'fav': friends if len(friends) < 2 else friends[:2],
                'box_info': boxes_info,
                'user_info': user_info
            }
        except Exception as e:
            print e, "dddddddd"
            res = {'result': 'error', 'msg': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class LoadNextFavPic(View):
    def get(self, request, *args, **kwargs):
        try:
            boxes = Box(request.user).get_all()
            last_user_id = request.GET.get('last_user_id', request.session['lastfavid'])
            mem = []
            flag = False
            fav = []
            count = 0
            for box in boxes:
                for grp in box.groups.all():
                    for members in grp.members.all():
                        if not members.id in mem:
                            mem.append(members.id)
                            user = members.profile.get_json()
                            if not user['id'] == request.user.id:
                                if flag and count < 2:
                                    count = count + 1
                                    data = {}
                                    data['id'] = user['id']
                                    data['profile_pic'] = user['profile_pic']
                                    data['name'] = user['name']
                                    data['location'] = user['location'].get('location_address', "") if user['location'] else ""
                                    data['fav_pic'] = user['fav_pic'] if not user[
                                                                                 'fav_pic'] == "" else "/static/images/pictures/pic-85.jpg"
                                    fav.append(data)
                                if int(last_user_id) == user['id']:
                                    flag = True
            request.session['lastfavid'] = fav[-1]['id'] if not len(fav) == 0 else None
            print fav
            print request.session['lastfavid']
            res = {'result': 'ok', 'next_user': fav}
        except Exception as e:
            print e, "ddd"
            res = {'result': 'error', 'next_user': []}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class AddFavoritePic(View):
    def post(self, request, *args, **kwargs):
        try:
            fav_pic_id = int(request.POST['pic_id'])
            pic = Picture.objects.get(id=fav_pic_id)
            user = UserProfile.objects.get(user=request.user)
            user.fav_pic = pic
            user.save()
            res = {"result": 'ok', 'fav': pic.file.url}
        except Exception as e:
            res = {"result": 'error', 'fav': str(e)}

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class AddUserSettings(View):
    def post(self, request, *args, **kwargs):
        try:
            newpassword = request.POST.get('newpassword', "")
            repassword = request.POST.get('repassword', "")
            name = request.POST.get('name', "")
            visibility = request.POST.get('visibility', "")
            location = simplejson.loads(request.POST.get('location', '{}'))
            user = User.objects.get(id=request.user.id)
            msg = ""
            if not newpassword == "" and not repassword == "":
                if newpassword == repassword:
                    user.set_password(str(newpassword))
                    token = Token.objects.get(user=user)
                    token.password_token = ""
                    token.save()
                else:
                    msg = "Password mismatch.."
            if not name == "":
                user.first_name = name
                user.save()
            if not visibility == "":
                settings = ProfileSettings.objects.get(user=request.user)
                settings.profile_visibility = visibility
                settings.save()
            if not location == {}:
                userprofile = UserProfile.objects.get(user=request.user)
                loc = LocationPoints.objects.create(traveller=request.user)
                lat = float(location['geometry']['location']['lat'])
                lon = float(location['geometry']['location']['lng'])
                location_address = str(location['name'])
                loc.location_point = GEOSGeometry('POINT(%f %f)' % (lat, lon))
                loc.location_address = location_address
                loc.save()
                userprofile.location = loc
                userprofile.save()
            res = {"result": 'ok', 'msg': msg}
        except Exception as msg:
            print msg
            res = {"result": 'error', 'msg': str(msg)}

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('groupbox', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LocationPoints',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location_point', django.contrib.gis.db.models.fields.PointField(help_text=b'Travel location map coordinates', srid=4326, null=True, verbose_name=b'Location Point')),
                ('location_address', models.TextField(null=True, verbose_name=b'Location Address', blank=True)),
                ('traveller', models.ForeignKey(related_name='traveller', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('post_type', models.CharField(default=b'general', max_length=20, verbose_name=b'Post type', choices=[(b'general', b'general'), (b'media', b'media'), (b'travel', b'travel'), (b'lifetime', b'lifetime'), (b'recommendation', b'recommendation'), (b'shared', b'shared')])),
                ('created_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Created date')),
                ('description', models.TextField(null=True, verbose_name=b'Description', blank=True)),
                ('media_file', models.FileField(null=True, upload_to=b'/home/ranjith/groupenv/groupinit-new/groupinit_new/groupinit/media', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MediaPost',
            fields=[
                ('post_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='posts.Post')),
                ('genre', models.TextField(null=True, verbose_name=b'Genre', blank=True)),
                ('year', models.TextField(null=True, verbose_name=b'Year', blank=True)),
                ('director', models.TextField(null=True, verbose_name=b'Director', blank=True)),
                ('stars', models.TextField(null=True, verbose_name=b'Stars', blank=True)),
                ('plot', models.TextField(max_length=1000, null=True, verbose_name=b'Plot', blank=True)),
                ('title', models.CharField(max_length=100, null=True, verbose_name=b'title', blank=True)),
                ('media_type', models.CharField(default=b'video', max_length=20, verbose_name=b'Media type', choices=[(b'video', b'video'), (b'audio', b'audio')])),
                ('poster', models.ForeignKey(related_name='poster', blank=True, to='albums.Picture', null=True)),
            ],
            options={
            },
            bases=('posts.post',),
        ),
        migrations.CreateModel(
            name='LifeTimePost',
            fields=[
                ('post_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='posts.Post')),
                ('title', models.CharField(max_length=100, null=True, verbose_name=b'title', blank=True)),
                ('cover_photo', models.ForeignKey(related_name='cover_photo', blank=True, to='albums.Picture', null=True)),
            ],
            options={
            },
            bases=('posts.post',),
        ),
        migrations.CreateModel(
            name='TravelPost',
            fields=[
                ('post_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='posts.Post')),
                ('locations', models.ManyToManyField(related_name='locations', null=True, to='posts.LocationPoints', blank=True)),
            ],
            options={
            },
            bases=('posts.post',),
        ),
        migrations.CreateModel(
            name='Url',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=150, null=True, verbose_name=b'Url', blank=True)),
                ('base_url', models.CharField(max_length=150, null=True, verbose_name=b'Base url', blank=True)),
                ('favicon', models.CharField(max_length=150, null=True, verbose_name=b'Favicon', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='post',
            name='attachments',
            field=models.ManyToManyField(related_name='attachments', null=True, to='albums.Picture', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='box',
            field=models.ManyToManyField(to='groupbox.FriendsBox', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='group',
            field=models.ManyToManyField(to='groupbox.Group', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='origin_post',
            field=models.ForeignKey(blank=True, to='posts.Post', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='post_creator',
            field=models.ForeignKey(related_name='post_creator', to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='urls',
            field=models.ManyToManyField(related_name='urls', null=True, to='posts.Url', blank=True),
            preserve_default=True,
        ),
    ]

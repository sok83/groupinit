# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('groupbox', '0001_initial'),
        ('posts', '0002_auto_20160730_0921'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='created_box',
            field=models.ForeignKey(related_name='created_box', blank=True, to='groupbox.FriendsBox', null=True),
            preserve_default=True,
        ),
    ]

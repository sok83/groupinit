# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('albums', '0001_initial'),
        ('posts', '0003_post_created_box'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationpoints',
            name='images',
            field=models.ManyToManyField(related_name='images', null=True, to='albums.Picture', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='locationpoints',
            name='videos',
            field=models.ManyToManyField(related_name='videos', null=True, to='albums.Picture', blank=True),
            preserve_default=True,
        ),
    ]

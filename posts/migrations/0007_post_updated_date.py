# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0006_auto_20160924_1033'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='updated_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'Updated date', null=True),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0005_auto_20160924_1017'),
    ]

    operations = [
        migrations.AlterField(
            model_name='discussionpost',
            name='category',
            field=models.IntegerField(blank=True, max_length=20, null=True, verbose_name=b'Category', choices=[(1, b'Auto / Cars / Bikes'), (2, b'Artists / Perfoming Art'), (3, b'Buy / Sell'), (4, b'Food / Restaurants / Cooking'), (5, b'Music / Movies'), (6, b'Media / Politics / News'), (7, b'People / Celebrities / Businesses'), (8, b'Reading / Writing / Painting'), (9, b'Sports / Games / Outdoor'), (10, b'Shopping / Fashion'), (11, b'Tourists / Places'), (12, b'Web / Net / Apps'), (13, b'Technology / Gadgets')]),

        ),
        migrations.AlterField(
            model_name='discussionpost',
            name='discussion_type',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name=b'discussion_type', choices=[(b'public', b'public'), (b'private', b'private')]),

        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0004_auto_20160901_1101'),
    ]

    operations = [
        migrations.CreateModel(
            name='DiscussionPost',
            fields=[
                ('post_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='posts.Post')),
                ('discussion_type', models.CharField(max_length=20, verbose_name=b'discussion_type', choices=[(b'public', b'public'), (b'private', b'private')])),
                ('category', models.IntegerField(max_length=20, verbose_name=b'Category', choices=[(1, b'Auto / Cars / Bikes'), (2, b'Artists / Perfoming Art'), (3, b'Buy / Sell'), (4, b'Food / Restaurants / Cooking'), (5, b'Music / Movies'), (6, b'Media / Politics / News'), (7, b'People / Celebrities / Businesses'), (8, b'Reading / Writing / Painting'), (9, b'Sports / Games / Outdoor'), (10, b'Shopping / Fashion'), (11, b'Tourists / Places'), (12, b'Web / Net / Apps'), (13, b'Technology / Gadgets')])),
                ('locations', models.ManyToManyField(related_name='location', null=True, to='posts.LocationPoints', blank=True)),
            ],
            options={
            },
            bases=('posts.post',),
        ),
        migrations.AlterField(
            model_name='post',
            name='post_type',
            field=models.CharField(default=b'general', max_length=20, verbose_name=b'Post type', choices=[(b'general', b'general'), (b'media', b'media'), (b'travel', b'travel'), (b'lifetime', b'lifetime'), (b'recommendation', b'recommendation'), (b'discussion', b'discussion'), (b'shared', b'shared')]),

        ),
    ]

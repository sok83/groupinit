
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import  (RemoveParticipants,GetAlbumImages,AddFavouriteName,AddFavourite,LoadDiscussion,LoadParticipants, PostView,DiscussionView,TravelPostView,PostCommentReply,PostDeleteCommentReply,LoadLikes,\
     PostLike,AddTag,RemoveTag,LifetimePostView,PostReshareView,PostAddComments,GetPost,\
     DeletePost,MediaPostView,PostDeleteComment,EditPost,LoadGroupPosts,LoadTravelPost,LoadLifeTimePost,LoadMediaPost,LoadComments,\
     GetBox,GetGroups,TestApi,GetTravelPost,GetLifeTimePost,GetMediaPost,GroupPost,LoadBox,LoadMorePost,RefreshPanel,Mute,GetAlbumComic,GetAlbumStatusImages)


urlpatterns = patterns('',

    url(r"^posts/$", login_required(PostView.as_view(), login_url='/login/'), name="posts"),
    url(r"^travel_posts/$", login_required(TravelPostView.as_view(), login_url='/login/'), name="travel_posts"),
    url(r"^lifetime_posts/$", login_required(LifetimePostView.as_view(), login_url='/login/'), name="lifetime_posts"),
    url(r"^media_posts/$", login_required(MediaPostView.as_view(), login_url='/login/'), name="media_posts"),
    url(r"^discussion_posts/$", login_required(DiscussionView.as_view(), login_url='/login/'), name="discussion_posts"),
    url(r"^add_tag/(?P<post_id>\d+)/$", login_required(AddTag.as_view(), login_url='/login/'), name="add_tag"),
    url(r"^remove_tag/(?P<post_id>\d+)/$", login_required(RemoveTag.as_view(), login_url='/login/'), name="remove_tag"),
    url(r"^post_resharing/$", login_required(PostReshareView.as_view(), login_url='/login/'), name="post_resharing"),
    url(r"^post_comments/$", login_required(PostAddComments.as_view(), login_url='/login/'), name="post_comments"),
    url(r"^delete_comments/$", login_required(PostDeleteComment.as_view(), login_url='/login/'), name="delete_comments"),
    url(r"^post_like/$", login_required(PostLike.as_view(), login_url='/login/'), name="post_like"),
    url(r"^post_reply/$", login_required(PostCommentReply.as_view(), login_url='/login/'), name="post_reply"),
    url(r"^delete_post_reply/$", login_required(PostDeleteCommentReply.as_view(), login_url='/login/'), name="delete_post_reply"),
    url(r"^delete_posts/$", login_required(DeletePost.as_view(), login_url='/login/'), name="delete_posts"),
    url(r"^edit_posts/$", login_required(EditPost.as_view(), login_url='/login/'), name="edit_posts"),
    url(r"^load_travel_post/$", login_required(LoadTravelPost.as_view(), login_url='/login/'), name="load_travel_post"),
    url(r"^load_lifetime_post/$", login_required(LoadLifeTimePost.as_view(), login_url='/login/'), name="load_lifetime_post"),
    url(r"^load_media_post/$", login_required(LoadMediaPost.as_view(), login_url='/login/'), name="load_media_post"),
    url(r"^load_group_post/$", login_required(LoadGroupPosts.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^load_group_comments/$", login_required(LoadComments.as_view(), login_url='/login/'), name="load_group_comments"),
    url(r"^load_liked_friends/$", login_required(LoadLikes.as_view(), login_url='/login/'), name="load_liked_friends"),

    url(r"^get_box_group/$", login_required(GetBox.as_view(), login_url='/login/'), name="get_box"),
    url(r"^get_groups/(?P<box_id>\d+)/$", login_required(GetGroups.as_view(), login_url='/login/'), name="get_groups"),
    url(r"^testapi/$", login_required(TestApi.as_view(), login_url='/login/'), name="get_friends"),

    url(r"^api/travel_post/load/$", login_required(GetTravelPost.as_view(), login_url='/login/'), name="load_travel_post"),
    url(r"^api/lifetime_post/load/$", login_required(GetLifeTimePost.as_view(), login_url='/login/'), name="load_lifetime_post"),
    url(r"^api/media_post/load/$", login_required(GetMediaPost.as_view(), login_url='/login/'), name="load_media_post"),

    url(r"^api/group_post/load/$", login_required(GroupPost.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^api/box/load/$", login_required(LoadBox.as_view(), login_url='/login/'), name="load_lifetime_post"),
    url(r"^api/more_posts/$", login_required(LoadMorePost.as_view(), login_url='/login/'), name="load_media_post"),
    url(r"^api/refresh_panel/$", login_required(RefreshPanel.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^api/mute/$", login_required(Mute.as_view(), login_url='/login/'), name="load_group_post"),

    url(r"^api/discussion/participants/$", login_required(LoadParticipants.as_view(), login_url='/login/'), name="load_participants"),
    url(r"^api/participants/remove/$", login_required(RemoveParticipants.as_view(), login_url='/login/'), name="remove_participants"),
    url(r"^api/box/discussion/$", login_required(LoadDiscussion.as_view(), login_url='/login/'), name="load_discussion_post"),
    url(r"^api/favourite/add/$", login_required(AddFavourite.as_view(), login_url='/login/'), name="load_discussion_post"),
    url(r"^api/favourite/name/$", login_required(AddFavouriteName.as_view(), login_url='/login/'), name="load_discussion_post"),

    url(r"^api/load/images/$", login_required(GetAlbumImages.as_view(), login_url='/login/'), name="load_album_images"),
    url(r"^api/load/comic/$", login_required(GetAlbumComic.as_view(), login_url='/login/'), name="load_album_comic"),
    url(r"^api/load/statusimages/$", login_required(GetAlbumStatusImages.as_view(), login_url='/login/'), name="load_album_comic"),
    url(r"^api/load/post/$", login_required(GetPost.as_view(), login_url='/login/'), name="load_post"),

)

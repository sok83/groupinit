# from posts.models import Post,MediaPost,TravelPost,LifeTimePost
# from groupbox.models import Grp
# from authentication.utils import get_db
# from django.contrib.auth.models import User
#
# class ViewPosts:
#
#     def __init__(self,user_id):
#         self.user_id=user_id
#         self.model_map = {'shared': Post,'general': Post, 'media': MediaPost, 'travel': TravelPost, 'lifetime': LifeTimePost}
#         self.frnd_post= FriendsPost(self.user_id)
#         self.grouppost=self.frnd_post.get_grouppost()
#         self.mute_user=self.frnd_post.get_mute()
#
#     def filter_post(self,resultList):
#         list=[Post.objects.filter(post_creator_id=user_id) for user_id in self.mute_user]
#         res=[]
#         for posts in list:
#             for post in posts:
#                 res.append(post.id)
#         for post_id in resultList:
#             try:
#                 pst=Post.objects.get(id=post_id)
#             except:
#                 resultList.remove(post_id)
#         result=set(resultList)-set(res)
#         return result
#
#     def limit_post(self,posts,last_post,offset):
#         result = sorted(posts, key=lambda x: x['created_date'], reverse=True)
#         index= 0 if last_post==None else result.index((item for item in result if item["id"] == last_post).next())+1
#         if type(offset)==int:
#             result = result[index:index+offset] if result[index:index+offset] else result[index:]
#         else:
#             result = result[index:]
#         return result
#
#     def box(self,box_id,last_post=None,offset=None):
#         resultList=[]
#         result=[]
#         posts=[]
#         try:
#             for grp in Grp.get_by_box(box_id):
#                 if str(grp.id) in self.grouppost.keys():
#                     resultList= list(set(resultList)|set(self.grouppost[str(grp.id)]))
#             resultList =self.filter_post(resultList)
#             for post_id in resultList:
#                 try:
#                     posts.append(self.model_map[Post.objects.get(id=post_id).post_type].objects.get(id=post_id).get_json())
#                 except:
#                     pass
#             result = self.limit_post(posts,last_post,offset)
#         except Exception as e:
#             print "ssss",e
#         return result
#
#     def group(self,group_id,last_post=None,offset=None):
#         result=[]
#         if str(group_id) in self.grouppost.keys():
#             resultList= self.grouppost[str(group_id)]
#             resultList =self.filter_post(resultList)
#             for post_id in resultList:
#                 try:
#                     result.append(self.model_map[Post.objects.get(id=post_id).post_type].objects.get(id=post_id).get_json())
#                 except:
#                     pass
#             result = self.limit_post(result,last_post,offset)
#         return result
#
#     def member(self,group_id,member_id,last_post=None,offset=None):
#         result=[]
#         if str(group_id) in self.grouppost.keys():
#             resultList = self.grouppost[str(group_id)]
#             resultList =self.filter_post(resultList)
#             for post_id in resultList:
#                 try:
#                     result.append(self.model_map[Post.objects.filter(id=post_id,post_creator_id=member_id)[0].post_type].objects.get(id=post_id).get_json())
#                 except:
#                     pass
#             result = self.limit_post(result,last_post,offset)
#         return result
#
#     def type(self,box_id,type,last_post=None,offset=None):
#         resultList=[]
#         result=[]
#         for grp in Grp.get_by_box(box_id):
#             if str(grp.id) in self.grouppost.keys():
#                 resultList= list(set(resultList)|set(self.grouppost[str(grp.id)]))
#         resultList =self.filter_post(resultList)
#         for post_id in resultList:
#             try:
#                 model=self.model_map[type]
#                 result.append(model.objects.get(id=post_id).get_json())
#             except:
#                 pass
#         result = self.limit_post(result,last_post,offset)
#         return result
#
#
# class FriendsPost:
#     def __init__(self,user_id):
#         self.user_id=user_id
#         con = get_db()
#         self.userposts = con.UserPosts
#         self.userpost = self.userposts.find_one({"user_id": self.user_id})
#         if not self.userpost:
#             self.userposts.insert({"user_id": self.user_id, "grouppost": [{}], "mute": [],"unread_post":[],"unread_comments":[]})
#             self.userpost = self.userposts.find_one({"user_id": self.user_id})
#
#     def get(self):
#         return self.userpost
#
#     def append_group_post(self,grp_id,post):
#         grp_id=str(grp_id)
#         post_id=post.id
#         try:
#             grplist=self.userpost["grouppost"][0]
#             mute=self.userpost.get("mute",[])
#             unread_post=self.userpost.get("unread_post",[])
#             unread_comments=self.userpost.get("unread_comments",[])
#             if grp_id in grplist.keys():
#                 if post_id not in grplist[grp_id]:
#                     grplist[grp_id].append(post_id)
#             else:
#                 grplist[grp_id]=[post_id]
#             self.userposts.update({"user_id": self.user_id}, {"user_id": self.user_id,
#                                                               "mute": mute,
#                                                               "unread_post":unread_post,
#                                                               "unread_comments":unread_comments,
#                                                               "grouppost": [grplist]})
#             Notify(self.user_id).add_post(post_id)
#             return True
#         except Exception as e:
#             print "exxx", e
#             return False
#
#     def mute(self, user_id):
#         try:
#             user_id=int(user_id)
#             if user_id not in self.userpost["mute"]:
#                 self.userposts.update({"user_id": self.user_id}, {"$addToSet": {"mute": user_id}}, upsert=True)
#                 print self.userposts.find_one({"user_id": self.user_id})
#                 return True
#             else:
#                 self.userposts.update({"user_id": self.user_id}, {"$pull": {"mute": user_id}}, upsert=True)
#                 print self.userposts.find_one({"user_id": self.user_id})
#                 return False
#         except Exception as e:
#             return None
#
#     def get_mute_user(self, user_id):
#         mute= self.userpost.get("mute", [])
#         if int(user_id) in mute:
#             return "Unmute"
#         else:
#             return 'Mute'
#
#     def get_mute(self):
#         mute= self.userpost.get("mute", [])
#         return mute
#
#     def get_grouppost(self):
#         return self.userpost.get("grouppost", [{}])[0]
#
#     def get_all_post(self,box_id):
#         resultList=[]
#         for grp in Grp.get_by_box(box_id):
#             grp_post=self.get_grouppost()
#             if str(grp.id) in grp_post.keys():
#                 resultList= list(set(resultList)|set(grp_post[str(grp.id)]))
#         return resultList
#
#
#
# class Notify:
#     def __init__(self, user_id):
#         self.user_id=user_id
#         con = get_db()
#         self.userposts = con.UserPosts
#         self.userpost = self.userposts.find_one({"user_id": self.user_id})
#
#     def add_post(self, post_id):
#         try:
#             if post_id not in self.userpost["unread_post"]:
#                 self.userposts.update({"user_id": self.user_id}, {"$addToSet": {"unread_post": post_id}}, upsert=True)
#             return True
#         except Exception as e:
#             return False
#
#     def remove_post(self, post_id):
#         try:
#             if post_id in self.userpost["unread_post"]:
#                 self.userposts.update({"user_id": self.user_id}, {"$pull": {"unread_post": post_id}}, upsert=True)
#             return True
#         except Exception as e:
#             return False
#
#     def box_unread_post(self,box_id):
#         unread= self.userpost["unread_post"]
#         box_post=FriendsPost(self.user_id).get_all_post(box_id=box_id)
#         box_unread=[post for post in unread if post in box_post]
#         return box_unread
#
#     def group_unread_post(self,group_id):
#         unread= self.userpost["unread_post"]
#         group_post=FriendsPost(self.user_id).get_grouppost().get(str(group_id),[])
#         group_unread=[post for post in unread if post in group_post]
#         return group_unread
#
#     def friend_unread_post(self,group_id,friend_id):
#         group_unread=self.group_unread_post(group_id)
#         friend_unread=[]
#         friend=User.objects.get(id=int(friend_id))
#         print friend
#         for post_id in group_unread:
#             try:
#                 post=Post.objects.get(id=int(post_id),post_creator=friend)
#                 if post:
#                     friend_unread.append(post.id)
#             except Exception as e:
#                 print e
#         return friend_unread
#
#     def unread_post_category(self,box_id,post_type):
#         box_unread=self.box_unread_post(box_id)
#         category_unread=[]
#         for post_id in box_unread:
#             try:
#                 post=Post.objects.get(id=int(post_id))
#                 if post.post_type==post_type:
#                     category_unread.append(post.id)
#             except Exception as e:
#                 print e
#         return category_unread
#
#     def add_comments(self, post_id=None, cmnd_id=None):
#         try:
#             return True
#         except Exception as e:
#             return False
#
#     def remove_comments(self, post_id=None, cmnd_id=None):
#         try:
#             return True
#         except Exception as e:
#             return False
#
#     def unread_comments(self,post_id):
#         try:
#             return True
#         except Exception as e:
#             return False
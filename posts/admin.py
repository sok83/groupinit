from django.contrib import admin

from models import Post, TravelPost, LifeTimePost,LocationPoints,MediaPost

admin.site.register(LocationPoints)
admin.site.register(Post)
admin.site.register(TravelPost)
admin.site.register(LifeTimePost)
admin.site.register(MediaPost)

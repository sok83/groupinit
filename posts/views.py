import json as simplejson
import os
from django.views.generic import View
from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.gis.geos import GEOSGeometry

from groupbox.models import FriendsBox, Group, FriendRequest, Grp, Box, NewRequest
from posts.models import DISCUSSION_CATEGORY, Discussion, Post, DiscussionPost, TravelPost, LifeTimePost, LocationPoints, MediaPost, \
    Url, Audience, ViewPosts, CommentsReply, FriendsPost, Notify, PostAlbum, fetch_attachments
from albums.models import Picture
from authentication.utils import *
from authentication.models import UserProfile, User
from announce import AnnounceClient
from datetime import datetime
from bson.objectid import ObjectId

import urllib
from django.core.files import File

announce_client = AnnounceClient()

import post_utils


def add_url(description, post):
    urls = post_utils.find_all_url(str(description))
    for url in urls:
        base_url = post_utils.get_base_url(url)
        favicon = post_utils.find_shortcut_favicon(base_url)
        ur = Url.objects.create(url=url, base_url=base_url, favicon=favicon)
        ur.save()
        post.urls.add(ur)


"""Audience popup"""


class Visibility:
    def __init__(self, post, user):
        self.post = post
        self.user = user

    def build(self, whocansee=None):
        print "build", whocansee
        FriendsPost(self.user.id).append_group_post(0, self.post)
        if whocansee == None:
            for box in Box(self.user).get_all():
                if box.box_type == 'box':
                    for grp in Grp.get_by_box(box.id):
                        for members in grp.members.all():
                            FriendsPost(members.id).append_group_post(grp.id, self.post)
                    self.post.box.add(box)
            self.post.save()
        else:
            for box_id in whocansee['boxes']:
                for grp in Grp.get_by_box(int(box_id)):
                    for members in grp.members.all():
                        FriendsPost(members.id).append_group_post(grp.id, self.post)
                self.post.box.add(Box.get(int(box_id)))
                print "all groups in ", box_id

            for grp_id in whocansee["groups"]:
                grp = Grp(int(grp_id)).get()
                for members in grp.members.all():
                    FriendsPost(members.id).append_group_post(grp.id, self.post)
                self.post.group.add(grp)
                print "all members in ", grp
            members = whocansee["members"]
            for grp_id, user_list in members.iteritems():
                FriendsPost(self.user.id).append_group_post(int(grp_id), self.post)
                for user_id in user_list:
                    added = FriendsPost(user_id).append_group_post(int(grp_id), self.post)
                    if added:
                        try:
                            mem, c = Audience.objects.get_or_create(group=Grp(int(grp_id)).get(),
                                                                    user=User.objects.get(id=int(user_id)))
                            #mem = Audience.objects.get(group=Grp(int(grp_id)).get(), user=User.objects.get(id=int(user_id)))
                            self.post.friends.add(mem)
                        except Exception as e:
                            print "audience", e
                        print "members: ", user_id, "in ", grp_id
        Notify(self.user.id).remove_post(self.post.id)
        return True


class GetPost(View):
    def get(self, request, *args, **kwargs):
        try:
            postid = request.GET.get('postid', '')
            model_map = {'discussion': DiscussionPost,'shared': Post, 'general': Post, 'media': MediaPost, 'travel': TravelPost, 'lifetime': LifeTimePost}
            post_obj = model_map[Post.objects.get(id=postid).post_type].objects.get(id=postid)

            post = post_obj.get_json()
            con = get_db()
            coll = con.PostAudienceComments
            tagged_audience = coll.find_one({"post_id": int(postid)})
            post['whocansee'] = tagged_audience['audience']
            post['tags'] = tagged_audience['tagged_audience']
            post['videos'] = fetch_attachments(post_obj.attachments, type='video')
            post['images'] = fetch_attachments(post_obj.attachments, type='image')
            post['can_edit'] = True if request.user == post_obj.post_creator else False
            response = simplejson.dumps(post)
            return HttpResponse(response, status=200)
        except Exception as e:
            print e


class PostView(View):
    def get(self, request, *args, **kwargs):
        posts = Post.objects.filter(post_creator=request.user)
        if request.is_ajax():
            res = {
                'posts': [x.get_json() for x in posts]
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', {
                'posts': posts
            })

    def post(self, request, *args, **kwargs):

        user = request.user
        box_id = request.session['box_id']
        """valdates fields for checking empty post """
        print len(request.POST.getlist('images[]'))
        if request.POST.get('description', '') == '' and len(request.POST.getlist('images[]')) == 0:
            res = {
                'result': 'error',
                'msg': 'post field is appears to be blank.Please write something or attach a link or photo to update your status.'
            }

        else:
            """creates a new post"""
            try:
                postid = request.POST.get('postid', '')
                post = Post.objects.get(id=postid, post_creator=request.user)
                action = 'edit'
            except:
                post=None
                res = {
                'result': 'error',
                'msg': 'Permission denied'
                }
            if request.POST.get('postid',"")=="":
                post = Post.objects.create(post_creator=request.user)
                action = 'create'

            if post:
                images = request.POST.getlist('images[]')
                post.post_type = request.POST['post_type']
                post.description = request.POST['description'] + " "
                post.created_box = Box.get(box_id)
                # code  for adding description url
                add_url(request.POST['description'], post)
                post.attachments.remove()
                if len(images) != 0:
                    for x in images:
                        img = Picture.objects.get(id=x)
                        img.status = "Success"
                        img.save()
                        post.attachments.add(img)

                videos = request.POST.getlist('videos[]')
                if len(videos) != 0:
                    for x in videos:
                        vid = Picture.objects.get(id=x)
                        vid.status = "Success"
                        vid.save()
                        post.attachments.add(vid)
                pst = post.save()
                print "saved"
                """set audience data"""
                try:
                    whocansee = simplejson.loads(request.POST['whocansee'])
                    rawtag = simplejson.loads(request.POST['rawtag'])
                    rawwcs = simplejson.loads(request.POST['rawwcs'])
                    tags = simplejson.loads(request.POST['tags'])
                except Exception as e:
                    whocansee = None
                    tags = []
                if whocansee['boxes'] == [] and whocansee['members'] == {} and whocansee['groups'] == []:
                    whocansee = None
                Visibility(post, request.user).build(whocansee=whocansee)

                unique_tags = {v['id']: v for v in tags}.values()
                for tag in unique_tags:
                    user = User.objects.get(id=int(tag['id']))
                    SharePost(user, post).add(description=post.description, whocansee=None)

                con = get_db()
                coll = con.PostAudienceComments
                print "post id ------->", post.id
                if action == 'create':
                    coll.insert({"post_id": post.id, 'like_minus': [], 'like': [], "like_plus": [],
                                 "audience": rawwcs, "tagged_audience": rawtag, "comments": [], "shares": []})
                else:
                    pass
                res = {
                    'result': 'ok',
                    'post': post.get_json(),
                    'msg': 'Post saved successfully'
                }
        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', res)


from socket import gethostname, gethostbyname


class DiscussionView(View):
    # def get(self, request, *args, **kwargs):
    #     posts = DiscussionPost.objects.filter(post_creator=request.user)
    #     if request.is_ajax():
    #         res = {
    #             'posts': [x.get_json() for x in posts]
    #         }
    #         response = simplejson.dumps(res)
    #         return HttpResponse(response, status=200,content_type="application/json")
    #     else:
    #         return render(request, 'posts.html', {
    #             'posts': posts
    #         })

    def post(self, request, *args, **kwargs):

        user = request.user
        print user, "xxsss"
        box_id = request.session['box_id']
        """valdates fields for checking empty post """
        print request.POST.get('category', "")
        if request.POST.get('description', '') == '' and request.POST.get('discussion_type', '') == '':
            res = {'result': 'error', 'msg': 'post field is appears to be blank.Please write something'}

        elif request.POST.get('discussion_type', '') == 'public' and request.POST.get('category', "") == "":
            res = {'result': 'error', 'msg': 'Please select a category'}

        else:
            """creates a new post"""
            try:
                postid = request.POST.get('postid', '')
                post = DiscussionPost.objects.get(id=postid, post_creator=request.user)
                action = 'edit'
            except:
                post=None
                res = {
                'result': 'error',
                'msg': 'Permission denied'
                }
            if request.POST.get('postid',"")=="":
                post = DiscussionPost.objects.create(post_creator=request.user)
                action = 'create'

            if post:
                if request.POST.get('discussion_type', '') == 'public':
                    ip = gethostbyname(gethostname())
                    urlFoLaction = "http://www.freegeoip.net/json/{0}".format('17.0.0.1')
                    locationInfo = simplejson.loads(urllib.urlopen(urlFoLaction).read())

                    '''this is the currect package to get location '''
                    # from geoip import geolite2
                    # match = geolite2.lookup(ip)
                    # print "ss",match

                    if not locationInfo['city'] == '':
                        loc = LocationPoints.objects.create(traveller=request.user)
                        lat = float(locationInfo['latitude'])
                        lon = float(locationInfo['longitude'])
                        location_address = str(locationInfo['city'])
                        loc.location_point = GEOSGeometry('POINT(%f %f)' % (lat, lon))
                        loc.location_address = location_address
                        loc.save()
                        post.locations.add(loc)
                    post.category = int(request.POST.get('category'))
                post.post_type = request.POST.get('post_type', 'discussion')
                post.discussion_type = request.POST.get('discussion_type', 'private')
                post.description = request.POST['description']
                post.created_box = Box.get(int(box_id))
                images = request.POST.getlist('images[]')
                post.attachments.remove()
                if len(images) != 0:
                    for x in images:
                        img = Picture.objects.get(id=x)
                        img.status = "Success"
                        img.save()
                        post.attachments.add(img)

                videos = request.POST.getlist('videos[]')
                if len(videos) != 0:
                    for x in videos:
                        vid = Picture.objects.get(id=x)
                        vid.status = "Success"
                        vid.save()
                        post.attachments.add(vid)
                post.save()
                print "saved"
                """set audience data"""
                try:
                    whocansee = simplejson.loads(request.POST['whocansee'])
                    rawtag = simplejson.loads(request.POST['rawtag'])
                    rawwcs = simplejson.loads(request.POST['rawwcs'])
                    tags = simplejson.loads(request.POST['tags'])
                    if whocansee.get('boxes') == [] and whocansee.get('members') == {} and whocansee.get('groups') == []:
                        whocansee = None
                except Exception as e:
                    whocansee = None
                    tags = []
                    rawtag = []
                    rawwcs = []

                Visibility(post, request.user).build(whocansee=whocansee)
                for tag in tags:
                    user = User.objects.get(id=int(tag['id']))
                    SharePost(user, post).add(description=post.description, whocansee=None)
                con = get_db()
                coll = con.PostAudienceComments
                print "post id ------->", post.id
                coll.insert({"post_id": post.id, 'like_minus': [], 'like': [], "like_plus": [],
                             "audience": rawwcs, "tagged_audience": rawtag, "comments": [], "shares": []})
                res = {
                    'result': 'ok',
                    'post': post.get_json(),
                    'msg': 'Post saved successfully'
                }
                print res
                # if not request.is_ajax():
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")
        # else:
        #     return render(request, 'posts.html', res)


class TravelPostView(View):
    def get(self, request, *args, **kwargs):
        travel_posts = TravelPost.objects.filter(post_creator=request.user)

        if request.is_ajax():
            res = {
                'travel_posts': [x.get_json() for x in travel_posts]
            }

            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, content_type="application/json")
        else:
            return render(request, 'posts.html', {

                'travel_posts': [x.get_json() for x in travel_posts]

            })

    def post(self, request, *args, **kwargs):
        """field validation"""
        # request.POST['description']="Desc"
        locations = simplejson.loads(request.POST.get('locations', '[]'))
        # print locations
        # print len(locations)
        if len(locations) == 0:
            res = {
                'result': 'error',
                'msg': 'post field is appears to be blank.Please write something or attach a link or photo to update your status.'
            }
        else:
            """save travelposts"""
            try:
                postid = request.POST.get('postid', '')
                travel_post = TravelPost.objects.get(id=postid, post_creator=request.user)
                action = 'edit'
            except:
                travel_post=None
                res = {
                'result': 'error',
                'msg': 'Permission denied'
                }
            if request.POST.get('postid',"")=="":
                travel_post = TravelPost.objects.create(post_creator=request.user)
                action = 'create'
            # location_points = request.POST.getlist('marker[][]')

            user = request.user
            box_id = request.session['box_id']
            if travel_post:
                """save location cordinates"""
                # print(simplejson.dumps(locations))
                if locations:
                    # print "-------->",locations
                    for location in locations:
                        # print location
                        try:
                            loc = LocationPoints.objects.create(traveller=request.user)
                            lat = float(location['geometry']['location']['lat'])
                            lon = float(location['geometry']['location']['lng'])
                            location_address = str(location['name'])
                            loc.location_point = GEOSGeometry('POINT(%f %f)' % (lat, lon))
                            loc.location_address = location_address
                            """save images"""
                            images = location['images']
                            loc.images.remove()
                            loc.videos.remove()
                            if len(images) != 0:
                                for x in images:
                                    img = Picture.objects.get(id=x['id'])
                                    print "img", img
                                    loc.images.add(img)
                            """save videos"""
                            videos = location['videos']
                            if len(videos) != 0:
                                for x in videos:
                                    vid = Picture.objects.get(id=x['id'])
                                    print "vid", vid
                                    loc.videos.add(vid)
                            loc.save()
                            travel_post.locations.add(loc)
                        except Exception as e:
                            print e
                else:
                    try:
                        up = UserProfile.objects.get(user=user)
                        travel_post.locations.add(up.location)
                    except Exception as e:
                        pass

                images = request.POST.getlist('images[]')
                """save attatchments"""
                if len(images) != 0:
                    for x in images:
                        img = Picture.objects.get(id=x)
                        travel_post.attachments.add(img)
                videos = request.POST.getlist('videos[]')
                if len(videos) != 0:
                    for x in videos:
                        vid = Picture.objects.get(id=x)
                        travel_post.attachments.add(vid)

                travel_post.post_type = 'travel'
                travel_post.description = request.POST['description'] + " "
                travel_post.created_box = Box.get(box_id)
                # add_url(request.POST['description'], travel_post)
                travel_post.save()
                """set audience data"""
                try:
                    whocansee = simplejson.loads(request.POST['whocansee'])
                    rawtag = simplejson.loads(request.POST['rawtag'])
                    rawwcs = simplejson.loads(request.POST['rawwcs'])
                    tags = simplejson.loads(request.POST['tags'])
                except Exception as e:
                    print e
                    whocansee = None
                    tags = []
                if whocansee['boxes'] == [] and whocansee['members'] == {} and whocansee['groups'] == []:
                    whocansee = None
                Visibility(travel_post, request.user).build(whocansee=whocansee)
                for tag in tags:
                    user = User.objects.get(id=int(tag['id']))
                    SharePost(user, travel_post).add(description=travel_post.description, whocansee=None)
                con = get_db()
                coll = con.PostAudienceComments
                print "post id ------->", travel_post.id
                if action == 'create':
                    coll.insert({"post_id": travel_post.id, 'like_minus': [], 'like': [], "like_plus": [],
                                 "audience": rawwcs, "tagged_audience": rawtag, "comments": [], "shares": []})
                res = {
                    'result': 'ok',
                    'travel_post': travel_post.get_json(),
                    'msg': 'post saved successfully',
                }

        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200, content_type="application/json")
        else:
            return render(request, 'posts.html', res)


class LifetimePostView(View):
    def get(self, request, *args, **kwargs):
        lifetime_posts = LifeTimePost.objects.filter(post_creator=request.user)
        if request.is_ajax():
            res = {
                'lifetime_posts': [x.get_json() for x in lifetime_posts]
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', {

                'lifetime_posts': [x.get_json() for x in lifetime_posts]

            })

    def post(self, request, *args, **kwargs):

        """filed validation"""
        user = request.user
        if request.POST.get('coverid', '') == '':
            res = {
                'result': 'error',
                'msg': 'Please add a cover picture..'
            }
        # post_data = ast.literal_eval(request.POST['post_data'])
        else:
            """saves lifetime post"""
            try:
                postid = request.POST.get('postid', '')
                lifetime_post = LifeTimePost.objects.get(id=postid, post_creator=request.user)
                action = 'edit'
            except:
                lifetime_post=None
                res = {
                'result': 'error',
                'msg': 'Permission denied'
                }
            if request.POST.get('postid',"")=="":
                lifetime_post = LifeTimePost.objects.create(post_creator=request.user)
                action = 'create'
            # lifetime_post, created = LifeTimePost.objects.get_or_create(post_creator=request.user)
            box_id = request.session['box_id']
            if lifetime_post:
                lifetime_post.post_type = request.POST['post_type']
                lifetime_post.title = request.POST['title'] if request.POST['title'] != '' else None
                lifetime_post.description = request.POST['description'] + " "
                lifetime_post.created_box = Box.get(box_id)
                add_url(request.POST['description'], lifetime_post)

                """saves attatchments"""
                images = request.POST.getlist('images[]')
                lifetime_post.attachments.remove()
                if len(images) != 0:
                    for x in images:
                        img = Picture.objects.get(id=x)
                        lifetime_post.attachments.add(img)
                videos = request.POST.getlist('videos[]')
                if len(videos) != 0:
                    for x in videos:
                        vid = Picture.objects.get(id=x)
                        lifetime_post.attachments.add(vid)
                cover_photo = request.POST.get('coverid', '')
                lifetime_post.cover_photo = None
                if cover_photo not in ["", None]:
                    lifetime_post.cover_photo = Picture.objects.get(id=cover_photo)
                lifetime_post.save()
                """set audience data"""
                try:
                    whocansee = simplejson.loads(request.POST['whocansee'])
                    rawtag = simplejson.loads(request.POST['rawtag'])
                    rawwcs = simplejson.loads(request.POST['rawwcs'])
                    tags = simplejson.loads(request.POST['tags'])
                except Exception as e:
                    print e
                    whocansee = None
                    tags = []
                if whocansee['boxes'] == [] and whocansee['members'] == {} and whocansee['groups'] == []:
                    whocansee = None
                Visibility(lifetime_post, request.user).build(whocansee=whocansee)
                for tag in tags:
                    user = User.objects.get(id=int(tag['id']))
                    SharePost(user, lifetime_post).add(description=lifetime_post.description, whocansee=None)
                con = get_db()
                coll = con.PostAudienceComments
                # print "post id ------->",lifetime_post.id
                if action == 'create':
                    coll.insert({"post_id": lifetime_post.id, 'like_minus': [], 'like': [], "like_plus": [],
                                 "audience": rawwcs, "tagged_audience": rawtag, "comments": [], "shares": []})
                res = {
                    'msg': 'Post saved successfully',
                    'result': 'ok',
                    'lifetime_post': lifetime_post.get_json()
                }

        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', res)


class MediaPostView(View):
    def get(self, request, *args, **kwargs):
        media_posts = MediaPost.objects.filter(post_creator=request.user)
        if request.is_ajax():
            res = {
                'media_posts': [x.get_json() for x in media_posts]
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', {

                'media_posts': [x.get_json() for x in media_posts]

            })

    def post(self, request, *args, **kwargs):
        """field validation"""
        user = request.user
        if request.POST.get('title', '') == '':
            res = {
                'result': 'error',
                'msg': 'Title is appears to be blank.Please write something.',
            }
        else:
            """save  media posts"""
            try:
                postid = request.POST.get('postid', '')
                media_post = MediaPost.objects.get(id=postid, post_creator=request.user)
                action = 'edit'
            except:
                media_post=None
                res = {
                'result': 'error',
                'msg': 'Permission denied'
                }
            if request.POST.get('postid',"")=="":
                media_post = MediaPost.objects.create(post_creator=request.user)
                action = 'create'

            box_id = request.session['box_id']
            if media_post:
                media_post.post_type = request.POST['post_type']
                media_post.title = request.POST['title']
                media_post.description = request.POST['description'] + " "
                media_post.created_box = Box.get(box_id)
                add_url(request.POST['description'], media_post)

                images = request.POST.getlist('images[]')
                media_post.attachments.remove()
                if len(images) != 0:
                    for x in images:
                        img = Picture.objects.get(id=x)
                        media_post.attachments.add(img)
                videos = request.POST.getlist('videos[]')
                if len(videos) != 0:
                    for x in videos:
                        vid = Picture.objects.get(id=x)
                        media_post.attachments.add(vid)
                poster = request.POST.get("poster", '')
                if poster and isinstance(poster, (str, unicode)):
                    image = urllib.urlretrieve(poster)
                    pic = Picture.objects.create(file_owner=self.request.user)
                    fname = os.path.basename(poster)
                    with open(image[0]) as fp:
                        pic.file.save(fname, File(fp))
                        pic.save()
                        media_post.poster = pic
                # if request.POST['posterid']:
                #     posterid = request.POST['posterid']
                #     media_post.poster = Picture.objects.get(id=posterid)
                media_post.genre = request.POST['genere']
                if str(request.POST['year']) == "Year":
                    media_post.year = ''
                else:
                    media_post.year = request.POST['year']
                media_post.director = request.POST['directer']
                media_post.stars = request.POST['stars']
                media_post.plot = request.POST['plot']
                media_post.media_type = request.POST['mediatype']
                media_post.save()
                """set audience data"""
                try:
                    whocansee = simplejson.loads(request.POST['whocansee'])
                    rawtag = simplejson.loads(request.POST['rawtag'])
                    rawwcs = simplejson.loads(request.POST['rawwcs'])
                    tags = simplejson.loads(request.POST['tags'])
                except Exception as e:
                    print e
                    whocansee = None
                    tags = []
                if whocansee['boxes'] == [] and whocansee['members'] == {} and whocansee['groups'] == []:
                    whocansee = None
                Visibility(media_post, request.user).build(whocansee=whocansee)
                for tag in tags:
                    user = User.objects.get(id=int(tag['id']))
                    SharePost(user, media_post).add(description=media_post.description, whocansee=None)
                con = get_db()
                coll = con.PostAudienceComments
                # print "post id ------->",media_post.id
                if action == 'create':
                    coll.insert({"post_id": media_post.id, 'like_minus': [], 'like': [], "like_plus": [],
                                 "audience": rawwcs, "tagged_audience": rawtag, "comments": [], "shares": []})
                res = {
                    'msg': 'Post saved successfully',
                    'result': 'ok',
                    'media_post': media_post.get_json()
                }
        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', res)


class DeletePost(View):
    def get(self, request, *args, **kwargs):

        post = Post.objects.get(id=request.GET.get('postid', ''))
        """ delete the attachments from media folder"""

        if post.post_creator == request.user:

            for i in post.attachments.all():
                fn = 'media/' + str(i.file)
                os.remove(fn) if os.path.exists(fn) else None

            # con = pymongo.MongoClient()
            # coll = con.groupinit.PostAudienceComments
            # coll1 = con.groupinit.Comment
            # data = coll.find_one({'post_id':post.id})
            # if data != None:
            #     if 'comments' in data:
            #         com = coll1.find_one({'_id':data['comments'][0]})
            #         if com !=None:
            #             if 'reply' in com:
            #                 rep = coll1.find_one({'_id':com['reply'][0]})
            #                 reply = coll1.find_one({'_id':com['reply'][0]}).remove()
            #             comm = coll1.find_one({'_id':data['comments'][0]}).remove()


            post.delete()
            res = {
                'result': 'ok',
                'msg': 'Post deleted successfully'
            }
        else:
            res = {
                'result': 'error',
                'msg': 'You are not allowed to delete this Post',
            }
        if request.is_ajax():
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', res)


class AddTag(View):
    def post(self, request, *args, **kwargs):
        try:
            post_id = request.POST['post_id']
            post = Post.objects.get(id=post_id)
            tagged_audience_data = request.POST['tagged_audience_data']
            con = get_db()
            coll = con.PostAudienceComments
            post_tagged_audience = coll.find_one({"post_id": post_id})
            for tag in tagged_audience_data:
                if tag not in post_tagged_audience['tagged_audience']:
                    coll.update({"post_id": post_id}, {"$addToSet": {"tagged_audience": tag}}, upsert=True)

            res = {
                'status': 'ok',
                'posts': post.get_json(),
                'tagged_audience': coll.find_one({"post_id": post_id})['tagged_audience']
            }
        except Exception as e:
            res = {'status': 'error', 'posts': None, 'tagged_audience': None}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class RemoveTag(View):
    def post(self, request, *args, **kwargs):
        try:
            post = Post.objects.get(id=kwargs['post_id'])
            tagged_audience_data = request.POST['tagged_audience_data']
            con = get_db()
            coll = con.PostAudienceComments
            post_tagged_audience = coll.find_one({"post_id": int(kwargs['post_id'])})
            for tag in tagged_audience_data:
                coll.update({"post_id": post.id}, {"$pull": {"tagged_audience": tag}}, upsert=True, multi=True)
            res = {
                'status': 'ok',
                'posts': post.get_json(),
                'tagged_audience': coll.find_one({"post_id": int(kwargs['post_id'])})['tagged_audience']
            }
        except Exception as e:
            res = {
                'status': 'ok', 'posts': None, 'tagged_audience': None}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class SharePost:
    def __init__(self, user, post):
        self.user = user
        self.post = post

    def add(self, description=None, whocansee=None):
        try:
            Post.objects.get(origin_post=self.post, post_creator=self.user)
            return None
        except:
            try:
                shared_post = Post.objects.create(post_creator=self.user)
                shared_post.origin_post = self.post
                shared_post.description = description if description else self.post.description
                shared_post.post_type = 'shared'
                # shared_post.attatchments = post.attatchments
                shared_post.save()
                """set audience data"""
                Visibility(shared_post, self.user).build(whocansee=whocansee)
                con = get_db()
                coll = con.PostAudienceComments
                coll.insert({"post_id": shared_post.id, 'like_minus': [], 'like': [], "like_plus": [],
                             "audience": [], "tagged_audience": [], "comments": [], "shares": []})
                return shared_post
            except Exception as e:
                print "ex", e
                return None


class PostReshareView(View):
    def get(self, request, *args, **kwargs):
        posts = Post.objects.get(id=kwargs['post_id'])
        if request.is_ajax():
            res = {
                'posts': posts.get_json(),
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)
        else:
            return render(request, 'posts.html', {
                'posts': posts.get_json()
            })

    def post(self, request, *args, **kwargs):
        postid = request.POST.get('post_id', "")
        whocansee = request.POST.get('whocansee', None)
        description = request.POST.get('description', "")
        # model_map = {'general': Post, 'media': MediaPost,'travel':TravelPost,'lifetime':LifeTimePost}
        post = Post.objects.get(id=postid)
        print "shere post type--------", post.post_type
        # model = model_map.get(post.post_type, "")
        shared_post = SharePost(request.user, post).add(description, whocansee)
        con = get_db()
        coll = con.PostAudienceComments
        coll.update({"post_id": post.id}, {"$addToSet": {"shares": request.user.id}}, upsert=True)
        res = {
            'posts': post.get_json(),
            'result': 'ok',
            'post_reshare': shared_post.get_json() if shared_post else []
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class PostAddComments(View):
    def get(self, request, *args, **kwargs):
        postid = int(request.GET.get('postid'))
        comments = CommentsReply(postid).get()
        res = {
            'result': "ok",
            'post_id': postid,
            'data': comments,
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")

    def post(self, request, *args, **kwargs):
        post_id = request.POST.get('postid', "")
        groupid = request.POST.get('group_id', "")
        groupid = "" if groupid == 0 else groupid
        comment = request.POST.get('comment', "")
        if comment == "" and post_id == "":
            res = {'result': "error"}
        else:
            con = get_db()
            coll = con.PostAudienceComments
            coll1 = con.Comment
            post = Post.objects.get(id=post_id)
            post.updated_date = datetime.now()
            post.save()
            try:
                comments = coll1.insert(
                    {"author": int(request.user.id), "text": comment, "group": groupid, "created_on": datetime.now()})
                coll.update({"post_id": post.id}, {"$addToSet": {"comments": comments}}, upsert=True)
            # pass
            except Exception as e:
                print e
            # announce_client.emit(int(post.post_creator.id), 'notifications', data={ 'msg' : request.user.first_name +' Commented on your post ',	'key':'notification'})
            audience_data = coll.find_one({"post_id": post.id})
            comment_data = coll1.find_one({"_id": audience_data['comments'][-1]})
            user = User.objects.get(id=comment_data['author'])
            comment = {'comment': comment_data['text'], 'group_id': comment_data['group'], 'post_id': post_id,
                       'id': str(comment_data['_id']),
                       'created_on': comment_data['created_on'].strftime('%Y/%m/%d %H:%M'),
                       'creator': UserProfile.objects.get(user=user).get_json()}
            Notify(user_id=request.user.id).add_comments(post_id=post.id, cmnd_id=str(comment_data['_id']))
            res = {'result': "ok", 'post_id': post_id, 'comment': comment}
        response = simplejson.dumps(res)
        print "cmt_response:", res
        return HttpResponse(response, status=200)


class PostDeleteComment(View):
    def post(self, request, *args, **kwargs):
        post_id = request.POST.get('postid', '')
        comm_id = request.POST.get('commentid', "")
        if post_id == "" and comm_id == "":
            res = {'result': "error", 'msg': "invalid arguments"}
        else:
            con = get_db()
            coll = con.PostAudienceComments
            coll1 = con.Comment

            comments = coll1.find_one({"_id": ObjectId(str(comm_id))})
            post = Post.objects.get(id=post_id)
            if request.user.id == comments['author'] or request.user.id == post.post_creator.id:
                comm = coll1.remove({"_id": ObjectId(str(comm_id))})
                post_audience = coll.update({"post_id": post.id}, {"$unset": {"comments": [comments]}}, upsert=True)
                res = {'result': "ok", "msg": "comment deleted succesfully"}
            else:
                res = {'result': "error", 'msg': "you dont have the permission to delete this comment"}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class PostDeleteCommentReply(View):
    def post(self, request, *args, **kwargs):

        post_id = request.POST['postid']
        comment_id = request.POST['commentid']
        reply_id = request.POST['id']
        con = get_db()
        coll1 = con.Comment
        comments = coll1.find_one({"_id": ObjectId(str(reply_id))})
        # comments = Comment.objects.get(id=ObjectId(str(reply_id)))

        post = Post.objects.get(id=post_id)
        if request.user.id == comments['author'] or request.user.id == post.post_creator.id:

            # post_audience = coll1.update( {"_id" :ObjectId(str(comment_id))}, { "$pull": { "reply":comments}}, upsert= True , multi= True )
            post_audience = Comment.objects.get(id=ObjectId(str(comment_id)))
            post_audience.reply.remove(comments)
            comments.delete()

            res = {
                'result': "ok",
                "msg": "reply deleted succesfully",
            }
        else:
            res = {
                'result': "error",
                'msg': "you dont have the permission to delete this comment"
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class PostLike(View):
    def update_likes(self, like_type, user_id, post_id):
        con = get_db()
        coll = con.PostAudienceComments
        post_like = coll.find_one({"post_id": int(post_id)})
        post_id = int(post_id)
        if not post_like:
            x = coll.insert({"post_id": post_id, 'like_minus': [], 'like': [], "like_plus": []})
            post_like = coll.find_one({"post_id": post_id})
        typedict = {'like': ['like_plus', 'like_minus'],
                    'like_plus': ['like', 'like_minus'],
                    'like_minus': ['like', 'like_plus']}
        ex_type = typedict[like_type]
        for ex_tp in ex_type:
            if user_id in post_like.get(ex_tp, []):
                lyk = coll.update({'post_id': post_id}, {"$pull": {ex_tp: user_id}}, upsert=True)
                new_post_like = coll.find_one({"post_id": post_id})
        like_data = coll.update({'post_id': post_id}, {"$addToSet": {like_type: [user_id[0]]}}, upsert=True)
        post_like = coll.find_one({"post_id": post_id})
        return post_like

    def user_details(self, like_type, post_like):
        like_data = post_like[like_type]
        total_like = []
        for i in range(0, len(like_data)):
            lyk = UserProfile.objects.get(user=User.objects.get(id=like_data[i][0])).get_json()
            total_like.append(lyk)
        return total_like

    def post(self, request, *args, **kwargs):
        lk_type = request.POST.get('type', '')
        post_id = request.POST.get('postid', '')
        if lk_type == '' and post_id == '':
            res = {'result': 'error', 'message': 'invalid arguments'}
        else:
            actual_type = {'like-minus': 'like_minus', 'like': 'like', 'like-plus': 'like_plus'}
            like_type = actual_type.get(lk_type, lk_type)
            user_id = [request.user.id]
            post = Post.objects.get(id=post_id)
            post.updated_date = datetime.now()
            post.save()
            post_like = self.update_likes(str(like_type), user_id, post_id)
            res = {
                'result': "ok",
                'liked_user': UserProfile.objects.get(user=request.user).get_json(),
                'like': self.user_details("like", post_like),
                'like_minus': self.user_details("like_minus", post_like),
                'like_plus': self.user_details("like_plus", post_like),
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class PostCommentReply(View):
    def post(self, request, *args, **kwargs):

        post_id = int(request.POST.get('postid'))
        commentid = request.POST.get('commentid', '')
        text = request.POST.get('reply')
        if post_id == '' and commentid == '' and text == '':
            res = {'result': 'error', 'message': 'invalid arguments'}
        else:
            comment = {}
            con = get_db()
            coll1 = con.Comment
            comments = coll1.insert({"author": int(request.user.id), "text": text, "created_on": datetime.now()})
            coll1.update({"_id": ObjectId(str(commentid))}, {"$addToSet": {"reply": comments}}, upsert=True)
            comments = coll1.find_one({"_id": comments})
            comment["comment"] = comments["text"]
            comment["created_on"] = comments['created_on'].strftime('%Y/%m/%d %H:%M')
            comment["id"] = str(comments["_id"])
            comment["creator"] = User.objects.get(id=comments["author"]).profile.get_json()
            post = Post.objects.get(id=post_id)
            post.updated_date = datetime.now()
            post.save()
            res = {
                'result': "ok",
                'post_id': post_id,
                'comment_id': commentid,
                'reply': comment,
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class EditPost(View):
    def get(self, request, *args, **kwargs):
        post = Post.objects.get(id=request.GET.get('postid', ''))
        if post.post_creator == request.user:

            if post.post_type == 'travel':
                travel_post = TravelPost.objects.get(id=request.GET.get('postid', ''))
                res = {
                    'result': 'ok',
                    'post': travel_post.get_json(),

                }
            if post.post_type == 'media':
                media_post = MediaPost.objects.get(id=request.GET.get('postid', ''))
                res = {
                    'result': 'ok',
                    'post': media_post.get_json(),

                }
            if post.post_type == 'lifetime':
                lifetime_post = LifeTimePost.objects.get(id=request.GET.get('postid', ''))

                res = {
                    'result': 'ok',
                    'post': lifetime_post.get_json(),

                }
            if post.post_type == 'general':
                res = {
                    'result': 'ok',
                    'post': post.get_json(),

                }

            response = simplejson.dumps(res)
        else:
            res = {
                'result': 'error',
                'msg': 'You dont have the permission to edit this post',

            }
            response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class LoadTravelPost(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            boxid = request.session['box_id']
            post_details = []
            box = FriendsBox.objects.filter(Q(owner=user) & Q(id=boxid))

            # friends = Friends.objects.filter(Q(user=user) & Q(box=box))
            friends = []
            if friends:
                for i in range(0, friends.count()):

                    if friends[i].friends.all() != 0:

                        for x in friends[i].friends.all():

                            friends_id = x.id

                            friend = User.objects.get(id=friends_id)

                            friend_req1 = FriendRequest.objects.filter(invitee=user, invitor=friend, state="accepted")

                            friend_req2 = FriendRequest.objects.filter(invitee=friend, invitor=user, state="accepted")

                            if friend_req1 != [] or friend_req2 != []:

                                friends_posts = TravelPost.objects.filter(post_creator=friend).order_by('created_date')

                                if friends_posts.count() != 0:

                                    for x in range(0, friends_posts.count()):
                                        aud_user = request.user.id
                                        con = get_db()
                                        coll = con.PostAudienceComments
                                        audience_data = coll.find_one({"post_id": friends_posts[x].id})
                                        # audience_data = PostAudienceComments.objects.get(post_id=friends_posts[x].id)
                                        if audience_data:
                                            if audience_data.has_key('audience'):
                                                for aud in audience_data['audience']:
                                                    if str(aud[0]) == str(aud_user):
                                                        post_details.append(friends_posts[x].get_json())
                res = {
                    'result': 'ok',
                    'posts': post_details,
                }
                response = simplejson.dumps(res)

            else:

                res = {
                    'result': 'error',
                    'msg': 'No other travel posts are for this box',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadLifeTimePost(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            boxid = request.session['box_id']
            post_details = []
            box = FriendsBox.objects.filter(Q(owner=user) & Q(id=boxid))

            # friends = Friends.objects.filter(Q(user=user) & Q(box=box))
            friends = []
            if friends:
                for i in range(0, friends.count()):

                    if friends[i].friends.all() != 0:

                        for x in friends[i].friends.all():

                            friends_id = x.id

                            friend = User.objects.get(id=friends_id)

                            friend_req1 = FriendRequest.objects.filter(invitee=user, invitor=friend, state="accepted")

                            friend_req2 = FriendRequest.objects.filter(invitee=friend, invitor=user, state="accepted")

                            if friend_req1 != [] or friend_req2 != []:

                                friends_posts = LifeTimePost.objects.filter(post_creator=friend).order_by('created_date')

                                if friends_posts.count() != 0:

                                    for x in range(0, friends_posts.count()):
                                        aud_user = request.user.id
                                        con = get_db()
                                        coll = con.PostAudienceComments
                                        audience_data = coll.find_one({"post_id": friends_posts[x].id})
                                        # audience_data = PostAudienceComments.objects.get(post_id=friends_posts[x].id)
                                        if audience_data:
                                            if audience_data.has_key('audience'):
                                                for aud in audience_data['audience']:
                                                    if str(aud[0]) == str(aud_user):
                                                        post_details.append(friends_posts[x].get_json())
                res = {
                    'result': 'ok',
                    'posts': post_details,
                }
                response = simplejson.dumps(res)

            else:

                res = {
                    'result': 'error',
                    'msg': 'No other Life Time posts are for this box',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadMediaPost(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            boxid = request.session['box_id']
            post_details = []
            box = FriendsBox.objects.filter(Q(owner=user) & Q(id=boxid))

            # friends = Friends.objects.filter(Q(user=user) & Q(box=box))
            friends = []
            if friends:
                for i in range(0, friends.count()):

                    if friends[i].friends.all() != 0:

                        for x in friends[i].friends.all():

                            friends_id = x.id

                            friend = User.objects.get(id=friends_id)

                            friend_req1 = FriendRequest.objects.filter(invitee=user, invitor=friend, state="accepted")

                            friend_req2 = FriendRequest.objects.filter(invitee=friend, invitor=user, state="accepted")

                            if friend_req1 != [] or friend_req2 != []:

                                friends_posts = MediaPost.objects.filter(post_creator=friend).order_by('created_date')

                                if friends_posts.count() != 0:

                                    for x in range(0, friends_posts.count()):
                                        aud_user = request.user.id
                                        con = get_db()
                                        coll = con.PostAudienceComments
                                        audience_data = coll.find_one({"post_id": friends_posts[x].id})
                                        # audience_data = PostAudienceComments.objects.get(post_id=friends_posts[x].id)
                                        if audience_data:
                                            if audience_data.has_key('audience'):
                                                for aud in audience_data['audience']:
                                                    if str(aud[0]) == str(aud_user):
                                                        post_details.append(friends_posts[x].get_json())
                res = {
                    'result': 'ok',
                    'posts': post_details,
                }
                response = simplejson.dumps(res)

            else:

                res = {
                    'result': 'error',
                    'msg': 'No other media posts are for this box',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadGroupPosts(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            boxid = request.session['box_id']
            groupid = request.GET.get('id', '')
            box = FriendsBox.objects.filter(owner=user, id=boxid)
            # group = Group.objects.get(id=groupid, box=box)
            group = Grp(groupid).get()
            aud_user = request.user.id
            post_details = []
            for grp in group.members.all():

                friends_post = Post.objects.filter(post_creator=grp).order_by('created_date')
                if friends_post.count() != 0:
                    for i in range(0, friends_post.count()):
                        # con = get_db()
                        # coll=con.PostAudienceComments
                        # audience_data = coll.find_one({"post_id":friends_post[i].id})
                        audience_data = PostAudienceComments.objects.get(post_id=friends_post[i].id)
                        if audience_data:
                            if audience_data.has_key('audience'):
                                for aud in audience_data['audience']:
                                    if str(aud[0]) == str(aud_user):
                                        if str(friends_post[i].post_type) == 'lifetime':
                                            friends_life_time_post = LifeTimePost.objects.get(id=friends_post[i].id)
                                            post = friends_life_time_post.get_json()
                                            for i in range(0, len(post['likes'])):
                                                l_id = post['likes'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'likes'
                                                else:
                                                    post['post_status'] = 'null'
                                            for i in range(0, len(post['like_plus'])):
                                                l_id = post['like_plus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_plus'
                                                else:
                                                    post['post_status'] = 'null'

                                            for i in range(0, len(post['like_minus'])):
                                                l_id = post['like_minus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_minus'
                                                else:
                                                    post['post_status'] = 'null'
                                            post_details.append(post)

                                        elif str(friends_post[i].post_type) == 'travel':

                                            friends_travel_post = TravelPost.objects.get(id=friends_post[i].id)
                                            # friends_travel_post =  friends_posts[i].TravelPost
                                            post = friends_travel_post.get_json()
                                            for i in range(0, len(post['likes'])):
                                                l_id = post['likes'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'likes'
                                                else:
                                                    post['post_status'] = 'null'
                                            for i in range(0, len(post['like_plus'])):
                                                l_id = post['like_plus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_plus'
                                                else:
                                                    post['post_status'] = 'null'

                                            for i in range(0, len(post['like_minus'])):
                                                l_id = post['like_minus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_minus'
                                                else:
                                                    post['post_status'] = 'null'
                                            post_details.append(post)


                                        elif str(friends_post[i].post_type) == 'media':

                                            friends_media_post = MediaPost.objects.get(id=friends_post[i].id)
                                            # friends_travel_post =  friends_posts[i].TravelPost
                                            post = friends_media_post.get_json()
                                            for i in range(0, len(post['likes'])):
                                                l_id = post['likes'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'likes'
                                                else:
                                                    post['post_status'] = 'null'
                                            for i in range(0, len(post['like_plus'])):
                                                l_id = post['like_plus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_plus'
                                                else:
                                                    post['post_status'] = 'null'

                                            for i in range(0, len(post['like_minus'])):
                                                l_id = post['like_minus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_minus'
                                                else:
                                                    post['post_status'] = 'null'
                                            post_details.append(post)
                                        else:
                                            post = friends_post[i].get_json()
                                            for i in range(0, len(post['likes'])):
                                                l_id = post['likes'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'likes'
                                                else:
                                                    post['post_status'] = 'null'
                                            for i in range(0, len(post['like_plus'])):
                                                l_id = post['like_plus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_plus'
                                                else:
                                                    post['post_status'] = 'null'

                                            for i in range(0, len(post['like_minus'])):
                                                l_id = post['like_minus'][i]['user']
                                                if request.user.id == l_id:
                                                    post['post_status'] = 'like_minus'
                                                else:
                                                    post['post_status'] = 'null'
                                            post_details.append(post)

                    res = {
                        'result': 'ok',
                        'posts': post_details,
                    }
                    response = simplejson.dumps(res)
                else:
                    res = {
                        'result': 'error',
                        'msg': 'no posts are in this group',
                    }
                    response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadComments(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            postid = request.GET.get('postid', '')
            comments = CommentsReply(postid).get()
            res = {
                'result': 'ok',
                'comments': comments,
            }
            response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadFriendsTravelPost(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            friends_id = request.GET.get('userid', '')
            boxid = request.session['box_id']
            post_details = []
            box = FriendsBox.objects.filter(Q(owner=user) & Q(id=boxid))
            friend = User.objects.get(id=friends_id)
            friends_posts = TravelPost.objects.filter(post_creator=friend).order_by('created_date')
            if friends_posts.count() != 0:

                for x in range(0, friends_posts.count()):
                    aud_user = request.user.id
                    con = get_db()
                    coll = con.PostAudienceComments
                    audience_data = coll.find_one({"post_id": friends_posts[x].id})
                    if audience_data:
                        if audience_data.has_key('audience'):
                            for aud in audience_data['audience']:
                                if str(aud[0]) == str(aud_user):
                                    post_details.append(friends_posts[x].get_json())
                res = {
                    'result': 'ok',
                    'posts': post_details,
                }
                response = simplejson.dumps(res)

            else:

                res = {
                    'result': 'error',
                    'msg': 'No other travel posts are for this box',
                }
                response = simplejson.dumps(res)
            return HttpResponse(response, status=200)


class LoadNewTravelPost(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            boxid = request.session['box_id']
            post_details = []
            box = FriendsBox.objects.filter(Q(owner=user) & Q(id=boxid))
            # friend = User.objects.get(id=friends_id)
            # user_profile = UserProfile.objects.get(user=request.user)
            # Private_groups = Group.objects.filter(group_admins=request.user, group_type='Private', box=box)
            # if Private_groups.count() != 0:
            #     for x in range(0, Private_groups.count()):
            #         friends = Private_groups[x].members.all()
            #         for i in range(0, friends.count()):
            #             friends_posts = Post.objects.filter(post_creator=friend[i],
            #                                                 created_date__gte=user_profile.last_activity).order_by('created_date')
            #             if friends_posts.count() != 0:
            #
            #                 for x in range(0, friends_posts.count()):
            #                     aud_user = request.user.id
            #                     con = get_db()
            #                     coll = con.PostAudienceComments
            #                     audience_data = coll.find_one({"post_id": friends_posts[x].id})
            #                     if audience_data:
            #                         if audience_data.has_key('audience'):
            #                             for aud in audience_data['audience']:
            #                                 if str(aud[0]) == str(aud_user):
            #                                     post_details.append(friends_posts[x].get_json())
            # shared_groups = user_profile.shared_groups.all()
            # if shared_groups.count() != 0:
            #     for x in range(0, shared_groups.count()):
            #         friends = shared_groups[x].members.all()
            #         for i in range(0, friends.count()):
            #             friends_posts = Post.objects.filter(post_creator=friend[i],
            #                                                 created_date__gte=user_profile.last_activity).order_by('created_date')
            #             if friends_posts.count() != 0:
            #
            #                 for x in range(0, friends_posts.count()):
            #                     aud_user = request.user.id
            #                     con = get_db()
            #                     coll = con.PostAudienceComments
            #                     audience_data = coll.find_one({"post_id": friends_posts[x].id})
            #                     if audience_data:
            #                         if audience_data.has_key('audience'):
            #                             for aud in audience_data['audience']:
            #                                 if str(aud[0]) == str(aud_user):
            #                                     post_details.append(friends_posts[x].get_json())
            new_map = sorted(post_details, key=lambda x: x['created_date'], reverse=True)
            res = {
                'result': 'ok',
                'posts': new_map,
            }
            response = simplejson.dumps(res)

        else:

            res = {
                'result': 'error',
                'msg': 'No other travel posts are for this box',
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class LoadLikes(View):
    def get(self, request, *args, **kwargs):
        if request.is_ajax():
            user = request.user
            like_type = request.GET.get('type', '')
            post_id = request.GET.get('post_id', '')
            con = get_db()
            coll = con.PostAudienceComments
            post = coll.find_one({"post_id": post_id})
            index = len(post) - 1
            if like_type == "like":
                lykes = post[index].likes
            if like_type == "like-plus":
                lykes = post[index].like_plus
            if like_type == "like-minus":
                lykes = post[index].like_minus
            like_details = []
            for i in lykes:
                user = UserProfile.objects.get(user__id=i)
                like_details.append(user.get_json())
            print like_details
            res = {
                'result': 'ok',
                'like_details': like_details,
            }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200)


class GetBox(View):
    """Never use this code again..this is not the right way to remove a key  """

    def remove_key(self, data, key_list):
        if not isinstance(data, (dict, list)):
            return data
        if isinstance(data, list):
            return [self.remove_key(v, key_list) for v in data]
        return {k: self.remove_key(v, key_list) for k, v in data.items()
                if k not in key_list}

    def get(self, request, *args, **kwargs):
        boxes = FriendsBox.objects.filter(owner=request.user).order_by('order')
        result = [box.get_json() for box in boxes]
        rm_keys = ['member_profile', 'is_friend', 'is_active_friend', 'default_sharing', 'hide_group_activity', 'group_created_on"']
        result = self.remove_key(result, rm_keys)
        res = {'box': result, 'status': 'ok'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class GetGroups(View):
    def get(self, request, *args, **kwargs):
        try:
            box_id = kwargs.get('box_id', "")
            groups = Grp.get_by_box(box_id)
            filter_list = ['name', 'id', 'group_type']
            result = [dict((k, group.get_json().get(k)) for k in filter_list) for group in groups]
            res = {'groups': result, 'status': 'ok'}
        except:
            res = {'status': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class GetTravelPost(View):
    def get(self, request, *args, **kwargs):
        box_id = request.session['box_id']
        post = ViewPosts(request.user.id).type(box_id=box_id, type="travel")
        response = simplejson.dumps(post)
        return HttpResponse(response, status=200, content_type='application/json')


class GetMediaPost(View):
    def get(self, request, *args, **kwargs):
        box_id = request.session['box_id']
        post = ViewPosts(request.user.id).type(box_id=box_id, type="media")
        response = simplejson.dumps(post)
        return HttpResponse(response, status=200, content_type='application/json')


class GetLifeTimePost(View):
    def get(self, request, *args, **kwargs):
        box_id = request.session['box_id']
        post = ViewPosts(request.user.id).type(box_id=box_id, type="lifetime")
        response = simplejson.dumps(post)
        return HttpResponse(response, status=200, content_type='application/json')


class GroupPost(View):
    def get(self, request, *args, **kwargs):
        group_id = request.GET['group_id']
        post = ViewPosts(request.user.id).group(group_id=group_id)
        response = simplejson.dumps(post)
        return HttpResponse(response, status=200, content_type='application/json')


def like_active(user_id, lk, lkpls, lkmns):
    for l in lk:
        if l['id'] == user_id:
            return 'like'
    for l in lkpls:
        if l['id'] == user_id:
            return 'like_plus'
    for l in lkmns:
        if l['id'] == user_id:
            return 'like_minus'
    return ""


def fetch_box(user):
    boxes = Box(user).get_all()
    box_details = []
    for box in boxes:
        box_data = {}
        box_data['name'] = box.name
        box_data['color'] = box.color
        box_data['box_type'] = box.box_type
        box_data['order'] = box.order
        box_data['id'] = box.id
        box_data['notification'] = len(Notify(user.id).box_unread_post(box.id))
        box_details.append(box_data)
    return box_details


from mongo_models.chat import FriendsChat


class LoadBox(View):
    def get(self, request, *args, **kwargs):
        box_id = int(request.GET.get('box_id', request.session['box_id']))
        post_type = request.GET.get('post_type', None)
        group_id = request.GET.get('group_id', None)
        friend_id = request.GET.get('friend_id', None)
        action = request.GET.get('action', None)
        last_post_id = request.GET.get('last_post', request.session.get('last_post', None))
        last_post = last_post_id if action == 'more' else None
        if not action == 'more':
            request.session['last_post'] = None

        request.session['box_id'] = box_id
        box = Box.get(box_id)
        box_data = fetch_box(request.user)
        res = {}

        if box.box_type == 'discussion':
            res['users'] = Discussion(request.user.id).users(box_id, 'private')
        else:
            post = ViewPosts(request.user.id)
            #newreq = NewRequest(request.user)
            newreqjson =[] #newreq.box(box) + newreq.get()
            #newreqjson = sorted(newreqjson, key=lambda x: x['created_date'], reverse=True)
            res['groups'] = self.grp_members(request.user, box_id)
            res['map_data'] = post.type(box_id=box_id, type="travel")[:5] + self.users_location(request.user)
            res['media_data'] = post.type(box_id=box_id, type="media")[:5]
            res['lifetime_data'] = post.type(box_id=box_id, type="lifetime")[:5]
            res['new_request'] = {'color': box.color, 'newrequests': newreqjson}
            res['notification'] = self.post_notification(request.user, box_id)

        unread_post = Notify(request.user.id).box_unread_post(box.id)
        if len(unread_post) == 0:
            offset = 15
            new_posts = self.filter_post(request.user, box_id=box_id, group_id=group_id, frnd_id=friend_id,
                                         post_type=post_type, last_post_id=last_post, offset=offset)
        else:
            offset = len(unread_post)
            new_posts = unread_post
        for inc in range(0, len(new_posts)):
            Notify(request.user.id).remove_post(new_posts[inc]["id"])
            new_posts[inc]["like_status"] = like_active(request.user.id, new_posts[inc]["likes"], new_posts[inc]["like_plus"],
                                                        new_posts[inc]["like_minus"])

        request.session['last_post'] = new_posts[offset - 1]["id"] if new_posts[offset - 1:] else new_posts[len(new_posts) - 1][
            "id"] if len(new_posts) > 0 else last_post
        print "session last post id ", request.session['last_post']
        res['result'] = 'ok'
        res['boxes'] = box_data
        res['create_box'] = True if len(box_data) < 7 else False
        res['posts'] = new_posts
        res['current_box'] = {'id': box.id, 'profile_pic': box.get_json()['profile_pic'], 'status': box.status,
                              'status_pic': box.status_pic, 'name': box.name, 'type': box.box_type,
                              'color': box.color, 'user_name': request.user.first_name + " " + request.user.last_name,
                              'notify_count': len(Notify(request.user.id).box_unread_post(box.id))}
        res['next_box'] = Box(request.user).next_box(box_id)

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')

    def filter_post(self, user, box_id=None, group_id=None, frnd_id=None, post_type=None, last_post_id=None, offset=15):
        if post_type:
            new_posts = ViewPosts(user.id).type(box_id=box_id, type=post_type, last_post=last_post_id, offset=offset)
        elif group_id and frnd_id:
            new_posts = ViewPosts(user.id).member(group_id=group_id, member_id=frnd_id, last_post=last_post_id, offset=offset)
        elif group_id:
            new_posts = ViewPosts(user.id).group(group_id=group_id, last_post=last_post_id, offset=offset)
        else:
            new_posts = ViewPosts(user.id).box(box_id=box_id, last_post=last_post_id, offset=offset)
        return new_posts

    def members(self, grp, user):
        gr = grp.get_json(user_id=user.id)
        meme = gr['members']
        meme[:] = [d for d in meme if d.get('id') != user.id]
        gr['members'] = meme
        for i in range(0, len(gr['members'])):
            gr['members'][i]['member_profile']['menu']['muteBtn'] = FriendsPost(user.id).get_mute_user(gr['members'][i]['id'])
            gr['members'][i]["msg"] = 'Friends'
            gr['members'][i]["notification"] = len(Notify(user.id).friend_unread_post(grp.id, gr['members'][i]['id']))
            gr['members'][i]["chat_notification"] = FriendsChat.notify_count(user.id, int(gr['members'][i]['id']))
        ntf = Notify(user.id)
        gr['notification'] = len(ntf.group_unread_post(grp.id))
        gr['updated_date'] = str(ntf.post_upadated_date(grp.id))
        gr['request'] = NewRequest(user).group(grp)
        return gr

    def grp_members(self, user, box_id):
        groups = []
        for grp in Grp.get_by_box(box_id):
            gr = self.members(grp, user)
            groups.append(gr)
        result = sorted(groups, key=lambda x: x['updated_date'], reverse=True)
        return result

    def post_notification(self, user, box_id):
        ntf = Notify(user.id)
        result = {'map_data': len(ntf.unread_post_category(box_id, 'travel')),
                  'media_data': len(ntf.unread_post_category(box_id, 'media')),
                  'lifetime_data': len(ntf.unread_post_category(box_id, 'lifetime'))}
        return result

    def users_location(self, user):
        user = UserProfile.objects.get(user=user).get_json()
        location_dict = {}
        if user['location']:
            location_dict['locations'] = [user['location']]
            location_dict['creator_profile'] = {'map_marker_grey':
                                                    "http://maps.gstatic.com/mapfiles/api-3/images/spotlight-poi.png"}
            return [location_dict]
        else:
            return []


class LoadParticipants(View):
    def get(self, request, *args, **kwargs):
        post_id = request.GET.get('post_id', None)
        try:
            group, friends = Discussion.participants(int(post_id))
            res = {}
            res['result'] = 'ok'
            res['participants'] = {'group': group, 'friends': friends}
        except Exception as e:
            res = {'result': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class RemoveParticipants(View):
    def get(self, request, *args, **kwargs):
        post_id = request.POST.get('post_id', "")
        member_id = request.POST.get('member_id', "")
        group_id = request.POST.get('group_id', "")
        try:
            discussion = DiscussionPost.objects.get(id=int(post_id))
            if not group_id == "":
                grp = Group.objects.get(id=int(group_id))
                discussion.group.remove(grp)
            if not member_id == "":
                member = Audience.objects.get(id=int(member_id))
                discussion.friends.remove(member)
            group, friends = Discussion.participants(int(post_id))
            res = {}
            res['result'] = 'ok'
            res['participants'] = {'group': group, 'friends': friends}
        except Exception as e:
            res = {'result': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class AddFavouriteName(View):
    def post(self, request, *args, **kwargs):
        fav_id = request.POST.get('fav_id', "")
        name = request.POST.get('name', None)
        try:
            result = FriendsPost(request.user.id).add_fav_name(name=name, fav_id=fav_id)
            res = {}
            res['result'] = 'ok'
            res['favourite'] = result
        except Exception as e:
            res = {'result': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class AddFavourite(View):
    def post(self, request, *args, **kwargs):
        cat = request.POST.get('category', "")
        category = cat.split(",")
        keyword = request.POST.get('keyword', "")
        location = request.POST.get('location', "")
        user_id = request.POST.get('user_id', "")
        if cat == "" and keyword == "" and location == "" and user_id == "":
            res = {'result': 'error'}
        else:
            try:

                result = FriendsPost(request.user.id).add_favorite(category=category,
                                                                   keyword=keyword,
                                                                   location=location,
                                                                   user_id=user_id)
                res = {}
                res['result'] = 'ok'
                res['favourite'] = result
            except Exception as e:
                res = {'result': 'error'}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class LoadDiscussion(View):
    def get(self, request, *args, **kwargs):
        box_id = int(request.GET.get('box_id', request.session['box_id']))
        disc_type = request.GET.get('type', 'private')

        friend_id = request.GET.get('user', None)
        cat = request.GET.get('category', "")
        category = cat.split(",") if not str(cat.split(",")[0]) == '' else []
        keyword = request.GET.get('keyword', "")
        location = request.GET.get('location', "")

        action = request.GET.get('action', None)
        last_post_id = request.GET.get('last_post', request.session.get('last_post', None))
        last_post = last_post_id if action == 'more' else None
        print "last postid", last_post

        request.session['box_id'] = box_id
        box = Box.get(box_id)
        box_data = fetch_box(request.user)
        res = {}
        offset = 15
        if disc_type == 'public':

            res['category'] = DISCUSSION_CATEGORY
            for cat_id, value in res['category'].items():
                cat_count = DiscussionPost.objects.filter(category=int(cat_id)).count()
                res['category'][str(cat_id)]['post_count'] = cat_count

            res['favourite'] = FriendsPost(request.user.id).get_all_favorite()
            new_posts = ViewPosts(request.user.id).public_discussion(user_id=friend_id,
                                                                     category=category,
                                                                     key_word=keyword,
                                                                     location=location,
                                                                     last_post=last_post,
                                                                     offset=offset)
        else:
            res['users'] = Discussion(request.user.id).users(box_id, disc_type)
            unread_count = len(Notify(request.user.id).box_unread_post(box.id))
            offset = 15 if unread_count == 0 else unread_count
            new_posts = ViewPosts(request.user.id).private_discussion(user_id=friend_id, last_post=last_post, offset=offset)

        for inc in range(0, len(new_posts)):
            Notify(request.user.id).remove_post(new_posts[inc]["id"])
            new_posts[inc]["like_status"] = like_active(request.user.id, new_posts[inc]["likes"], new_posts[inc]["like_plus"],
                                                        new_posts[inc]["like_minus"])

        request.session['last_post'] = new_posts[offset - 1]["id"] if new_posts[offset - 1:] else new_posts[len(new_posts) - 1][
            "id"] if len(new_posts) > 0 else last_post
        print "session last post id ", request.session['last_post']

        res['result'] = 'ok'
        res['boxes'] = box_data
        res['posts'] = new_posts
        res['current_box'] = {'id': box.id, 'profile_pic': box.get_json()['profile_pic'], 'status': box.status,
                              'status_pic': box.status_pic, 'name': box.name, 'type': box.box_type, 'color': box.color}
        res['next_box'] = Box(request.user).next_box(box_id)

        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class LoadMorePost(View):
    def get(self, request, *args, **kwargs):
        box_id = request.session['box_id']
        last_post = request.session['last_post']
        post = ViewPosts(request.user.id)
        new_posts = post.box(box_id)
        index = new_posts.index((item for item in new_posts if item["id"] == last_post).next())
        next_box = Box(request.user).next_box(box_id)
        request.session['last_post'] = new_posts[index + 5]["id"] if new_posts[index + 5:] else None
        posts = new_posts[index:index + 5] if new_posts[index:index + 5] else new_posts[index:]
        for res in range(0, len(new_posts)):
            Notify(request.user.id).remove_post(posts[res]["id"])
            posts[res]["like_status"] = like_active(request.user.id, posts[res]["likes"], posts[res]["like_plus"],
                                                    posts[res]["like_minus"])
        res = {
            'result': 'ok',
            'posts': posts,
            'next_box': next_box,
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class RefreshPanel(View):
    def get(self, request, *args, **kwargs):
        box_id = request.GET['box_id']
        request.session['box_id'] = box_id

        groups = [grp.get_json() for grp in Grp.get_by_box(box_id)]
        post = ViewPosts(request.user.id)
        lifetimepost = post.type(box_id=box_id, type="lifetime")
        mediapost = post.type(box_id=box_id, type="media")
        travelpost = post.type(box_id=box_id, type="travel")
        friend_requests_count = 0
        res = {
            'result': 'ok',
            'groups': groups,
            'map_data': travelpost,
            'media_data': mediapost,
            'lifetime_data': lifetimepost,
            'friend_request_count': friend_requests_count,
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class Mute(View):
    def post(self, request, *args, **kwargs):
        user_id = request.POST['user_id']
        mute = FriendsPost(request.user.id).mute(user_id)
        print mute
        if mute == None:
            res = {"result": 'error', 'mute': ""}
        else:
            res = {"result": 'ok', 'mute': mute}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class GetAlbumImages(View):
    def get(self, request, *args, **kwargs):
        images = PostAlbum(request.user).get_images()
        res = {
            'result': 'ok' if images else 'error',
            'images': images if images else []
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class GetAlbumComic(View):
    def get(self, request, *args, **kwargs):
        comics = os.listdir("./authentication/static/images/profile-svg-icons/")
        comiclist = [w.replace(w, '/static/images/profile-svg-icons/' + w) for w in comics]
        res = {
            'result': 'ok' if comiclist else 'error',
            'comics': comiclist
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class GetAlbumStatusImages(View):
    def get(self, request, *args, **kwargs):
        statusimg = os.listdir("./authentication/static/images/status/")
        statuslist = [w.replace(w, '/static/images/status/' + w) for w in statusimg]
        res = {
            'result': 'ok' if statuslist else 'error',
            'statusimages': statuslist
        }
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')


class TestApi(View):
    def get(self, request, *args, **kwargs):
        res = []
        try:
            res = Notify(request.user.id).unread_comments()
            # res = Discussion(9).private_users(35)
            # res=FriendsPost(9).add_fav_name(fav_id="57eceb6a4bafe5172ae4098d",name="fav11")
            # res = FriendsPost(9).get_favorite()
            # print "xxx",re
            # res=FriendsPost(9).add_favorite(category=[3,5],keyword="bike",location='calicut')
            # res=FriendsPost(9).add_favorite(category=[2,1],keyword="car",location='ekm',user_id=13)
            # res=FriendsPost(9).add_favorite(category=[6,5],keyword="movie",location='kannur',user_id=9)
            # res = PostAlbum(request.user).get_images()
            print "kkk", res, "ccc"
        except Exception as e:
            print e
        # res = {'result': "", "exception": ""}
        # print res
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type='application/json')

# utility functions for handling urls from post

import urllib2
from BeautifulSoup import BeautifulSoup
import re

# Return a list of urls from given string
def find_all_url(string):
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    return urls

# parse base url from given url
def get_base_url(url):
    pathArray = url.split('/')
    protocol = pathArray[0]
    host = pathArray[2]
    url = protocol + '//' + host
    return url

# return favicon link from website
def find_shortcut_favicon(url):
    try:
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        icon_link = soup.find("link", rel="shortcut icon")
        return url + icon_link['href']
    except:
        return "/static/images/Link-64.png"

def build_url_template(url):
    urls = """<br><a href="%s">%s</a>
      <div class="row" style="padding:0 0 0 15px" >
          <img style="float:left; height:32px"  src="%s"/>
          <div class="content-heading"><h4>%s</h4></div>
      </div>"""%(url['url'], url['url'], url['favicon'], url['base_url'])
    return urls

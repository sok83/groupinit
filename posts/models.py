from django.db import models
from mongoengine import *
import pymongo
from authentication.utils import *
from django.contrib.auth.models import User
from django.contrib.gis.geos import fromstr, fromfile
from django.contrib.gis.geos import Point
from django.conf import settings
# from mongo_models.friends_post import FriendsPost
from groupbox.models import Group, FriendsBox, Grp, Box, Friends
from authentication.utils import get_json_user
from albums.models import Picture
# from authentication.models import UserProfile
from albums.serialize import serialize
import calendar
from django.contrib.contenttypes import generic
from notification.models import Notification
from posts.post_utils import build_url_template
import re
from django.contrib.gis.db.models import PointField
from bson.objectid import ObjectId

POST_TYPE = (
    ('general', 'general'),
    ('media', 'media'),
    ('travel', 'travel'),
    ('lifetime', 'lifetime'),
    ('recommendation', 'recommendation'),
    ('discussion', 'discussion'),
    ('shared', 'shared'),
)
MEDIA_TYPE = (
    ('video', 'video'),
    ('audio', 'audio'),
)

DISCUSSION_TYPE = (
    ('public', 'public'),
    ('private', 'private'),
)
CATEGORY = (
    (1, 'Auto / Cars / Bikes'),
    (2, 'Artists / Perfoming Art'),
    (3, 'Buy / Sell'),
    (4, 'Food / Restaurants / Cooking'),
    (5, 'Music / Movies'),
    (6, 'Media / Politics / News'),
    (7, 'People / Celebrities / Businesses'),
    (8, 'Reading / Writing / Painting'),
    (9, 'Sports / Games / Outdoor'),
    (10, 'Shopping / Fashion'),
    (11, 'Tourists / Places'),
    (12, 'Web / Net / Apps'),
    (13, 'Technology / Gadgets'),
)
DISCUSSION_CATEGORY = {
    '1': {'id': 1, 'category': 'Auto / Cars / Bikes', 'pic': "icon-auto.png", 'post_count': 0},
    '2': {'id': 2, 'category': 'Artists / Perfoming Art', 'pic': "icon-artists.png", 'post_count': 0},
    '3': {'id': 3, 'category': 'Buy / Sell', 'pic': "icon-buy.png", 'post_count': 0},
    '4': {'id': 4, 'category': 'Food / Restaurants / Cooking', 'pic': "icon-food.png", 'post_count': 0},
    '5': {'id': 5, 'category': 'Music / Movies', 'pic': "icon-music.png", 'post_count': 0},
    '6': {'id': 6, 'category': 'Media / Politics / News', 'pic': "icon-media.png", 'post_count': 0},
    '7': {'id': 7, 'category': 'People / Celebrities / Businesses', 'pic': "icon-people.png", 'post_count': 0},
    '8': {'id': 8, 'category': 'Reading / Writing / Painting', 'pic': "icon-reading.png", 'post_count': 0},
    '9': {'id': 9, 'category': 'Sports / Games / Outdoor', 'pic': "icon-sports.png", 'post_count': 0},
    '10': {'id': 10, 'category': 'Shopping / Fashion', 'pic': "icon-shopping.png", 'post_count': 0},
    '11': {'id': 11, 'category': 'Tourists / Places', 'pic': "icon-tourists.png", 'post_count': 0},
    '12': {'id': 12, 'category': 'Web / Net / Apps', 'pic': "icon-web.png", 'post_count': 0},
    '13': {'id': 13, 'category': 'Technology / Gadgets', 'pic': "icon-technology.png", 'post_count': 0},

}


class LocationPoints(models.Model):
    traveller = models.ForeignKey(User, related_name="traveller")
    location_point = PointField('Location Point', null=True, help_text='Travel location map coordinates')
    location_address = models.TextField('Location Address', null=True, blank=True, )
    images = models.ManyToManyField(Picture, related_name="images", null=True, blank=True)
    videos = models.ManyToManyField(Picture, related_name="videos", null=True, blank=True)

    def __unicode__(self):
        return self.traveller.first_name

    def get_location(self):
        return Point(self.location_point.lng, self.location_point.lat)

    def get_json(self):
        return {
            'id': self.id,
            'traveller': get_json_user(self.traveller),
            'location_address': self.location_address if self.location_address else None,
            'latitude': self.location_point.x if self.location_point else None,
            'longitude': self.location_point.y if self.location_point else None,
            'videos': fetch_attachments(self.videos),
            'images': fetch_attachments(self.images)
        }


class Url(models.Model):
    url = models.CharField('Url', max_length=150, null=True, blank=True)
    base_url = models.CharField('Base url', max_length=150, null=True, blank=True)
    favicon = models.CharField('Favicon', max_length=150, null=True, blank=True)

    def __unicode__(self):
        return self.url


# class Audience(models.Model):
#     box = models.ForeignKey(FriendsBox,null=True, blank=True)
#     group = models.ManyToManyField(AudienceGroup, null=True, blank=True)
#
#     def __unicode__(self):
#         return self.box.name
#
# class AudienceGroup(models.Model):
#     group = models.ForeignKey(Group, null=True, blank=True)
#     friends = models.ManyToManyField(User,null=True,blank=True)
#
#     def __unicode__(self):
#         return self.group.name


def fetch_like(lyk_type, audience_data):
    likes = []
    if lyk_type in audience_data:
        like_data = audience_data[lyk_type]
        for i in range(0, len(like_data)):
            lyk = User.objects.get(id=like_data[i][0]).profile.get_json()
            likes.append(lyk)
    return likes


def like_active(user_id, lk, lkpls, lkmns):
    for l in lk:
        print l['id'], user_id
        if l['id'] == user_id:
            return 'like'
    return "sss"


def fetch_shared_post(origin_post):
    shared_post_data = []
    if origin_post:
        model_map = {'shared': Post, 'general': Post, 'media': MediaPost, 'travel': TravelPost, 'lifetime': LifeTimePost}
        model = model_map.get(origin_post.post_type, "")
        post = model.objects.get(id=origin_post.id)
        shared_post_data = post.get_json()
    return shared_post_data


class CommentsReply:
    def __init__(self, post_id, user=None):
        self.post_id = post_id
        self.login_user = user

    def get(self, box_type=None):
        con = get_db()
        audience = con.PostAudienceComments
        cmnts = con.Comment
        post_comment = audience.find_one({"post_id": int(self.post_id)})
        comments = []
        try:
            comment_data = post_comment['comments']
            gp = {}
            gflg = False
            for i in range(0, len(comment_data)):
                try:
                    if len(comment_data) != 0:
                        comm = cmnts.find_one({"_id": {"$in": [comment_data[i]]}})
                        if comm != None:
                            comment = {}
                            comment['comment'] = comm['text']
                            comment['id'] = str(comm['_id'])
                            comment['group'] = str(comm['group'])
                            comment['created_on'] = comm['created_on'].strftime('%Y/%m/%d %H:%M')
                            user = User.objects.get(id=int(comm['author']))
                            if box_type == "discussion":
                                box = FriendsBox.objects.get(owner=user, box_type='discussion')
                            else:
                                box = Friends(user=self.login_user).friends_box(user=user)
                            comment['creator'] = user.profile.get_json(box=box)
                            comment['reply'] = []
                            if 'reply' in comm:
                                reply_data = comm['reply']
                                for i in range(0, len(reply_data)):
                                    if len(reply_data) != 0:
                                        rep = cmnts.find_one({"_id": {"$in": [reply_data[i]]}})
                                        reply = {}
                                        if rep != None:
                                            reply['comment'] = rep['text']
                                            reply['id'] = str(rep['_id'])
                                            reply['created_on'] = rep['created_on'].strftime('%Y/%m/%d %H:%M')
                                            user = User.objects.get(id=int(rep['author']))
                                            if box_type == "discussion":
                                                box = FriendsBox.objects.get(owner=user, box_type='discussion')
                                            else:
                                                box = Friends(user=self.login_user).friends_box(user=user)
                                            reply['creator'] = user.profile.get_json(box=box)
                                            comment['reply'].append(reply)
                            else:
                                comment['reply'] = []
                            flag = True
                            for i in range(0, len(comments)):
                                if comments[i]["id"] == int(comm['group']):
                                    comments[i]["comments"].append(comment)
                                    flag = False
                            if flag:

                                if comm['group'] == '0':
                                    gp = {'id': int(comm['group']), "group_bar_color": '#F65B42', 'group_name': "Public comments",
                                          'comments': [comment]}
                                    gflg = True
                                else:
                                    grps = Grp(int(comm['group'])).get()
                                    gp = {'id': int(comm['group']), "group_bar_color": grps.group_bar_color,
                                          'group_name': grps.name, 'comments': [comment]}
                                comments.append(gp)
                except Exception as e:
                    print "comments", e
                    pass
            if not gflg:
                gp = {'id': 0, "group_bar_color": '#F65B42', 'group_name': "Public comments", 'comments': []}
                comments.append(gp)
        except Exception as e:
            print e
            comments = []
        return comments


def build_url(urls, description):
    url_list = []
    for x in urls.all():
        url_flag = True
        ur = {"url": x.url, "base_url": x.base_url, "favicon": x.favicon}
        url_list.append(ur)
        url = build_url_template(ur)
        # self.description = self.description.replace(r'\b'+x.url+'\b', url,1)
        description = re.sub(x.url + '[ \t\n\r\f\s]', url, description)
    return description


def fetch_attachments(attachments, type=None):
    attatch = []
    for x in attachments.all():
        att = serialize(x)
        att["id"] = x.id
        if type == 'image':
            if att['type'].split('/')[0] == type:
                attatch.append(att)
        elif type == 'video':
            if att['type'].split('/')[0] == type:
                attatch.append(att)
        else:
            attatch.append(att)
    return attatch


def fetch_share(audience_data):
    return audience_data['shares']


def fetch_tagged_audience(audience_data):
    return audience_data['tagged_audience']


def Audience_group_comments(box, group, friends, comments):
    box_data = []
    group_data = []
    for x in box.all():
        data = {
            'id': x.id,
            'name': x.name,
        }
        # group_data = [{"id":grp.id,'group_name':grp.name,"group_bar_color":grp.group_bar_color,'comments':[]} for grp in x.groups.all()]
        for grp in x.groups.all():
            try:
                group_data.append({"id": grp.id, 'group_name': grp.name, "group_bar_color": grp.group_bar_color, 'comments': []})
            except:
                pass
        box_data.append(data)
    for x in group.all():
        if x.group_type != 'Private':
            data = {
                'id': x.id,
                'group_name': x.name,
                "group_bar_color": x.group_bar_color,
                'comments': []
            }
            group_data.append(data)
    for frnd in friends.all():
        if frnd.group.group_type != 'Private':
            data = {
                'id': frnd.group.id,
                'group_name': frnd.group.name,
                "group_bar_color": frnd.group.group_bar_color,
                'comments': []
            }
            group_data.append(data)
    # unique_group = {v['id']:v for v in group_data+comments}.values()
    grp_cmd = {x['id']: x for x in group_data + comments}.values()
    return grp_cmd


class Audience(models.Model):
    group = models.ForeignKey(Group, null=True, blank=True)
    user = models.ForeignKey(User, null=True, blank=True)


class Post(models.Model):
    post_type = models.CharField('Post type', max_length=20, choices=POST_TYPE, default='general')
    origin_post = models.ForeignKey('self', null=True, blank=True)
    post_creator = models.ForeignKey(User, related_name="post_creator")
    created_box = models.ForeignKey(FriendsBox, related_name="created_box", null=True, blank=True)
    created_date = models.DateTimeField('Created date', auto_now_add=True)
    updated_date = models.DateTimeField('Updated date', auto_now_add=True, null=True)
    box = models.ManyToManyField(FriendsBox, null=True, blank=True)
    group = models.ManyToManyField(Group, null=True, blank=True)
    friends = models.ManyToManyField(Audience, null=True, blank=True)
    # title = models.CharField('Name', max_length=200, null=True, blank= True)
    description = models.TextField('Description', null=True, blank=True)
    urls = models.ManyToManyField(Url, related_name="urls", null=True, blank=True)
    attachments = models.ManyToManyField(Picture, related_name="attachments", null=True, blank=True)
    media_file = models.FileField(null=True, blank=True,
                                  upload_to="/home/ranjith/groupenv/groupinit-new/groupinit_new/groupinit/media")
    notification = generic.GenericRelation(Notification)

    def __unicode__(self):
        return self.post_creator.first_name

    def get_json(self, user=None):
        comments = url_list = []
        likes = like_plus = like_minus = []
        aud = 0
        shares = 0
        descrpton = self.description
        # self.description = build_url(self.urls, self.description)
        attatch = fetch_attachments(self.attachments)
        created_month = self.created_date.month
        month = str(calendar.month_abbr[created_month])
        postid = int(self.id)
        con = get_db()
        coll = con.PostAudienceComments
        coll1 = con.Comment
        audience_data = coll.find_one({"post_id": self.id})
        if audience_data:
            comments = CommentsReply(self.id).get()
            likes = fetch_like("like", audience_data)
            like_plus = fetch_like("like_plus", audience_data)
            like_minus = fetch_like("like_minus", audience_data)
            shares = fetch_share(audience_data)
            tagged_audience = fetch_tagged_audience(audience_data)
        latest_activity = likes + like_plus + like_minus
        shared_post_data = fetch_shared_post(self.origin_post)
        grp_cmd = Audience_group_comments(self.box, self.group, self.friends, comments)
        if user == self.post_creator:
            box = self.created_box
        else:
            box = Friends(user=user).friends_box(user=self.post_creator)
        result = {
            'description': self.description,
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'id': self.id,
            # 'box':box_data,
            'groups': grp_cmd,
            'attachments': attatch,
            'url_list': url_list,
            'post_created_date': self.created_date.strftime('%d') + ' ' + month + ' ' + self.created_date.strftime('%H:%M'),
            # 'creator': get_json_user(self.post_creator),
            'creator_profile': self.post_creator.profile.get_json(box=box),
            'post_type': self.post_type,
            'origin_post': self.origin_post.id if self.origin_post else None,
            'post_audience': audience_data["audience"] if aud == 1 else None,
            'like_status': "",
            'like_count': len(likes),
            'like_plus_count': len(like_plus),
            'like_minus_count': len(like_minus),
            'likes': likes,
            'like_plus': like_plus,
            'like_minus': like_minus,
            'tags': tagged_audience,
            'share_count': len(shares),
            'latest_activity': latest_activity,
            'origin_post_details': shared_post_data,
        }
        # print "normal post", result
        return result


class DiscussionPost(Post):
    discussion_type = models.CharField('discussion_type', max_length=20, choices=DISCUSSION_TYPE, null=True, blank=True)
    category = models.IntegerField('Category', max_length=20, choices=CATEGORY, null=True, blank=True)
    locations = models.ManyToManyField(LocationPoints, related_name="location", null=True, blank=True)

    def __unicode__(self):
        return self.post_creator.first_name

    def get_json(self, user=None):
        comments = attatch = shares = []
        likes = like_plus = like_minus = []
        aud = 0
        # self.description = build_url(self.urls, self.description)
        attatch = fetch_attachments(self.attachments)
        locations_list = []
        for x in self.locations.all():
            locations_list.append(x.get_json())
        created_month = self.created_date.month
        month = str(calendar.month_abbr[created_month])

        con = get_db()
        coll = con.PostAudienceComments
        coll1 = con.Comment
        audience_data = coll.find_one({"post_id": self.id})
        if audience_data:
            comments = CommentsReply(self.id, user=user).get(box_type="discussion")
            likes = fetch_like("like", audience_data)
            like_plus = fetch_like("like_plus", audience_data)
            like_minus = fetch_like("like_minus", audience_data)
            shares = fetch_share(audience_data)
            tagged_audience = fetch_tagged_audience(audience_data)
        if comments == []:
            comments = [{'id': 0, 'comments': [], 'group_name': 'global comments', 'group_bar_color': '#64d156'}]
        latest_activity = likes + like_plus + like_minus
        shared_post_data = fetch_shared_post(self.origin_post)
        # grp_cmd = Audience_group_comments(self.box, self.group,self.friends, comments)
        location = self.post_creator.profile.location.get_json() if self.post_creator.profile.location else ""
        box = FriendsBox.objects.get(owner=self.post_creator, box_type='discussion')
        result = {
            'discussion_type': self.discussion_type,
            'description': self.description,
            'category': DISCUSSION_CATEGORY.get(str(self.category), ''),
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'post_created_date': self.created_date.strftime('%d') + ' ' + month + ' ' + self.created_date.strftime('%H:%M'),
            'id': self.id,
            # 'creator': get_json_user(self.post_creator),
            'post_type': self.post_type,
            'origin_post': self.origin_post.id if self.origin_post else None,
            'creator_profile': self.post_creator.profile.get_json(box=box),
            'attachments': attatch,
            'locations': locations_list,
            'current_locations': locations_list[-1] if locations_list  else location,
            # 'box':box_data,
            'groups': comments,
            'post_audience': audience_data["audience"] if aud == 1 else None,
            'comments': comments,
            'like_status': "",
            'likes': likes,
            'like_plus': like_plus,
            'like_minus': like_minus,
            'tags': tagged_audience,
            'share_count': len(shares),
            'latest_activity': latest_activity,
            'origin_post_details': shared_post_data,

        }
        # print "travel post", result
        return result


class TravelPost(Post):
    # starting_point = PointField('Start Coordinates', srid=4326, null=True, blank=True,  help_text='Travel location map coordinates')
    # end_point = PointField('End Coordinates', srid=4326, null=True, blank=True,  help_text='Travel location map coordinates')
    # location_address = models.TextField()
    locations = models.ManyToManyField(LocationPoints, related_name="locations", null=True, blank=True)

    def __unicode__(self):
        return self.post_creator.first_name

    def get_json(self, user=None):
        comments = attatch = shares = []
        likes = like_plus = like_minus = []
        aud = 0
        # self.description = build_url(self.urls, self.description)
        attatch = fetch_attachments(self.attachments)
        locations_list = []
        for x in self.locations.all():
            locations_list.append(x.get_json())
        created_month = self.created_date.month
        month = str(calendar.month_abbr[created_month])

        con = get_db()
        coll = con.PostAudienceComments
        coll1 = con.Comment
        audience_data = coll.find_one({"post_id": self.id})
        if audience_data:
            comments = CommentsReply(self.id).get()
            likes = fetch_like("like", audience_data)
            like_plus = fetch_like("like_plus", audience_data)
            like_minus = fetch_like("like_minus", audience_data)
            shares = fetch_share(audience_data)
            tagged_audience = fetch_tagged_audience(audience_data)
        latest_activity = likes + like_plus + like_minus
        shared_post_data = fetch_shared_post(self.origin_post)
        grp_cmd = Audience_group_comments(self.box, self.group, self.friends, comments)
        location = self.post_creator.profile.location.get_json() if self.post_creator.profile.location else ""
        if user == self.post_creator:
            box = self.created_box
        else:
            box = Friends(user=user).friends_box(user=self.post_creator)
        result = {

            'description': self.description,
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'post_created_date': self.created_date.strftime('%d') + ' ' + month + ' ' + self.created_date.strftime('%H:%M'),
            'id': self.id,
            # 'creator': get_json_user(self.post_creator),
            'post_type': self.post_type,
            'origin_post': self.origin_post.id if self.origin_post else None,
            'creator_profile': self.post_creator.profile.get_json(box=box),
            'attachments': attatch,
            'locations': locations_list,
            'current_locations': locations_list[-1] if locations_list  else location,
            # 'box':box_data,
            'groups': grp_cmd,
            'post_audience': audience_data["audience"] if aud == 1 else None,
            'comments': comments,
            'like_status': "",
            'likes': likes,
            'like_plus': like_plus,
            'like_minus': like_minus,
            'tags': tagged_audience,
            'share_count': len(shares),
            'latest_activity': latest_activity,
            'origin_post_details': shared_post_data,

        }
        # print "travel post", result
        return result


class LifeTimePost(Post):
    cover_photo = models.ForeignKey(Picture, related_name="cover_photo", null=True, blank=True)
    title = models.CharField("title", max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.post_creator.first_name

    def get_json(self, user=None):
        comments = attatch = shares = []
        likes = like_plus = like_minus = []
        aud = 0
        attatch = fetch_attachments(self.attachments)
        created_month = self.created_date.month
        month = str(calendar.month_abbr[created_month])

        con = get_db()
        coll = con.PostAudienceComments
        coll1 = con.Comment
        audience_data = coll.find_one({"post_id": self.id})
        if audience_data:
            comments = CommentsReply(self.id).get()
            likes = fetch_like("like", audience_data)
            like_plus = fetch_like("like_plus", audience_data)
            like_minus = fetch_like("like_minus", audience_data)
            shares = fetch_share(audience_data)
            tagged_audience = fetch_tagged_audience(audience_data)
        latest_activity = likes + like_plus + like_minus
        shared_post_data = fetch_shared_post(self.origin_post)
        grp_cmd = Audience_group_comments(self.box, self.group, self.friends, comments)
        if user == self.post_creator:
            box = self.created_box
        else:
            box = Friends(user=user).friends_box(user=self.post_creator)
        result = {

            'cover_photo': serialize(self.cover_photo) if self.cover_photo else None,
            'description': self.description,
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'id': self.id,
            'post_created_date': self.created_date.strftime('%d') + ' ' + month + '  ' + self.created_date.strftime('%H:%M'),
            'attachments': attatch,
            'title': self.title,
            # 'creator': get_json_user(self.post_creator),
            'creator_profile': self.post_creator.profile.get_json(box=box),
            'post_type': self.post_type,
            'origin_post': self.origin_post.id if self.origin_post else None,
            # 'box':box_data,
            'groups': grp_cmd,
            'post_audience': audience_data["audience"] if aud == 1 else None,
            'comments': comments,
            'like_status': "",
            'likes': likes,
            'like_plus': like_plus,
            'like_minus': like_minus,
            'tags': tagged_audience,
            'share_count': len(shares),
            'latest_activity': latest_activity,
            'origin_post_details': shared_post_data,

        }
        # print "lifetime_post", result
        return result


class MediaPost(Post):
    poster = models.ForeignKey(Picture, related_name="poster", null=True, blank=True)
    genre = models.TextField('Genre', null=True, blank=True)
    year = models.TextField('Year', null=True, blank=True)
    director = models.TextField('Director', null=True, blank=True)
    stars = models.TextField('Stars', null=True, blank=True)
    plot = models.TextField('Plot', max_length=1000, null=True, blank=True)
    title = models.CharField("title", max_length=100, null=True, blank=True)
    media_type = models.CharField('Media type', max_length=20, choices=MEDIA_TYPE, default='video')

    def __unicode__(self):
        return self.post_creator.first_name

    def get_json(self, user=None):
        comments = attatch = shares = []
        likes = like_plus = like_minus = []
        aud = 0
        attatch = fetch_attachments(self.attachments)
        created_month = self.created_date.month
        month = str(calendar.month_abbr[created_month])

        con = get_db()
        coll = con.PostAudienceComments
        coll1 = con.Comment
        audience_data = coll.find_one({"post_id": self.id})

        if audience_data:
            comments = CommentsReply(self.id).get()
            likes = fetch_like("like", audience_data)
            like_plus = fetch_like("like_plus", audience_data)
            like_minus = fetch_like("like_minus", audience_data)
            shares = fetch_share(audience_data)
            tagged_audience = fetch_tagged_audience(audience_data)
        latest_activity = likes + like_plus + like_minus
        shared_post_data = fetch_shared_post(self.origin_post)
        grp_cmd = Audience_group_comments(self.box, self.group, self.friends, comments)
        if user == self.post_creator:
            box = self.created_box
        else:
            box = Friends(user=user).friends_box(user=self.post_creator)
        result = {

            'poster': serialize(self.poster) if self.poster else {'url':'/static/images/pictures/pic-66.jpg'},
            'genre': self.genre if self.genre else "--",
            'year': self.year if self.year else "--",
            'director': self.director if self.director else "--",
            'stars': self.stars if self.stars else "--",
            'plot': self.plot if self.plot else "",
            'media_type': self.media_type,
            'description': build_url(self.urls, self.description),
            'created_date': self.created_date.strftime('%Y/%m/%d %H:%M'),
            'id': self.id,
            'post_created_date': self.created_date.strftime('%d') + ' ' + month + '  ' + self.created_date.strftime('%H:%M'),
            'attachments': attatch,
            'title': self.title,
            # 'creator': get_json_user(self.post_creator),
            'creator_profile': self.post_creator.profile.get_json(box=box),
            'post_type': self.post_type,
            'origin_post': self.origin_post.id if self.origin_post else None,
            # 'box':box_data,
            'groups': grp_cmd,
            'post_audience': audience_data["audience"] if aud == 1 else None,
            'comments': comments,
            'like_status': "",
            'likes': likes,
            'like_plus': like_plus,
            'like_minus': like_minus,
            'tags': tagged_audience,
            'share_count': len(shares),
            'latest_activity': latest_activity,
            'origin_post_details': shared_post_data,

        }
        # print "media result", result
        return result


from django.db.models import Q


class ViewPosts:
    def __init__(self, user_id):
        self.user_id = user_id
        self.user = User.objects.get(id=self.user_id)
        self.model_map = {'shared': Post, 'general': Post, 'media': MediaPost, 'travel': TravelPost, 'lifetime': LifeTimePost}
        self.frnd_post = FriendsPost(self.user_id)
        self.grouppost = self.frnd_post.get_grouppost()
        self.mute_user = self.frnd_post.get_mute()

    def filter_post(self, resultList, box_id=None):
        list = [Post.objects.filter(post_creator_id=user_id) for user_id in self.mute_user]
        remove_list = []
        for posts in list:
            for post in posts:
                remove_list.append(post.id)
        for post_id in resultList:
            try:
                pst = Post.objects.get(id=post_id)
                try:
                    creater_post = Post.objects.get(id=post_id, post_creator_id=self.user_id)
                    if not box_id == None:
                        if not creater_post.created_box.id == box_id:
                            remove_list.append(post_id)
                except Exception as e:
                    pass
            except:
                remove_list.append(post_id)

        result = set(resultList) - set(remove_list)
        return result

    def limit_post(self, posts, last_post, offset):
        result = sorted(posts, key=lambda x: x['created_date'], reverse=True)
        index = 0 if last_post == None else result.index((item for item in result if item["id"] == last_post).next()) + 1
        if type(offset) == int:
            result = result[index:index + offset] if result[index:index + offset] else result[index:]
        else:
            result = result[index:]
        return result

    def box(self, box_id, last_post=None, offset=None):
        try:
            if Box(self.user_id).get(box_id).box_type == 'discussion':
                ''' code to view discussion '''
                posts = self.private_discussion(last_post, offset)
            else:
                posts = self.post(box_id=box_id, last_post=last_post, offset=offset)

        except Exception as e:
            print "ssss", e
        return posts

    def post(self, box_id=None, last_post=None, offset=None):
        resultList = []
        result = []
        posts = []
        try:
            resultList = list(set(resultList) | set(self.grouppost.get(str(0), [])))
            for grp in Grp.get_by_box(box_id):
                if str(grp.id) in self.grouppost.keys():
                    resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
            resultList = self.filter_post(resultList, box_id=box_id)
            for post_id in resultList:
                try:
                    posts.append(
                        self.model_map[Post.objects.get(id=post_id).post_type].objects.get(id=post_id).get_json(user=self.user))
                except:
                    pass
            result = self.limit_post(posts, last_post, offset)
        except Exception as e:
            print "ssss", e
        return result

    def private_discussion(self, last_post=None, offset=None, user_id=None):
        resultList = []
        result = []
        posts = []
        type = 'private'
        try:
            ''' code to view discussion '''
            try:
                user = User.objects.get(id=int(user_id))
            except:
                user = None
            resultList = list(set(resultList) | set(self.grouppost.get(str(0), [])))
            for box in Box(self.user_id).get_all():
                for grp in Grp.get_by_box(box.id):
                    if str(grp.id) in self.grouppost.keys():
                        resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
            for post_id in resultList:
                try:
                    if Post.objects.get(id=post_id).post_type == 'discussion':
                        search_prams = [Q(id=post_id, discussion_type=type)]
                        if not user == None:
                            search_prams.append(Q(post_creator=user))
                        posts.append(DiscussionPost.objects.get(*search_prams).get_json(user=self.user))
                except Exception as e:
                    pass
            result = self.limit_post(posts, last_post, offset)
        except Exception as e:
            print "ssss", e
        return result

    def public_discussion(self, user_id=None, category=[], key_word=None, location=None, last_post=None, offset=None):
        resultList = []
        result = []
        posts = []
        try:
            ''' code to view public discussion '''
            try:
                user = User.objects.get(id=int(user_id))
            except:
                user = None
            search_prams = [Q(discussion_type='public', description__contains=key_word)]
            if not category == []:
                search_prams.append(Q(category__in=category))
            if not user == None:
                search_prams.append(Q(post_creator=user))
            public_dis = DiscussionPost.objects.filter(*search_prams)
            for dic in public_dis:
                dicussion = dic.get_json(user=self.user)
                if location == None or location == "":
                    resultList.append(dicussion)
                else:
                    for loc in dic.locations.all():
                        if location.lower() == loc.location_address.lower():
                            resultList.append(dicussion)
            result = self.limit_post(resultList, last_post, offset)
        except Exception as e:
            print e
        return result

    def group(self, group_id, last_post=None, offset=None):
        result = []
        if str(group_id) in self.grouppost.keys():
            resultList = self.grouppost[str(group_id)]
            resultList = self.filter_post(resultList)
            for post_id in resultList:
                try:
                    result.append(
                        self.model_map[Post.objects.get(id=post_id).post_type].objects.get(id=post_id).get_json(user=self.user))
                except:
                    pass
            result = self.limit_post(result, last_post, offset)
        return result

    def member(self, group_id, member_id, last_post=None, offset=None):
        result = []
        if str(group_id) in self.grouppost.keys():
            resultList = self.grouppost[str(group_id)]
            resultList = self.filter_post(resultList)
            for post_id in resultList:
                try:
                    result.append(
                        self.model_map[Post.objects.filter(id=post_id, post_creator_id=member_id)[0].post_type].objects.get(
                            id=post_id).get_json(user=self.user))
                except:
                    pass
            result = self.limit_post(result, last_post, offset)
        return result

    def type(self, box_id, type, last_post=None, offset=None):
        resultList = []
        result = []
        resultList = list(set(resultList) | set(self.grouppost.get(str(0), [])))
        for grp in Grp.get_by_box(box_id):
            if str(grp.id) in self.grouppost.keys():
                resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
        resultList = self.filter_post(resultList)
        for post_id in resultList:
            try:
                model = self.model_map[type]
                result.append(model.objects.get(id=post_id).get_json(user=self.user))
            except:
                pass
        result = self.limit_post(result, last_post, offset)
        return result


class Discussion:
    def __init__(self, user_id):
        self.user_id = user_id
        self.user = User.objects.get(id=self.user_id)
        self.frnd_post = FriendsPost(self.user_id)
        self.grouppost = self.frnd_post.get_grouppost()
        self.mute_user = self.frnd_post.get_mute()

    def users(self, box_id, type='private'):
        resultList = []
        users = []
        user_unique = []
        try:
            if Box(self.user_id).get(box_id).box_type == 'discussion':
                ''' code to view discussion '''
                for box in Box(self.user_id).get_all():
                    for grp in Grp.get_by_box(box.id):
                        if str(grp.id) in self.grouppost.keys():
                            resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
                for post_id in resultList:
                    try:

                        post_creator = DiscussionPost.objects.get(id=post_id, post_type='discussion',
                                                                  discussion_type=type).post_creator
                        box = FriendsBox.objects.get(owner=post_creator, box_type='discussion')
                        creater_user = post_creator.profile.get_json(box=box)
                        creater_user['api'] = '/api/box/discussion/?type=private&user=' + str(creater_user['id'])
                        creater_user['notification'] = 0
                        users.append(creater_user)
                    except Exception as e:
                        pass
                user_unique = {v['user']: v for v in users}.values()
        except Exception as e:
            print "ssss", e
        return user_unique

    def fav_user(self, box_id):
        resultList = []
        users = []
        try:
            if Box(self.user_id).get(box_id).box_type == 'discussion':
                ''' code to view discussion '''
                resultList = list(set(resultList) | set(self.grouppost.get(str(0), [])))
                for box in Box(self.user_id).get_all():
                    for grp in Grp.get_by_box(box.id):
                        if str(grp.id) in self.grouppost.keys():
                            resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
                for post_id in resultList:
                    try:
                        post_creator = DiscussionPost.objects.get(id=post_id, post_type='discussion',
                                                                  discussion_type=type).post_creator
                        box = FriendsBox.objects.get(owner=post_creator, box_type='discussion')
                        users.append(post_creator.profile.get_json(box=box))
                    except Exception as e:
                        pass
                user_unique = {v['user']: v for v in users}.values()
        except Exception as e:
            print "ssss", e
        return user_unique

    def fav_filter(self, box_id):
        resultList = []
        users = []
        try:
            if Box(self.user_id).get(box_id).box_type == 'discussion':
                ''' code to view discussion '''
                for box in Box(self.user_id).get_all():
                    for grp in Grp.get_by_box(box.id):
                        if str(grp.id) in self.grouppost.keys():
                            resultList = list(set(resultList) | set(self.grouppost[str(grp.id)]))
                for post_id in resultList:
                    try:
                        post_creator = DiscussionPost.objects.get(id=post_id, post_type='discussion',
                                                                  discussion_type=type).post_creator
                        box = FriendsBox.objects.get(owner=post_creator, box_type='discussion')
                        users.append(post_creator.profile.get_json(box=box))
                    except Exception as e:
                        pass
                user_unique = {v['user']: v for v in users}.values()
        except Exception as e:
            print "ssss", e
        return user_unique

    @staticmethod
    def participants(post_id):
        group_list = []
        friends_list = []
        try:
            disc = DiscussionPost.objects.get(id=post_id, post_type='discussion')

            for box in disc.box.all():
                for grp in box.groups.all():
                    group_list.append(grp.get_json())
            for grp in disc.group.all():
                group_list.append(grp.get_json())
            for friend in disc.friends.all():
                box = FriendsBox.objects.get(owner=friend.user, box_type='discussion')
                frnd = friend.user.profile.get_json(box=box)
                frnd['member_id'] = friend.id
                friends_list.append(frnd)
        except Exception as e:
            pass
        group_unique = {v['id']: v for v in group_list}.values()
        user_unique = {v['id']: v for v in friends_list}.values()
        return group_unique, user_unique


class FriendsPost:
    def __init__(self, user_id):
        self.user_id = user_id
        con = get_db()
        self.userposts = con.UserPosts
        self.favorite = con.Favorite
        self.userpost = self.userposts.find_one({"user_id": self.user_id})
        if not self.userpost:
            self.userposts.insert(
                {"user_id": self.user_id, "grouppost": [{}], "mute": [], "unread_post": [], "unread_comments": []})
            self.userpost = self.userposts.find_one({"user_id": self.user_id})

    def get(self):
        return self.userpost

    def append_group_post(self, grp_id, post):
        grp_id = str(grp_id)
        post_id = post.id
        try:
            grplist = self.userpost["grouppost"][0]
            mute = self.userpost.get("mute", [])
            unread_post = self.userpost.get("unread_post", [])
            unread_comments = self.userpost.get("unread_comments", [])
            fav = self.userpost.get("favorite", [])
            if grp_id in grplist.keys():
                if post_id not in grplist[grp_id]:
                    grplist[grp_id].append(post_id)
                else:
                    return False
            else:
                grplist[grp_id] = [post_id]
            self.userposts.update({"user_id": self.user_id}, {"user_id": self.user_id,
                                                              "favorite": fav,
                                                              "mute": mute,
                                                              "unread_post": unread_post,
                                                              "unread_comments": unread_comments,
                                                              "grouppost": [grplist]})
            Notify(self.user_id).add_post(post_id)
            return True
        except Exception as e:
            print e
            return False

    def mute(self, user_id):
        try:
            user_id = int(user_id)
            if user_id not in self.userpost["mute"]:
                self.userposts.update({"user_id": self.user_id}, {"$addToSet": {"mute": user_id}}, upsert=True)
                print self.userposts.find_one({"user_id": self.user_id})
                return True
            else:
                self.userposts.update({"user_id": self.user_id}, {"$pull": {"mute": user_id}}, upsert=True)
                print self.userposts.find_one({"user_id": self.user_id})
                return False
        except Exception as e:
            return None

    def get_mute_user(self, user_id):
        mute = self.userpost.get("mute", [])
        if int(user_id) in mute:
            return "Unmute"
        else:
            return 'Mute'

    def get_mute(self):
        mute = self.userpost.get("mute", [])
        return mute

    def get_grouppost(self):
        return self.userpost.get("grouppost", [{}])[0]

    def get_all_post(self, box_id):
        resultList = []
        for grp in Grp.get_by_box(box_id):
            grp_post = self.get_grouppost()
            if str(grp.id) in grp_post.keys():
                resultList = list(set(resultList) | set(grp_post[str(grp.id)]))
        return resultList

    def add_favorite(self, category=None, keyword=None, location=None, user_id=None):
        try:
            if user_id not in self.userpost["mute"]:
                favorite = self.favorite.insert(
                    {'name': None, 'category': category, 'keyword': keyword, 'location': location, 'user': user_id})

                self.userposts.update({"user_id": self.user_id}, {"$addToSet": {"favorite": favorite}}, upsert=True)
                return self.get_favorite(str(favorite))
        except Exception as e:
            print "faverortrr", e
            return [str(e)]

    def add_fav_name(self, name, fav_id):
        try:
            self.favorite.update({"_id": ObjectId(str(fav_id))}, {"$set": {"name": name}}, upsert=True)
            return self.get_favorite(fav_id)
        except:
            return False

    '''work in progress'''

    def get_favorite(self, fav_id):
        result = {}
        # process need for struct
        try:
            fav = self.favorite.find_one({"_id": ObjectId(str(fav_id))})
            try:
                user_obj = User.objects.get(id=int(fav['user']))
                box = FriendsBox.objects.get(owner=user_obj, box_type='discussion')
                user = user_obj.profile.get_json(box=box)
            except:
                user = None
            result['name'] = fav['name'] if user == None else user['name']
            if user == None:
                pic = DISCUSSION_CATEGORY[str(fav['category'][0])]['pic'] if not fav['category'] == [""] else ""
                result['profile_pic'] = '/static/images/time_icons/' + pic
            else:
                result['profile_pic'] = user['profile_pic']
            cate = ''
            msg_cat = ''
            for cat in fav['category']:
                if not cate == "":
                    cate = cate + ','
                    msg_cat = msg_cat + ', '
                cate = cate + str(cat)
                msg_cat = msg_cat + DISCUSSION_CATEGORY.get(str(cat), {}).get('category', "")
            keyword = str(fav.get('keyword', '')) if not fav['keyword'] == "" else ""
            location = str(fav.get('location', '')) if not fav['location'] == "" else ""
            user = str(fav.get('user', '')) if not fav['user'] == None else ""
            result['id'] = str(fav['_id'])
            result['notification'] = 0
            result['about'] = msg_cat + ", " + keyword + ", " + location
            result[
                'api'] = '/api/box/discussion/?type=public&keyword=' + keyword + '&location=' + location + '&user=' + user + '&category=' + cate
        except Exception as e:
            print "xxx", e
        return result

    def get_all_favorite(self):
        res = self.userposts.find_one({"user_id": self.user_id})
        all_result = []
        favs = res.get('favorite', [])
        for fav_id in favs:
            result = {}
            # process need for struct
            try:
                result = self.get_favorite(fav_id)
                all_result.append(result)
            except Exception as e:
                print "xxx", e
        return all_result

    def del_favorite(self):
        mute = self.userpost.get("favorite", [])
        return mute


class Notify:
    def __init__(self, user_id):
        self.user_id = user_id
        con = get_db()
        self.userposts = con.UserPosts
        self.userpost = self.userposts.find_one({"user_id": self.user_id})

    def add_post(self, post_id):
        try:
            if post_id not in self.userpost.get("unread_post", []):
                self.userposts.update({"user_id": self.user_id}, {"$addToSet": {"unread_post": post_id}}, upsert=True)
            return True
        except Exception as e:
            return False

    def remove_post(self, post_id):
        try:
            if post_id in self.userpost.get("unread_post", []):
                self.userposts.update({"user_id": self.user_id}, {"$pull": {"unread_post": post_id}}, upsert=True)
            return True
        except Exception as e:
            return False

    def box_unread_post(self, box_id):
        unread = self.userpost.get("unread_post", []) if self.userpost else []
        box_post = FriendsPost(self.user_id).get_all_post(box_id=box_id)
        box_unread = []
        # box_unread=[post for post in unread if post in box_post]
        for post in unread:
            if post in box_post:
                try:
                    post_obj = Post.objects.get(id=post)
                    if post_obj and not post_obj.post_type == 'discussion':
                        box_unread.append(post_obj.get_json())
                except Exception as e:
                    print e
        return box_unread

    def group_unread_post(self, group_id):
        unread = self.userpost.get("unread_post", []) if self.userpost else []
        group_post = FriendsPost(self.user_id).get_grouppost().get(str(group_id), [])
        group_unread = []
        # group_unread=[post for post in unread if post in group_post]
        for post in unread:
            if post in group_post:
                try:
                    post_obj = Post.objects.get(id=post)
                    if post_obj and not post_obj.post_type == 'discussion':
                        group_unread.append(post)
                except Exception as e:
                    print e
        return group_unread

    def post_upadated_date(self, group_id):
        try:
            group_post = FriendsPost(self.user_id).get_grouppost().get(str(group_id), [])
            d_list = []
            for post_id in group_post:
                post_obj = Post.objects.get(id=int(post_id))
                if not post_obj.updated_date == None:
                    d_list.append(post_obj.updated_date)
            latest_date = max(d_list)
            # print "-------------------->",latest_date
            return latest_date
        except Exception as e:
            return None

    def friend_unread_post(self, group_id, friend_id):
        group_unread = self.group_unread_post(group_id)
        friend_unread = []
        friend = User.objects.get(id=int(friend_id))
        print friend
        for post_id in group_unread:
            try:
                post = Post.objects.get(id=int(post_id), post_creator=friend)
                if post and not post.post_type == 'discussion':
                    friend_unread.append(post.id)
            except Exception as e:
                print e
        return friend_unread

    def unread_post_category(self, box_id, post_type):
        box_unread = self.box_unread_post(box_id)
        category_unread = []
        for post_id in box_unread:
            try:
                post = Post.objects.get(id=int(post_id))
                if post.post_type == post_type:
                    category_unread.append(post.id)
            except Exception as e:
                print e
        return category_unread

    def add_comments(self, post_id=None, cmnd_id=None):
        try:
            post_id = str(post_id)
            cmnd_id = str(cmnd_id)
            unred_cmd = self.userpost.get("unread_comments")
            if unred_cmd == []:
                unred_cmd = {post_id: [cmnd_id]}
            else:
                try:
                    unred_cmd[post_id].append(cmnd_id)
                except Exception as e:
                    print e
                    unred_cmd[post_id] = [cmnd_id]
            self.userposts.update({"user_id": self.user_id}, {"$set": {"unread_comments": unred_cmd}}, upsert=True)
            return unred_cmd
        except Exception as e:
            print "faverortrr", e
            return [str(e)]

    def remove_comments(self, post_id=None, cmnd_id=None):
        try:
            post_id = str(post_id)
            cmnd_id = str(cmnd_id)
            unred_cmd = self.userpost.get("unread_comments")
            try:
                unred_cmd[post_id].remove(cmnd_id)
            except Exception as e:
                print e
            self.userposts.update({"user_id": self.user_id}, {"$set": {"unread_comments": unred_cmd}}, upsert=True)
            return unred_cmd
        except Exception as e:
            print "faverortrr", e
            return [str(e)]

    def unread_comments(self, post_id=None):
        try:
            unred_cmd = []
            if post_id == None:
                unred_cmds = self.userpost.get("unread_comments")
                for key, val in unred_cmds.items():
                    unred_cmd = unred_cmd + val
            else:
                post_id = str(post_id)
                try:
                    unred_cmd = self.userpost.get("unread_comments").get(post_id, [])
                except Exception as e:
                    unred_cmd = []
                    print e
            return unred_cmd
        except Exception as e:
            print "faverortrr", e
            return [str(e)]


class PostAlbum:
    def __init__(self, user):
        self.user = user

    def get_images(self):
        try:
            posts = Post.objects.filter(post_creator=self.user)
            images = []
            for post in posts:
                images = images + fetch_attachments(post.attachments, type='image')
            return images
        except Exception as e:
            return False

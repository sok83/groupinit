from django.db import models
from mongoengine import *
import pymongo
from djangotoolbox.fields import ListField, EmbeddedModelField
from datetime import datetime
from groupbox.models import Grp
from authentication.utils import get_db
from bson.objectid import ObjectId

class Chat(Document):
    first_user = IntField()
    second_user = IntField()
    chat = ListField(EmbeddedModelField('Message'))

    class Meta:
        app_label = 'mongo_models'

class Message(Document):
    sender = IntField()
    read = BooleanField
    message = StringField()
    created_on = DateTimeField(default=datetime.now())

    class Meta:
        app_label = 'mongo_models'

class Comment(Document):
    created_on = DateTimeField(default=datetime.now())
    group = IntField()
    author = IntField()
    text = StringField()
    reply = ListField(EmbeddedModelField('Comment'))

    class Meta:
        app_label = 'mongo_models'


class PostReshare(Document):
    shared_by = IntField('Shared By User')
    title = StringField(max_length=300)
    audience = ListField()
    tagged_audience = ListField()

    class Meta:
        app_label = 'mongo_models'


class PostAudienceComments(Document):
    post_id = IntField()
    audience = ListField()
    tagged_audience = ListField()
    comments = ListField(EmbeddedModelField('Comment'))
    shares = ListField(EmbeddedModelField('PostReshare'))
    likes = ListField()
    like_plus = ListField()
    like_minus = ListField()

    class Meta:
        app_label = 'mongo_models'

    def get_json(self):
        return {
            'post_id': self.post_id,
            'audience': self.audience if self.audience else None,
            'tagged_audience': self.tagged_audience if self.tagged_audience else None,
            'comments': self.comments if self.comments else None,
            'shares': self.shares if self.shares else None,
            'likes': self.likes if self.likes else None,
            'like_plus': self.like_plus if self.like_plus else None,
            'like_minus': self.like_minus if self.like_minus else None,
        }



class Messages(Document):
    Participents = ListField()
    message = ListField(EmbeddedModelField('Comment'))

    class Meta:
        app_label = 'mongo_models'


class GroupOrder(Document):
    user = IntField()
    box_id = IntField()
    group_id = IntField()
    order = IntField()

    class Meta:
        app_label = 'mongo_models'

    def get_json(self):
        return {
            'userid': self.user,
            'box_id': self.box_id,
            'group_id': self.group_id,
            'order': self.order,
        }


class GroupMessage(Document):
    group = IntField()
    message = ListField(EmbeddedModelField('Comment'))

    class Meta:
        app_label = 'mongo_models'


class Favorite(Document):
    name = StringField(max_length=100)
    category = ListField()
    keyword = StringField(max_length=100)
    location = StringField(max_length=100)
    user_id = IntField()

    class Meta:
        app_label = 'mongo_models'

class UserPosts(Document):
    user_id = models.IntegerField()
    posts = ListField()
    grouppost = ListField()
    mute = ListField()
    unread_post = ListField()
    unread_comments = ListField()
    favorite = ListField(EmbeddedModelField('Favorite'))

    class Meta:
        app_label = 'mongo_models'



class PostGroup(Document):
    group_id = models.IntegerField()
    posts = ListField()

    class Meta:
        app_label = 'mongo_models'

from authentication.utils import get_db
from bson.objectid import ObjectId
from django.contrib.auth.models import User
from authentication.models import UserProfile
from datetime import datetime

class FriendsChat:
    def __init__(self,sender,reciever):
        self.sender=sender
        self.reciever=reciever
        con = get_db()
        self.chat = con.Chat
        self.message = con.Message
        self.friend_chat = self.chat.find_one({"$or":[{"first_user":sender,"second_user":reciever},{"first_user":reciever,"second_user":sender}]})
        #print self.friend_chat
        if not self.friend_chat:
            self.chat.insert({"first_user":sender,"second_user":reciever,"chat":[]})
            self.friend_chat = self.chat.find_one({"$or":[{"first_user":sender,"second_user":reciever},{"first_user":reciever,"second_user":sender}]})

    def get(self):
        return self.chat

    def send(self,message):
        try:
            messageobj = self.message.insert({"sender": int(self.sender), "message": message, "created_on": datetime.now(),"read":False})
            self.chat.update({"$or":[{"first_user":self.sender,"second_user":self.reciever},{"first_user":self.reciever,"second_user":self.sender}]},{"$addToSet": {"chat": messageobj}}, upsert=True)
            chat=self.message.find_one({"_id": messageobj})
            msg=self.parse_message(chat)
            return msg
        except Exception as e:
            print "exxx", e
            return False

    def load(self, msg_id=None, offset=None, login_user=""):
        try:
            full_chat =self.friend_chat.get("chat", [])
            clear_id=""
            if self.friend_chat.get("first_user", "") ==login_user:
                clear_id =self.friend_chat.get("first_clear_id", "")
            elif self.friend_chat.get("second_user", "") ==login_user:
                clear_id =self.friend_chat.get("second_clear_id", "")
            result=[]
            for chat in full_chat:
                try:
                    self.message.update({"_id": chat,'sender':self.reciever}, {"$set": {"read": True}})
                    if clear_id =="" or chat > ObjectId(clear_id):
                        message =self.message.find_one({"_id": chat})
                        msg=self.parse_message(message)
                        result.append(msg)
                except:
                    pass
            if offset<len(result):
                result=result[-offset:]

            return result
        except Exception as e:
            print "nnn",e
            return None

    def unread(self, last_id):
        try:
            full_chat =self.friend_chat.get("chat", [])
            result=[]
            for chat in full_chat:
                try:
                    message =self.message.find_one({"_id": chat})
                    if message['_id'] > ObjectId(last_id) and message['sender']==self.reciever:
                        self.message.update({"_id": chat,'sender':self.reciever}, {"$set": {"read": True}})
                        msg=self.parse_message(message)
                        result.append(msg)
                except:
                    pass
            return result
        except Exception as e:
            print e
            return None

    def parse_message(self,message):
        msg={}
        msg['message'] = message['message']
        msg['read'] = message['read']
        msg['created_on'] = message['created_on'].strftime('%Y/%m/%d %H:%M')
        msg['msg_id'] = str(message['_id'])
        up=UserProfile.objects.get(user__id=message['sender'])
        msg['sender'] = up.user.first_name
        msg['user_id'] = up.user.id
        msg['profile_pic'] = up.get_json()['profile_pic']
        return msg

    @staticmethod
    def notification(sender):
        try:
            con = get_db()
            chat = con.Chat
            message = con.Message
            friend_all = chat.find({"$or":[{"first_user":sender},{"second_user":sender}]})
            notify=[]

            for fnds in friend_all:
                try:
                    result={}
                    full_chat =fnds.get("chat", [])
                    from_user=fnds["second_user"] if fnds["first_user"]==sender else fnds["first_user"]
                    result['id']=from_user
                    up=UserProfile.objects.get(user__id=int(from_user))
                    result['name'] = up.user.first_name
                    result['profile_pic'] = up.get_json()['profile_pic' ]
                    result['last_msg']=""
                    result['status']=True
                    count=0
                    date=""
                    for chat in full_chat:
                        msg =message.find_one({"_id": chat,'sender':from_user})
                        #result['last_msg']=msg.get('message',"dd")

                        if msg and msg['read']==False:
                            result['last_msg']=msg['message']
                            count=count+1
                            date=msg['created_on'].strftime('%Y/%m/%d %H:%M')
                    result['unread']=count
                    result['msg_date']=date
                    notify.append(result)
                    notify = sorted(notify, key=lambda x: x['msg_date'], reverse=True)
                except Exception as e:
                    pass
            return notify
        except Exception as e:
            print e
            return False

    @staticmethod
    def notify_count(sender,reciver):
        try:
            con = get_db()
            chat = con.Chat
            message = con.Message
            fnds = chat.find({"$or":[{"first_user":sender,"second_user":reciver},{"second_user":sender,"first_user":reciver}]})[0]
            count=0
            try:
                result={}
                full_chat =fnds.get("chat", [])
                from_user=fnds["second_user"] if fnds["first_user"]==sender else fnds["first_user"]
                for chat in full_chat:
                    msg =message.find_one({"_id": chat,'sender':from_user})
                    if msg and msg['read']==False:
                        result['last_msg']=msg['message']
                        count=count+1
            except Exception as e:
                print "zz",e
            return count
        except Exception as e:
            return 0

    def delete(self,id,login_user):
        try:
            message = self.message.find_one({"_id":ObjectId(str(id))})
            if message['sender']==login_user:
                comm = self.message.remove({"_id":ObjectId(str(id))})
            else:
                return False
            #chat = self.chat.update( {"$or":[{"first_user":self.sender,"second_user":self.reciever},{"first_user":self.reciever,"second_user":self.sender}]}, { "$unset": { "chat":ObjectId(str(id))}}, upsert= True )
            return True
        except Exception as e:
            print e
            return False

    def clear(self, id, login_user):
        try:
            fst=self.friend_chat.get("first_user")
            scnd=self.friend_chat.get("second_user")
            if int(fst) == int(login_user):
                set={"first_clear_id": id}
            elif int(scnd) == int(login_user):
                set={"second_clear_id": id}
            chat = self.chat.update( {"$or":[{"first_user":self.sender,"second_user":self.reciever},{"first_user":self.reciever,"second_user":self.sender}]},
                                     {"$set": set}, upsert= True )
            return True
        except Exception as e:
            print e
            return False
from rest_framework_mongoengine.serializers import MongoEngineModelSerializer,DocumentSerializer
from mongo_models.models import *

class UserPostSerializer(DocumentSerializer):
    class Meta:
        model = UserPosts
        depth = 2
        fields = ('id', 'user_id','posts')
       
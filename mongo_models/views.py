from django.http import HttpResponse
from django.views.generic import View
import json as simplejson
from .chat import FriendsChat


class ChatNotify(View):
    def get(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        #print from_user
        try:
            notify=FriendsChat.notification(from_user)
            res = {'result': 'ok', 'Notification': notify}
        except Exception as e:
            res = {'result': 'error','message': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class ChatLoad(View):
    def get(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        print from_user
        to_user = int(request.GET["user_id"])
        msg_id = request.GET.get("msg_id",None)
        try:
            load=FriendsChat(from_user,to_user).load(msg_id=msg_id,offset=300,login_user=from_user)
            res = {'result': 'ok', 'messages': load}
        except Exception as e:
            res = {'result': 'error','message': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class ChatSend(View):
    def post(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        to_user = int(request.POST["user_id"])
        message = request.POST["message"]
        if not message=="":
            try:
                sent=FriendsChat(from_user,to_user).send(message)
                if sent:
                    res = {'result': 'ok', 'messages': sent}
                else:
                    res = {'result': 'error','messages':message}
            except Exception as e:
                res = {'result': 'error','message': message}
        else:
            res = {'result': 'error','message': "empty message"}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class ChatUpdate(View):
    def get(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        to_user = int(request.GET["user_id"])
        msg_id = request.GET["last_msg_id"]
        try:
            unread=FriendsChat(from_user,to_user).unread(msg_id)
            res = {'result': 'ok', 'messages': unread}
        except Exception as e:
            res = {'result': 'error','message': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")


class ChatDelete(View):
    def post(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        to_user = int(request.POST["user_id"])
        msg_id = request.POST["msg_id"]
        try:
            deleted=FriendsChat(from_user,to_user).delete(msg_id,from_user)
            if deleted:
                res = {'result': 'ok'}
            else:
                res = {'result': 'error'}
        except Exception as e:
            res = {'result': 'error','message': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")

class ChatClear(View):
    def post(self, request, *args, **kwargs):
        from_user = int(request.user.id)
        to_user = int(request.POST["user_id"])
        last_msg_id = request.POST["last_msg_id"]
        try:
            cleared=FriendsChat(from_user,to_user).clear(id=last_msg_id, login_user=from_user)
            if cleared:
                res = {'result': 'ok','messages':[]}
            else:
                res = {'result': 'error'}
        except Exception as e:
            res = {'result': 'error','message': str(e)}
        response = simplejson.dumps(res)
        return HttpResponse(response, status=200, content_type="application/json")
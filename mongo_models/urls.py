
from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from .views import  (ChatNotify,ChatSend,ChatLoad,ChatUpdate,ChatDelete,ChatClear)


urlpatterns = patterns('',
    url(r"^api/chat/notification/$", login_required(ChatNotify.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^api/chat/send/$", login_required(ChatSend.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^api/chat/load/$", login_required(ChatLoad.as_view(), login_url='/login/'), name="load_lifetime_post"),
    url(r"^api/chat/update/$", login_required(ChatUpdate.as_view(), login_url='/login/'), name="load_media_post"),
    url(r"^api/chat/delete/$", login_required(ChatDelete.as_view(), login_url='/login/'), name="load_group_post"),
    url(r"^api/chat/clear/$", login_required(ChatClear.as_view(), login_url='/login/'), name="load_group_post"),
)

# from django.conf.urls import patterns, url
# from django.contrib.auth.decorators import login_required
# from .views import  (SearchPeople, AcceptFriendRequest, ProfileView, FriendRequestView, AddFriendToGroup,\
#  DeleteFriendRequest, FriendSuggestion, FriendsBoxView,FindMembers, DeleteFriendsBox, ReorderBox, ReorderGroup,GroupView,DeleteGroup, GroupAddMember,\
#  GroupExit,MoveFriend,ConvertToGroupRequest,AcceptGroupRequest,AddRequestToBox,SendInvitation,AssignGroup,EditGroup)
#
#
#
#
# urlpatterns = patterns('',
#
#     url(r"^search_people/$", login_required(SearchPeople.as_view(), login_url='/login/'), name="search_people"),
#     url(r"^find_members/$", login_required(FindMembers.as_view(), login_url='/login/'), name="find_members"),
#
#     # url(r"^friend_request/(?P<userid>\d+)/$", login_required(FriendRequestView.as_view(), login_url='/login/'), name="friend_request"),
#     url(r"^friend_request/$", login_required(FriendRequestView.as_view(), login_url='/login/'), name="friend_request"),
#
#     url(r"^accept_request/$", login_required(AcceptFriendRequest.as_view(), login_url='/login/'), name="accept_request"),
#     url(r"^accept_group_request/$", login_required(AcceptGroupRequest.as_view(), login_url='/login/'), name="accept_group_request"),
#     url(r"^add_request/$", login_required(AddRequestToBox.as_view(), login_url='/login/'), name="add_request"),
#
#     url(r"^reject_friend_request/$", login_required(DeleteFriendRequest.as_view(), login_url='/login/'), name="reject_friend_request"),
#     url(r"^add_to_group/$", login_required(AddFriendToGroup.as_view(), login_url='/login/'), name="add_to_group"),
#     url(r"^profile/(?P<slug>[\w\-]+)/(?P<userid>\d+)/$", login_required(ProfileView.as_view(), login_url='/login/'), name="profile_view"),
#     url(r"^profile/(?P<slug>[\w\-]+)/(?P<userid>\d+)/$", login_required(ProfileView.as_view(), login_url='/login/'), name="profile_view"),
#     url(r"^friend_suggestions/$", login_required(FriendSuggestion.as_view(), login_url='/login/'), name="friend_suggestions"),
#
#     url(r"^friends_box/$", login_required(FriendsBoxView.as_view(), login_url='/login/'), name="friend_box_list"),
#     url(r"^friends_box/(?P<box_id>\d+)/$", login_required(FriendsBoxView.as_view(), login_url='/login/'), name="friend_box_detail"),
#     url(r"^delete_friends_box/(?P<box_id>\d+)/$", login_required(DeleteFriendsBox.as_view(), login_url='/login/'), name="delete_friend_box"),
#     url(r"^reorder_friends_box/$", login_required(ReorderBox.as_view(), login_url='/login/'), name="reorder_friend_box"),
#     url(r"^reorder_friends_group/$", login_required(ReorderGroup.as_view(), login_url='/login/'), name="reorder_friends_group"),
#     url(r"^friends_group/$", login_required(GroupView.as_view(), login_url='/login/'), name="groups"),
#
#     # url(r"^groups/(?P<box_id>\d+)/$", login_required(GroupView.as_view(), login_url='/login/'), name="groups"),
#     # url(r"^group/(?P<group_id>\d+)/$", login_required(GroupView.as_view(), login_url='/login/'), name="group"),
#     url(r"^delete_group/$", login_required(DeleteGroup.as_view(), login_url='/login/'), name="delete_group"),
#     url(r"^edit_group/$", login_required(EditGroup.as_view(), login_url='/login/'), name="edit_group"),
#
#     url(r"^exit_group/$", login_required(GroupExit.as_view(), login_url='/login/'), name="exit_group"),
#
#     url(r"^add_group_members/$", login_required(GroupAddMember.as_view(), login_url='/login/'), name="add_group_members"),
#     url(r"^move_friend/$", login_required(MoveFriend.as_view(), login_url='/login/'), name="move_friend"),
#     url(r"^convert_to_group_request/$", login_required(ConvertToGroupRequest.as_view(), login_url='/login/'), name="convert_to_group_request"),
#     url(r"^send_invitation/$", login_required(SendInvitation.as_view(), login_url='/login/'), name="send_invitation"),
#     url(r"^assign_group/$", login_required(AssignGroup.as_view(), login_url='/login/'), name="assign_group"),
# )

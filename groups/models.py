# import os
# import uuid
#
# from django.db import models
# from django.contrib.auth.models import User
# from django.utils import timezone
# from django.contrib.contenttypes import generic
# from notification.models import Notification
#
#
#
# # import reversion
#
# from slugify import slugify
#
# BOX_TYPES = (
#     ('box', 'box'),
#     ('discussion', 'discussion'),
#     ('public group','public group'),
# )
#
#
# class FriendsBox(models.Model):
#     name = models.CharField('Box Name', max_length=200)
#     owner = models.ForeignKey(User, related_name="Owner")
#     color = models.CharField('Box color Hexcode', max_length="7")
#     order = models.IntegerField('Box display order', max_length=1)
#     box_type = models.CharField('Box Type', default='box', max_length=15, choices=BOX_TYPES)
#     profile_pic = models.ImageField(upload_to = 'uploads/', null=True, blank=True)
#
#     def __unicode__(self):
#         return self.name +"-"+ self.owner.first_name
#
#
#     def get_json(self):
#         groups = Group.objects.filter(box_id=self.id,group_type ="Shared")
#         #filter_list = ['name','id','number_of_members','group_type', 'members', 'group_bar_color']
#         all_group = [group.get_json() for group in groups]
#         return {
#             'name': self.name,
#             'owner': self.owner.id,
#             'color': self.color,
#             'order': self.order,
#             'id': self.id,
#             'groups':all_group,
#             'box_type':self.box_type,
#         }
#
#     def change_color(self, color):
#         self.color = color
#         self.save()
#
#     def change_order(self, order):
#         boxes = FriendsBox.objects.filter(owner=self.owner).order_by('order')
#         if self.order > order:
#             for box in boxes:
#                 if box.order >= order and box.order <=self.order:
#                     box.order = box.order + 1
#                     box.save()
#             self.order = order
#             self.save()
#         elif self.order < order:
#             for box in boxes:
#                 if box.order <= order and box.order >=self.order:
#                     box.order = box.order - 1
#                     box.save()
#             self.order = order
#             self.save()
#
# GROUP_TYPE = (
#     ('Private', 'Private'),
#     ('Shared', 'Shared')
# )
#
# class Group(models.Model):
#     box = models.ForeignKey(FriendsBox)
#     name = models.CharField('Group Name', max_length=200)
#     group_admins = models.ManyToManyField(User,related_name="group_admins",null=True, blank=True)
#     group_bar_color = models.CharField('Group color Hexcode', max_length="7", null=True, blank=True)
#     group_created_on = models.DateTimeField(default=timezone.now)
#     default_sharing = models.BooleanField('Default Sharing',default=False)
#     hide_group_activity = models.BooleanField('Hide Group Activity',default=False)
#     members = models.ManyToManyField(User, null=True, blank=True)
#     group_type = models.CharField('Group Type', max_length=15, choices=GROUP_TYPE, default='Private')
#     notification = generic.GenericRelation(Notification)
#
#     def __unicode__(self):
#         return self.name
#
#     def get_json(self):
#         return {
#             'id':self.id,
#             'box': self.box.id,
#             'name': self.name,
#             'members': [
#                 {
#                     'id': x.id,
#                     'name': x.username,
#                     'is_friend': self.is_friend(x),
#                     'profile_pic':x.profile.profile_pic,
#                     'first_name':x.first_name,
#                     'last_name':x.profile.last_name,
#                     'is_active_friend': self.is_active_friend(x),
#                     'member_profile':x.profile.get_json(),
#
#                 } for x in self.members.all()
#             ],
#             'number_of_members': self.members.all().count(),
#             'group_type': self.group_type,
#             'default_sharing': self.default_sharing,
#             'hide_group_activity': self.hide_group_activity,
#             'group_bar_color': self.group_bar_color,
#             'group_created_on': self.group_created_on.strftime('%d/%m/%Y')
#         }
#
#     def is_friend(self, user):
#         if self.box.owner.all_friends.all().count() > 0:
#
#             return True if user in self.box.owner.all_friends.all()[0].friends.all() else False
#         else :
#             return False
#
#     def is_active_friend(self, user):
#         if self.box.owner.all_friends.all().count() > 0:
#             pass
#         else:
#             return False
#         friends = self.box.owner.all_friends.all()[0].friends.all().filter(group=self)
#         if user in friends:
#             frequest = FriendRequest.objects.filter(invitor=self.box.owner, invitee=user)
#             if frequest.count() > 0 :
#                 frequest = frequest[0]
#                 if frequest.state == 'accepted':
#                     return True
#                 else:
#                     return False
#             else:
#                 return True
#         else:
#             return False
#
#
# STATE_CHOICES = [
#     ("applied", "applied"),
#     ("invited", "invited"),
#     ("declined", "declined"),
#     ("rejected", "rejected"),
#     ("accepted", "accepted"),
#     ("auto joined", "auto joined")
# ]
# class Friends(models.Model):
#     user = models.ForeignKey(User, related_name="all_friends")
#     box = models.ForeignKey(FriendsBox, null=True, blank=True)
#     group = models.ForeignKey(Group, null=True, blank=True)
#     friends = models.ManyToManyField(User,null=True,blank=True)
#
#     def __unicode__(self):
#         return u"{0}".format(self.user)
#
#     def get_json(self):
#         return {
#             'id':self.id,
#             'owner_user_id': self.user.id,
#             'box':self.box.get_json(),
#             'group':self.group.get_json(),
#             'friends': [
#                     {
#                     'id': x.id,
#                     'username': x.username,
#                     'first_name': x.first_name,
#                     } for x in self.friends.all()
#                 ],
#         }
#
# class FriendRequest(models.Model):
#     invitor = models.ForeignKey(User, related_name="invitor", null=True)
#     invitee = models.ForeignKey(User, related_name="invitee", null=True)
#     state = models.CharField(max_length=20, choices=STATE_CHOICES)
#     created_date = models.DateTimeField(default=timezone.now)
#     invited_group = models.ForeignKey(Group, related_name="invited Group", null=True)
#     invited_box = models.ForeignKey(FriendsBox, related_name="invited box", null=True)
#     notification = generic.GenericRelation(Notification)
#
#     def accept(self, by):
#         if self.state == "applied":
#             self.state = "accepted"
#             self.save()
#             return True
#         return False
#
#     def reject(self, by):
#         if self.state == "applied":
#             self.state = "rejected"
#             self.save()
#             return True
#         return False
#
#
#     def get_json(self):
#         return{
#            'state':self.state,
#            'invitee':self.invitee.id,
#            'invitor_profile':(self.invitor).profile.get_json(),
#            'invitee_profile':(self.invitee).profile.get_json(),
#            'invitor':self.invitor.id,
#            'invitor_name':self.invitor.first_name,
#            'invited_group':self.invited_group.get_json() if self.invited_group else None,
#            'created_date':self.created_date.strftime('%Y/%m/%d %H:%M'),
#            'invited_box':self.invited_box.get_json() if self.invited_box else None,
#            'is_grouprequest':'True' if self.invited_group else 'False',
#         }
#
#     # @property
#     # def invitee(self):
#     #     return self.user or self.invite.to_user_email

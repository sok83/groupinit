
from django.dispatch import receiver
from django.db.models.signals import post_save

from kaleo.signals import invite_accepted, joined_independently

from .models import Group, Membership


@receiver(post_save, sender=Group)
def handle_group_save(sender, **kwargs):
    created = kwargs.pop("created")
    group = kwargs.pop("instance")
    if created:
        group.memberships.get_or_create(
            user=group.creator,
            defaults={
                "role": Membership.ROLE_OWNER,
                "state": Membership.STATE_AUTO_JOINED
            }
        )


@receiver([invite_accepted, joined_independently])
def handle_invite_used(sender, invitation, **kwargs):
    for membership in invitation.memberships.all():
        membership.joined()
